<?php

return [
    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    // Socialite
    'facebook' => [
            'client_id' => env('FACEBOOK_APP_ID'),
            'client_secret' => env('FACEBOOK_APP_SECRET'),
            'redirect' => env('FACEBOOK_REDIRECT'),
    ],

    'google' => [
            'client_id' => env('GOOGLE_APP_ID'),
            'client_secret' => env('GOOGLE_APP_SECRET'),
            'redirect' => env('GOOGLE_REDIRECT'),
    ],

    'yahoo' => [
            'client_id' => env('YAHOO_APP_ID'),
            'client_secret' => env('YAHOO_APP_SECRET'),
            'redirect' => env('YAHOO_REDIRECT'),
    ],

];
