<?php

return array(

    'token' => env('PUSHOVER_APP_TOKEN'),
    'user_key' => env('PUSHOVER_USER_KEY'),

);
