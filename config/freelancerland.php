<?php

return array(
    'IMAGE_POST_UPLOAD_PATH' => env('IMAGE_POST_UPLOAD_PATH', '/home/vagrant/Code/freelancerland/public/uploads/posts/'),
    'POST_IMAGE_THUMB_WIDTH' => 194,
    'POST_IMAGE_THUMB_HEIGHT' => 129,
    'POST_IMAGE_WIDTH' => 600,
    'POST_IMAGE_HEIGHT' => 400,

    'IMAGE_PORTFOLIO_UPLOAD_PATH' => env('IMAGE_PORTFOLIO_UPLOAD_PATH', '/home/vagrant/Code/freelancerland/public/uploads/portfolios/'),
    'PORTFOLIO_IMAGE_THUMB_WIDTH' => 194,
    'PORTFOLIO_IMAGE_THUMB_HEIGHT' => 129,
    'PORTFOLIO_IMAGE_WIDTH' => 120,
    'PORTFOLIO_IMAGE_HEIGHT' => 120,

    'IMAGE_AVATAR_WIDTH' => 120,
    'IMAGE_AVATAR_HEIGHT' => 120,

    'CROPPED_IMAGE_PREFIX' => 'cropped-',
    'THUMB_IMAGE_PREFIX' => 'thumb-',

    // Firewall
    'fireWallEnable' => env('FIREWALL_ENABLE', true),
    'whiteListIps' => [
        // '116.105.223.222',
        // '116.105.223',
    '192.168',
    ],

    'EMAIL_ADMIN' => 'admin@freelancerland.vn',
    'EMAIL_LIENHE' => 'lienhe@freelancerland.vn',
    'EMAIL_KYTHUAT' => 'kythuat@freelancerland.vn',
    'EMAIL_THANHTOAN' => 'thanhtoan@freelancerland.vn',

    'pagination_count' => 20,
    'numberMessagesShow' => 10, // Show in Notifications, Header

    'numberRelatedPosts' => 5, // Show in Tin chi tiết
    'numberTinNBQCDXCount' => 50, // Số lượng tin get từ DB cho tin nổi bật, tin quảng cáo và tin đã xem

    'tin_free' => 5, // Tin free mỗi ngày
    'uptin_free' => 10, // Up tin free mỗi ngày

    'notifications_messages_pagination_count' => 5,

    'watermarkPath' => '/images/watermark/mark-100h.png', // Relative path to watermark image

    'CRYPT_KEY' => '123',

    'media_path_lookup' => '/uploads/posts',

    // 'CDN_MINIFY' => '//cdn.chodangtin.tk',
    'CDN_MINIFY' => '',
    'cdn' => array(
            // "cdn.keycdn.com" => "css|js|eot|woff|ttf",
            "cdn.freelancerland.tk" => "jpg|jpeg|png|gif|svg|ico",
            "cdn.freelancerland.vn" => "jpg|jpeg|png|gif|svg|ico"
            // "all.keycdn.com" => ""
    ),

    // API NganLuong
    'NGANLUONG_URL' => 'https://www.nganluong.vn/checkout.php',
    'RECEIVER' => 'kieutrinh1010@gmail.com',
    'MERCHANTNL_ID' => '46550',
    'MERCHANT_PASS' => '85038148a508060cf29705328c09a124',
    // End API NganLuong

    // API BaoKim
    'EMAIL_BUSINESS' => 'dev.baokim@bk.vn',     //Email Bảo kim
    'MERCHANT_ID' => '647',                      // Mã website tích hợp
    'SECURE_PASS' =>'ae543c080ad91c23',           // Mật khẩu

    'API_USER' => 'merchant',   //API USER
    'API_PWD' => '1234',           //API PASSWORD
    //Phần dưới này không được tab vào
    'PRIVATE_KEY_BAOKIM' => '-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDZZBAIQz1UZtVm
p0Jwv0SnoIkGYdHUs7vzdfXYBs1wvznuLp/SfC/MHzHVQw7urN8qv+ZDxzTMgu2Q
3FhMOQ+LIoqYNnklm+5EFsE8hz01sZzg+uRBbyNEdcTa39I4X88OFr13KoJC6sBE
397+5HG1HPjip8a83v8G4/IPcna5/3ydVbJ9ZeMSUXP6ZyKAKay4M22/Wli7PLrm
1XNR9JgIuQLma74yCGkaXtCJQswjyYAmwDPpz4ZknSGuBYUmwaHMgrDOQsOXFW7/
7M2KbjenwggAW98f0f97AR2DEq9Eb5r8vzyHURnHGD3/noZxl993lM2foPI3SKBO
1KpSeXRzAgMBAAECggEANMINBgRTgQVH6xbSkAxLPCdAufTJeMZ56bcKB/h2qVMv
Wvejv/B1pSM489nHaPM5YeWam35f+PYZc5uWLkF23TxvyEsIEbGLHKktEmR73WkS
eqNI+/xd4cJ3GOtS2G2gEXpBVwdQ/657JPvz4YZNdjfmyxMOr02rNN/jIg6Uc8Tz
vbpGdtP49nhqcOUpbKEyUxdDo6TgLVgmLAKkGJVW40kwvU9hTTo6GXledLNtL2kD
l6gpVWAiT6xlTsD5m74YzsxCSjkh60NdYeUDYwMbv0WWH3kJq6qD063ac3i/i8H+
B5nGf4KbKg1bBjPLNymUj7RRnKjHr301i2u8LUQYuQKBgQD15YCoa5uHd6DHUXEK
kejU34Axznr3Gs6LqcisE7t0oQ9hB4s16U9f4DBHDOvnkLb0zkadwdEmwo/D/Tdf
5c/JEk8q/aO9Wk8uV4Bswnx1OV9uKMzMOZbv/So1DQg1aW1MgvRnj3SiKpDUkNwr
en4NT9tbH21SmVIO9Da5KpiFRwKBgQDiUrg1hp8EDaeZFTG9DvcwyTTrpD/YT9Wr
s/NtEnPMjy0NXWcEXwGzx90P+qjJ+J29Hk89QHON6S7o0X2lUIer3uXokc86ce76
5UIbR6u7R1T6TUNfwqwwNfIbgtFN4+7ybodPNZ5DWslKLqMr5wpwIOr7/U5ih7BH
JK0cSriddQKBgGXzNZiepOlRrBN3rMqZHFPGJrx/w3PYZXJ6fnz54WrFrD6qhglg
Jky2As4yiUyFL5XoQFcAGNtdJ4Y24lKcUb4oHTLR3qWPX+zy0ohFSpy/oNVnjSHP
bskpyeoc8R5UC8EBOpwFWnIx+8JmHSLZspGKXoQ1T3pDn0Yb8uRqyLnZAoGBAKdk
NwqfvwzobIU0v8ztPLbAmnuOyAndQlP0jJ6nfy5U1yWDZ6Y7/q5RrJcc9aosT76I
pGLRQKY9SYy5JQ0YOsBL5A/XiEXZ7r9ywSocIFAruhZG/wXcni4qOB9Q6i2J4Dk+
tqVHKv72LtrHE7hs8bNtJV+rQkZtxVtZLRA308PhAoGBALVEaYMRm97V+Tnsej6q
fuT/6oKHPqZpur2rNfEKVn5Aq2kmFrvyUhvXi0IAWQ/XS3XJ7faQnprrWT6pYiSy
2YQuaghlNG1SATVd5eUadq2pA8DuSzqWFa0Ac1IAyliBO2uLPL7LzuEKmmuQk0vI
TU2Q8idAb77K7mvVguA3LDhN
-----END PRIVATE KEY-----',


    'BAOKIM_API_SELLER_INFO' => '/payment/rest/payment_pro_api/get_seller_info',
    'BAOKIM_API_PAY_BY_CARD' => '/payment/rest/payment_pro_api/pay_by_card',
    'BAOKIM_API_PAYMENT' => '/payment/order/version11',
    //'BAOKIM_URL' => 'https://www.baokim.vn',
    'BAOKIM_URL' => 'http://kiemthu.baokim.vn',
    'PAYMENT_METHOD_TYPE_LOCAL_CARD' => 1,
    'PAYMENT_METHOD_TYPE_CREDIT_CARD' => 2,
    'PAYMENT_METHOD_TYPE_INTERNET_BANKING' => 3,
    'PAYMENT_METHOD_TYPE_ATM_TRANSFER' => 4,
    'PAYMENT_METHOD_TYPE_BANK_TRANSFER' => 5
    // End API BaoKim
);
