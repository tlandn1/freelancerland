<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/


$factory->define(App\User::class, function (Faker\Generator $faker) {
    $nameAccount = $faker->userName;
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt('123456'),
        'remember_token' => str_random(10),
        'phone' => $faker->tollFreePhoneNumber,
        'note' => $faker->sentence(5),
        'sodu_tk' => $faker->randomNumber(8),
        'skype' => $nameAccount,
        'facebook' => $nameAccount.'@gmail.com',
<<<<<<< HEAD
        'website' => $faker->domainName,
=======
        'website' => 'http://'.$faker->domainName,
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
        'user_status_id' => USER_STATUS_ACTIVE_ID,
        'role_id' => USER_ROLE_FREELANCER_ID,
        'avatar' =>'/images/users/avatar-default.jpg',
        'district_id' => rand(1,9),
        'cmnd_number' => $faker->creditCardNumber,
        'cmnd_date_of_birth' => $faker->Date,
        'cmnd_address' => $faker->address,
        'about_job_title' => $faker->sentence(5),
        'about_description' => $faker->text(400),
    ];
});
