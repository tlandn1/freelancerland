<?php

use Illuminate\Database\Seeder;

class UserStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('user_statuses')->delete();

        \DB::table('user_statuses')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'active',
                'label' => 'Active',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'banned',
                'label' => 'Banned',
            ),
        ));
    }
}
