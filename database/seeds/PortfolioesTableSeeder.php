<?php

use Illuminate\Database\Seeder;

class PortfolioesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('portfolio')->delete();

        \DB::table('portfolio')->insert(array (
            0 =>
            array (
                'id' => 1,
                'title' => 'Lập trình viên web PHP',
                'description' => 'Tôi thành thạo những kỹ năng: MySQL, Laravel, jQuery, Bootstrap, CSS.
                                  Tôi có thể quản lý tốt thời gian để mang lại hiệu quả cho công việc.
                                  Ngoài ra tôi cũng có khả năng tìm hiểu nhanh để đáp ứng tốt với các yêu cầu mới.',
                'link' => 'user2.com',
                'user_id' => '2',
            ),
            1 =>
            array (
                'id' => 2,
                'title' => 'Chuyên gia thiết kế đồ họa',
                'description' => 'Tôi thành thạo những kỹ năng: Photoshop, Corel',
                'link' => 'user3.com',
                'user_id' => '3',
            ),
            2 =>
            array (
                'id' => 3,
                'title' => 'Lập trình viên web PHP',
                'description' => 'Tôi thành thạo những kỹ năng: MySQL, Laravel, jQuery, Bootstrap, CSS.
                                  Tôi có thể quản lý tốt thời gian để mang lại hiệu quả cho công việc.
                                  Ngoài ra tôi cũng có khả năng tìm hiểu nhanh để đáp ứng tốt với các yêu cầu mới.',
                'link' => 'user4.com',
                'user_id' => '4',
            ),
            3 =>
            array (
                'id' => 4,
                'title' => 'Chuyên gia thiết kế đồ họa',
                'description' => 'Tôi thành thạo những kỹ năng: Photoshop, 3D',
                'link' => 'user5.com',
                'user_id' => '5',
            ),
        ));
    }
}
