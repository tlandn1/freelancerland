<?php

use Illuminate\Database\Seeder;
use App\Static_Page;
use Carbon\Carbon;

class SlugSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = App\City::all();

        foreach ($cities as $city) {
            $city->sluggify();
            $city->save();
        }

        $districts = App\District::all();

        foreach ($districts as $district) {
            $district->sluggify();
            $district->save();
        }

        $firstcategories = App\FirstCategory::all();

        foreach ($firstcategories as $firstcategory) {
            $firstcategory->sluggify();
            $firstcategory->save();
        }

        $seconcategories = App\SecondCategory::all();

        foreach ($seconcategories as $seconcategory) {
            $seconcategory->sluggify();
            $seconcategory->save();
        }

        $skills = App\Skill::all();

        foreach ($skills as $skill) {
            $skill->sluggify();
            $skill->save();
        }
    }
}
