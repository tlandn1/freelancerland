<?php

use Illuminate\Database\Seeder;

class EducationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \DB::table('education')->delete();

      \DB::table('education')->insert(array (
          0 =>
          array (
              'id' => 1,
              'name_school' => 'Đại học Bách Khoa - ĐHĐN',
              'year' => '1990-11-22',
              'degree' => 'Kỹ sư',
              'description' => 'Lập trình website',
              'user_id' => '2',
          ),
          1 =>
          array (
              'id' => 2,
              'name_school' => 'Đại học Bách Khoa - ĐHĐN',
              'year' => '1998-11-22',
              'degree' => 'Kỹ sư',
              'description' => 'Lập trình website',
              'user_id' => '3',
          ),
          2 =>
          array (
              'id' => 3,
              'name_school' => 'Đại học Bách Khoa - ĐHĐN',
              'year' => '2000-11-22',
              'degree' => 'Kỹ sư',
              'description' => 'Lập trình website',
              'user_id' => '4',
          ),
          3 =>
          array (
              'id' => 4,
              'name_school' => 'Đại học Bách Khoa - ĐHĐN',
              'year' => '2010-11-22',
              'degree' => 'Kỹ sư',
              'description' => 'Lập trình website',
              'user_id' => '5',
          ),
      ));
    }
}
