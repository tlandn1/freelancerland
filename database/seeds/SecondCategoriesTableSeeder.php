<?php

use Illuminate\Database\Seeder;

class SecondCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \DB::table('second_categories')->delete();

      \DB::table('second_categories')->insert(array (
          0 =>
          array (
              'id' => 1,
              'name' => 'Lập trình phần mềm',
              'first_category_id' => 1,
              'slug' => 'lap-trinh-phan-mem',
          ),
          1 =>
          array (
              'id' => 2,
              'name' => 'Lập trình website',
              'first_category_id' => 1,
              'slug' => 'lap-trinh-website',
          ),
          2 =>
          array (
              'id' => 3,
              'name' => 'Lập trình Game',
              'first_category_id' => 1,
              'slug' => 'lap-trinh-game',
          ),
          3 =>
          array (
              'id' => 4,
              'name' => 'Lập trình di động',
              'first_category_id' => 1,
              'slug' => 'lap-trinh-di-dong',
          ),
          4 =>
          array (
              'id' => 5,
              'name' => 'QA Tester',
              'first_category_id' => 1,
              'slug' => 'qa-tester',
          ),
          5 =>
          array (
              'id' => 6,
              'name' => 'Thiết kế',
              'first_category_id' => 1,
              'slug' => 'thiet-ke',
          ),
          6 =>
          array (
              'id' => 7,
              'name' => 'Việc lập trình khác',
              'first_category_id' => 1,
              'slug' => 'viec-lap-trinh-khac',
          ),
          7 =>
          array (
              'id' => 8,
              'name' => 'Thiết kế, quản trị hệ thống mạng',
              'first_category_id' => 2,
              'slug' => 'thiet-ke-quan-tri-he-thong-mang',
          ),
          8 =>
          array (
              'id' => 9,
              'name' => 'Tối ưu cho công cụ tìm kiếm – SEO',
              'first_category_id' => 2,
              'slug' => 'toi-uu-cho-cong-cu-tim-kiem-seo',
          ),
          9 =>
          array (
              'id' => 10,
              'name' => 'Tăng lượt truy cập cho trang web của tôi',
              'first_category_id' => 2,
              'slug' => 'tang-luot-truy-cap-cho-trang-web-cua-toi',
          ),
          10 =>
          array (
              'id' => 11,
              'name' => 'Bảo mật thông tin',
              'first_category_id' => 2,
              'slug' => 'bao-mat-thong-tin',
          ),
          11 =>
          array (
              'id' => 12,
              'name' => 'Quản trị cơ sở dữ liệu',
              'first_category_id' => 2,
              'slug' => 'quan-tri-co-so-du-lieu',
          ),
          12 =>
          array (
              'id' => 13,
              'name' => 'Hệ thống thương mại điện tử',
              'first_category_id' => 2,
              'slug' => 'he-thong-thuong-mai-dien-tu',
          ),
          // 13 =>
          // array (
          //     'id' => 14,
          //     'name' => 'Cho thuê, cần thuê ô tô, xe tải',
          //     'first_category_id' => 2,
          //     'slug' => 'cho-thue-can-thue-o-to-xe-tai',
          // ),
          14 =>
          array (
              'id' => 15,
              'name' => 'Quản lý dự án',
              'first_category_id' => 2,
              'slug' => 'quan-ly-du-an',
          ),
          15 =>
          array (
              'id' => 16,
              'name' => 'Các IT và mạng khác',
              'first_category_id' => 2,
              'slug' => 'cac-it-va-mang-khac',
          ),
          16 =>
          array (
              'id' => 17,
              'name' => '3D Modeling và CAD',
              'first_category_id' => 3,
              'slug' => '3d-modeling-va-cad',
          ),
          17 =>
          array (
              'id' => 18,
              'name' => 'Kiến trúc sư',
              'first_category_id' => 3,
              'slug' => 'kien-truc-su',
          ),
          18 =>
          array (
              'id' => 19,
              'name' => 'Kỹ sư hóa học',
              'first_category_id' => 3,
              'slug' => 'ky-su-hoa-hoc',
          ),
          19 =>
          array (
              'id' => 20,
              'name' => 'Kỹ sư xây dựng và kết cấu',
              'first_category_id' => 3,
              'slug' => 'ky-su-xay-dung-va-ket-cau',
          ),
          20 =>
          array (
              'id' => 21,
              'name' => 'Kỹ sư điện',
              'first_category_id' => 3,
              'slug' => 'ky-su-dien',
          ),
          21 =>
          array (
              'id' => 22,
              'name' => 'Kỹ sư cơ khí',
              'first_category_id' => 3,
              'slug' => 'ky-su-co-khi',
          ),
          22 =>
          array (
              'id' => 23,
              'name' => 'Thiết kế và trang trí nội thất',
              'first_category_id' => 3,
              'slug' => 'thiet-ke-va-trang-tri-noi-that',
          ),
          23 =>
          array (
              'id' => 24,
              'name' => 'Contract Manufacturing',
              'first_category_id' => 3,
              'slug' => 'contract-manufacturing',
          ),
          24 =>
          array (
              'id' => 25,
              'name' => 'Các kỹ sư và kến trúc sư khác',
              'first_category_id' => 3,
              'slug' => 'cac-ky-su-va-kien-truc-su-khac',
          ),
          25 =>
          array (
              'id' => 26,
              'name' => 'Ảnh và chỉnh sửa ảnh',
              'first_category_id' => 4,
              'slug' => 'anh-va-chinh-sua-anh',
          ),
          26 =>
          array (
              'id' => 27,
              'name' => 'Animation',
              'first_category_id' => 4,
              'slug' => 'animation',
          ),
          27 =>
          array (
              'id' => 28,
              'name' => 'Audio Production',
              'first_category_id' => 4,
              'slug' => 'audio-production',
          ),
          28 =>
          array (
              'id' => 29,
              'name' => 'Làm video clip',
              'first_category_id' => 4,
              'slug' => 'lam-video-clip',
          ),
          29 =>
          array (
              'id' => 30,
              'name' => 'Thiết kế đồ họa',
              'first_category_id' => 4,
              'slug' => 'thiet-ke-do-hoa',
          ),
          30 =>
          array (
              'id' => 31,
              'name' => 'Thiết kế Logo và Nhãn hiệu và Banner',
              'first_category_id' => 4,
              'slug' => 'thiet-ke-logo-va-nhan-hieu-va-banner',
          ),
          31 =>
          array (
              'id' => 32,
              'name' => 'Illustration',
              'first_category_id' => 4,
              'slug' => 'illustration',
          ),
          32 =>
          array (
              'id' => 33,
              'name' => 'Photography',
              'first_category_id' => 4,
              'slug' => 'photography',
          ),
          33 =>
          array (
              'id' => 34,
              'name' => 'Các việc thiết kế khác',
              'first_category_id' => 4,
              'slug' => 'cac-viec-thiet-ke-khac',
          ),
          34 =>
          array (
              'id' => 35,
              'name' => 'Biên tập và chỉnh sửa nội dung',
              'first_category_id' => 5,
              'slug' => 'bien-tap-va-chinh-sua-noi-dung',
          ),
          35 =>
          array (
              'id' => 36,
              'name' => 'Viết bài PR và quảng cáo sáng tạo',
              'first_category_id' => 5,
              'slug' => 'viet-bai-pr-va-quang-cao-sang-tao',
          ),
          36 =>
          array (
              'id' => 37,
              'name' => 'Copywriting',
              'first_category_id' => 5,
              'slug' => 'copywriting',
          ),
          37 =>
          array (
              'id' => 38,
              'name' => 'Quản lý blog và fanpage',
              'first_category_id' => 5,
              'slug' => 'quan-li-blog-va-fanpage',
          ),
          38 =>
          array (
              'id' => 39,
              'name' => 'Viết báo và tạp chí',
              'first_category_id' => 5,
              'slug' => 'viet-bao-va-tap-chi',
          ),
          39 =>
          array (
              'id' => 40,
              'name' => 'Viết nội dung cho website',
              'first_category_id' => 5,
              'slug' => 'viet-noi-dung-cho-website',
          ),
          40 =>
          array (
              'id' => 41,
              'name' => 'Viết sách',
              'first_category_id' => 5,
              'slug' => 'viet-sach',
          ),
          41 =>
          array (
              'id' => 42,
              'name' => 'Dịch thuật',
              'first_category_id' => 5,
              'slug' => 'dich-thuat',
          ),
          42 =>
          array (
              'id' => 43,
              'name' => 'Viết lách và dịch thuật khác',
              'first_category_id' => 5,
              'slug' => 'viet-lach-va-dich-thuat-khac',
          ),
          43 =>
          array (
              'id' => 44,
              'name' => 'Tư vấn bán hàng và Giới thiệu sản phẩm',
              'first_category_id' => 6,
              'slug' => 'tu-van-ban-hang-va-gioi-thieu-san-pham',
          ),
          44 =>
          array (
              'id' => 45,
              'name' => 'Nghiên cứu và khảo sát thị trường',
              'first_category_id' => 6,
              'slug' => 'nghien-cuu-va-khao-sat-thi-truong',
          ),
          45 =>
          array (
              'id' => 46,
              'name' => 'Quảng cáo facebook, Google Adwords',
              'first_category_id' => 6,
              'slug' => 'quang-cao-facebook-google-adwords',
          ),
          46 =>
          array (
              'id' => 47,
              'name' => 'Email marketing',
              'first_category_id' => 6,
              'slug' => 'email-marketing',
          ),
          47 =>
          array (
              'id' => 48,
              'name' => 'Tổ chức sự kiện',
              'first_category_id' => 6,
              'slug' => 'to-chuc-su-kien',
          ),
          48 =>
          array (
              'id' => 49,
              'name' => 'Tư vấn, lập kế hoạch và triển khai marketing online',
              'first_category_id' => 6,
              'slug' => 'tu-van-lap-ke-hoach-va-trien-khai-marketing-online',
          ),
          49 =>
          array (
              'id' => 50,
              'name' => 'SEM – Search Engine Marketing',
              'first_category_id' => 6,
              'slug' => 'sem-search-engine-marketing ',
          ),
          50 =>
          array (
              'id' => 51,
              'name' => 'SEO – Search Engine Optimization',
              'first_category_id' => 6,
              'slug' => 'seo–search-engine-optimization',
          ),
          51 =>
          array (
              'id' => 52,
              'name' => 'SMM – Social Media Marketing',
              'first_category_id' => 6,
              'slug' => 'smm–social-media-marketing',
          ),
          52 =>
          array (
              'id' => 53,
              'name' => 'Trợ lý từ xa',
              'first_category_id' => 6,
              'slug' => 'tro-ly-tu-xa',
          ),
          53 =>
          array (
              'id' => 54,
              'name' => 'Việc KD và marketing khác',
              'first_category_id' => 6,
              'slug' => 'viec-kd-va-marketing-khac',
          ),
          54 =>
          array (
              'id' => 55,
              'name' => 'Nhập dữ liệu',
              'first_category_id' => 7,
              'slug' => 'nhap-du-lieu',
          ),
          55 =>
          array (
              'id' => 56,
              'name' => 'Xử lý dữ liệu',
              'first_category_id' => 7,
              'slug' => 'xu-ly-du-lieu',
          ),
          56 =>
          array (
              'id' => 57,
              'name' => 'Chăm sóc khách hàng',
              'first_category_id' => 7,
              'slug' => 'cham-soc-khach-hang',
          ),
          57 =>
          array (
              'id' => 58,
              'name' => 'Hỗ trợ kỹ thuật',
              'first_category_id' => 7,
              'slug' => 'ho-tro-ky-thuat',
          ),
          58 =>
          array (
              'id' => 59,
              'name' => 'Dịch vụ khách hàng khác',
              'first_category_id' => 7,
              'slug' => 'dich-vu-khach-hang-khac',
          ),
          59 =>
          array (
              'id' => 60,
              'name' => 'Hồ sơ pháp lý',
              'first_category_id' => 8,
              'slug' => 'ho-so-phap-ly',
          ),
          60 =>
          array (
              'id' => 61,
              'name' => 'Kế toán từ xa',
              'first_category_id' => 8,
              'slug' => 'ke-toan-tu-xa',
          ),
          61 =>
          array (
              'id' => 62,
              'name' => 'Financial Planning',
              'first_category_id' => 8,
              'slug' => 'financial-planning',
          ),
          62 =>
          array (
              'id' => 63,
              'name' => 'Human Resources',
              'first_category_id' => 8,
              'slug' => 'human-resources',
          ),
          63 =>
          array (
              'id' => 64,
              'name' => 'Management Consulting',
              'first_category_id' => 8,
              'slug' => 'management-consulting',
          ),
          64 =>
          array (
              'id' => 65,
              'name' => 'Tư vấn pháp lý',
              'first_category_id' => 8,
              'slug' => 'tu-van-phap-ly',
          ),
          65 =>
          array (
              'id' => 66,
              'name' => 'Bảo hiểm',
              'first_category_id' => 9,
              'slug' => 'bao-hiem',
          ),
          66 =>
          array (
              'id' => 67,
              'name' => 'Bất kì công việc gì',
              'first_category_id' => 9,
              'slug' => 'bat-ki-cong-viec-gi',
          ),
          67 =>
          array (
              'id' => 68,
              'name' => 'Cưới hỏi',
              'first_category_id' => 9,
              'slug' => 'cuoi-hoi',
          ),
          68 =>
          array (
              'id' => 69,
              'name' => 'Dinh dưỡng',
              'first_category_id' => 9,
              'slug' => 'dinh-duong',
          ),
          69 =>
          array (
              'id' => 70,
              'name' => 'Đánh giá và Thẩm định',
              'first_category_id' => 9,
              'slug' => 'danh-gia-va-tham-dinh',
          ),
          70 =>
          array (
              'id' => 71,
              'name' => 'Gia sư',
              'first_category_id' => 9,
              'slug' => 'gia-su',
          ),
          71 =>
          array (
              'id' => 72,
              'name' => 'Lịch sử',
              'first_category_id' => 9,
              'slug' => 'lich-su',
          ),
          72 =>
          array (
              'id' => 73,
              'name' => 'Pha chế đồ uống',
              'first_category_id' => 9,
              'slug' => 'pha-che-do-uong',
          ),
          73 =>
          array (
              'id' => 74,
              'name' => 'Nấu ăn',
              'first_category_id' => 9,
              'slug' => 'nau-an',
          ),
          74 =>
          array (
              'id' => 75,
              'name' => 'Năng lượng',
              'first_category_id' => 9,
              'slug' => 'nang-luong',
          ),
          75 =>
          array (
              'id' => 76,
              'name' => 'Tâm lý học',
              'first_category_id' => 9,
              'slug' => 'tam-ly-hoc',
          ),
          76 =>
          array (
              'id' => 77,
              'name' => 'Thể thao',
              'first_category_id' => 9,
              'slug' => 'the-thao',
          ),
          77 =>
          array (
              'id' => 78,
              'name' => 'Thiết kế hoa văn',
              'first_category_id' => 9,
              'slug' => 'thiet-ke-hoa-van',
          ),
          78 =>
          array (
              'id' => 79,
              'name' => 'Tự động hóa',
              'first_category_id' => 9,
              'slug' => 'tu-dong-hoa',
          ),
      ));
    }
}
