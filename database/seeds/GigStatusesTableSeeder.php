<?php

use Illuminate\Database\Seeder;

class GigStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\DB::table('gig_statuses')->delete();

    	\DB::table('gig_statuses')->insert(array (
    		0 =>
    		array (
    			'id' => 1,
    			'name' => 'waitingreview',
    			'label' => 'Waiting Review',
    			),
    		1 =>
    		array (
    			'id' => 2,
    			'name' => 'active',
    			'label' => 'Active',
    			),
    		2 =>
    		array (
    			'id' => 3,
    			'name' => 'refused',
    			'label' => 'Refused',
    			),
    		3 =>
            array (
                'id' => 4,
                'name' => 'archived',
                'label' => 'Archived',
                ),
            ));
        }
    }
