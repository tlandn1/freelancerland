<?php

use Illuminate\Database\Seeder;

class PortfoliosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        \DB::table('portfolio')->delete();

        \DB::table('portfolio')->insert(array (
            0 =>
            array (
                'id' => 1,
                'title' => 'Lập trình viên web PHP',
                'description' => 'Tôi thành thạo những kỹ năng: MySQL, Laravel, jQuery, Bootstrap, CSS.
                                  Tôi có thể quản lý tốt thời gian để mang lại hiệu quả cho công việc.
                                  Ngoài ra tôi cũng có khả năng tìm hiểu nhanh để đáp ứng tốt với các yêu cầu mới.',
                'link' => 'http://user2.com',
                'user_id' => '2',
            ),
            1 =>
            array (
                'id' => 2,
                'title' => 'Chuyên gia thiết kế đồ họa',
                'description' => 'Tôi thành thạo những kỹ năng: Photoshop, Corel',
                'link' => 'http://user3.com',
                'user_id' => '3',
            ),
            2 =>
            array (
                'id' => 3,
                'title' => 'Lập trình viên web PHP',
                'description' => 'Tôi thành thạo những kỹ năng: MySQL, Laravel, jQuery, Bootstrap, CSS.
                                  Tôi có thể quản lý tốt thời gian để mang lại hiệu quả cho công việc.
                                  Ngoài ra tôi cũng có khả năng tìm hiểu nhanh để đáp ứng tốt với các yêu cầu mới.',
                'link' => 'http://user4.com',
                'user_id' => '4',
            ),
            3 =>
            array (
                'id' => 4,
                'title' => 'Chuyên gia thiết kế đồ họa',
                'description' => 'Tôi thành thạo những kỹ năng: Photoshop, 3D',
                'link' => 'http://user5.com',
                'user_id' => '5',
            ),
            4 =>
            array (
                'id' => 5,
                'title' => $faker->sentence(4),
                'description' => $faker->text(400),
                'link' => 'http://'.$faker->domainName,
                'user_id' => '6',
            ),
            5 =>
            array (
                'id' => 6,
                'title' => $faker->sentence(4),
                'description' => $faker->text(400),
                'link' => 'http://'.$faker->domainName,
                'user_id' => '7',
            ),
            6 =>
            array (
                'id' => 7,
                'title' => $faker->sentence(4),
                'description' => $faker->text(400),
                'link' => 'http://'.$faker->domainName,
                'user_id' => '8',
            ),
            7 =>
            array (
                'id' => 8,
                'title' => $faker->sentence(4),
                'description' => $faker->text(400),
                'link' => 'http://'.$faker->domainName,
                'user_id' => '9',
            ),
            8 =>
            array (
                'id' => 9,
                'title' => $faker->sentence(4),
                'description' => $faker->text(400),
                'link' => 'http://'.$faker->domainName,
                'user_id' => '10',
            ),
            9 =>
            array (
                'id' => 10,
                'title' => $faker->sentence(4),
                'description' => $faker->text(400),
                'link' => 'http://'.$faker->domainName,
                'user_id' => '11',
            ),
            10 =>
            array (
                'id' => 11,
                'title' => $faker->sentence(4),
                'description' => $faker->text(400),
                'link' => 'http://'.$faker->domainName,
                'user_id' => '12',
            ),
            11 =>
            array (
                'id' => 12,
                'title' => $faker->sentence(4),
                'description' => $faker->text(400),
                'link' => 'http://'.$faker->domainName,
                'user_id' => '13',
            ),
            12 =>
            array (
                'id' => 13,
                'title' => $faker->sentence(4),
                'description' => $faker->text(400),
                'link' => 'http://'.$faker->domainName,
                'user_id' => '14',
            ),
            13 =>
            array (
                'id' => 14,
                'title' => $faker->sentence(4),
                'description' => $faker->text(400),
                'link' => 'http://'.$faker->domainName,
                'user_id' => '15',
            ),
            14 =>
            array (
                'id' => 15,
                'title' => $faker->sentence(4),
                'description' => $faker->text(400),
                'link' => 'http://'.$faker->domainName,
                'user_id' => '16',
            ),

        ));
    }
}
