<?php

use Illuminate\Database\Seeder;

class DistrictsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \DB::table('districts')->delete();

      \DB::table('districts')->insert(array (
          0 =>
          array (
              'id' => 1,
              'name' => 'Ba Đình',
              'type' => 'Quận',
              'city_id' => 1,
          ),
          1 =>
          array (
              'id' => 2,
              'name' => 'Hoàn Kiếm',
              'type' => 'Quận',
              'city_id' => 1,
          ),
          2 =>
          array (
              'id' => 3,
              'name' => 'Tây Hồ',
              'type' => 'Quận',
              'city_id' => 1,
          ),
          3 =>
          array (
              'id' => 4,
              'name' => 'Long Biên',
              'type' => 'Quận',
              'city_id' => 1,
          ),
          4 =>
          array (
              'id' => 5,
              'name' => 'Cầu Giấy',
              'type' => 'Quận',
              'city_id' => 1,
          ),
          5 =>
          array (
              'id' => 6,
              'name' => 'Đống Đa',
              'type' => 'Quận',
              'city_id' => 1,
          ),
          6 =>
          array (
              'id' => 7,
              'name' => 'Hai Bà Trưng',
              'type' => 'Quận',
              'city_id' => 1,
          ),
          7 =>
          array (
              'id' => 8,
              'name' => 'Hoàng Mai',
              'type' => 'Quận',
              'city_id' => 1,
          ),
          8 =>
          array (
              'id' => 9,
              'name' => 'Thanh Xuân',
              'type' => 'Quận',
              'city_id' => 1,
          ),
          9 =>
          array (
              'id' => 16,
              'name' => 'Sóc Sơn',
              'type' => 'Huyện',
              'city_id' => 1,
          ),
          10 =>
          array (
              'id' => 17,
              'name' => 'Đông Anh',
              'type' => 'Huyện',
              'city_id' => 1,
          ),
          11 =>
          array (
              'id' => 18,
              'name' => 'Gia Lâm',
              'type' => 'Huyện',
              'city_id' => 1,
          ),
          12 =>
          array (
              'id' => 19,
              'name' => 'Bắc Từ Liêm',
              'type' => 'Quận',
              'city_id' => 1,
          ),
          1200 =>
          array (
              'id' => 20,
              'name' => 'Nam Từ Liêm',
              'type' => 'Quận',
              'city_id' => 1,
          ),
          13 =>
          array (
              'id' => 21,
              'name' => 'Thanh Trì',
              'type' => 'Huyện',
              'city_id' => 1,
          ),
          14 =>
          array (
              'id' => 24,
              'name' => 'Hà Giang',
              'type' => 'Thị Xã',
              'city_id' => 2,
          ),
          15 =>
          array (
              'id' => 26,
              'name' => 'Đồng Văn',
              'type' => 'Huyện',
              'city_id' => 2,
          ),
          16 =>
          array (
              'id' => 27,
              'name' => 'Mèo Vạc',
              'type' => 'Huyện',
              'city_id' => 2,
          ),
          17 =>
          array (
              'id' => 28,
              'name' => 'Yên Minh',
              'type' => 'Huyện',
              'city_id' => 2,
          ),
          18 =>
          array (
              'id' => 29,
              'name' => 'Quản Bạ',
              'type' => 'Huyện',
              'city_id' => 2,
          ),
          19 =>
          array (
              'id' => 30,
              'name' => 'Vị Xuyên',
              'type' => 'Huyện',
              'city_id' => 2,
          ),
          20 =>
          array (
              'id' => 31,
              'name' => 'Bắc Mê',
              'type' => 'Huyện',
              'city_id' => 2,
          ),
          21 =>
          array (
              'id' => 32,
              'name' => 'Hoàng Su Phì',
              'type' => 'Huyện',
              'city_id' => 2,
          ),
          22 =>
          array (
              'id' => 33,
              'name' => 'Xín Mần',
              'type' => 'Huyện',
              'city_id' => 2,
          ),
          23 =>
          array (
              'id' => 34,
              'name' => 'Bắc Quang',
              'type' => 'Huyện',
              'city_id' => 2,
          ),
          24 =>
          array (
              'id' => 35,
              'name' => 'Quang Bình',
              'type' => 'Huyện',
              'city_id' => 2,
          ),
          25 =>
          array (
              'id' => 40,
              'name' => 'Cao Bằng',
              'type' => 'Thị Xã',
              'city_id' => 4,
          ),
          26 =>
          array (
              'id' => 42,
              'name' => 'Bảo Lâm',
              'type' => 'Huyện',
              'city_id' => 4,
          ),
          27 =>
          array (
              'id' => 43,
              'name' => 'Bảo Lạc',
              'type' => 'Huyện',
              'city_id' => 4,
          ),
          28 =>
          array (
              'id' => 44,
              'name' => 'Thông Nông',
              'type' => 'Huyện',
              'city_id' => 4,
          ),
          29 =>
          array (
              'id' => 45,
              'name' => 'Hà Quảng',
              'type' => 'Huyện',
              'city_id' => 4,
          ),
          30 =>
          array (
              'id' => 46,
              'name' => 'Trà Lĩnh',
              'type' => 'Huyện',
              'city_id' => 4,
          ),
          31 =>
          array (
              'id' => 47,
              'name' => 'Trùng Khánh',
              'type' => 'Huyện',
              'city_id' => 4,
          ),
          32 =>
          array (
              'id' => 48,
              'name' => 'Hạ Lang',
              'type' => 'Huyện',
              'city_id' => 4,
          ),
          33 =>
          array (
              'id' => 49,
              'name' => 'Quảng Uyên',
              'type' => 'Huyện',
              'city_id' => 4,
          ),
          34 =>
          array (
              'id' => 50,
              'name' => 'Phục Hoà',
              'type' => 'Huyện',
              'city_id' => 4,
          ),
          35 =>
          array (
              'id' => 51,
              'name' => 'Hoà An',
              'type' => 'Huyện',
              'city_id' => 4,
          ),
          36 =>
          array (
              'id' => 52,
              'name' => 'Nguyên Bình',
              'type' => 'Huyện',
              'city_id' => 4,
          ),
          37 =>
          array (
              'id' => 53,
              'name' => 'Thạch An',
              'type' => 'Huyện',
              'city_id' => 4,
          ),
          38 =>
          array (
              'id' => 58,
              'name' => 'Bắc Kạn',
              'type' => 'Thị Xã',
              'city_id' => 6,
          ),
          39 =>
          array (
              'id' => 60,
              'name' => 'Pác Nặm',
              'type' => 'Huyện',
              'city_id' => 6,
          ),
          40 =>
          array (
              'id' => 61,
              'name' => 'Ba Bể',
              'type' => 'Huyện',
              'city_id' => 6,
          ),
          41 =>
          array (
              'id' => 62,
              'name' => 'Ngân Sơn',
              'type' => 'Huyện',
              'city_id' => 6,
          ),
          42 =>
          array (
              'id' => 63,
              'name' => 'Bạch Thông',
              'type' => 'Huyện',
              'city_id' => 6,
          ),
          43 =>
          array (
              'id' => 64,
              'name' => 'Chợ Đồn',
              'type' => 'Huyện',
              'city_id' => 6,
          ),
          44 =>
          array (
              'id' => 65,
              'name' => 'Chợ Mới',
              'type' => 'Huyện',
              'city_id' => 6,
          ),
          45 =>
          array (
              'id' => 66,
              'name' => 'Na Rì',
              'type' => 'Huyện',
              'city_id' => 6,
          ),
          46 =>
          array (
              'id' => 70,
              'name' => 'Tuyên Quang',
              'type' => 'Thành Phố',
              'city_id' => 8,
          ),
          47 =>
          array (
              'id' => 72,
              'name' => 'Na Hang',
              'type' => 'Huyện',
              'city_id' => 8,
          ),
          48 =>
          array (
              'id' => 73,
              'name' => 'Chiêm Hóa',
              'type' => 'Huyện',
              'city_id' => 8,
          ),
          49 =>
          array (
              'id' => 74,
              'name' => 'Hàm Yên',
              'type' => 'Huyện',
              'city_id' => 8,
          ),
          50 =>
          array (
              'id' => 75,
              'name' => 'Yên Sơn',
              'type' => 'Huyện',
              'city_id' => 8,
          ),
          51 =>
          array (
              'id' => 76,
              'name' => 'Sơn Dương',
              'type' => 'Huyện',
              'city_id' => 8,
          ),
          52 =>
          array (
              'id' => 80,
              'name' => 'Lào Cai',
              'type' => 'Thành Phố',
              'city_id' => 10,
          ),
          53 =>
          array (
              'id' => 82,
              'name' => 'Bát Xát',
              'type' => 'Huyện',
              'city_id' => 10,
          ),
          54 =>
          array (
              'id' => 83,
              'name' => 'Mường Khương',
              'type' => 'Huyện',
              'city_id' => 10,
          ),
          55 =>
          array (
              'id' => 84,
              'name' => 'Si Ma Cai',
              'type' => 'Huyện',
              'city_id' => 10,
          ),
          56 =>
          array (
              'id' => 85,
              'name' => 'Bắc Hà',
              'type' => 'Huyện',
              'city_id' => 10,
          ),
          57 =>
          array (
              'id' => 86,
              'name' => 'Bảo Thắng',
              'type' => 'Huyện',
              'city_id' => 10,
          ),
          58 =>
          array (
              'id' => 87,
              'name' => 'Bảo Yên',
              'type' => 'Huyện',
              'city_id' => 10,
          ),
          59 =>
          array (
              'id' => 88,
              'name' => 'Sa Pa',
              'type' => 'Huyện',
              'city_id' => 10,
          ),
          60 =>
          array (
              'id' => 89,
              'name' => 'Văn Bàn',
              'type' => 'Huyện',
              'city_id' => 10,
          ),
          61 =>
          array (
              'id' => 94,
              'name' => 'Điện Biên Phủ',
              'type' => 'Thành Phố',
              'city_id' => 11,
          ),
          62 =>
          array (
              'id' => 95,
              'name' => 'Mường Lay',
              'type' => 'Thị Xã',
              'city_id' => 11,
          ),
          63 =>
          array (
              'id' => 96,
              'name' => 'Mường Nhé',
              'type' => 'Huyện',
              'city_id' => 11,
          ),
          64 =>
          array (
              'id' => 97,
              'name' => 'Mường Chà',
              'type' => 'Huyện',
              'city_id' => 11,
          ),
          65 =>
          array (
              'id' => 98,
              'name' => 'Tủa Chùa',
              'type' => 'Huyện',
              'city_id' => 11,
          ),
          66 =>
          array (
              'id' => 99,
              'name' => 'Tuần Giáo',
              'type' => 'Huyện',
              'city_id' => 11,
          ),
          67 =>
          array (
              'id' => 100,
              'name' => 'Điện Biên',
              'type' => 'Huyện',
              'city_id' => 11,
          ),
          68 =>
          array (
              'id' => 101,
              'name' => 'Điện Biên Đông',
              'type' => 'Huyện',
              'city_id' => 11,
          ),
          69 =>
          array (
              'id' => 102,
              'name' => 'Mường Ảng',
              'type' => 'Huyện',
              'city_id' => 11,
          ),
          70 =>
          array (
              'id' => 104,
              'name' => 'Lai Châu',
              'type' => 'Thành Phố',
              'city_id' => 12,
          ),
          71 =>
          array (
              'id' => 105,
              'name' => 'Tam Đường',
              'type' => 'Huyện',
              'city_id' => 12,
          ),
          7100 =>
          array (
              'id' => 106,
              'name' => 'Nậm Nhùn',
              'type' => 'Huyện',
              'city_id' => 12,
          ),
          72 =>
          array (
              'id' => 107,
              'name' => 'Mường Tè',
              'type' => 'Huyện',
              'city_id' => 12,
          ),
          73 =>
          array (
              'id' => 108,
              'name' => 'Sìn Hồ',
              'type' => 'Huyện',
              'city_id' => 12,
          ),
          74 =>
          array (
              'id' => 109,
              'name' => 'Phong Thổ',
              'type' => 'Huyện',
              'city_id' => 12,
          ),
          75 =>
          array (
              'id' => 110,
              'name' => 'Than Uyên',
              'type' => 'Huyện',
              'city_id' => 12,
          ),
          76 =>
          array (
              'id' => 111,
              'name' => 'Tân Uyên',
              'type' => 'Huyện',
              'city_id' => 12,
          ),
          77 =>
          array (
              'id' => 116,
              'name' => 'Sơn La',
              'type' => 'Thành Phố',
              'city_id' => 14,
          ),
          7800 =>
          array (
              'id' => 117,
              'name' => 'Vân Hồ',
              'type' => 'Huyện',
              'city_id' => 14,
          ),
          78 =>
          array (
              'id' => 118,
              'name' => 'Quỳnh Nhai',
              'type' => 'Huyện',
              'city_id' => 14,
          ),
          79 =>
          array (
              'id' => 119,
              'name' => 'Thuận Châu',
              'type' => 'Huyện',
              'city_id' => 14,
          ),
          80 =>
          array (
              'id' => 120,
              'name' => 'Mường La',
              'type' => 'Huyện',
              'city_id' => 14,
          ),
          81 =>
          array (
              'id' => 121,
              'name' => 'Bắc Yên',
              'type' => 'Huyện',
              'city_id' => 14,
          ),
          82 =>
          array (
              'id' => 122,
              'name' => 'Phù Yên',
              'type' => 'Huyện',
              'city_id' => 14,
          ),
          83 =>
          array (
              'id' => 123,
              'name' => 'Mộc Châu',
              'type' => 'Huyện',
              'city_id' => 14,
          ),
          84 =>
          array (
              'id' => 124,
              'name' => 'Yên Châu',
              'type' => 'Huyện',
              'city_id' => 14,
          ),
          85 =>
          array (
              'id' => 125,
              'name' => 'Mai Sơn',
              'type' => 'Huyện',
              'city_id' => 14,
          ),
          86 =>
          array (
              'id' => 126,
              'name' => 'Sông Mã',
              'type' => 'Huyện',
              'city_id' => 14,
          ),
          87 =>
          array (
              'id' => 127,
              'name' => 'Sốp Cộp',
              'type' => 'Huyện',
              'city_id' => 14,
          ),
          88 =>
          array (
              'id' => 132,
              'name' => 'Yên Bái',
              'type' => 'Thành Phố',
              'city_id' => 15,
          ),
          89 =>
          array (
              'id' => 133,
              'name' => 'Nghĩa Lộ',
              'type' => 'Thị Xã',
              'city_id' => 15,
          ),
          90 =>
          array (
              'id' => 135,
              'name' => 'Lục Yên',
              'type' => 'Huyện',
              'city_id' => 15,
          ),
          91 =>
          array (
              'id' => 136,
              'name' => 'Văn Yên',
              'type' => 'Huyện',
              'city_id' => 15,
          ),
          92 =>
          array (
              'id' => 137,
              'name' => 'Mù Cang Chải',
              'type' => 'Huyện',
              'city_id' => 15,
          ),
          93 =>
          array (
              'id' => 138,
              'name' => 'Trấn Yên',
              'type' => 'Huyện',
              'city_id' => 15,
          ),
          94 =>
          array (
              'id' => 139,
              'name' => 'Trạm Tấu',
              'type' => 'Huyện',
              'city_id' => 15,
          ),
          95 =>
          array (
              'id' => 140,
              'name' => 'Văn Chấn',
              'type' => 'Huyện',
              'city_id' => 15,
          ),
          96 =>
          array (
              'id' => 141,
              'name' => 'Yên Bình',
              'type' => 'Huyện',
              'city_id' => 15,
          ),
          97 =>
          array (
              'id' => 148,
              'name' => 'Hòa Bình',
              'type' => 'Thành Phố',
              'city_id' => 17,
          ),
          98 =>
          array (
              'id' => 150,
              'name' => 'Đà Bắc',
              'type' => 'Huyện',
              'city_id' => 17,
          ),
          99 =>
          array (
              'id' => 151,
              'name' => 'Kỳ Sơn',
              'type' => 'Huyện',
              'city_id' => 17,
          ),
          100 =>
          array (
              'id' => 152,
              'name' => 'Lương Sơn',
              'type' => 'Huyện',
              'city_id' => 17,
          ),
          101 =>
          array (
              'id' => 153,
              'name' => 'Kim Bôi',
              'type' => 'Huyện',
              'city_id' => 17,
          ),
          102 =>
          array (
              'id' => 154,
              'name' => 'Cao Phong',
              'type' => 'Huyện',
              'city_id' => 17,
          ),
          103 =>
          array (
              'id' => 155,
              'name' => 'Tân Lạc',
              'type' => 'Huyện',
              'city_id' => 17,
          ),
          104 =>
          array (
              'id' => 156,
              'name' => 'Mai Châu',
              'type' => 'Huyện',
              'city_id' => 17,
          ),
          105 =>
          array (
              'id' => 157,
              'name' => 'Lạc Sơn',
              'type' => 'Huyện',
              'city_id' => 17,
          ),
          106 =>
          array (
              'id' => 158,
              'name' => 'Yên Thủy',
              'type' => 'Huyện',
              'city_id' => 17,
          ),
          107 =>
          array (
              'id' => 159,
              'name' => 'Lạc Thủy',
              'type' => 'Huyện',
              'city_id' => 17,
          ),
          108 =>
          array (
              'id' => 164,
              'name' => 'Thái Nguyên',
              'type' => 'Thành Phố',
              'city_id' => 19,
          ),
          109 =>
          array (
              'id' => 165,
              'name' => 'Sông Công',
              'type' => 'Thị Xã',
              'city_id' => 19,
          ),
          110 =>
          array (
              'id' => 167,
              'name' => 'Định Hóa',
              'type' => 'Huyện',
              'city_id' => 19,
          ),
          111 =>
          array (
              'id' => 168,
              'name' => 'Phú Lương',
              'type' => 'Huyện',
              'city_id' => 19,
          ),
          112 =>
          array (
              'id' => 169,
              'name' => 'Đồng Hỷ',
              'type' => 'Huyện',
              'city_id' => 19,
          ),
          113 =>
          array (
              'id' => 170,
              'name' => 'Võ Nhai',
              'type' => 'Huyện',
              'city_id' => 19,
          ),
          114 =>
          array (
              'id' => 171,
              'name' => 'Đại Từ',
              'type' => 'Huyện',
              'city_id' => 19,
          ),
          115 =>
          array (
              'id' => 172,
              'name' => 'Phổ Yên',
              'type' => 'Huyện',
              'city_id' => 19,
          ),
          116 =>
          array (
              'id' => 173,
              'name' => 'Phú Bình',
              'type' => 'Huyện',
              'city_id' => 19,
          ),
          117 =>
          array (
              'id' => 178,
              'name' => 'Lạng Sơn',
              'type' => 'Thành Phố',
              'city_id' => 20,
          ),
          118 =>
          array (
              'id' => 180,
              'name' => 'Tràng Định',
              'type' => 'Huyện',
              'city_id' => 20,
          ),
          119 =>
          array (
              'id' => 181,
              'name' => 'Bình Gia',
              'type' => 'Huyện',
              'city_id' => 20,
          ),
          120 =>
          array (
              'id' => 182,
              'name' => 'Văn Lãng',
              'type' => 'Huyện',
              'city_id' => 20,
          ),
          121 =>
          array (
              'id' => 183,
              'name' => 'Cao Lộc',
              'type' => 'Huyện',
              'city_id' => 20,
          ),
          122 =>
          array (
              'id' => 184,
              'name' => 'Văn Quan',
              'type' => 'Huyện',
              'city_id' => 20,
          ),
          123 =>
          array (
              'id' => 185,
              'name' => 'Bắc Sơn',
              'type' => 'Huyện',
              'city_id' => 20,
          ),
          124 =>
          array (
              'id' => 186,
              'name' => 'Hữu Lũng',
              'type' => 'Huyện',
              'city_id' => 20,
          ),
          125 =>
          array (
              'id' => 187,
              'name' => 'Chi Lăng',
              'type' => 'Huyện',
              'city_id' => 20,
          ),
          126 =>
          array (
              'id' => 188,
              'name' => 'Lộc Bình',
              'type' => 'Huyện',
              'city_id' => 20,
          ),
          127 =>
          array (
              'id' => 189,
              'name' => 'Đình Lập',
              'type' => 'Huyện',
              'city_id' => 20,
          ),
          128 =>
          array (
              'id' => 193,
              'name' => 'Hạ Long',
              'type' => 'Thành Phố',
              'city_id' => 22,
          ),
          129 =>
          array (
              'id' => 194,
              'name' => 'Móng Cái',
              'type' => 'Thành Phố',
              'city_id' => 22,
          ),
          130 =>
          array (
              'id' => 195,
              'name' => 'Cẩm Phả',
              'type' => 'Thành Phố',
              'city_id' => 22,
          ),
          131 =>
          array (
              'id' => 196,
              'name' => 'Uông Bí',
              'type' => 'Thành Phố',
              'city_id' => 22,
          ),
          132 =>
          array (
              'id' => 198,
              'name' => 'Bình Liêu',
              'type' => 'Huyện',
              'city_id' => 22,
          ),
          133 =>
          array (
              'id' => 199,
              'name' => 'Tiên Yên',
              'type' => 'Huyện',
              'city_id' => 22,
          ),
          134 =>
          array (
              'id' => 200,
              'name' => 'Đầm Hà',
              'type' => 'Huyện',
              'city_id' => 22,
          ),
          135 =>
          array (
              'id' => 201,
              'name' => 'Hải Hà',
              'type' => 'Huyện',
              'city_id' => 22,
          ),
          136 =>
          array (
              'id' => 202,
              'name' => 'Ba Chẽ',
              'type' => 'Huyện',
              'city_id' => 22,
          ),
          137 =>
          array (
              'id' => 203,
              'name' => 'Vân Đồn',
              'type' => 'Huyện',
              'city_id' => 22,
          ),
          138 =>
          array (
              'id' => 204,
              'name' => 'Hoành Bồ',
              'type' => 'Huyện',
              'city_id' => 22,
          ),
          139 =>
          array (
              'id' => 205,
              'name' => 'Đông Triều',
              'type' => 'Huyện',
              'city_id' => 22,
          ),
          140 =>
          array (
              'id' => 206,
              'name' => 'Yên Hưng',
              'type' => 'Huyện',
              'city_id' => 22,
          ),
          141 =>
          array (
              'id' => 207,
              'name' => 'Cô Tô',
              'type' => 'Huyện',
              'city_id' => 22,
          ),
          142 =>
          array (
              'id' => 213,
              'name' => 'Bắc Giang',
              'type' => 'Thành Phố',
              'city_id' => 24,
          ),
          143 =>
          array (
              'id' => 215,
              'name' => 'Yên Thế',
              'type' => 'Huyện',
              'city_id' => 24,
          ),
          144 =>
          array (
              'id' => 216,
              'name' => 'Tân Yên',
              'type' => 'Huyện',
              'city_id' => 24,
          ),
          145 =>
          array (
              'id' => 217,
              'name' => 'Lạng Giang',
              'type' => 'Huyện',
              'city_id' => 24,
          ),
          146 =>
          array (
              'id' => 218,
              'name' => 'Lục Nam',
              'type' => 'Huyện',
              'city_id' => 24,
          ),
          147 =>
          array (
              'id' => 219,
              'name' => 'Lục Ngạn',
              'type' => 'Huyện',
              'city_id' => 24,
          ),
          148 =>
          array (
              'id' => 220,
              'name' => 'Sơn Động',
              'type' => 'Huyện',
              'city_id' => 24,
          ),
          149 =>
          array (
              'id' => 221,
              'name' => 'Yên Dũng',
              'type' => 'Huyện',
              'city_id' => 24,
          ),
          150 =>
          array (
              'id' => 222,
              'name' => 'Việt Yên',
              'type' => 'Huyện',
              'city_id' => 24,
          ),
          151 =>
          array (
              'id' => 223,
              'name' => 'Hiệp Hòa',
              'type' => 'Huyện',
              'city_id' => 24,
          ),
          152 =>
          array (
              'id' => 227,
              'name' => 'Việt Trì',
              'type' => 'Thành Phố',
              'city_id' => 25,
          ),
          153 =>
          array (
              'id' => 228,
              'name' => 'Phú Thọ',
              'type' => 'Thị Xã',
              'city_id' => 25,
          ),
          154 =>
          array (
              'id' => 230,
              'name' => 'Đoan Hùng',
              'type' => 'Huyện',
              'city_id' => 25,
          ),
          155 =>
          array (
              'id' => 231,
              'name' => 'Hạ Hoà',
              'type' => 'Huyện',
              'city_id' => 25,
          ),
          156 =>
          array (
              'id' => 232,
              'name' => 'Thanh Ba',
              'type' => 'Huyện',
              'city_id' => 25,
          ),
          157 =>
          array (
              'id' => 233,
              'name' => 'Phù Ninh',
              'type' => 'Huyện',
              'city_id' => 25,
          ),
          158 =>
          array (
              'id' => 234,
              'name' => 'Yên Lập',
              'type' => 'Huyện',
              'city_id' => 25,
          ),
          159 =>
          array (
              'id' => 235,
              'name' => 'Cẩm Khê',
              'type' => 'Huyện',
              'city_id' => 25,
          ),
          160 =>
          array (
              'id' => 236,
              'name' => 'Tam Nông',
              'type' => 'Huyện',
              'city_id' => 25,
          ),
          161 =>
          array (
              'id' => 237,
              'name' => 'Lâm Thao',
              'type' => 'Huyện',
              'city_id' => 25,
          ),
          162 =>
          array (
              'id' => 238,
              'name' => 'Thanh Sơn',
              'type' => 'Huyện',
              'city_id' => 25,
          ),
          163 =>
          array (
              'id' => 239,
              'name' => 'Thanh Thuỷ',
              'type' => 'Huyện',
              'city_id' => 25,
          ),
          164 =>
          array (
              'id' => 240,
              'name' => 'Tân Sơn',
              'type' => 'Huyện',
              'city_id' => 25,
          ),
          165 =>
          array (
              'id' => 243,
              'name' => 'Vĩnh Yên',
              'type' => 'Thành Phố',
              'city_id' => 26,
          ),
          166 =>
          array (
              'id' => 244,
              'name' => 'Phúc Yên',
              'type' => 'Thị Xã',
              'city_id' => 26,
          ),
          167 =>
          array (
              'id' => 246,
              'name' => 'Lập Thạch',
              'type' => 'Huyện',
              'city_id' => 26,
          ),
          168 =>
          array (
              'id' => 247,
              'name' => 'Tam Dương',
              'type' => 'Huyện',
              'city_id' => 26,
          ),
          169 =>
          array (
              'id' => 248,
              'name' => 'Tam Đảo',
              'type' => 'Huyện',
              'city_id' => 26,
          ),
          170 =>
          array (
              'id' => 249,
              'name' => 'Bình Xuyên',
              'type' => 'Huyện',
              'city_id' => 26,
          ),
          171 =>
          array (
              'id' => 250,
              'name' => 'Mê Linh',
              'type' => 'Huyện',
              'city_id' => 1,
          ),
          172 =>
          array (
              'id' => 251,
              'name' => 'Yên Lạc',
              'type' => 'Huyện',
              'city_id' => 26,
          ),
          173 =>
          array (
              'id' => 252,
              'name' => 'Vĩnh Tường',
              'type' => 'Huyện',
              'city_id' => 26,
          ),
          174 =>
          array (
              'id' => 253,
              'name' => 'Sông Lô',
              'type' => 'Huyện',
              'city_id' => 26,
          ),
          175 =>
          array (
              'id' => 256,
              'name' => 'Bắc Ninh',
              'type' => 'Thành Phố',
              'city_id' => 27,
          ),
          176 =>
          array (
              'id' => 258,
              'name' => 'Yên Phong',
              'type' => 'Huyện',
              'city_id' => 27,
          ),
          177 =>
          array (
              'id' => 259,
              'name' => 'Quế Võ',
              'type' => 'Huyện',
              'city_id' => 27,
          ),
          178 =>
          array (
              'id' => 260,
              'name' => 'Tiên Du',
              'type' => 'Huyện',
              'city_id' => 27,
          ),
          179 =>
          array (
              'id' => 261,
              'name' => 'Từ Sơn',
              'type' => 'Thị Xã',
              'city_id' => 27,
          ),
          180 =>
          array (
              'id' => 262,
              'name' => 'Thuận Thành',
              'type' => 'Huyện',
              'city_id' => 27,
          ),
          181 =>
          array (
              'id' => 263,
              'name' => 'Gia Bình',
              'type' => 'Huyện',
              'city_id' => 27,
          ),
          182 =>
          array (
              'id' => 264,
              'name' => 'Lương Tài',
              'type' => 'Huyện',
              'city_id' => 27,
          ),
          183 =>
          array (
              'id' => 268,
              'name' => 'Hà Đông',
              'type' => 'Quận',
              'city_id' => 1,
          ),
          184 =>
          array (
              'id' => 269,
              'name' => 'Sơn Tây',
              'type' => 'Thị Xã',
              'city_id' => 1,
          ),
          185 =>
          array (
              'id' => 271,
              'name' => 'Ba Vì',
              'type' => 'Huyện',
              'city_id' => 1,
          ),
          186 =>
          array (
              'id' => 272,
              'name' => 'Phúc Thọ',
              'type' => 'Huyện',
              'city_id' => 1,
          ),
          187 =>
          array (
              'id' => 273,
              'name' => 'Đan Phượng',
              'type' => 'Huyện',
              'city_id' => 1,
          ),
          188 =>
          array (
              'id' => 274,
              'name' => 'Hoài Đức',
              'type' => 'Huyện',
              'city_id' => 1,
          ),
          189 =>
          array (
              'id' => 275,
              'name' => 'Quốc Oai',
              'type' => 'Huyện',
              'city_id' => 1,
          ),
          190 =>
          array (
              'id' => 276,
              'name' => 'Thạch Thất',
              'type' => 'Huyện',
              'city_id' => 1,
          ),
          191 =>
          array (
              'id' => 277,
              'name' => 'Chương Mỹ',
              'type' => 'Huyện',
              'city_id' => 1,
          ),
          192 =>
          array (
              'id' => 278,
              'name' => 'Thanh Oai',
              'type' => 'Huyện',
              'city_id' => 1,
          ),
          193 =>
          array (
              'id' => 279,
              'name' => 'Thường Tín',
              'type' => 'Huyện',
              'city_id' => 1,
          ),
          194 =>
          array (
              'id' => 280,
              'name' => 'Phú Xuyên',
              'type' => 'Huyện',
              'city_id' => 1,
          ),
          195 =>
          array (
              'id' => 281,
              'name' => 'Ứng Hòa',
              'type' => 'Huyện',
              'city_id' => 1,
          ),
          196 =>
          array (
              'id' => 282,
              'name' => 'Mỹ Đức',
              'type' => 'Huyện',
              'city_id' => 1,
          ),
          197 =>
          array (
              'id' => 288,
              'name' => 'Hải Dương',
              'type' => 'Thành Phố',
              'city_id' => 30,
          ),
          198 =>
          array (
              'id' => 290,
              'name' => 'Chí Linh',
              'type' => 'Huyện',
              'city_id' => 30,
          ),
          199 =>
          array (
              'id' => 291,
              'name' => 'Nam Sách',
              'type' => 'Huyện',
              'city_id' => 30,
          ),
          200 =>
          array (
              'id' => 292,
              'name' => 'Kinh Môn',
              'type' => 'Huyện',
              'city_id' => 30,
          ),
          201 =>
          array (
              'id' => 293,
              'name' => 'Kim Thành',
              'type' => 'Huyện',
              'city_id' => 30,
          ),
          202 =>
          array (
              'id' => 294,
              'name' => 'Thanh Hà',
              'type' => 'Huyện',
              'city_id' => 30,
          ),
          203 =>
          array (
              'id' => 295,
              'name' => 'Cẩm Giàng',
              'type' => 'Huyện',
              'city_id' => 30,
          ),
          204 =>
          array (
              'id' => 296,
              'name' => 'Bình Giang',
              'type' => 'Huyện',
              'city_id' => 30,
          ),
          205 =>
          array (
              'id' => 297,
              'name' => 'Gia Lộc',
              'type' => 'Huyện',
              'city_id' => 30,
          ),
          206 =>
          array (
              'id' => 298,
              'name' => 'Tứ Kỳ',
              'type' => 'Huyện',
              'city_id' => 30,
          ),
          207 =>
          array (
              'id' => 299,
              'name' => 'Ninh Giang',
              'type' => 'Huyện',
              'city_id' => 30,
          ),
          208 =>
          array (
              'id' => 300,
              'name' => 'Thanh Miện',
              'type' => 'Huyện',
              'city_id' => 30,
          ),
          209 =>
          array (
              'id' => 303,
              'name' => 'Hồng Bàng',
              'type' => 'Quận',
              'city_id' => 31,
          ),
          210 =>
          array (
              'id' => 304,
              'name' => 'Ngô Quyền',
              'type' => 'Quận',
              'city_id' => 31,
          ),
          211 =>
          array (
              'id' => 305,
              'name' => 'Lê Chân',
              'type' => 'Quận',
              'city_id' => 31,
          ),
          212 =>
          array (
              'id' => 306,
              'name' => 'Hải An',
              'type' => 'Quận',
              'city_id' => 31,
          ),
          213 =>
          array (
              'id' => 307,
              'name' => 'Kiến An',
              'type' => 'Quận',
              'city_id' => 31,
          ),
          214 =>
          array (
              'id' => 308,
              'name' => 'Đồ Sơn',
              'type' => 'Quận',
              'city_id' => 31,
          ),
          215 =>
          array (
              'id' => 309,
              'name' => 'Dương Kinh',
              'type' => 'Quận',
              'city_id' => 31,
          ),
          216 =>
          array (
              'id' => 311,
              'name' => 'Thuỷ Nguyên',
              'type' => 'Huyện',
              'city_id' => 31,
          ),
          217 =>
          array (
              'id' => 312,
              'name' => 'An Dương',
              'type' => 'Huyện',
              'city_id' => 31,
          ),
          218 =>
          array (
              'id' => 313,
              'name' => 'An Lão',
              'type' => 'Huyện',
              'city_id' => 31,
          ),
          219 =>
          array (
              'id' => 314,
              'name' => 'Kiến Thụy',
              'type' => 'Huyện',
              'city_id' => 31,
          ),
          220 =>
          array (
              'id' => 315,
              'name' => 'Tiên Lãng',
              'type' => 'Huyện',
              'city_id' => 31,
          ),
          221 =>
          array (
              'id' => 316,
              'name' => 'Vĩnh Bảo',
              'type' => 'Huyện',
              'city_id' => 31,
          ),
          222 =>
          array (
              'id' => 317,
              'name' => 'Cát Hải',
              'type' => 'Huyện',
              'city_id' => 31,
          ),
          223 =>
          array (
              'id' => 318,
              'name' => 'Bạch Long Vĩ',
              'type' => 'Huyện',
              'city_id' => 31,
          ),
          224 =>
          array (
              'id' => 323,
              'name' => 'Hưng Yên',
              'type' => 'Thành Phố',
              'city_id' => 33,
          ),
          225 =>
          array (
              'id' => 325,
              'name' => 'Văn Lâm',
              'type' => 'Huyện',
              'city_id' => 33,
          ),
          226 =>
          array (
              'id' => 326,
              'name' => 'Văn Giang',
              'type' => 'Huyện',
              'city_id' => 33,
          ),
          227 =>
          array (
              'id' => 327,
              'name' => 'Yên Mỹ',
              'type' => 'Huyện',
              'city_id' => 33,
          ),
          228 =>
          array (
              'id' => 328,
              'name' => 'Mỹ Hào',
              'type' => 'Huyện',
              'city_id' => 33,
          ),
          229 =>
          array (
              'id' => 329,
              'name' => 'Ân Thi',
              'type' => 'Huyện',
              'city_id' => 33,
          ),
          230 =>
          array (
              'id' => 330,
              'name' => 'Khoái Châu',
              'type' => 'Huyện',
              'city_id' => 33,
          ),
          231 =>
          array (
              'id' => 331,
              'name' => 'Kim Động',
              'type' => 'Huyện',
              'city_id' => 33,
          ),
          232 =>
          array (
              'id' => 332,
              'name' => 'Tiên Lữ',
              'type' => 'Huyện',
              'city_id' => 33,
          ),
          233 =>
          array (
              'id' => 333,
              'name' => 'Phù Cừ',
              'type' => 'Huyện',
              'city_id' => 33,
          ),
          234 =>
          array (
              'id' => 336,
              'name' => 'Thái Bình',
              'type' => 'Thành Phố',
              'city_id' => 34,
          ),
          235 =>
          array (
              'id' => 338,
              'name' => 'Quỳnh Phụ',
              'type' => 'Huyện',
              'city_id' => 34,
          ),
          236 =>
          array (
              'id' => 339,
              'name' => 'Hưng Hà',
              'type' => 'Huyện',
              'city_id' => 34,
          ),
          237 =>
          array (
              'id' => 340,
              'name' => 'Đông Hưng',
              'type' => 'Huyện',
              'city_id' => 34,
          ),
          238 =>
          array (
              'id' => 341,
              'name' => 'Thái Thụy',
              'type' => 'Huyện',
              'city_id' => 34,
          ),
          239 =>
          array (
              'id' => 342,
              'name' => 'Tiền Hải',
              'type' => 'Huyện',
              'city_id' => 34,
          ),
          240 =>
          array (
              'id' => 343,
              'name' => 'Kiến Xương',
              'type' => 'Huyện',
              'city_id' => 34,
          ),
          241 =>
          array (
              'id' => 344,
              'name' => 'Vũ Thư',
              'type' => 'Huyện',
              'city_id' => 34,
          ),
          242 =>
          array (
              'id' => 347,
              'name' => 'Phủ Lý',
              'type' => 'Thành Phố',
              'city_id' => 35,
          ),
          243 =>
          array (
              'id' => 349,
              'name' => 'Duy Tiên',
              'type' => 'Huyện',
              'city_id' => 35,
          ),
          244 =>
          array (
              'id' => 350,
              'name' => 'Kim Bảng',
              'type' => 'Huyện',
              'city_id' => 35,
          ),
          245 =>
          array (
              'id' => 351,
              'name' => 'Thanh Liêm',
              'type' => 'Huyện',
              'city_id' => 35,
          ),
          246 =>
          array (
              'id' => 352,
              'name' => 'Bình Lục',
              'type' => 'Huyện',
              'city_id' => 35,
          ),
          247 =>
          array (
              'id' => 353,
              'name' => 'Lý Nhân',
              'type' => 'Huyện',
              'city_id' => 35,
          ),
          248 =>
          array (
              'id' => 356,
              'name' => 'Nam Định',
              'type' => 'Thành Phố',
              'city_id' => 36,
          ),
          249 =>
          array (
              'id' => 358,
              'name' => 'Mỹ Lộc',
              'type' => 'Huyện',
              'city_id' => 36,
          ),
          250 =>
          array (
              'id' => 359,
              'name' => 'Vụ Bản',
              'type' => 'Huyện',
              'city_id' => 36,
          ),
          251 =>
          array (
              'id' => 360,
              'name' => 'Ý Yên',
              'type' => 'Huyện',
              'city_id' => 36,
          ),
          252 =>
          array (
              'id' => 361,
              'name' => 'Nghĩa Hưng',
              'type' => 'Huyện',
              'city_id' => 36,
          ),
          253 =>
          array (
              'id' => 362,
              'name' => 'Nam Trực',
              'type' => 'Huyện',
              'city_id' => 36,
          ),
          254 =>
          array (
              'id' => 363,
              'name' => 'Trực Ninh',
              'type' => 'Huyện',
              'city_id' => 36,
          ),
          255 =>
          array (
              'id' => 364,
              'name' => 'Xuân Trường',
              'type' => 'Huyện',
              'city_id' => 36,
          ),
          256 =>
          array (
              'id' => 365,
              'name' => 'Giao Thủy',
              'type' => 'Huyện',
              'city_id' => 36,
          ),
          257 =>
          array (
              'id' => 366,
              'name' => 'Hải Hậu',
              'type' => 'Huyện',
              'city_id' => 36,
          ),
          258 =>
          array (
              'id' => 369,
              'name' => 'Ninh Bình',
              'type' => 'Thành Phố',
              'city_id' => 37,
          ),
          259 =>
          array (
              'id' => 370,
              'name' => 'Tam Điệp',
              'type' => 'Thị Xã',
              'city_id' => 37,
          ),
          260 =>
          array (
              'id' => 372,
              'name' => 'Nho Quan',
              'type' => 'Huyện',
              'city_id' => 37,
          ),
          261 =>
          array (
              'id' => 373,
              'name' => 'Gia Viễn',
              'type' => 'Huyện',
              'city_id' => 37,
          ),
          262 =>
          array (
              'id' => 374,
              'name' => 'Hoa Lư',
              'type' => 'Huyện',
              'city_id' => 37,
          ),
          263 =>
          array (
              'id' => 375,
              'name' => 'Yên Khánh',
              'type' => 'Huyện',
              'city_id' => 37,
          ),
          264 =>
          array (
              'id' => 376,
              'name' => 'Kim Sơn',
              'type' => 'Huyện',
              'city_id' => 37,
          ),
          265 =>
          array (
              'id' => 377,
              'name' => 'Yên Mô',
              'type' => 'Huyện',
              'city_id' => 37,
          ),
          266 =>
          array (
              'id' => 380,
              'name' => 'Thanh Hóa',
              'type' => 'Thành Phố',
              'city_id' => 38,
          ),
          267 =>
          array (
              'id' => 381,
              'name' => 'Bỉm Sơn',
              'type' => 'Thị Xã',
              'city_id' => 38,
          ),
          268 =>
          array (
              'id' => 382,
              'name' => 'Sầm Sơn',
              'type' => 'Thị Xã',
              'city_id' => 38,
          ),
          269 =>
          array (
              'id' => 384,
              'name' => 'Mường Lát',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          270 =>
          array (
              'id' => 385,
              'name' => 'Quan Hóa',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          271 =>
          array (
              'id' => 386,
              'name' => 'Bá Thước',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          272 =>
          array (
              'id' => 387,
              'name' => 'Quan Sơn',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          273 =>
          array (
              'id' => 388,
              'name' => 'Lang Chánh',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          274 =>
          array (
              'id' => 389,
              'name' => 'Ngọc Lặc',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          275 =>
          array (
              'id' => 390,
              'name' => 'Cẩm Thủy',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          276 =>
          array (
              'id' => 391,
              'name' => 'Thạch Thành',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          277 =>
          array (
              'id' => 392,
              'name' => 'Hà Trung',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          278 =>
          array (
              'id' => 393,
              'name' => 'Vĩnh Lộc',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          279 =>
          array (
              'id' => 394,
              'name' => 'Yên Định',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          280 =>
          array (
              'id' => 395,
              'name' => 'Thọ Xuân',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          281 =>
          array (
              'id' => 396,
              'name' => 'Thường Xuân',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          282 =>
          array (
              'id' => 397,
              'name' => 'Triệu Sơn',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          283 =>
          array (
              'id' => 398,
              'name' => 'Thiệu Hoá',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          284 =>
          array (
              'id' => 399,
              'name' => 'Hoằng Hóa',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          285 =>
          array (
              'id' => 400,
              'name' => 'Hậu Lộc',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          286 =>
          array (
              'id' => 401,
              'name' => 'Nga Sơn',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          287 =>
          array (
              'id' => 402,
              'name' => 'Như Xuân',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          288 =>
          array (
              'id' => 403,
              'name' => 'Như Thanh',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          289 =>
          array (
              'id' => 404,
              'name' => 'Nông Cống',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          290 =>
          array (
              'id' => 405,
              'name' => 'Đông Sơn',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          291 =>
          array (
              'id' => 406,
              'name' => 'Quảng Xương',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          292 =>
          array (
              'id' => 407,
              'name' => 'Tĩnh Gia',
              'type' => 'Huyện',
              'city_id' => 38,
          ),
          293 =>
          array (
              'id' => 412,
              'name' => 'Vinh',
              'type' => 'Thành Phố',
              'city_id' => 40,
          ),
          294 =>
          array (
              'id' => 413,
              'name' => 'Hoàng Mai',
              'type' => 'Thị Xã',
              'city_id' => 40,
          ),
          295 =>
          array (
              'id' => 410,
              'name' => 'Thái Hòa',
              'type' => 'Thị Xã',
              'city_id' => 40,
          ),
          296 =>
          array (
              'id' => 415,
              'name' => 'Quế Phong',
              'type' => 'Huyện',
              'city_id' => 40,
          ),
          297 =>
          array (
              'id' => 416,
              'name' => 'Quỳ Châu',
              'type' => 'Huyện',
              'city_id' => 40,
          ),
          298 =>
          array (
              'id' => 417,
              'name' => 'Kỳ Sơn',
              'type' => 'Huyện',
              'city_id' => 40,
          ),
          299 =>
          array (
              'id' => 418,
              'name' => 'Tương Dương',
              'type' => 'Huyện',
              'city_id' => 40,
          ),
          300 =>
          array (
              'id' => 419,
              'name' => 'Nghĩa Đàn',
              'type' => 'Huyện',
              'city_id' => 40,
          ),
          301 =>
          array (
              'id' => 420,
              'name' => 'Quỳ Hợp',
              'type' => 'Huyện',
              'city_id' => 40,
          ),
          302 =>
          array (
              'id' => 421,
              'name' => 'Quỳnh Lưu',
              'type' => 'Huyện',
              'city_id' => 40,
          ),
          303 =>
          array (
              'id' => 422,
              'name' => 'Con Cuông',
              'type' => 'Huyện',
              'city_id' => 40,
          ),
          304 =>
          array (
              'id' => 423,
              'name' => 'Tân Kỳ',
              'type' => 'Huyện',
              'city_id' => 40,
          ),
          305 =>
          array (
              'id' => 424,
              'name' => 'Anh Sơn',
              'type' => 'Huyện',
              'city_id' => 40,
          ),
          306 =>
          array (
              'id' => 425,
              'name' => 'Diễn Châu',
              'type' => 'Huyện',
              'city_id' => 40,
          ),
          307 =>
          array (
              'id' => 426,
              'name' => 'Yên Thành',
              'type' => 'Huyện',
              'city_id' => 40,
          ),
          308 =>
          array (
              'id' => 427,
              'name' => 'Đô Lương',
              'type' => 'Huyện',
              'city_id' => 40,
          ),
          309 =>
          array (
              'id' => 428,
              'name' => 'Thanh Chương',
              'type' => 'Huyện',
              'city_id' => 40,
          ),
          310 =>
          array (
              'id' => 429,
              'name' => 'Nghi Lộc',
              'type' => 'Huyện',
              'city_id' => 40,
          ),
          311 =>
          array (
              'id' => 430,
              'name' => 'Nam Đàn',
              'type' => 'Huyện',
              'city_id' => 40,
          ),
          312 =>
          array (
              'id' => 431,
              'name' => 'Hưng Nguyên',
              'type' => 'Huyện',
              'city_id' => 40,
          ),
          313 =>
          array (
              'id' => 436,
              'name' => 'Hà Tĩnh',
              'type' => 'Thành Phố',
              'city_id' => 42,
          ),
          314 =>
          array (
              'id' => 437,
              'name' => 'Hồng Lĩnh',
              'type' => 'Thị Xã',
              'city_id' => 42,
          ),
          315 =>
          array (
              'id' => 439,
              'name' => 'Hương Sơn',
              'type' => 'Huyện',
              'city_id' => 42,
          ),
          316 =>
          array (
              'id' => 440,
              'name' => 'Đức Thọ',
              'type' => 'Huyện',
              'city_id' => 42,
          ),
          317 =>
          array (
              'id' => 441,
              'name' => 'Vũ Quang',
              'type' => 'Huyện',
              'city_id' => 42,
          ),
          318 =>
          array (
              'id' => 442,
              'name' => 'Nghi Xuân',
              'type' => 'Huyện',
              'city_id' => 42,
          ),
          319 =>
          array (
              'id' => 443,
              'name' => 'Can Lộc',
              'type' => 'Huyện',
              'city_id' => 42,
          ),
          320 =>
          array (
              'id' => 444,
              'name' => 'Hương Khê',
              'type' => 'Huyện',
              'city_id' => 42,
          ),
          321 =>
          array (
              'id' => 445,
              'name' => 'Thạch Hà',
              'type' => 'Huyện',
              'city_id' => 42,
          ),
          322 =>
          array (
              'id' => 446,
              'name' => 'Cẩm Xuyên',
              'type' => 'Huyện',
              'city_id' => 42,
          ),
          323 =>
          array (
              'id' => 447,
              'name' => 'Kỳ Anh',
              'type' => 'Huyện',
              'city_id' => 42,
          ),
          324 =>
          array (
              'id' => 448,
              'name' => 'Lộc Hà',
              'type' => 'Huyện',
              'city_id' => 42,
          ),
          325 =>
          array (
              'id' => 450,
              'name' => 'Đồng Hới',
              'type' => 'Thành Phố',
              'city_id' => 44,
          ),
          326 =>
          array (
              'id' => 452,
              'name' => 'Minh Hóa',
              'type' => 'Huyện',
              'city_id' => 44,
          ),
          327 =>
          array (
              'id' => 453,
              'name' => 'Tuyên Hóa',
              'type' => 'Huyện',
              'city_id' => 44,
          ),
          328 =>
          array (
              'id' => 454,
              'name' => 'Quảng Trạch',
              'type' => 'Huyện',
              'city_id' => 44,
          ),
          329 =>
          array (
              'id' => 455,
              'name' => 'Bố Trạch',
              'type' => 'Huyện',
              'city_id' => 44,
          ),
          330 =>
          array (
              'id' => 456,
              'name' => 'Quảng Ninh',
              'type' => 'Huyện',
              'city_id' => 44,
          ),
          331 =>
          array (
              'id' => 457,
              'name' => 'Lệ Thủy',
              'type' => 'Huyện',
              'city_id' => 44,
          ),
          332 =>
          array (
              'id' => 461,
              'name' => 'Đông Hà',
              'type' => 'Thành Phố',
              'city_id' => 45,
          ),
          333 =>
          array (
              'id' => 462,
              'name' => 'Quảng Trị',
              'type' => 'Thị Xã',
              'city_id' => 45,
          ),
          334 =>
          array (
              'id' => 464,
              'name' => 'Vĩnh Linh',
              'type' => 'Huyện',
              'city_id' => 45,
          ),
          335 =>
          array (
              'id' => 465,
              'name' => 'Hướng Hóa',
              'type' => 'Huyện',
              'city_id' => 45,
          ),
          336 =>
          array (
              'id' => 466,
              'name' => 'Gio Linh',
              'type' => 'Huyện',
              'city_id' => 45,
          ),
          337 =>
          array (
              'id' => 467,
              'name' => 'Đa Krông',
              'type' => 'Huyện',
              'city_id' => 45,
          ),
          338 =>
          array (
              'id' => 468,
              'name' => 'Cam Lộ',
              'type' => 'Huyện',
              'city_id' => 45,
          ),
          339 =>
          array (
              'id' => 469,
              'name' => 'Triệu Phong',
              'type' => 'Huyện',
              'city_id' => 45,
          ),
          340 =>
          array (
              'id' => 470,
              'name' => 'Hải Lăng',
              'type' => 'Huyện',
              'city_id' => 45,
          ),
          341 =>
          array (
              'id' => 471,
              'name' => 'Cồn Cỏ',
              'type' => 'Huyện',
              'city_id' => 45,
          ),
          342 =>
          array (
              'id' => 474,
              'name' => 'Huế',
              'type' => 'Thành Phố',
              'city_id' => 46,
          ),
          343 =>
          array (
              'id' => 476,
              'name' => 'Phong Điền',
              'type' => 'Huyện',
              'city_id' => 46,
          ),
          344 =>
          array (
              'id' => 477,
              'name' => 'Quảng Điền',
              'type' => 'Huyện',
              'city_id' => 46,
          ),
          345 =>
          array (
              'id' => 478,
              'name' => 'Phú Vang',
              'type' => 'Huyện',
              'city_id' => 46,
          ),
          346 =>
          array (
              'id' => 479,
              'name' => 'Hương Thủy',
              'type' => 'Huyện',
              'city_id' => 46,
          ),
          347 =>
          array (
              'id' => 480,
              'name' => 'Hương Trà',
              'type' => 'Huyện',
              'city_id' => 46,
          ),
          348 =>
          array (
              'id' => 481,
              'name' => 'A Lưới',
              'type' => 'Huyện',
              'city_id' => 46,
          ),
          349 =>
          array (
              'id' => 482,
              'name' => 'Phú Lộc',
              'type' => 'Huyện',
              'city_id' => 46,
          ),
          350 =>
          array (
              'id' => 483,
              'name' => 'Nam Đông',
              'type' => 'Huyện',
              'city_id' => 46,
          ),
          351 =>
          array (
              'id' => 490,
              'name' => 'Liên Chiểu',
              'type' => 'Quận',
              'city_id' => 48,
          ),
          352 =>
          array (
              'id' => 491,
              'name' => 'Thanh Khê',
              'type' => 'Quận',
              'city_id' => 48,
          ),
          353 =>
          array (
              'id' => 492,
              'name' => 'Hải Châu',
              'type' => 'Quận',
              'city_id' => 48,
          ),
          354 =>
          array (
              'id' => 493,
              'name' => 'Sơn Trà',
              'type' => 'Quận',
              'city_id' => 48,
          ),
          355 =>
          array (
              'id' => 494,
              'name' => 'Ngũ Hành Sơn',
              'type' => 'Quận',
              'city_id' => 48,
          ),
          356 =>
          array (
              'id' => 495,
              'name' => 'Cẩm Lệ',
              'type' => 'Quận',
              'city_id' => 48,
          ),
          357 =>
          array (
              'id' => 497,
              'name' => 'Hoà Vang',
              'type' => 'Huyện',
              'city_id' => 48,
          ),
          358 =>
          array (
              'id' => 498,
              'name' => 'Hoàng Sa',
              'type' => 'Huyện',
              'city_id' => 48,
          ),
          359 =>
          array (
              'id' => 502,
              'name' => 'Tam Kỳ',
              'type' => 'Thành Phố',
              'city_id' => 49,
          ),
          360 =>
          array (
              'id' => 503,
              'name' => 'Hội An',
              'type' => 'Thành Phố',
              'city_id' => 49,
          ),
          361 =>
          array (
              'id' => 504,
              'name' => 'Tây Giang',
              'type' => 'Huyện',
              'city_id' => 49,
          ),
          362 =>
          array (
              'id' => 505,
              'name' => 'Đông Giang',
              'type' => 'Huyện',
              'city_id' => 49,
          ),
          363 =>
          array (
              'id' => 506,
              'name' => 'Đại Lộc',
              'type' => 'Huyện',
              'city_id' => 49,
          ),
          364 =>
          array (
              'id' => 507,
              'name' => 'Điện Bàn',
              'type' => 'Thị Xã',
              'city_id' => 49,
          ),
          365 =>
          array (
              'id' => 508,
              'name' => 'Duy Xuyên',
              'type' => 'Huyện',
              'city_id' => 49,
          ),
          366 =>
          array (
              'id' => 509,
              'name' => 'Quế Sơn',
              'type' => 'Huyện',
              'city_id' => 49,
          ),
          367 =>
          array (
              'id' => 510,
              'name' => 'Nam Giang',
              'type' => 'Huyện',
              'city_id' => 49,
          ),
          368 =>
          array (
              'id' => 511,
              'name' => 'Phước Sơn',
              'type' => 'Huyện',
              'city_id' => 49,
          ),
          369 =>
          array (
              'id' => 512,
              'name' => 'Hiệp Đức',
              'type' => 'Huyện',
              'city_id' => 49,
          ),
          370 =>
          array (
              'id' => 513,
              'name' => 'Thăng Bình',
              'type' => 'Huyện',
              'city_id' => 49,
          ),
          371 =>
          array (
              'id' => 514,
              'name' => 'Tiên Phước',
              'type' => 'Huyện',
              'city_id' => 49,
          ),
          372 =>
          array (
              'id' => 515,
              'name' => 'Bắc Trà My',
              'type' => 'Huyện',
              'city_id' => 49,
          ),
          373 =>
          array (
              'id' => 516,
              'name' => 'Nam Trà My',
              'type' => 'Huyện',
              'city_id' => 49,
          ),
          374 =>
          array (
              'id' => 517,
              'name' => 'Núi Thành',
              'type' => 'Huyện',
              'city_id' => 49,
          ),
          375 =>
          array (
              'id' => 518,
              'name' => 'Phú Ninh',
              'type' => 'Huyện',
              'city_id' => 49,
          ),
          376 =>
          array (
              'id' => 519,
              'name' => 'Nông Sơn',
              'type' => 'Huyện',
              'city_id' => 49,
          ),
          377 =>
          array (
              'id' => 522,
              'name' => 'Quảng Ngãi',
              'type' => 'Thành Phố',
              'city_id' => 51,
          ),
          378 =>
          array (
              'id' => 524,
              'name' => 'Bình Sơn',
              'type' => 'Huyện',
              'city_id' => 51,
          ),
          379 =>
          array (
              'id' => 525,
              'name' => 'Trà Bồng',
              'type' => 'Huyện',
              'city_id' => 51,
          ),
          380 =>
          array (
              'id' => 526,
              'name' => 'Tây Trà',
              'type' => 'Huyện',
              'city_id' => 51,
          ),
          381 =>
          array (
              'id' => 527,
              'name' => 'Sơn Tịnh',
              'type' => 'Huyện',
              'city_id' => 51,
          ),
          382 =>
          array (
              'id' => 528,
              'name' => 'Tư Nghĩa',
              'type' => 'Huyện',
              'city_id' => 51,
          ),
          383 =>
          array (
              'id' => 529,
              'name' => 'Sơn Hà',
              'type' => 'Huyện',
              'city_id' => 51,
          ),
          384 =>
          array (
              'id' => 530,
              'name' => 'Sơn Tây',
              'type' => 'Huyện',
              'city_id' => 51,
          ),
          385 =>
          array (
              'id' => 531,
              'name' => 'Minh Long',
              'type' => 'Huyện',
              'city_id' => 51,
          ),
          386 =>
          array (
              'id' => 532,
              'name' => 'Nghĩa Hành',
              'type' => 'Huyện',
              'city_id' => 51,
          ),
          387 =>
          array (
              'id' => 533,
              'name' => 'Mộ Đức',
              'type' => 'Huyện',
              'city_id' => 51,
          ),
          388 =>
          array (
              'id' => 534,
              'name' => 'Đức Phổ',
              'type' => 'Huyện',
              'city_id' => 51,
          ),
          389 =>
          array (
              'id' => 535,
              'name' => 'Ba Tơ',
              'type' => 'Huyện',
              'city_id' => 51,
          ),
          390 =>
          array (
              'id' => 536,
              'name' => 'Lý Sơn',
              'type' => 'Huyện',
              'city_id' => 51,
          ),
          391 =>
          array (
              'id' => 540,
              'name' => 'Qui Nhơn',
              'type' => 'Thành Phố',
              'city_id' => 52,
          ),
          392 =>
          array (
              'id' => 542,
              'name' => 'An Lão',
              'type' => 'Huyện',
              'city_id' => 52,
          ),
          393 =>
          array (
              'id' => 543,
              'name' => 'Hoài Nhơn',
              'type' => 'Huyện',
              'city_id' => 52,
          ),
          394 =>
          array (
              'id' => 544,
              'name' => 'Hoài Ân',
              'type' => 'Huyện',
              'city_id' => 52,
          ),
          395 =>
          array (
              'id' => 545,
              'name' => 'Phù Mỹ',
              'type' => 'Huyện',
              'city_id' => 52,
          ),
          396 =>
          array (
              'id' => 546,
              'name' => 'Vĩnh Thạnh',
              'type' => 'Huyện',
              'city_id' => 52,
          ),
          397 =>
          array (
              'id' => 547,
              'name' => 'Tây Sơn',
              'type' => 'Huyện',
              'city_id' => 52,
          ),
          398 =>
          array (
              'id' => 548,
              'name' => 'Phù Cát',
              'type' => 'Huyện',
              'city_id' => 52,
          ),
          399 =>
          array (
              'id' => 549,
              'name' => 'An Nhơn',
              'type' => 'Huyện',
              'city_id' => 52,
          ),
          400 =>
          array (
              'id' => 550,
              'name' => 'Tuy Phước',
              'type' => 'Huyện',
              'city_id' => 52,
          ),
          401 =>
          array (
              'id' => 551,
              'name' => 'Vân Canh',
              'type' => 'Huyện',
              'city_id' => 52,
          ),
          402 =>
          array (
              'id' => 555,
              'name' => 'Tuy Hòa',
              'type' => 'Thành Phố',
              'city_id' => 54,
          ),
          403 =>
          array (
              'id' => 557,
              'name' => 'Sông Cầu',
              'type' => 'Thị Xã',
              'city_id' => 54,
          ),
          404 =>
          array (
              'id' => 558,
              'name' => 'Đồng Xuân',
              'type' => 'Huyện',
              'city_id' => 54,
          ),
          405 =>
          array (
              'id' => 559,
              'name' => 'Tuy An',
              'type' => 'Huyện',
              'city_id' => 54,
          ),
          406 =>
          array (
              'id' => 560,
              'name' => 'Sơn Hòa',
              'type' => 'Huyện',
              'city_id' => 54,
          ),
          407 =>
          array (
              'id' => 561,
              'name' => 'Sông Hinh',
              'type' => 'Huyện',
              'city_id' => 54,
          ),
          408 =>
          array (
              'id' => 562,
              'name' => 'Tây Hoà',
              'type' => 'Huyện',
              'city_id' => 54,
          ),
          409 =>
          array (
              'id' => 563,
              'name' => 'Phú Hoà',
              'type' => 'Huyện',
              'city_id' => 54,
          ),
          410 =>
          array (
              'id' => 564,
              'name' => 'Đông Hoà',
              'type' => 'Huyện',
              'city_id' => 54,
          ),
          411 =>
          array (
              'id' => 568,
              'name' => 'Nha Trang',
              'type' => 'Thành Phố',
              'city_id' => 56,
          ),
          412 =>
          array (
              'id' => 569,
              'name' => 'Cam Ranh',
              'type' => 'Thành Phố',
              'city_id' => 56,
          ),
          413 =>
          array (
              'id' => 570,
              'name' => 'Cam Lâm',
              'type' => 'Huyện',
              'city_id' => 56,
          ),
          414 =>
          array (
              'id' => 571,
              'name' => 'Vạn Ninh',
              'type' => 'Huyện',
              'city_id' => 56,
          ),
          415 =>
          array (
              'id' => 572,
              'name' => 'Ninh Hòa',
              'type' => 'Thị Xã',
              'city_id' => 56,
          ),
          416 =>
          array (
              'id' => 573,
              'name' => 'Khánh Vĩnh',
              'type' => 'Huyện',
              'city_id' => 56,
          ),
          417 =>
          array (
              'id' => 574,
              'name' => 'Diên Khánh',
              'type' => 'Huyện',
              'city_id' => 56,
          ),
          418 =>
          array (
              'id' => 575,
              'name' => 'Khánh Sơn',
              'type' => 'Huyện',
              'city_id' => 56,
          ),
          419 =>
          array (
              'id' => 576,
              'name' => 'Trường Sa',
              'type' => 'Huyện',
              'city_id' => 56,
          ),
          420 =>
          array (
              'id' => 582,
              'name' => 'Phan Rang-Tháp Chàm',
              'type' => 'Thành Phố',
              'city_id' => 58,
          ),
          421 =>
          array (
              'id' => 584,
              'name' => 'Bác Ái',
              'type' => 'Huyện',
              'city_id' => 58,
          ),
          422 =>
          array (
              'id' => 585,
              'name' => 'Ninh Sơn',
              'type' => 'Huyện',
              'city_id' => 58,
          ),
          423 =>
          array (
              'id' => 586,
              'name' => 'Ninh Hải',
              'type' => 'Huyện',
              'city_id' => 58,
          ),
          424 =>
          array (
              'id' => 587,
              'name' => 'Ninh Phước',
              'type' => 'Huyện',
              'city_id' => 58,
          ),
          425 =>
          array (
              'id' => 588,
              'name' => 'Thuận Bắc',
              'type' => 'Huyện',
              'city_id' => 58,
          ),
          426 =>
          array (
              'id' => 589,
              'name' => 'Thuận Nam',
              'type' => 'Huyện',
              'city_id' => 58,
          ),
          427 =>
          array (
              'id' => 593,
              'name' => 'Phan Thiết',
              'type' => 'Thành Phố',
              'city_id' => 60,
          ),
          428 =>
          array (
              'id' => 594,
              'name' => 'La Gi',
              'type' => 'Thị Xã',
              'city_id' => 60,
          ),
          429 =>
          array (
              'id' => 595,
              'name' => 'Tuy Phong',
              'type' => 'Huyện',
              'city_id' => 60,
          ),
          430 =>
          array (
              'id' => 596,
              'name' => 'Bắc Bình',
              'type' => 'Huyện',
              'city_id' => 60,
          ),
          431 =>
          array (
              'id' => 597,
              'name' => 'Hàm Thuận Bắc',
              'type' => 'Huyện',
              'city_id' => 60,
          ),
          432 =>
          array (
              'id' => 598,
              'name' => 'Hàm Thuận Nam',
              'type' => 'Huyện',
              'city_id' => 60,
          ),
          433 =>
          array (
              'id' => 599,
              'name' => 'Tánh Linh',
              'type' => 'Huyện',
              'city_id' => 60,
          ),
          434 =>
          array (
              'id' => 600,
              'name' => 'Đức Linh',
              'type' => 'Huyện',
              'city_id' => 60,
          ),
          435 =>
          array (
              'id' => 601,
              'name' => 'Hàm Tân',
              'type' => 'Huyện',
              'city_id' => 60,
          ),
          436 =>
          array (
              'id' => 602,
              'name' => 'Phú Quí',
              'type' => 'Huyện',
              'city_id' => 60,
          ),
          437 =>
          array (
              'id' => 608,
              'name' => 'Kon Tum',
              'type' => 'Thành Phố',
              'city_id' => 62,
          ),
          438 =>
          array (
              'id' => 610,
              'name' => 'Đắk Glei',
              'type' => 'Huyện',
              'city_id' => 62,
          ),
          439 =>
          array (
              'id' => 611,
              'name' => 'Ngọc Hồi',
              'type' => 'Huyện',
              'city_id' => 62,
          ),
          440 =>
          array (
              'id' => 612,
              'name' => 'Đắk Tô',
              'type' => 'Huyện',
              'city_id' => 62,
          ),
          441 =>
          array (
              'id' => 613,
              'name' => 'Kon Plông',
              'type' => 'Huyện',
              'city_id' => 62,
          ),
          442 =>
          array (
              'id' => 614,
              'name' => 'Kon Rẫy',
              'type' => 'Huyện',
              'city_id' => 62,
          ),
          443 =>
          array (
              'id' => 615,
              'name' => 'Đắk Hà',
              'type' => 'Huyện',
              'city_id' => 62,
          ),
          444 =>
          array (
              'id' => 616,
              'name' => 'Sa Thầy',
              'type' => 'Huyện',
              'city_id' => 62,
          ),
          445 =>
          array (
              'id' => 617,
              'name' => 'Tu Mơ Rông',
              'type' => 'Huyện',
              'city_id' => 62,
          ),
          446 =>
          array (
              'id' => 622,
              'name' => 'Pleiku',
              'type' => 'Thành Phố',
              'city_id' => 64,
          ),
          447 =>
          array (
              'id' => 623,
              'name' => 'An Khê',
              'type' => 'Thị Xã',
              'city_id' => 64,
          ),
          448 =>
          array (
              'id' => 624,
              'name' => 'Ayun Pa',
              'type' => 'Thị Xã',
              'city_id' => 64,
          ),
          449 =>
          array (
              'id' => 625,
              'name' => 'Kbang',
              'type' => 'Huyện',
              'city_id' => 64,
          ),
          450 =>
          array (
              'id' => 626,
              'name' => 'Đăk Đoa',
              'type' => 'Huyện',
              'city_id' => 64,
          ),
          451 =>
          array (
              'id' => 627,
              'name' => 'Chư Păh',
              'type' => 'Huyện',
              'city_id' => 64,
          ),
          452 =>
          array (
              'id' => 628,
              'name' => 'Ia Grai',
              'type' => 'Huyện',
              'city_id' => 64,
          ),
          453 =>
          array (
              'id' => 629,
              'name' => 'Mang Yang',
              'type' => 'Huyện',
              'city_id' => 64,
          ),
          454 =>
          array (
              'id' => 630,
              'name' => 'Kông Chro',
              'type' => 'Huyện',
              'city_id' => 64,
          ),
          455 =>
          array (
              'id' => 631,
              'name' => 'Đức Cơ',
              'type' => 'Huyện',
              'city_id' => 64,
          ),
          456 =>
          array (
              'id' => 632,
              'name' => 'Chư Prông',
              'type' => 'Huyện',
              'city_id' => 64,
          ),
          457 =>
          array (
              'id' => 633,
              'name' => 'Chư Sê',
              'type' => 'Huyện',
              'city_id' => 64,
          ),
          458 =>
          array (
              'id' => 634,
              'name' => 'Đăk Pơ',
              'type' => 'Huyện',
              'city_id' => 64,
          ),
          459 =>
          array (
              'id' => 635,
              'name' => 'Ia Pa',
              'type' => 'Huyện',
              'city_id' => 64,
          ),
          460 =>
          array (
              'id' => 637,
              'name' => 'Krông Pa',
              'type' => 'Huyện',
              'city_id' => 64,
          ),
          461 =>
          array (
              'id' => 638,
              'name' => 'Phú Thiện',
              'type' => 'Huyện',
              'city_id' => 64,
          ),
          462 =>
          array (
              'id' => 639,
              'name' => 'Chư Pưh',
              'type' => 'Huyện',
              'city_id' => 64,
          ),
          463 =>
          array (
              'id' => 643,
              'name' => 'Buôn Ma Thuột',
              'type' => 'Thành Phố',
              'city_id' => 66,
          ),
          464 =>
          array (
              'id' => 644,
              'name' => 'Buôn Hồ',
              'type' => 'Thị Xã',
              'city_id' => 66,
          ),
          465 =>
          array (
              'id' => 645,
              'name' => 'Ea H\'leo',
              'type' => 'Huyện',
              'city_id' => 66,
          ),
          466 =>
          array (
              'id' => 646,
              'name' => 'Ea Súp',
              'type' => 'Huyện',
              'city_id' => 66,
          ),
          467 =>
          array (
              'id' => 647,
              'name' => 'Buôn Đôn',
              'type' => 'Huyện',
              'city_id' => 66,
          ),
          468 =>
          array (
              'id' => 648,
              'name' => 'Cư M\'gar',
              'type' => 'Huyện',
              'city_id' => 66,
          ),
          469 =>
          array (
              'id' => 649,
              'name' => 'Krông Búk',
              'type' => 'Huyện',
              'city_id' => 66,
          ),
          470 =>
          array (
              'id' => 650,
              'name' => 'Krông Năng',
              'type' => 'Huyện',
              'city_id' => 66,
          ),
          471 =>
          array (
              'id' => 651,
              'name' => 'Ea Kar',
              'type' => 'Huyện',
              'city_id' => 66,
          ),
          472 =>
          array (
              'id' => 652,
              'name' => 'M\'đrắk',
              'type' => 'Huyện',
              'city_id' => 66,
          ),
          473 =>
          array (
              'id' => 653,
              'name' => 'Krông Bông',
              'type' => 'Huyện',
              'city_id' => 66,
          ),
          474 =>
          array (
              'id' => 654,
              'name' => 'Krông Pắc',
              'type' => 'Huyện',
              'city_id' => 66,
          ),
          475 =>
          array (
              'id' => 655,
              'name' => 'Krông A Na',
              'type' => 'Huyện',
              'city_id' => 66,
          ),
          476 =>
          array (
              'id' => 656,
              'name' => 'Lắk',
              'type' => 'Huyện',
              'city_id' => 66,
          ),
          477 =>
          array (
              'id' => 657,
              'name' => 'Cư Kuin',
              'type' => 'Huyện',
              'city_id' => 66,
          ),
          478 =>
          array (
              'id' => 660,
              'name' => 'Gia Nghĩa',
              'type' => 'Thị Xã',
              'city_id' => 67,
          ),
          479 =>
          array (
              'id' => 661,
              'name' => 'Đắk Glong',
              'type' => 'Huyện',
              'city_id' => 67,
          ),
          480 =>
          array (
              'id' => 662,
              'name' => 'Cư Jút',
              'type' => 'Huyện',
              'city_id' => 67,
          ),
          481 =>
          array (
              'id' => 663,
              'name' => 'Đắk Mil',
              'type' => 'Huyện',
              'city_id' => 67,
          ),
          482 =>
          array (
              'id' => 664,
              'name' => 'Krông Nô',
              'type' => 'Huyện',
              'city_id' => 67,
          ),
          483 =>
          array (
              'id' => 665,
              'name' => 'Đắk Song',
              'type' => 'Huyện',
              'city_id' => 67,
          ),
          484 =>
          array (
              'id' => 666,
              'name' => 'Đắk R\'lấp',
              'type' => 'Huyện',
              'city_id' => 67,
          ),
          485 =>
          array (
              'id' => 667,
              'name' => 'Tuy Đức',
              'type' => 'Huyện',
              'city_id' => 67,
          ),
          486 =>
          array (
              'id' => 672,
              'name' => 'Đà Lạt',
              'type' => 'Thành Phố',
              'city_id' => 68,
          ),
          487 =>
          array (
              'id' => 673,
              'name' => 'Bảo Lộc',
              'type' => 'Thị Xã',
              'city_id' => 68,
          ),
          488 =>
          array (
              'id' => 674,
              'name' => 'Đam Rông',
              'type' => 'Huyện',
              'city_id' => 68,
          ),
          489 =>
          array (
              'id' => 675,
              'name' => 'Lạc Dương',
              'type' => 'Huyện',
              'city_id' => 68,
          ),
          490 =>
          array (
              'id' => 676,
              'name' => 'Lâm Hà',
              'type' => 'Huyện',
              'city_id' => 68,
          ),
          491 =>
          array (
              'id' => 677,
              'name' => 'Đơn Dương',
              'type' => 'Huyện',
              'city_id' => 68,
          ),
          492 =>
          array (
              'id' => 678,
              'name' => 'Đức Trọng',
              'type' => 'Huyện',
              'city_id' => 68,
          ),
          493 =>
          array (
              'id' => 679,
              'name' => 'Di Linh',
              'type' => 'Huyện',
              'city_id' => 68,
          ),
          494 =>
          array (
              'id' => 680,
              'name' => 'Bảo Lâm',
              'type' => 'Huyện',
              'city_id' => 68,
          ),
          495 =>
          array (
              'id' => 681,
              'name' => 'Đạ Huoai',
              'type' => 'Huyện',
              'city_id' => 68,
          ),
          496 =>
          array (
              'id' => 682,
              'name' => 'Đạ Tẻh',
              'type' => 'Huyện',
              'city_id' => 68,
          ),
          497 =>
          array (
              'id' => 683,
              'name' => 'Cát Tiên',
              'type' => 'Huyện',
              'city_id' => 68,
          ),
          498 =>
          array (
              'id' => 688,
              'name' => 'Phước Long',
              'type' => 'Thị Xã',
              'city_id' => 70,
          ),
          499 =>
          array (
              'id' => 689,
              'name' => 'Đồng Xoài',
              'type' => 'Thị Xã',
              'city_id' => 70,
          ),
      ));
      \DB::table('districts')->insert(array (
          0 =>
          array (
              'id' => 690,
              'name' => 'Bình Long',
              'type' => 'Thị Xã',
              'city_id' => 70,
          ),
          1 =>
          array (
              'id' => 691,
              'name' => 'Bù Gia Mập',
              'type' => 'Huyện',
              'city_id' => 70,
          ),
          2 =>
          array (
              'id' => 692,
              'name' => 'Lộc Ninh',
              'type' => 'Huyện',
              'city_id' => 70,
          ),
          3 =>
          array (
              'id' => 693,
              'name' => 'Bù Đốp',
              'type' => 'Huyện',
              'city_id' => 70,
          ),
          4 =>
          array (
              'id' => 694,
              'name' => 'Hớn Quản',
              'type' => 'Huyện',
              'city_id' => 70,
          ),
          5 =>
          array (
              'id' => 695,
              'name' => 'Đồng Phú',
              'type' => 'Huyện',
              'city_id' => 70,
          ),
          6 =>
          array (
              'id' => 696,
              'name' => 'Bù Đăng',
              'type' => 'Huyện',
              'city_id' => 70,
          ),
          7 =>
          array (
              'id' => 697,
              'name' => 'Chơn Thành',
              'type' => 'Huyện',
              'city_id' => 70,
          ),
          8 =>
          array (
              'id' => 703,
              'name' => 'Tây Ninh',
              'type' => 'Thị Xã',
              'city_id' => 72,
          ),
          9 =>
          array (
              'id' => 705,
              'name' => 'Tân Biên',
              'type' => 'Huyện',
              'city_id' => 72,
          ),
          10 =>
          array (
              'id' => 706,
              'name' => 'Tân Châu',
              'type' => 'Huyện',
              'city_id' => 72,
          ),
          11 =>
          array (
              'id' => 707,
              'name' => 'Dương Minh Châu',
              'type' => 'Huyện',
              'city_id' => 72,
          ),
          12 =>
          array (
              'id' => 708,
              'name' => 'Châu Thành',
              'type' => 'Huyện',
              'city_id' => 72,
          ),
          13 =>
          array (
              'id' => 709,
              'name' => 'Hòa Thành',
              'type' => 'Huyện',
              'city_id' => 72,
          ),
          14 =>
          array (
              'id' => 710,
              'name' => 'Gò Dầu',
              'type' => 'Huyện',
              'city_id' => 72,
          ),
          15 =>
          array (
              'id' => 711,
              'name' => 'Bến Cầu',
              'type' => 'Huyện',
              'city_id' => 72,
          ),
          16 =>
          array (
              'id' => 712,
              'name' => 'Trảng Bàng',
              'type' => 'Huyện',
              'city_id' => 72,
          ),
          17 =>
          array (
              'id' => 718,
              'name' => 'Thủ Dầu Một',
              'type' => 'Thành Phố',
              'city_id' => 74,
          ),
          18 =>
          array (
              'id' => 720,
              'name' => 'Dầu Tiếng',
              'type' => 'Huyện',
              'city_id' => 74,
          ),
          19 =>
          array (
              'id' => 721,
              'name' => 'Bến Cát',
              'type' => 'Thị Xã',
              'city_id' => 74,
          ),
          20 =>
          array (
              'id' => 722,
              'name' => 'Phú Giáo',
              'type' => 'Huyện',
              'city_id' => 74,
          ),
          21 =>
          array (
              'id' => 723,
              'name' => 'Tân Uyên',
              'type' => 'Thị Xã',
              'city_id' => 74,
          ),
          22 =>
          array (
              'id' => 724,
              'name' => 'Dĩ An',
              'type' => 'Thị Xã',
              'city_id' => 74,
          ),
          23 =>
          array (
              'id' => 725,
              'name' => 'Thuận An',
              'type' => 'Thị Xã',
              'city_id' => 74,
          ),
          2300 =>
          array (
              'id' => 726,
              'name' => 'Bàu Bàng',
              'type' => 'Huyện',
              'city_id' => 74,
          ),
          231 =>
          array (
              'id' => 727,
              'name' => 'Bắc Tân Uyên',
              'type' => 'Huyện',
              'city_id' => 74,
          ),
          24 =>
          array (
              'id' => 731,
              'name' => 'Biên Hòa',
              'type' => 'Thành Phố',
              'city_id' => 75,
          ),
          25 =>
          array (
              'id' => 732,
              'name' => 'Long Khánh',
              'type' => 'Thị Xã',
              'city_id' => 75,
          ),
          26 =>
          array (
              'id' => 734,
              'name' => 'Tân Phú',
              'type' => 'Huyện',
              'city_id' => 75,
          ),
          27 =>
          array (
              'id' => 735,
              'name' => 'Vĩnh Cửu',
              'type' => 'Huyện',
              'city_id' => 75,
          ),
          28 =>
          array (
              'id' => 736,
              'name' => 'Định Quán',
              'type' => 'Huyện',
              'city_id' => 75,
          ),
          29 =>
          array (
              'id' => 737,
              'name' => 'Trảng Bom',
              'type' => 'Huyện',
              'city_id' => 75,
          ),
          30 =>
          array (
              'id' => 738,
              'name' => 'Thống Nhất',
              'type' => 'Huyện',
              'city_id' => 75,
          ),
          31 =>
          array (
              'id' => 739,
              'name' => 'Cẩm Mỹ',
              'type' => 'Huyện',
              'city_id' => 75,
          ),
          32 =>
          array (
              'id' => 740,
              'name' => 'Long Thành',
              'type' => 'Huyện',
              'city_id' => 75,
          ),
          33 =>
          array (
              'id' => 741,
              'name' => 'Xuân Lộc',
              'type' => 'Huyện',
              'city_id' => 75,
          ),
          34 =>
          array (
              'id' => 742,
              'name' => 'Nhơn Trạch',
              'type' => 'Huyện',
              'city_id' => 75,
          ),
          35 =>
          array (
              'id' => 747,
              'name' => 'Vũng Tầu',
              'type' => 'Thành Phố',
              'city_id' => 77,
          ),
          36 =>
          array (
              'id' => 748,
              'name' => 'Bà Rịa',
              'type' => 'Thị Xã',
              'city_id' => 77,
          ),
          37 =>
          array (
              'id' => 750,
              'name' => 'Châu Đức',
              'type' => 'Huyện',
              'city_id' => 77,
          ),
          38 =>
          array (
              'id' => 751,
              'name' => 'Xuyên Mộc',
              'type' => 'Huyện',
              'city_id' => 77,
          ),
          39 =>
          array (
              'id' => 752,
              'name' => 'Long Điền',
              'type' => 'Huyện',
              'city_id' => 77,
          ),
          40 =>
          array (
              'id' => 753,
              'name' => 'Đất Đỏ',
              'type' => 'Huyện',
              'city_id' => 77,
          ),
          41 =>
          array (
              'id' => 754,
              'name' => 'Tân Thành',
              'type' => 'Huyện',
              'city_id' => 77,
          ),
          42 =>
          array (
              'id' => 755,
              'name' => 'Côn Đảo',
              'type' => 'Huyện',
              'city_id' => 77,
          ),
          43 =>
          array (
              'id' => 760,
              'name' => '1',
              'type' => 'Quận',
              'city_id' => 79,
          ),
          44 =>
          array (
              'id' => 761,
              'name' => '12',
              'type' => 'Quận',
              'city_id' => 79,
          ),
          45 =>
          array (
              'id' => 762,
              'name' => 'Thủ Đức',
              'type' => 'Quận',
              'city_id' => 79,
          ),
          46 =>
          array (
              'id' => 763,
              'name' => '9',
              'type' => 'Quận',
              'city_id' => 79,
          ),
          47 =>
          array (
              'id' => 764,
              'name' => 'Gò Vấp',
              'type' => 'Quận',
              'city_id' => 79,
          ),
          48 =>
          array (
              'id' => 765,
              'name' => 'Bình Thạnh',
              'type' => 'Quận',
              'city_id' => 79,
          ),
          49 =>
          array (
              'id' => 766,
              'name' => 'Tân Bình',
              'type' => 'Quận',
              'city_id' => 79,
          ),
          50 =>
          array (
              'id' => 767,
              'name' => 'Tân Phú',
              'type' => 'Quận',
              'city_id' => 79,
          ),
          51 =>
          array (
              'id' => 768,
              'name' => 'Phú Nhuận',
              'type' => 'Quận',
              'city_id' => 79,
          ),
          52 =>
          array (
              'id' => 769,
              'name' => '2',
              'type' => 'Quận',
              'city_id' => 79,
          ),
          53 =>
          array (
              'id' => 770,
              'name' => '3',
              'type' => 'Quận',
              'city_id' => 79,
          ),
          54 =>
          array (
              'id' => 771,
              'name' => '10',
              'type' => 'Quận',
              'city_id' => 79,
          ),
          55 =>
          array (
              'id' => 772,
              'name' => '11',
              'type' => 'Quận',
              'city_id' => 79,
          ),
          56 =>
          array (
              'id' => 773,
              'name' => '4',
              'type' => 'Quận',
              'city_id' => 79,
          ),
          57 =>
          array (
              'id' => 774,
              'name' => '5',
              'type' => 'Quận',
              'city_id' => 79,
          ),
          58 =>
          array (
              'id' => 775,
              'name' => '6',
              'type' => 'Quận',
              'city_id' => 79,
          ),
          59 =>
          array (
              'id' => 776,
              'name' => '8',
              'type' => 'Quận',
              'city_id' => 79,
          ),
          60 =>
          array (
              'id' => 777,
              'name' => 'Bình Tân',
              'type' => 'Quận',
              'city_id' => 79,
          ),
          61 =>
          array (
              'id' => 778,
              'name' => '7',
              'type' => 'Quận',
              'city_id' => 79,
          ),
          62 =>
          array (
              'id' => 783,
              'name' => 'Củ Chi',
              'type' => 'Huyện',
              'city_id' => 79,
          ),
          63 =>
          array (
              'id' => 784,
              'name' => 'Hóc Môn',
              'type' => 'Huyện',
              'city_id' => 79,
          ),
          64 =>
          array (
              'id' => 785,
              'name' => 'Bình Chánh',
              'type' => 'Huyện',
              'city_id' => 79,
          ),
          65 =>
          array (
              'id' => 786,
              'name' => 'Nhà Bè',
              'type' => 'Huyện',
              'city_id' => 79,
          ),
          66 =>
          array (
              'id' => 787,
              'name' => 'Cần Giờ',
              'type' => 'Huyện',
              'city_id' => 79,
          ),
          67 =>
          array (
              'id' => 794,
              'name' => 'Tân An',
              'type' => 'Thành Phố',
              'city_id' => 80,
          ),
          6700 =>
          array (
              'id' => 795,
              'name' => 'Kiến Tường',
              'type' => 'Thị Xã',
              'city_id' => 80,
          ),
          68 =>
          array (
              'id' => 796,
              'name' => 'Tân Hưng',
              'type' => 'Huyện',
              'city_id' => 80,
          ),
          69 =>
          array (
              'id' => 797,
              'name' => 'Vĩnh Hưng',
              'type' => 'Huyện',
              'city_id' => 80,
          ),
          70 =>
          array (
              'id' => 798,
              'name' => 'Mộc Hóa',
              'type' => 'Huyện',
              'city_id' => 80,
          ),
          71 =>
          array (
              'id' => 799,
              'name' => 'Tân Thạnh',
              'type' => 'Huyện',
              'city_id' => 80,
          ),
          72 =>
          array (
              'id' => 800,
              'name' => 'Thạnh Hóa',
              'type' => 'Huyện',
              'city_id' => 80,
          ),
          73 =>
          array (
              'id' => 801,
              'name' => 'Đức Huệ',
              'type' => 'Huyện',
              'city_id' => 80,
          ),
          74 =>
          array (
              'id' => 802,
              'name' => 'Đức Hòa',
              'type' => 'Huyện',
              'city_id' => 80,
          ),
          75 =>
          array (
              'id' => 803,
              'name' => 'Bến Lức',
              'type' => 'Huyện',
              'city_id' => 80,
          ),
          76 =>
          array (
              'id' => 804,
              'name' => 'Thủ Thừa',
              'type' => 'Huyện',
              'city_id' => 80,
          ),
          77 =>
          array (
              'id' => 805,
              'name' => 'Tân Trụ',
              'type' => 'Huyện',
              'city_id' => 80,
          ),
          78 =>
          array (
              'id' => 806,
              'name' => 'Cần Đước',
              'type' => 'Huyện',
              'city_id' => 80,
          ),
          79 =>
          array (
              'id' => 807,
              'name' => 'Cần Giuộc',
              'type' => 'Huyện',
              'city_id' => 80,
          ),
          80 =>
          array (
              'id' => 808,
              'name' => 'Châu Thành',
              'type' => 'Huyện',
              'city_id' => 80,
          ),
          81 =>
          array (
              'id' => 815,
              'name' => 'Mỹ Tho',
              'type' => 'Thành Phố',
              'city_id' => 82,
          ),
          82 =>
          array (
              'id' => 816,
              'name' => 'Gò Công',
              'type' => 'Thị Xã',
              'city_id' => 82,
          ),
          83 =>
          array (
              'id' => 818,
              'name' => 'Tân Phước',
              'type' => 'Huyện',
              'city_id' => 82,
          ),
          84 =>
          array (
              'id' => 819,
              'name' => 'Cái Bè',
              'type' => 'Huyện',
              'city_id' => 82,
          ),
          85 =>
          array (
              'id' => 820,
              'name' => 'Cai Lậy',
              'type' => 'Huyện',
              'city_id' => 82,
          ),
          86 =>
          array (
              'id' => 821,
              'name' => 'Châu Thành',
              'type' => 'Huyện',
              'city_id' => 82,
          ),
          87 =>
          array (
              'id' => 822,
              'name' => 'Chợ Gạo',
              'type' => 'Huyện',
              'city_id' => 82,
          ),
          88 =>
          array (
              'id' => 823,
              'name' => 'Gò Công Tây',
              'type' => 'Huyện',
              'city_id' => 82,
          ),
          89 =>
          array (
              'id' => 824,
              'name' => 'Gò Công Đông',
              'type' => 'Huyện',
              'city_id' => 82,
          ),
          90 =>
          array (
              'id' => 825,
              'name' => 'Tân Phú Đông',
              'type' => 'Huyện',
              'city_id' => 82,
          ),
          91 =>
          array (
              'id' => 829,
              'name' => 'Bến Tre',
              'type' => 'Thành Phố',
              'city_id' => 83,
          ),
          92 =>
          array (
              'id' => 831,
              'name' => 'Châu Thành',
              'type' => 'Huyện',
              'city_id' => 83,
          ),
          93 =>
          array (
              'id' => 832,
              'name' => 'Chợ Lách',
              'type' => 'Huyện',
              'city_id' => 83,
          ),
          94 =>
          array (
              'id' => 833,
              'name' => 'Mỏ Cày Nam',
              'type' => 'Huyện',
              'city_id' => 83,
          ),
          95 =>
          array (
              'id' => 834,
              'name' => 'Giồng Trôm',
              'type' => 'Huyện',
              'city_id' => 83,
          ),
          96 =>
          array (
              'id' => 835,
              'name' => 'Bình Đại',
              'type' => 'Huyện',
              'city_id' => 83,
          ),
          97 =>
          array (
              'id' => 836,
              'name' => 'Ba Tri',
              'type' => 'Huyện',
              'city_id' => 83,
          ),
          98 =>
          array (
              'id' => 837,
              'name' => 'Thạnh Phú',
              'type' => 'Huyện',
              'city_id' => 83,
          ),
          99 =>
          array (
              'id' => 838,
              'name' => 'Mỏ Cày Bắc',
              'type' => 'Huyện',
              'city_id' => 83,
          ),
          100 =>
          array (
              'id' => 842,
              'name' => 'Trà Vinh',
              'type' => 'Thị Xã',
              'city_id' => 84,
          ),
          101 =>
          array (
              'id' => 844,
              'name' => 'Càng Long',
              'type' => 'Huyện',
              'city_id' => 84,
          ),
          102 =>
          array (
              'id' => 845,
              'name' => 'Cầu Kè',
              'type' => 'Huyện',
              'city_id' => 84,
          ),
          103 =>
          array (
              'id' => 846,
              'name' => 'Tiểu Cần',
              'type' => 'Huyện',
              'city_id' => 84,
          ),
          104 =>
          array (
              'id' => 847,
              'name' => 'Châu Thành',
              'type' => 'Huyện',
              'city_id' => 84,
          ),
          105 =>
          array (
              'id' => 848,
              'name' => 'Cầu Ngang',
              'type' => 'Huyện',
              'city_id' => 84,
          ),
          106 =>
          array (
              'id' => 849,
              'name' => 'Trà Cú',
              'type' => 'Huyện',
              'city_id' => 84,
          ),
          107 =>
          array (
              'id' => 850,
              'name' => 'Duyên Hải',
              'type' => 'Huyện',
              'city_id' => 84,
          ),
          108 =>
          array (
              'id' => 855,
              'name' => 'Vĩnh Long',
              'type' => 'Thành Phố',
              'city_id' => 86,
          ),
          109 =>
          array (
              'id' => 857,
              'name' => 'Long Hồ',
              'type' => 'Huyện',
              'city_id' => 86,
          ),
          110 =>
          array (
              'id' => 858,
              'name' => 'Mang Thít',
              'type' => 'Huyện',
              'city_id' => 86,
          ),
          111 =>
          array (
              'id' => 859,
              'name' => 'Vũng Liêm',
              'type' => 'Huyện',
              'city_id' => 86,
          ),
          112 =>
          array (
              'id' => 860,
              'name' => 'Tam Bình',
              'type' => 'Huyện',
              'city_id' => 86,
          ),
          113 =>
          array (
              'id' => 861,
              'name' => 'Bình Minh',
              'type' => 'Thị Xã',
              'city_id' => 86,
          ),
          114 =>
          array (
              'id' => 862,
              'name' => 'Trà Ôn',
              'type' => 'Huyện',
              'city_id' => 86,
          ),
          115 =>
          array (
              'id' => 863,
              'name' => 'Bình Tân',
              'type' => 'Huyện',
              'city_id' => 86,
          ),
          117 =>
          array (
              'id' => 867,
              'name' => 'Sa Đéc',
              'type' => 'Thành Phố',
              'city_id' => 87,
          ),
          119 =>
          array (
              'id' => 869,
              'name' => 'Tân Hồng',
              'type' => 'Huyện',
              'city_id' => 87,
          ),
          120 =>
          array (
              'id' => 870,
              'name' => 'Hồng Ngự',
              'type' => 'Huyện',
              'city_id' => 87,
          ),
          1204 =>
          array (
              'id' => 879,
              'name' => 'Hồng Ngự',
              'type' => 'Thị Xã',
              'city_id' => 87,
          ),
          121 =>
          array (
              'id' => 871,
              'name' => 'Tam Nông',
              'type' => 'Huyện',
              'city_id' => 87,
          ),
          122 =>
          array (
              'id' => 872,
              'name' => 'Tháp Mười',
              'type' => 'Huyện',
              'city_id' => 87,
          ),
          123 =>
          array (
              'id' => 873,
              'name' => 'Cao Lãnh',
              'type' => 'Huyện',
              'city_id' => 87,
          ),
          12300 =>
          array (
              'id' => 878,
              'name' => 'Cao Lãnh',
              'type' => 'Thành Phố',
              'city_id' => 87,
          ),
          124 =>
          array (
              'id' => 874,
              'name' => 'Thanh Bình',
              'type' => 'Huyện',
              'city_id' => 87,
          ),
          125 =>
          array (
              'id' => 875,
              'name' => 'Lấp Vò',
              'type' => 'Huyện',
              'city_id' => 87,
          ),
          126 =>
          array (
              'id' => 876,
              'name' => 'Lai Vung',
              'type' => 'Huyện',
              'city_id' => 87,
          ),
          127 =>
          array (
              'id' => 877,
              'name' => 'Châu Thành',
              'type' => 'Huyện',
              'city_id' => 87,
          ),
          128 =>
          array (
              'id' => 883,
              'name' => 'Long Xuyên',
              'type' => 'Thành Phố',
              'city_id' => 89,
          ),
          129 =>
          array (
              'id' => 884,
              'name' => 'Châu Đốc',
              'type' => 'Thành Phố',
              'city_id' => 89,
          ),
          130 =>
          array (
              'id' => 886,
              'name' => 'An Phú',
              'type' => 'Huyện',
              'city_id' => 89,
          ),
          131 =>
          array (
              'id' => 887,
              'name' => 'Tân Châu',
              'type' => 'Thị Xã',
              'city_id' => 89,
          ),
          132 =>
          array (
              'id' => 888,
              'name' => 'Phú Tân',
              'type' => 'Huyện',
              'city_id' => 89,
          ),
          133 =>
          array (
              'id' => 889,
              'name' => 'Châu Phú',
              'type' => 'Huyện',
              'city_id' => 89,
          ),
          134 =>
          array (
              'id' => 890,
              'name' => 'Tịnh Biên',
              'type' => 'Huyện',
              'city_id' => 89,
          ),
          135 =>
          array (
              'id' => 891,
              'name' => 'Tri Tôn',
              'type' => 'Huyện',
              'city_id' => 89,
          ),
          136 =>
          array (
              'id' => 892,
              'name' => 'Châu Thành',
              'type' => 'Huyện',
              'city_id' => 89,
          ),
          137 =>
          array (
              'id' => 893,
              'name' => 'Chợ Mới',
              'type' => 'Huyện',
              'city_id' => 89,
          ),
          138 =>
          array (
              'id' => 894,
              'name' => 'Thoại Sơn',
              'type' => 'Huyện',
              'city_id' => 89,
          ),
          139 =>
          array (
              'id' => 899,
              'name' => 'Rạch Giá',
              'type' => 'Thành Phố',
              'city_id' => 91,
          ),
          140 =>
          array (
              'id' => 900,
              'name' => 'Hà Tiên',
              'type' => 'Thị Xã',
              'city_id' => 91,
          ),
          141 =>
          array (
              'id' => 902,
              'name' => 'Kiên Lương',
              'type' => 'Huyện',
              'city_id' => 91,
          ),
          142 =>
          array (
              'id' => 903,
              'name' => 'Hòn Đất',
              'type' => 'Huyện',
              'city_id' => 91,
          ),
          143 =>
          array (
              'id' => 904,
              'name' => 'Tân Hiệp',
              'type' => 'Huyện',
              'city_id' => 91,
          ),
          144 =>
          array (
              'id' => 905,
              'name' => 'Châu Thành',
              'type' => 'Huyện',
              'city_id' => 91,
          ),
          145 =>
          array (
              'id' => 906,
              'name' => 'Giồng Giềng',
              'type' => 'Huyện',
              'city_id' => 91,
          ),
          146 =>
          array (
              'id' => 907,
              'name' => 'Gò Quao',
              'type' => 'Huyện',
              'city_id' => 91,
          ),
          147 =>
          array (
              'id' => 908,
              'name' => 'An Biên',
              'type' => 'Huyện',
              'city_id' => 91,
          ),
          148 =>
          array (
              'id' => 909,
              'name' => 'An Minh',
              'type' => 'Huyện',
              'city_id' => 91,
          ),
          149 =>
          array (
              'id' => 910,
              'name' => 'Vĩnh Thuận',
              'type' => 'Huyện',
              'city_id' => 91,
          ),
          150 =>
          array (
              'id' => 911,
              'name' => 'Phú Quốc',
              'type' => 'Huyện',
              'city_id' => 91,
          ),
          151 =>
          array (
              'id' => 912,
              'name' => 'Kiên Hải',
              'type' => 'Huyện',
              'city_id' => 91,
          ),
          152 =>
          array (
              'id' => 913,
              'name' => 'U Minh Thượng',
              'type' => 'Huyện',
              'city_id' => 91,
          ),
          153 =>
          array (
              'id' => 914,
              'name' => 'Giang Thành',
              'type' => 'Huyện',
              'city_id' => 91,
          ),
          154 =>
          array (
              'id' => 916,
              'name' => 'Ninh Kiều',
              'type' => 'Quận',
              'city_id' => 92,
          ),
          155 =>
          array (
              'id' => 917,
              'name' => 'Ô Môn',
              'type' => 'Quận',
              'city_id' => 92,
          ),
          156 =>
          array (
              'id' => 918,
              'name' => 'Bình Thuỷ',
              'type' => 'Quận',
              'city_id' => 92,
          ),
          157 =>
          array (
              'id' => 919,
              'name' => 'Cái Răng',
              'type' => 'Quận',
              'city_id' => 92,
          ),
          158 =>
          array (
              'id' => 923,
              'name' => 'Thốt Nốt',
              'type' => 'Quận',
              'city_id' => 92,
          ),
          159 =>
          array (
              'id' => 924,
              'name' => 'Vĩnh Thạnh',
              'type' => 'Huyện',
              'city_id' => 92,
          ),
          160 =>
          array (
              'id' => 925,
              'name' => 'Cờ Đỏ',
              'type' => 'Huyện',
              'city_id' => 92,
          ),
          161 =>
          array (
              'id' => 926,
              'name' => 'Phong Điền',
              'type' => 'Huyện',
              'city_id' => 92,
          ),
          162 =>
          array (
              'id' => 927,
              'name' => 'Thới Lai',
              'type' => 'Huyện',
              'city_id' => 92,
          ),
          163 =>
          array (
              'id' => 930,
              'name' => 'Vị Thanh',
              'type' => 'Thị Xã',
              'city_id' => 93,
          ),
          164 =>
          array (
              'id' => 931,
              'name' => 'Ngã Bảy',
              'type' => 'Thị Xã',
              'city_id' => 93,
          ),
          165 =>
          array (
              'id' => 932,
              'name' => 'Châu Thành A',
              'type' => 'Huyện',
              'city_id' => 93,
          ),
          166 =>
          array (
              'id' => 933,
              'name' => 'Châu Thành',
              'type' => 'Huyện',
              'city_id' => 93,
          ),
          167 =>
          array (
              'id' => 934,
              'name' => 'Phụng Hiệp',
              'type' => 'Huyện',
              'city_id' => 93,
          ),
          168 =>
          array (
              'id' => 935,
              'name' => 'Vị Thuỷ',
              'type' => 'Huyện',
              'city_id' => 93,
          ),
          169 =>
          array (
              'id' => 936,
              'name' => 'Long Mỹ',
              'type' => 'Huyện',
              'city_id' => 93,
          ),
          170 =>
          array (
              'id' => 941,
              'name' => 'Sóc Trăng',
              'type' => 'Thành Phố',
              'city_id' => 94,
          ),
          171 =>
          array (
              'id' => 942,
              'name' => 'Châu Thành',
              'type' => 'Huyện',
              'city_id' => 94,
          ),
          172 =>
          array (
              'id' => 943,
              'name' => 'Kế Sách',
              'type' => 'Huyện',
              'city_id' => 94,
          ),
          173 =>
          array (
              'id' => 944,
              'name' => 'Mỹ Tú',
              'type' => 'Huyện',
              'city_id' => 94,
          ),
          174 =>
          array (
              'id' => 945,
              'name' => 'Cù Lao Dung',
              'type' => 'Huyện',
              'city_id' => 94,
          ),
          175 =>
          array (
              'id' => 946,
              'name' => 'Long Phú',
              'type' => 'Huyện',
              'city_id' => 94,
          ),
          176 =>
          array (
              'id' => 947,
              'name' => 'Mỹ Xuyên',
              'type' => 'Huyện',
              'city_id' => 94,
          ),
          177 =>
          array (
              'id' => 948,
              'name' => 'Ngã Năm',
              'type' => 'Huyện',
              'city_id' => 94,
          ),
          178 =>
          array (
              'id' => 949,
              'name' => 'Thạnh Trị',
              'type' => 'Huyện',
              'city_id' => 94,
          ),
          179 =>
          array (
              'id' => 950,
              'name' => 'Vĩnh Châu',
              'type' => 'Thị Xã',
              'city_id' => 94,
          ),
          180 =>
          array (
              'id' => 951,
              'name' => 'Trần Đề',
              'type' => 'Huyện',
              'city_id' => 94,
          ),
          181 =>
          array (
              'id' => 954,
              'name' => 'Bạc Liêu',
              'type' => 'Thị Xã',
              'city_id' => 95,
          ),
          182 =>
          array (
              'id' => 956,
              'name' => 'Hồng Dân',
              'type' => 'Huyện',
              'city_id' => 95,
          ),
          183 =>
          array (
              'id' => 957,
              'name' => 'Phước Long',
              'type' => 'Huyện',
              'city_id' => 95,
          ),
          184 =>
          array (
              'id' => 958,
              'name' => 'Vĩnh Lợi',
              'type' => 'Huyện',
              'city_id' => 95,
          ),
          185 =>
          array (
              'id' => 959,
              'name' => 'Giá Rai',
              'type' => 'Huyện',
              'city_id' => 95,
          ),
          186 =>
          array (
              'id' => 960,
              'name' => 'Đông Hải',
              'type' => 'Huyện',
              'city_id' => 95,
          ),
          187 =>
          array (
              'id' => 961,
              'name' => 'Hoà Bình',
              'type' => 'Huyện',
              'city_id' => 95,
          ),
          188 =>
          array (
              'id' => 964,
              'name' => 'Cà Mau',
              'type' => 'Thành Phố',
              'city_id' => 96,
          ),
          189 =>
          array (
              'id' => 966,
              'name' => 'U Minh',
              'type' => 'Huyện',
              'city_id' => 96,
          ),
          190 =>
          array (
              'id' => 967,
              'name' => 'Thới Bình',
              'type' => 'Huyện',
              'city_id' => 96,
          ),
          191 =>
          array (
              'id' => 968,
              'name' => 'Trần Văn Thời',
              'type' => 'Huyện',
              'city_id' => 96,
          ),
          192 =>
          array (
              'id' => 969,
              'name' => 'Cái Nước',
              'type' => 'Huyện',
              'city_id' => 96,
          ),
          193 =>
          array (
              'id' => 970,
              'name' => 'Đầm Dơi',
              'type' => 'Huyện',
              'city_id' => 96,
          ),
          194 =>
          array (
              'id' => 971,
              'name' => 'Năm Căn',
              'type' => 'Huyện',
              'city_id' => 96,
          ),
          195 =>
          array (
              'id' => 972,
              'name' => 'Phú Tân',
              'type' => 'Huyện',
              'city_id' => 96,
          ),
          196 =>
          array (
              'id' => 973,
              'name' => 'Ngọc Hiển',
              'type' => 'Huyện',
              'city_id' => 96,
          ),
      ));
    }
}
