<?php

use Illuminate\Database\Seeder;

class ContestsRegulationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \DB::table('contests_regulation')->delete();

      \DB::table('contests_regulation')->insert(array (
          0 =>
          array (
              'id' => 2,
              'name' => 'guaranteed',
              'label' => 'Guaranteed',
              'description' => 'Guarantee freelancers that a winner will be chosen and awarded the prize. This will attract better entries from more freelancers.Moneyback guarantee is not applicable if a contest has a guaranteed upgrade.',
          ),
          1 =>
          array (
              'id' => 3,
              'name' => 'featured',
              'label' => 'Featured',
              'description' => 'Featured contests attract more, higher-quality entries and are displayed prominently in the Featured Jobs and Contests page.',
          )
      ));
    }
}
