<?php

use Illuminate\Database\Seeder;

class UsersSkillsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \DB::table('users_skills')->delete();

      \DB::table('users_skills')->insert(array (
          0 =>
          array (
              'id' => 1,
              'user_id' => 2,
              'skill_id' => 4 ,
          ),
          1 =>
          array (
              'id' => 2,
              'user_id' => 2,
              'skill_id' => 7,
          ),
          2 =>
          array (
              'id' => 3,
              'user_id' => 2,
              'skill_id' => 6,
          ),
          3 =>
          array (
              'id' => 4,
              'user_id' => 2,
              'skill_id' => 12,
          ),
          4 =>
          array (
              'id' => 5,
              'user_id' => 2,
              'skill_id' => 14,
          ),
          5 =>
          array (
              'id' => 6,
              'user_id' => 2,
              'skill_id' => 18,
          ),
          6 =>
          array (
              'id' => 7,
              'user_id' => 3,
              'skill_id' => 115,
          ),
          7 =>
          array (
              'id' => 8,
              'user_id' => 3,
              'skill_id' => 116,
          ),
          8 =>
          array (
              'id' => 9,
              'user_id' => 3,
              'skill_id' => 117,
          ),
          9 =>
          array (
              'id' => 10,
              'user_id' => 3,
              'skill_id' => 118,
          ),
          10 =>
          array (
              'id' => 11,
              'user_id' => 3,
              'skill_id' => 119,
          ),
          11 =>
          array (
              'id' => 12,
              'user_id' => 4,
              'skill_id' => 6,
          ),
          12 =>
          array (
              'id' => 13,
              'user_id' => 4,
              'skill_id' => 11,
          ),
          13 =>
          array (
              'id' => 14,
              'user_id' => 4,
              'skill_id' => 14,
          ),
          14 =>
          array (
              'id' => 15,
              'user_id' => 4,
              'skill_id' => 21,
          ),
          15 =>
          array (
              'id' => 16,
              'user_id' => 5,
              'skill_id' => 117,
          ),
          16 =>
          array (
              'id' => 17,
              'user_id' => 5,
              'skill_id' => 122,
          ),
          17 =>
          array (
              'id' => 18,
              'user_id' => 5,
              'skill_id' => 139,
          ),
          18 =>
          array (
              'id' => 19,
              'user_id' => 6,
              'skill_id' => 120,
          ),
          19 =>
          array (
              'id' => 20,
              'user_id' => 7,
              'skill_id' => 111,
          ),
          20 =>
          array (
              'id' => 21,
              'user_id' => 8,
              'skill_id' => 39,
          ),
          21 =>
          array (
              'id' => 22,
              'user_id' => 9,
              'skill_id' => 78,
          ),
          22 =>
          array (
              'id' => 23,
              'user_id' => 10,
              'skill_id' => 19,
          ),
          23 =>
          array (
              'id' => 24,
              'user_id' => 11,
              'skill_id' => 15,
          ),
          24 =>
          array (
              'id' => 25,
              'user_id' => 12,
              'skill_id' => 16,
          ),
          25 =>
          array (
              'id' => 26,
              'user_id' => 13,
              'skill_id' => 59,
          ),
          26 =>
          array (
              'id' => 27,
              'user_id' => 14,
              'skill_id' => 69,
          ),
          27 =>
          array (
              'id' => 28,
              'user_id' => 15,
              'skill_id' => 47,
          ),
          28 =>
          array (
              'id' => 29,
              'user_id' => 16,
              'skill_id' => 27,
          ),
          29 =>
          array (
              'id' => 30,
              'user_id' => 6,
              'skill_id' => rand(1,292),
          ),
          30 =>
          array (
              'id' => 31,
              'user_id' => 7,
              'skill_id' => rand(1,292),
          ),
          31 =>
          array (
              'id' => 32,
              'user_id' => 8,
              'skill_id' => rand(1,292),
          ),
          32 =>
          array (
              'id' => 33,
              'user_id' => 9,
              'skill_id' => rand(1,292),
          ),
          33 =>
          array (
              'id' => 34,
              'user_id' => 10,
              'skill_id' => rand(1,292),
          ),
          34 =>
          array (
              'id' => 35,
              'user_id' => 11,
              'skill_id' => rand(1,292),
          ),
          35 =>
          array (
              'id' => 36,
              'user_id' => 12,
              'skill_id' => rand(1,292),
          ),
          36 =>
          array (
              'id' => 37,
              'user_id' => 13,
              'skill_id' => rand(1,292),
          ),
          37 =>
          array (
              'id' => 38,
              'user_id' => 14,
              'skill_id' => rand(1,292),
          ),
          38 =>
          array (
              'id' => 39,
              'user_id' => 15,
              'skill_id' => rand(1,292),
          ),
          39 =>
          array (
              'id' => 40,
              'user_id' => 16,
              'skill_id' => rand(1,292),
          ),
      ));
    }
}
