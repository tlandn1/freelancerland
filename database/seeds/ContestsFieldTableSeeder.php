<?php

use Illuminate\Database\Seeder;

class ContestsFieldTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \DB::table('contests_field')->delete();

      \DB::table('contests_field')->insert(array (
          0 =>
          array (
              'id' => 1,
              'name' => 'Design',
          ),
          1 =>
          array (
              'id' => 2,
              'name' => 'Mobile',
          ),
          2 =>
          array (
              'id' => 3,
              'name' => 'Writing',
          ),
          3 =>
          array (
              'id' => 4,
              'name' => 'Websites IT & Software',
          ),
          4 =>
          array (
              'id'  => 5,
              'name' =>'Sales & Marketing',
          )
      ));
    }
}
