<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CitiesTableSeeder::class);
        $this->call(DistrictsTableSeeder::class);
        $this->call(UserStatusesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(FirstCategoriesTableSeeder::class);
        $this->call(SecondCategoriesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(EducationsTableSeeder::class);
<<<<<<< HEAD
        $this->call(PortfolioesTableSeeder::class);
        $this->call(SkillTablesSeeder::class);
        $this->call(WorkingCompanyTablesSeeder::class);
        $this->call(UsersSecondCategoryTableSeeder::class);
=======
        $this->call(PortfoliosTableSeeder::class);
        $this->call(SkillTablesSeeder::class);
        $this->call(WorkingCompanyTablesSeeder::class);
        $this->call(UsersSecondCategoryTableSeeder::class);
        $this->call(UsersSkillsTableSeeder::class);
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
        $this->call(BudgetEstimateTableSeeder::class);
        $this->call(ProjectStatusTableSeeder::class);
        $this->call(SlugSeeder::class);
        $this->call(StaticPagesTableSeeder::class);
        $this->call(GigStatusesTableSeeder::class);
        $this->call(GigsTableSeeder::class);
        $this->call(GigsExtraTableSeeder::class);
        $this->call(ProjectGigStatusesTableSeeder::class);
        /// Contest Seeder
        $this->call(ContestsFieldTableSeeder::class);
        $this->call(ContestsServiceTableSeeder::class);
        $this->call(ContestsRegulationTableSeeder::class);

    }
}
