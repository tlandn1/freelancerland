<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        // Role admin
        $nameAccount = $faker->userName;
        App\User::create([
            'name' => 'Admin freelancerland.vn',
            'email' => 'admin@freelancerland.vn',
            'password' => bcrypt('12345678'),
            'remember_token' => str_random(10),
            'phone' => $faker->tollFreePhoneNumber,
            'note' => $faker->sentence(5),
            'sodu_tk' => $faker->randomNumber(8),
            'skype' => $nameAccount,
            'facebook' => $nameAccount.'@gmail.com',
<<<<<<< HEAD
            'website' => $faker->domainName,
=======
            'website' => 'http://'.$faker->domainName,
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
            'user_status_id' => USER_STATUS_ACTIVE_ID,
            'role_id' => USER_ROLE_ADMIN_ID,
            'avatar' => '/images/users/avatar-default.jpg',
            'district_id' => rand(1,9),
            'cmnd_number' => $faker->creditCardNumber,
            'cmnd_date_of_birth' => $faker->Date,
            'cmnd_address' => $faker->address,
            'about_job_title' => $faker->sentence(5),
            'about_description' => $faker->text(400),
        ]);

        // Role user
        $nameAccount1 = $faker->userName;
        App\User::create([
            'name' => 'Freelancer 1',
            'email' => 'user@freelancerland.vn',
            'password' => bcrypt('12345678'),
            'remember_token' => str_random(10),
            'phone' => $faker->tollFreePhoneNumber,
            'note' => $faker->sentence(5),
            'sodu_tk' => $faker->randomNumber(8),
            'skype' => $nameAccount1,
            'facebook' => $nameAccount1.'@gmail.com',
<<<<<<< HEAD
            'website' => $faker->domainName,
=======
            'website' => 'http://'.$faker->domainName,
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
            'user_status_id' => USER_STATUS_ACTIVE_ID,
            'role_id' => USER_ROLE_FREELANCER_ID,
            'avatar' => '/images/users/avatar-default.jpg',
            'district_id' => rand(1,9),
            'cmnd_number' => $faker->creditCardNumber,
            'cmnd_date_of_birth' => $faker->Date,
            'cmnd_address' => $faker->address,
            'about_job_title' => $faker->sentence(5),
            'about_description' => $faker->text(400),
        ]);

        $nameAccount2 = $faker->userName;
        App\User::create([
            'name' => 'Freelancer 2',
            'email' => 'user2@freelancerland.vn',
            'password' => bcrypt('12345678'),
            'remember_token' => str_random(10),
            'phone' => $faker->tollFreePhoneNumber,
            'note' => $faker->sentence(5),
            'sodu_tk' => $faker->randomNumber(8),
            'skype' => $nameAccount2,
            'facebook' => $nameAccount2.'@gmail.com',
<<<<<<< HEAD
            'website' => $faker->domainName,
=======
            'website' => 'http://'.$faker->domainName,
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
            'user_status_id' => USER_STATUS_ACTIVE_ID,
            'role_id' => USER_ROLE_FREELANCER_ID,
            'avatar' => '/images/users-seeder-sample-images/sauro-128.jpg',
            'district_id' => rand(1,9),
            'cmnd_number' => $faker->creditCardNumber,
            'cmnd_date_of_birth' => $faker->Date,
            'cmnd_address' => $faker->address,
            'about_job_title' => $faker->sentence(5),
            'about_description' => $faker->text(400),
        ]);

        $nameAccount3 = $faker->userName;
        App\User::create([
            'name' => 'Freelancer 3',
            'email' => 'user3@freelancerland.vn',
            'password' => bcrypt('12345678'),
            'remember_token' => str_random(10),
            'phone' => $faker->tollFreePhoneNumber,
            'note' => $faker->sentence(5),
            'sodu_tk' => $faker->randomNumber(8),
            'skype' => $nameAccount3,
            'facebook' => $nameAccount3.'@gmail.com',
<<<<<<< HEAD
            'website' => $faker->domainName,
=======
            'website' => 'http://'.$faker->domainName,
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
            'user_status_id' => USER_STATUS_ACTIVE_ID,
            'role_id' => USER_ROLE_FREELANCER_ID,
            'avatar' => '/images/users-seeder-sample-images/andyvitale-128.jpg',
            'district_id' => rand(1,9),
            'cmnd_number' => $faker->creditCardNumber,
            'cmnd_date_of_birth' => $faker->Date,
            'cmnd_address' => $faker->address,
            'about_job_title' => $faker->sentence(5),
            'about_description' => $faker->text(400),
        ]);

        // Role reviewer
        $nameAccount4 = $faker->userName;
        App\User::create([
            'name' => 'Freelancer 4',
            'email' => 'user4@freelancerland.vn',
            'password' => bcrypt('12345678'),
            'remember_token' => str_random(10),
            'phone' => $faker->tollFreePhoneNumber,
            'note' => $faker->sentence(5),
            'sodu_tk' => $faker->randomNumber(8),
            'skype' => $nameAccount4,
            'facebook' => $nameAccount4.'@gmail.com',
<<<<<<< HEAD
            'website' => $faker->domainName,
=======
            'website' => 'http://'.$faker->domainName,
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
            'user_status_id' => USER_STATUS_ACTIVE_ID,
            'role_id' => USER_ROLE_FREELANCER_ID,
            'avatar' => '/images/users/avatar-default.jpg',
            'district_id' => rand(1,9),
            'cmnd_number' => $faker->creditCardNumber,
            'cmnd_date_of_birth' => $faker->Date,
            'cmnd_address' => $faker->address,
            'about_job_title' => $faker->sentence(5),
            'about_description' => $faker->text(400),
        ]);

        $nameAccount5 = $faker->userName;
        App\User::create([
            'name' => 'Freelancer 5',
            'email' => 'user5@freelancerland.vn',
            'password' => bcrypt('12345678'),
            'remember_token' => str_random(10),
            'phone' => $faker->tollFreePhoneNumber,
            'note' => $faker->sentence(5),
            'sodu_tk' => $faker->randomNumber(8),
            'skype' => $nameAccount5,
            'facebook' => $nameAccount5.'@gmail.com',
<<<<<<< HEAD
            'website' => $faker->domainName,
=======
            'website' => 'http://'.$faker->domainName,
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
            'user_status_id' => USER_STATUS_ACTIVE_ID,
            'role_id' => USER_ROLE_FREELANCER_ID,
            'avatar' => '/images/users/avatar-default.jpg',
            'district_id' => rand(1,9),
            'cmnd_number' => $faker->creditCardNumber,
            'cmnd_date_of_birth' => $faker->Date,
            'cmnd_address' => $faker->address,
            'about_job_title' => $faker->sentence(5),
            'about_description' => $faker->text(400),
        ]);

        $nameAccount6 = $faker->userName;
        App\User::create([
            'name' => 'Client 1',
            'email' => 'client1@freelancerland.vn',
            'password' => bcrypt('12345678'),
            'remember_token' => str_random(10),
            'phone' => $faker->tollFreePhoneNumber,
            'note' => $faker->sentence(5),
            'sodu_tk' => $faker->randomNumber(8),
            'skype' => $nameAccount6,
            'facebook' => $nameAccount6.'@gmail.com',
<<<<<<< HEAD
            'website' => $faker->domainName,
=======
            'website' => 'http://'.$faker->domainName,
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
            'user_status_id' => USER_STATUS_ACTIVE_ID,
            'role_id' => USER_ROLE_CLIENT_ID,
            'avatar' => '/images/users/avatar-default.jpg',
            'district_id' => rand(1,9),
            'cmnd_number' => $faker->creditCardNumber,
            'cmnd_date_of_birth' => $faker->Date,
            'cmnd_address' => $faker->address,
            'about_job_title' => $faker->sentence(5),
            'about_description' => $faker->text(400),
        ]);

        $nameAccount7 = $faker->userName;
        App\User::create([
            'name' => 'Client 2',
            'email' => 'client2@freelancerland.vn',
            'password' => bcrypt('12345678'),
            'remember_token' => str_random(10),
            'phone' => $faker->tollFreePhoneNumber,
            'note' => $faker->sentence(5),
            'sodu_tk' => $faker->randomNumber(8),
            'skype' => $nameAccount7,
            'facebook' => $nameAccount7.'@gmail.com',
<<<<<<< HEAD
            'website' => $faker->domainName,
=======
            'website' => 'http://'.$faker->domainName,
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
            'user_status_id' => USER_STATUS_ACTIVE_ID,
            'role_id' => USER_ROLE_CLIENT_ID,
            'avatar' => '/images/users/avatar-default.jpg',
            'district_id' => rand(1,9),
            'cmnd_number' => $faker->creditCardNumber,
            'cmnd_date_of_birth' => $faker->Date,
            'cmnd_address' => $faker->address,
            'about_job_title' => $faker->sentence(5),
            'about_description' => $faker->text(400),
        ]);

        // Random users
        factory('App\User', 8)->create();
    }
}
