<?php

use Illuminate\Database\Seeder;

class WorkingCompanyTablesSeeder extends Seeder
{
  public function run()
  {
    \DB::table('working_companies')->delete();

    \DB::table('working_companies')->insert(array (
        0 =>
        array (
            'id' => 1,
            'company_name' => 'công ty TNHH GMO',
            'position' => 'Lập trình viên',
            'description' => 'Nhân viên kế website tại công ty TNHH GMO',
            'from' => '2008-01-07',
            'to' => '2016-01-01',
            'user_id' => 1,
        ),
        1 =>
        array (
            'id' => 2,
            'company_name' => 'công ty TNHH ABC',
            'position' => 'Nhân viên',
            'description' => 'Nhân viên design logo, banner tại công ty TNHH ABC',
            'from' => '2010-01-05',
            'to' => '2015-02-07',
            'user_id' => 2,
        ),
        2 =>
        array (
            'id' => 3,
            'company_name' => 'công ty TNHH Toàn Cầu Xanh',
            'position' => 'Nhân viên',
            'description' => 'Nhân viên kế website tại công ty TNHH Toàn Cầu Xanh',
            'from' => '2013-05-25',
            'to' => '2015-08-05',
            'user_id' => 3,
        ),
        3 =>
        array (
            'id' => 4,
            'company_name' => 'công ty TNHH DEF',
            'position' => 'Nhân viên',
            'description' => 'Nhân viên design logo, banner tại công ty TNHH DEF',
            'from' => '2011-01-08',
            'to' => '2016-03-03',
            'user_id' => 4,
        ),
    ));
  }
}
