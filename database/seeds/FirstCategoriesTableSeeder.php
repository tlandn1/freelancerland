<?php

use Illuminate\Database\Seeder;

class FirstCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \DB::table('first_categories')->delete();

      \DB::table('first_categories')->insert(array (
          0 =>
          array (
              'id' => 1,
              'name' => 'Website, Mobile và Lập trình',
              'slug' => 'website-mobile-va-lap-trinh',
          ),
          1 =>
          array (
              'id' => 2,
              'name' => 'IT và mạng',
              'slug' => 'it-va-mang',
          ),
          2 =>
          array (
              'id' => 3,
              'name' => 'Kỹ sư và kiến trúc sư',
              'slug' => 'ky-su-va-kien-truc-su',
          ),
          3 =>
          array (
              'id' => 4,
              'name' => 'Thiết kế',
              'slug' => 'thiet-ke',
          ),
          4 =>
          array (
              'id' => 5,
              'name' => 'Viết lách và dịch thuật',
              'slug' => 'viet-lach-va-dich-thuat',
          ),
          5 =>
          array (
              'id' => 6,
              'name' => 'Marketing, Bán hàng, Kinh doanh',
              'slug' => 'marketing-ban-hang-kinh-doanh',
          ),
          6 =>
          array (
              'id' => 7,
              'name' => 'Nhập liệu, Dịch vụ khách hàng',
              'slug' => 'nhap-lieu-dich-vu-khach-hang',
          ),
          7 =>
          array (
              'id' => 8,
              'name' => 'Kế toán và Pháp lý',
              'slug' => 'ke-toan-va-phap-ly',
          ),
          8 =>
          array (
              'id' => 9,
              'name' => 'Lĩnh vực khác',
              'slug' => 'linh-vuc-khac',
          ),
      ));
    }
}
