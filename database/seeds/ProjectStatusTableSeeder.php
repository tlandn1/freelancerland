<?php

use Illuminate\Database\Seeder;

class ProjectStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \DB::table('project_statuses')->delete();

      \DB::table('project_statuses')->insert(array (
          0 =>
          array (
              'id' => 1,
              'name' => 'pending',
              'label' => 'Waiting Review',
          ),
          1 =>
          array (
              'id' => 2,
              'name' => 'active',
              'label' => 'Active',
          ),
          2 =>
          array (
              'id' => 3,
              'name' => 'suspend ',
              'label' => 'Suspend',
          ),
          3 =>
          array (
              'id' => 4,
              'name' => 'assigned',
              'label' => 'Assigned',
          ),
          4 =>
          array (
              'id' => 5,
              'name' => 'finish',
              'label' => 'Finish',
          ),
      ));
    }
}
