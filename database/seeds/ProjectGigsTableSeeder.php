<?php

use Illuminate\Database\Seeder;

class ProjectGigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 200; ++$i) {
          $projectGig = new App\ProjectGig();

          $projectGig->user_id = rand(1, 3);
          $projectGig->gig_id = rand(1, 100);
          $projectGig->project_gig_status_id = rand(1, 3);
          $projectGig->user_review_text = $faker->paragraph(2);
          $projectGig->user_review_time = $faker->date($format = 'Y-m-d H:i:s', $max = 'now');
          $projectGig->user_review_rating = rand(1, 5);
          $projectGig->seller_review_text = $faker->paragraph(2);
          $projectGig->seller_review_rating = rand(1, 5);
          $projectGig->seller_review_time = $faker->date($format = 'Y-m-d H:i:s', $max = 'now');
          $projectGig->save();
      }
  }
}
