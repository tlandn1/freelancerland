<?php

use Illuminate\Database\Seeder;

class GigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create();

        for ($i = 0; $i < 100; ++$i) {
            $gig = new App\Gig();
            $gig->title = $faker->sentence(5);
            $gig->description = $faker->paragraph(7);
            $gig->price = $faker->numberBetween($min = 100, $max = 2000).'000';
            $gig->deliver_day = rand(3, 10);
            $gig->user_id = rand(1, 3);
            $gig->gig_status_id = rand(1,3);

            $gig->save();
        }
    }
}
