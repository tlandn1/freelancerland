<?php

use Illuminate\Database\Seeder;

class BudgetEstimateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \DB::table('budget_estimate')->delete();

      \DB::table('budget_estimate')->insert(array (
          0 =>
          array (
              'id' => 1,
              'label' => '100.000đ - 500.000đ',
          ),
          1 =>
          array (
              'id' => 2,
              'label' => '500.000đ - 2.000.000đ',
          ),
          2 =>
          array (
              'id' => 3,
              'label' => '2.000.000đ - 4.000.000đ',
          ),
          3 =>
          array (
              'id' => 4,
              'label' => '4.000.000đ - 6.000.000đ',
          ),
          4 =>
          array (
              'id' => 5,
              'label' => '6.000.000đ - 8.000.000đ',
          ),
          5 =>
          array (
              'id' => 6,
              'label' => '8.000.000đ - 10.000.000đ',
          ),
          6 =>
          array (
              'id' => 7,
              'label' => '10.000.000đ - 15.000.000đ',
          ),
          7 =>
          array (
              'id' => 8,
              'label' => '15.000.000đ - 20.000.000đ',
          ),
          8 =>
          array (
              'id' => 9,
              'label' => '20.000.000đ - 25.000.000đ',
          ),
          9 =>
          array (
              'id' => 10,
              'label' => '25.000.000đ - 30.000.000đ',
          ),
          10 =>
          array (
              'id' => 11,
              'label' => '30.000.000đ - 40.000.000đ',
          ),
          11 =>
          array (
              'id' => 12,
              'label' => '40.000.000đ - 50.000.000đ',
          ),
          12 =>
          array (
              'id' => 13,
              'label' => '> 50.000.000đ',
          ),
      ));
    }
}
