<?php

use Illuminate\Database\Seeder;

class ContestsServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \DB::table('contests_service')->delete();

      \DB::table('contests_service')->insert(array (
          0 =>
          array (
              'id' => 1,
              'name' => 'Design a Logo',
              'contests_field_id' => 1,
          ),
          1 =>
          array (
              'id' => 2,
              'name' => 'Design a Banner',
              'contests_field_id' => 1,
          ),
          2 =>
          array (
              'id' => 3,
              'name' => 'Design some Icons',
              'contests_field_id' => 1,
          ),
          3 =>
          array (
              'id' => 4,
              'name' => 'Design a Website Mockup',
              'contests_field_id' => 1,
          ),
          4 =>
          array (
              'id'  => 5,
              'name' =>'Design an Advertisement',
              'contests_field_id' => 1,
          )
      ));
<<<<<<< HEAD

      $faker = Faker\Factory::create();

      // Role admin
      for ($i=0; $i < 30 ; $i++) {
        App\ContestsService::create([
          'name' => $faker->paragraph,
          'contests_field_id' => rand(2,5),
        ]);
      }

=======
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
    }
}
