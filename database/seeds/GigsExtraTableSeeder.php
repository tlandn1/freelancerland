<?php

use Illuminate\Database\Seeder;

class GigsExtraTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create();

    	for ($i = 0; $i < 200; ++$i) {
    		$gigExtra = new App\GigExtra();
    		$gigExtra->title = $faker->sentence(5);
    		$gigExtra->price = $faker->numberBetween($min = 50, $max = 200).'000';
    		$gigExtra->deliver_day = rand(1, 5);
    		$gigExtra->gig_id = rand(1, 100);

    		$gigExtra->save();
    	}
    }
}
