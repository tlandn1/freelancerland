<?php

use Illuminate\Database\Seeder;

class SkillTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \DB::table('skills')->delete();

      \DB::table('skills')->insert(array (
          0 =>
          array (
              'id' => 1,
              'name' => 'ajax',
              'label' => 'Ajax',
          ),
          1 =>
          array (
              'id' => 2,
              'name' => 'android',
              'label' => 'Android',
          ),
          2 =>
          array (
              'id' => 3,
              'name' => 'angularjs',
              'label' => 'AngularJS',
          ),
          3 =>
          array (
              'id' => 4,
              'name' => 'asp_net',
              'label' => 'ASP .NET',
          ),
          4 =>
          array (
              'id' => 5,
              'name' => 'blackberry',
              'label' => 'Blackberry',
          ),
          5 =>
          array (
              'id' => 6,
              'name' => 'bootstrap_3',
              'label' => 'Bootstrap 3',
          ),
          6 =>
          array (
              'id' => 7,
              'name' => 'cakephp',
              'label' => 'CakePHP',
          ),
          7 =>
          array (
              'id' => 8,
              'name' => 'c',
              'label' => 'C',
          ),
          8 =>
          array (
              'id' => 9,
              'name' => 'c++',
              'label' => 'C++',
          ),
          9 =>
          array (
              'id' => 10,
              'name' => 'c#',
              'label' => 'C#',
          ),
          10 =>
          array (
              'id' => 11,
              'name' => 'css',
              'label' => 'CSS',
          ),
          11 =>
          array (
              'id' => 12,
              'name' => 'codeigniter',
              'label' => 'Codeigniter',
          ),
          12 =>
          array (
              'id' => 13,
              'name' => 'dotnetnuke',
              'label' => 'DotNetNuke',
          ),
          13 =>
          array (
              'id' => 14,
              'name' => 'html5',
              'label' => 'HTML5',
          ),
          14 =>
          array (
              'id' => 15,
              'name' => 'ember.js',
              'label' => 'Ember.js',
          ),
          15 =>
          array (
              'id' => 16,
              'name' => 'flash',
              'label' => 'Flash',
          ),
          16 =>
          array (
              'id' => 17,
              'name' => 'game_design',
              'label' => 'Game Design',
          ),
          17 =>
          array (
              'id' => 18,
              'name' => 'git',
              'label' => 'Git',
          ),
          18 =>
          array (
              'id' => 19,
              'name' => 'ios',
              'label' => 'iOS',
          ),
          19 =>
          array (
              'id' => 20,
              'name' => 'java',
              'label' => 'Java',
          ),
          20 =>
          array (
              'id' => 21,
              'name' => 'javaScript',
              'label' => 'JavaScript',
          ),
          21 =>
          array (
              'id' => 22,
              'name' => 'j2ee',
              'label' => 'J2EE',
          ),
          22 =>
          array (
              'id' => 23,
              'name' => 'j2me',
              'label' => 'J2ME',
          ),
          23 =>
          array (
              'id' => 24,
              'name' => 'joomla',
              'label' => 'Joomla',
          ),
          24 =>
          array (
              'id' => 25,
              'name' => 'jquery',
              'label' => 'jQuery ',
          ),
          25 =>
          array (
              'id' => 26,
              'name' => 'json',
              'label' => 'JSON',
          ),
          26 =>
          array (
              'id' => 27,
              'name' => 'jsp',
              'label' => 'JSP',
          ),
          27 =>
          array (
              'id' => 28,
              'name' => 'lamp',
              'label' => 'LAMP',
          ),
          28 =>
          array (
              'id' => 29,
              'name' => 'laravel',
              'label' => 'Laravel',
          ),
          29 =>
          array (
              'id' => 30,
              'name' => 'less_sass_scss',
              'label' => 'LESS/SASS/SCSS',
          ),
          30 =>
          array (
              'id' => 31,
              'name' => 'linux',
              'label' => 'Linux',
          ),
          31 =>
          array (
              'id' => 32,
              'name' => 'php',
              'label' => 'PHP',
          ),
          32 =>
          array (
              'id' => 33,
              'name' => 'phalcon',
              'label' => 'Phalcon',
          ),
          33 =>
          array (
              'id' => 34,
              'name' => 'magento',
              'label' => 'Magento',
          ),
          34 =>
          array (
              'id' => 35,
              'name' => 'meteor.js',
              'label' => 'Meteor.js',
          ),
          35 =>
          array (
              'id' => 36,
              'name' => 'microsoft_access',
              'label' => 'Microsoft Access',
          ),
          36 =>
          array (
              'id' => 37,
              'name' => 'microsoft_office',
              'label' => 'Microsoft Office',
          ),
          37 =>
          array (
              'id' => 38,
              'name' => 'microsoft_sql_server',
              'label' => 'Microsoft SQL Server',
          ),
          38 =>
          array (
              'id' => 39,
              'name' => 'mysql',
              'label' => 'MySQL',
          ),
          39 =>
          array (
              'id' => 40,
              'name' => 'mongodb',
              'label' => 'MongoDB',
          ),
          40 =>
          array (
              'id' => 41,
              'name' => 'node.js',
              'label' => 'Node.js',
          ),
          41 =>
          array (
              'id' => 42,
              'name' => 'rest',
              'label' => 'REST',
          ),
          42 =>
          array (
              'id' => 43,
              'name' => 'ruby',
              'label' => 'Ruby',
          ),
          43 =>
          array (
              'id' => 44,
              'name' => 'ruby_on_rails',
              'label' => 'Ruby on Rails',
          ),
          44 =>
          array (
              'id' => 45,
              'name' => 'test_va_kiem_tra_loi',
              'label' => 'Test và kiểm tra lỗi',
          ),
          45 =>
          array (
              'id' => 46,
              'name' => 'sap',
              'label' => 'SAP',
          ),
          46 =>
          array (
              'id' => 47,
              'name' => 'scrum',
              'label' => 'Scrum',
          ),
          47 =>
          array (
              'id' => 48,
              'name' => 'seo',
              'label' => 'SEO',
          ),
          48 =>
          array (
              'id' => 49,
              'name' => 'sql',
              'label' => 'SQL',
          ),
          49 =>
          array (
              'id' => 50,
              'name' => 'sqlite',
              'label' => 'SQLite',
          ),
          50 =>
          array (
              'id' => 51,
              'name' => 'swift',
              'label' => 'Swift',
          ),
          51 =>
          array (
              'id' => 52,
              'name' => 'thiet_ke_uml',
              'label' => 'Thiết kế UML',
          ),
          52 =>
          array (
              'id' => 53,
              'name' => 'ubuntu',
              'label' => 'Ubuntu',
          ),
          53 =>
          array (
              'id' => 54,
              'name' => 'unix',
              'label' => 'UNIX',
          ),
          54 =>
          array (
              'id' => 55,
              'name' => 'vb.net',
              'label' => 'VB.NET',
          ),
          55 =>
          array (
              'id' => 56,
              'name' => 'oracle',
              'label' => 'Oracle',
          ),
          56 =>
          array (
              'id' => 57,
              'name' => 'opencv',
              'label' => 'OpenCV',
          ),
          57 =>
          array (
              'id' => 58,
              'name' => 'virtual_cart',
              'label' => 'Virtual Cart',
          ),
          58 =>
          array (
              'id' => 59,
              'name' => 'windows_mobile',
              'label' => 'Windows Mobile',
          ),
          59 =>
          array (
              'id' => 60,
              'name' => 'wordpress',
              'label' => 'Wordpress',
          ),
          60 =>
          array (
              'id' => 61,
              'name' => 'wpf',
              'label' => 'WPF',
          ),
          61 =>
          array (
              'id' => 62,
              'name' => 'xml',
              'label' => 'XML',
          ),
          62 =>
          array (
              'id' => 63,
              'name' => 'yii_yii2',
              'label' => 'Yii/Yii2',
          ),
          63 =>
          array (
              'id' => 64,
              'name' => 'zend',
              'label' => 'Zend',
          ),
          64 =>
          array (
              'id' => 65,
              'name' => 'zendesk',
              'label' => 'Zendesk',
          ),
          65 =>
          array (
              'id' => 66,
              'name' => 'an_ninh_web',
              'label' => 'An ninh Web',
          ),
          66 =>
          array (
              'id' => 67,
              'name' => 'an_ninh_may_tinh',
              'label' => 'An ninh máy tính',
          ),
          67 =>
          array (
              'id' => 68,
              'name' => 'bao_mat_internet',
              'label' => 'Bảo mật Internet',
          ),
          68 =>
          array (
              'id' => 69,
              'name' => 'iis',
              'label' => 'IIS',
          ),
          69 =>
          array (
              'id' => 70,
              'name' => 'luu_tru_du_lieu',
              'label' => 'Lưu trữ dữ liệu',
          ),
          70 =>
          array (
              'id' => 71,
              'name' => 'lap_trinh_co_so_du_lieu',
              'label' => 'Lập trình cơ sở dữ liệu',
          ),
          71 =>
          array (
              'id' => 72,
              'name' => 'phan_mem_cai_san',
              'label' => 'Phần mềm cài sẵn',
          ),
          72 =>
          array (
              'id' => 73,
              'name' => 'phat_trien_co_so_du_lieu',
              'label' => 'Phát triển cơ sở dữ liệu',
          ),
          73 =>
          array (
              'id' => 74,
              'name' => 'quan_tri_co_so_du_lieu',
              'label' => 'Quản trị cơ sở dữ liệu',
          ),
          74 =>
          array (
              'id' => 75,
              'name' => 'quan_tri_mang',
              'label' => 'Quản trị mạng',
          ),
          75 =>
          array (
              'id' => 76,
              'name' => 'quan_tri_he_thong',
              'label' => 'Quản trị hệ thống',
          ),
          76 =>
          array (
              'id' => 77,
              'name' => 'quan_ly_website',
              'label' => 'Quản lý website',
          ),
          77 =>
          array (
              'id' => 78,
              'name' => 'trien_khai_erp',
              'label' => 'Triển khai ERP',
          ),
          78 =>
          array (
              'id' => 79,
              'name' => 'sua_laptop',
              'label' => 'Sửa laptop',
          ),
          79 =>
          array (
              'id' => 80,
              'name' => 'sua_may_tinh_de_ban',
              'label' => 'Sửa máy tính để bàn',
          ),
          80 =>
          array (
              'id' => 81,
              'name' => 'vmware',
              'label' => 'Vmware',
          ),
          81 =>
          array (
              'id' => 82,
              'name' => 'voicexml',
              'label' => 'VoiceXML',
          ),
          82 =>
          array (
              'id' => 83,
              'name' => 'voip',
              'label' => 'VoIP',
          ),
          83 =>
          array (
              'id' => 84,
              'name' => 'web_hosting',
              'label' => 'Web Hosting',
          ),
          84 =>
          array (
              'id' => 85,
              'name' => 'web_scraping',
              'label' => 'Web Scraping',
          ),
          85 =>
          array (
              'id' => 86,
              'name' => 'windows_8',
              'label' => 'Windows 8',
          ),
          86 =>
          array (
              'id' => 87,
              'name' => 'windows_api',
              'label' => 'Windows API',
          ),
          87 =>
          array (
              'id' => 88,
              'name' => 'youtube',
              'label' => 'YouTube',
          ),
          89 =>
          array (
              'id' => 90,
              'name' => 'autocad',
              'label' => 'AutoCAD',
          ),
          90 =>
          array (
              'id' => 91,
              'name' => 'cad_cam',
              'label' => 'CAD/CAM',
          ),
          91 =>
          array (
              'id' => 92,
              'name' => 'cong_nghe_hang_khong',
              'label' => 'Công nghệ hàng không',
          ),
          92 =>
          array (
              'id' => 93,
              'name' => 'cong_nghe_sinh_hoc',
              'label' => 'Công nghệ sinh học',
          ),
          93 =>
          array (
              'id' => 94,
              'name' => 'cong_nghe_sach',
              'label' => 'Công nghệ sạch',
          ),
          94 =>
          array (
              'id' => 95,
              'name' => 'co_dien_tu',
              'label' => 'Cơ điện tử ',
          ),
          95 =>
          array (
              'id' => 96,
              'name' => 'dia_chat_cong_trinh',
              'label' => 'Địa chất công trình',
          ),
          96 =>
          array (
              'id' => 97,
              'name' => 'ky_thuat_dau_khi',
              'label' => 'Kỹ thuật dầu khí',
          ),
          97 =>
          array (
              'id' => 98,
              'name' => 'ky_thuat_dien',
              'label' => 'Kỹ thuật điện',
          ),
          98 =>
          array (
              'id' => 99,
              'name' => 'ky_thuat_hoa_hoc',
              'label' => 'Kỹ thuật hóa học',
          ),
          99 =>
          array (
              'id' => 100,
              'name' => 'ky_thuat_phat_thanh',
              'label' => 'Kỹ thuật phát thanh',
          ),
          100 =>
          array (
              'id' => 101,
              'name' => 'ky_thuat_dan_dung',
              'label' => 'Kỹ thuật dân dụng',
          ),
          101 =>
          array (
              'id' => 102,
              'name' => 'ky_thuat_cong_nghep',
              'label' => 'Kỹ thuật công nghiệp',
          ),
          102 =>
          array (
              'id' => 103,
              'name' => 'ky_thuat_vat_lieu',
              'label' => 'Kỹ thuật vật liệu',
          ),
          103 =>
          array (
              'id' => 104,
              'name' => 'ky_thuat_co_khi',
              'label' => 'Kỹ thuật cơ khí',
          ),
          104 =>
          array (
              'id' => 105,
              'name' => 'ky_thuat_vien_thong',
              'label' => 'Kỹ thuật viễn thông',
          ),
          105 =>
          array (
              'id' => 106,
              'name' => 'ky_thuat_det_may',
              'label' => 'Kỹ thuật dệt may',
          ),
          106 =>
          array (
              'id' => 107,
              'name' => 'khoa_hoc_ve_khi_hau',
              'label' => 'Khoa học về khí hậu',
          ),
          107 =>
          array (
              'id' => 108,
              'name' => 'khoa_hoc_nhan_van',
              'label' => 'Khoa học Nhân văn',
          ),
          108 =>
          array (
              'id' => 109,
              'name' => 'khai_thac_du_lieu',
              'label' => 'Khai thác dữ liệu',
          ),
          109 =>
          array (
              'id' => 110,
              'name' => 'thiet_ke_vi_mach',
              'label' => 'Thiết kế vi mạch',
          ),
          110 =>
          array (
              'id' => 111,
              'name' => 'thiet_ke_nha',
              'label' => 'Thiết kế Nhà',
          ),
          111 =>
          array (
              'id' => 112,
              'name' => 'toan_hoc',
              'label' => 'Toán học',
          ),
          112 =>
          array (
              'id' => 113,
              'name' => 'vat_ly_hoc',
              'label' => 'Vật lý học',
          ),
          113 =>
          array (
              'id' => 114,
              'name' => 'y_khoa',
              'label' => 'Y khoa',
          ),
          114 =>
          array (
              'id' => 115,
              'name' => '2d',
              'label' => '2D',
          ),
          115 =>
          array (
              'id' => 116,
              'name' => '3d',
              'label' => '3D',
          ),
          116 =>
          array (
              'id' => 117,
              'name' => '3ds_max',
              'label' => '3ds Max',
          ),
          117 =>
          array (
              'id' => 118,
              'name' => 'adobe_dreamweaver',
              'label' => 'Adobe DreamWeaver',
          ),
          118 =>
          array (
              'id' => 119,
              'name' => 'adobe_photoshop',
              'label' => 'Adobe Photoshop',
          ),
          119 =>
          array (
              'id' => 120,
              'name' => 'am_nhac',
              'label' => 'Âm nhạc',
          ),
          120 =>
          array (
              'id' => 121,
              'name' => 'anh_hoa',
              'label' => 'Ảnh hóa',
          ),
          121 =>
          array (
              'id' => 122,
              'name' => 'chinh_sua_hinh_anh',
              'label' => 'Chỉnh sửa hình ảnh',
          ),
          122 =>
          array (
              'id' => 123,
              'name' => 'chinh_sua_video',
              'label' => 'Chỉnh sửa Video',
          ),
          123 =>
          array (
              'id' => 124,
              'name' => 'chup_anh_su_kien',
              'label' => 'Chụp ảnh sự kiện',
          ),
          124 =>
          array (
              'id' => 125,
              'name' => 'chup_chan_dung',
              'label' => 'Chụp chân dung',
          ),
          125 =>
          array (
              'id' => 126,
              'name' => 'chup_anh_cuoi',
              'label' => 'Chụp hình cưới',
          ),
          126 =>
          array (
              'id' => 127,
              'name' => 'chup_mau_san_pham',
              'label' => 'Chụp mẫu sản phẩm',
          ),
          127 =>
          array (
              'id' => 128,
              'name' => 'chup_phong_canh',
              'label' => 'Chụp phong cảnh',
          ),
          128 =>
          array (
              'id' => 129,
              'name' => 'coreldraw',
              'label' => 'CorelDraw',
          ),
          129 =>
          array (
              'id' => 130,
              'name' => 'danh_thiep',
              'label' => 'Danh thiếp',
          ),
          130 =>
          array (
              'id' => 131,
              'name' => 'dich_vu_audio',
              'label' => 'Dịch vụ audio',
          ),
          131 =>
          array (
              'id' => 132,
              'name' => 'dich_vu_video',
              'label' => 'Dịch vụ video',
          ),
          132 =>
          array (
              'id' => 133,
              'name' => 'dung_motion_video',
              'label' => 'Dựng motion video',
          ),
          133 =>
          array (
              'id' => 134,
              'name' => 'do_hoa_chuyen_dong',
              'label' => 'Đồ họa chuyển động',
          ),
          134 =>
          array (
              'id' => 135,
              'name' => 'dang_tai_video',
              'label' => 'Đăng tải video',
          ),
          135 =>
          array (
              'id' => 136,
              'name' => 'kien_truc_xay_dung',
              'label' => 'Kiến trúc xây dựng',
          ),
          136 =>
          array (
              'id' => 137,
              'name' => 'ky_thuat_phat_thanh',
              'label' => 'Kỹ thuật phát thanh',
          ),
          137 =>
          array (
              'id' => 138,
              'name' => 'ky_thuat_video',
              'label' => 'Kỹ thuật video',
          ),
          138 =>
          array (
              'id' => 139,
              'name' => 'flash_3d',
              'label' => 'Flash 3D',
          ),
          139 =>
          array (
              'id' => 140,
              'name' => 'lam_video_gioi_thieu_cong_ty',
              'label' => 'Làm video giới thiệu công ty',
          ),
          140 =>
          array (
              'id' => 141,
              'name' => 'lam_video_gioi_thieu_ve_san_pham',
              'label' => 'Làm video giới thiệu về sản phẩm',
          ),
          141 =>
          array (
              'id' => 142,
              'name' => 'lam_clip_ngan_cho_fanpage',
              'label' => 'Làm clip ngắn cho fanpage',
          ),
          142 =>
          array (
              'id' => 143,
              'name' => 'phoi_nhac',
              'label' => 'Phối nhạc',
          ),
          143 =>
          array (
              'id' => 144,
              'name' => 'quay_clip_am_nhac',
              'label' => 'Quay clip âm nhạc',
          ),
          144 =>
          array (
              'id' => 145,
              'name' => 'quay_clip_quang_cao',
              'label' => 'Quay clip quảng cáo',
          ),
          145 =>
          array (
              'id' => 146,
              'name' => 'quang_cao',
              'label' => 'Quảng cáo',
          ),
          146 =>
          array (
              'id' => 147,
              'name' => 'mo_hinh_thoi_trang',
              'label' => 'Mô hình thời trang',
          ),
          147 =>
          array (
              'id' => 148,
              'name' => 'nhiep_anh',
              'label' => 'Nhiếp ảnh',
          ),
          148 =>
          array (
              'id' => 149,
              'name' => 'in_an',
              'label' => 'In ấn',
          ),
          149 =>
          array (
              'id' => 150,
              'name' => 'tranh_biem_hoa_va_phim_hoat_hinh',
              'label' => 'Tranh biếm họa và Phim hoạt hình',
          ),
          151 =>
          array (
              'id' => 152,
              'name' => 'tranh_y_tuong',
              'label' => 'Tranh ý tưởng',
          ),
          152 =>
          array (
              'id' => 153,
              'name' => 'thu_cong_va_my_nghe',
              'label' => 'Thủ công và Mỹ nghệ',
          ),
          153 =>
          array (
              'id' => 154,
              'name' => 'thiet_ke_am_thanh',
              'label' => 'Thiết kế âm thanh',
          ),
          154 =>
          array (
              'id' => 155,
              'name' => 'thiet_ke_ao_phong_tshirt',
              'label' => 'Thiết kế áo phông, T-shirt',
          ),
          155 =>
          array (
              'id' => 156,
              'name' => 'thiet_ke_brochure',
              'label' => 'Thiết kế Brochure',
          ),
          156 =>
          array (
              'id' => 157,
              'name' => 'thiet_ke_banner',
              'label' => 'Thiết kế Banner',
          ),
          157 =>
          array (
              'id' => 158,
              'name' => 'thiet_ke_blog',
              'label' => 'Thiết kế Blog',
          ),
          158 =>
          array (
              'id' => 159,
              'name' => 'thiet_ke_sach_quang_cao',
              'label' => 'Thiết kế sách quảng cáo',
          ),
          159 =>
          array (
              'id' => 160,
              'name' => 'thiet_ke_sang_tao',
              'label' => 'Thiết kế sáng tạo',
          ),
          160 =>
          array (
              'id' => 161,
              'name' => 'thiet_ke_thoi_trang',
              'label' => 'Thiết kế thời trang',
          ),
          161 =>
          array (
              'id' => 162,
              'name' => 'thiet_ke_to_roi',
              'label' => 'Thiết kế tờ rơi',
          ),
          162 =>
          array (
              'id' => 163,
              'name' => 'thiet_ke_do_hoa',
              'label' => 'Thiết kế đồ họa',
          ),
          163 =>
          array (
              'id' => 164,
              'name' => 'thiet_ke_noi_that',
              'label' => 'Thiết kế nội thất',
          ),
          164 =>
          array (
              'id' => 165,
              'name' => 'thiet_ke_thiep_moi',
              'label' => 'Thiết kế thiếp mời',
          ),
          165 =>
          array (
              'id' => 166,
              'name' => 'thiet_ke_bao_bi',
              'label' => 'Thiết kế bao bì',
          ),
          166 =>
          array (
              'id' => 167,
              'name' => 'thiet_ke_nhan_hieu',
              'label' => 'Thiết kế nhãn hiệu',
          ),
          167 =>
          array (
              'id' => 168,
              'name' => 'thiet_ke_logo',
              'label' => 'Thiết kế logo',
          ),
          168 =>
          array (
              'id' => 169,
              'name' => 'thiet_ke_poster',
              'label' => 'Thiết kế Poster',
          ),
          169 =>
          array (
              'id' => 170,
              'name' => 'thiet_ke_van_phong_pham',
              'label' => 'Thiết kế văn phòng phẩm',
          ),
          170 =>
          array (
              'id' => 171,
              'name' => 'thiet_ke_hinh_dan_hinh_xam',
              'label' => 'Thiết kế hình dán hình xăm',
          ),
          171 =>
          array (
              'id' => 172,
              'name' => 'thiet_ke_theo_yeu_cau',
              'label' => 'Thiết kế theo yêu cầu',
          ),
          172 =>
          array (
              'id' => 173,
              'name' => 'thu_am',
              'label' => 'Thu âm',
          ),
          173 =>
          array (
              'id' => 174,
              'name' => 'thu_cong_va_my_nghe',
              'label' => 'Thủ công và Mỹ nghệ',
          ),
          174 =>
          array (
              'id' => 175,
              'name' => 'san_xuat_hoat_hinh',
              'label' => 'Sản xuất hoạt hình',
          ),
          175 =>
          array (
              'id' => 176,
              'name' => 'san_xuat_am_thanh',
              'label' => 'Sản xuất âm thanh',
          ),
          176 =>
          array (
              'id' => 177,
              'name' => 'san_xuat_video',
              'label' => 'Sản xuất video',
          ),
          177 =>
          array (
              'id' => 178,
              'name' => 've_hinh_minh_hoa',
              'label' => 'Vẽ hình minh họa',
          ),
          178 =>
          array (
              'id' => 179,
              'name' => 've_tay_theo_yeu_cau',
              'label' => 'Vẽ tay theo yêu cầu',
          ),
          179 =>
          array (
              'id' => 180,
              'name' => 'bao_chi',
              'label' => 'Báo chí',
          ),
          180 =>
          array (
              'id' => 181,
              'name' => 'bien_soan_tai_lieu',
              'label' => 'Biên soạn tài liệu',
          ),
          181 =>
          array (
              'id' => 182,
              'name' => 'bien_tap',
              'label' => 'Biên tập',
          ),
          182 =>
          array (
              'id' => 183,
              'name' => 'bien_tap_sach',
              'label' => 'Biên tập sách',
          ),
          183 =>
          array (
              'id' => 184,
              'name' => 'cap_nhat_noi_dung',
              'label' => 'Cập nhật nội dung',
          ),
          184 =>
          array (
              'id' => 185,
              'name' => 'chinh_sua_noi_dung',
              'label' => 'Chỉnh sửa nội dung',
          ),
          185 =>
          array (
              'id' => 186,
              'name' => 'chep_loi',
              'label' => 'Chép lời',
          ),
          186 =>
          array (
              'id' => 187,
              'name' => 'dich_bai_viet',
              'label' => 'Dịch bài viết',
          ),
          187 =>
          array (
              'id' => 188,
              'name' => 'dich_phu_de',
              'label' => 'Dịch phụ đề',
          ),
          188 =>
          array (
              'id' => 189,
              'name' => 'dich_sach',
              'label' => 'Dịch sách',
          ),
          189 =>
          array (
              'id' => 190,
              'name' => 'dich_tai_lieu_ky_thuat',
              'label' => 'Dịch tài liệu kỹ thuật',
          ),
          190 =>
          array (
              'id' => 191,
              'name' => 'dich_thuat',
              'label' => 'Dịch thuật',
          ),
          191 =>
          array (
              'id' => 192,
              'name' => 'dich_thuat_ban_luat',
              'label' => 'Dịch văn bản luật',
          ),
          192 =>
          array (
              'id' => 193,
              'name' => 'danh_may_van_ban',
              'label' => 'Đánh máy văn bản',
          ),
          193 =>
          array (
              'id' => 194,
              'name' => 'dang_bai_len_website',
              'label' => 'Đăng bài lên website',
          ),
          194 =>
          array (
              'id' => 195,
              'name' => 'dang_tin_rao_vat',
              'label' => 'Đăng tin rao vặt',
          ),
          195 =>
          array (
              'id' => 196,
              'name' => 'dang_bai_len_forum',
              'label' => 'Đăng bài lên Forum',
          ),
          196 =>
          array (
              'id' => 197,
              'name' => 'phien_dich',
              'label' => 'Phiên dịch',
          ),
          197 =>
          array (
              'id' => 198,
              'name' => 'quan_ly_blog_va_fanpage',
              'label' => 'Quản lý blog và fanpage',
          ),
          198 =>
          array (
              'id' => 199,
              'name' => 'seo_website',
              'label' => 'SEO website',
          ),
          199 =>
          array (
              'id' => 200,
              'name' => 'tu_van_pr_san_pham',
              'label' => 'Tư vấn PR sản phẩm',
          ),
          200 =>
          array (
              'id' => 201,
              'name' => 'viet_mo_ta_san_pham',
              'label' => 'Viết mô tả sản phẩm',
          ),
          201 =>
          array (
              'id' => 202,
              'name' => 'viet_don_thu',
              'label' => 'Viết đơn, thư',
          ),
          202 =>
          array (
              'id' => 203,
              'name' => 'viet_bai_pr',
              'label' => 'Viết bài PR',
          ),
          203 =>
          array (
              'id' => 204,
              'name' => 'viet_bai_review',
              'label' => 'Viết bài review',
          ),
          204 =>
          array (
              'id' => 205,
              'name' => 'viet_kich_ban',
              'label' => 'Viết kịch bản',
          ),
          205 =>
          array (
              'id' => 206,
              'name' => 'viet_bao_cao',
              'label' => 'Viết báo cáo',
          ),
          206 =>
          array (
              'id' => 207,
              'name' => 'viet_noi_dung_bao_chi',
              'label' => 'Viết nội dung báo chí',
          ),
          207 =>
          array (
              'id' => 208,
              'name' => 'viet_bai_phong_van_phat_bieu',
              'label' => 'Viết bài phỏng vấn, phát biểu',
          ),
          208 =>
          array (
              'id' => 209,
              'name' => 'viet_noi_dung_cho_blog',
              'label' => 'Viết nội dung cho blog',
          ),
          209 =>
          array (
              'id' => 210,
              'name' => 'viet_noi_dung_cho_website',
              'label' => 'Viết nội dung cho website',
          ),
          210 =>
          array (
              'id' => 211,
              'name' => 'viet_hoc_thuat',
              'label' => 'Viết học thuật',
          ),
          211 =>
          array (
              'id' => 212,
              'name' => 'viet_sach',
              'label' => 'Viết sách',
          ),
          212 =>
          array (
              'id' => 213,
              'name' => 'viet_bai_kinh_te',
              'label' => 'Viết bài kinh tế',
          ),
          213 =>
          array (
              'id' => 214,
              'name' => 'viet_du_lich',
              'label' => 'Viết du lịch',
          ),
          214 =>
          array (
              'id' => 215,
              'name' => 'viet_loi_quang_cao',
              'label' => 'Viết lời quảng cáo',
          ),
          215 =>
          array (
              'id' => 216,
              'name' => 'viet_sang_tao',
              'label' => 'Viết sáng tạo',
          ),
          216 =>
          array (
              'id' => 217,
              'name' => 'viet_don_xin_tai_tro',
              'label' => 'Viết đơn xin tài trợ',
          ),
          217 =>
          array (
              'id' => 218,
              'name' => 'viet_truyen_ngan',
              'label' => 'Viết truyện ngắn',
          ),
          218 =>
          array (
              'id' => 219,
              'name' => 'viet_dien_van',
              'label' => 'Viết diễn văn',
          ),
          219 =>
          array (
              'id' => 220,
              'name' => 'xuat_ban',
              'label' => 'Xuất bản',
          ),
          220 =>
          array (
              'id' => 221,
              'name' => 'ban_hang_online',
              'label' => 'Bán hàng online',
          ),
          221 =>
          array (
              'id' => 222,
              'name' => 'chien_luoc_cong_ty',
              'label' => 'Chiến lược công ty',
          ),
          222 =>
          array (
              'id' => 223,
              'name' => 'dao_tao_ky_ban_hang',
              'label' => 'Đào tạo kỹ năng bán hàng',
          ),
          223 =>
          array (
              'id' => 224,
              'name' => 'dao_tao_ky_nang_mem',
              'label' => 'Đào tạo kỹ năng mềm',
          ),
          224 =>
          array (
              'id' => 225,
              'name' => 'dao_tao_nghiep_vu_marketing',
              'label' => 'Đào tạo nghiệp vụ Marketing',
          ),
          225 =>
          array (
              'id' => 226,
              'name' => 'dao_tao_ky_nang_phuc_vu_khach_hang',
              'label' => 'Đào tạo kỹ năng phục vụ khách hàng',
          ),
          226 =>
          array (
              'id' => 227,
              'name' => 'khao_sat_thi_truong',
              'label' => 'Khảo sát thị trường',
          ),
          227 =>
          array (
              'id' => 228,
              'name' => 'ke_hoach_kinh_doanh',
              'label' => 'Kế hoạch kinh doanh',
          ),
          228 =>
          array (
              'id' => 229,
              'name' => 'lap_ke_hoach_quang_cao',
              'label' => 'Lập kế hoạch quảng cáo',
          ),
          229 =>
          array (
              'id' => 230,
              'name' => 'gioi_thieu_san_pham',
              'label' => 'Giới thiệu sản phẩm',
          ),
          230 =>
          array (
              'id' => 231,
              'name' => 'quang_cao_facebook',
              'label' => 'Quảng cáo facebook',
          ),
          231 =>
          array (
              'id' => 232,
              'name' => 'quang_ba_truyen_thong',
              'label' => 'Quảng bá truyền thông',
          ),
          232 =>
          array (
              'id' => 233,
              'name' => 'google_adsense',
              'label' => 'Google Adsense',
          ),
          233 =>
          array (
              'id' => 234,
              'name' => 'google_adwords',
              'label' => 'Google Adwords',
          ),
          234 =>
          array (
              'id' => 235,
              'name' => 'phan_tich_kinh_doanh',
              'label' => 'Phân tích kinh doanh',
          ),
          235 =>
          array (
              'id' => 236,
              'name' => 'nghien_cuu_thi_truong',
              'label' => 'Nghiên cứu thị trường ',
          ),
          236 =>
          array (
              'id' => 237,
              'name' => 'seo_cho_trang_web',
              'label' => 'SEO cho trang web',
          ),
          237 =>
          array (
              'id' => 238,
              'name' => 'tim_kiem_khach_hang',
              'label' => 'Tìm kiếm khách hàng',
          ),
          238 =>
          array (
              'id' => 239,
              'name' => 'tim_nguon_cung_ung_san_pham',
              'label' => 'Tìm nguồn cung ứng sản phẩm',
          ),
          239 =>
          array (
              'id' => 240,
              'name' => 'tim_kiem_qua_mang',
              'label' => 'Tìm kiếm qua mạng',
          ),
          240 =>
          array (
              'id' => 241,
              'name' => 'tiep_thi_qua_internet',
              'label' => 'Tiếp thị qua Internet',
          ),
          241 =>
          array (
              'id' => 242,
              'name' => 'tiep_thi_qua_mail',
              'label' => 'Tiếp thị qua mail',
          ),
          242 =>
          array (
              'id' => 243,
              'name' => 'to_chuc_su_kien',
              'label' => 'Tổ chức sự kiện',
          ),
          243 =>
          array (
              'id' => 244,
              'name' => 'tu_van_thanh_lap_doanh_nghiep',
              'label' => 'Tư vấn thành lập doanh nghiệp ',
          ),
          244 =>
          array (
              'id' => 245,
              'name' => 'tu_van_luat_doanh_nghiep',
              'label' => 'Tư vấn luật doanh nghiệp',
          ),
          245 =>
          array (
              'id' => 246,
              'name' => 'tu_van_tai_chinh_doanh_nghiep',
              'label' => 'Tư vấn tài chính doanh nghiệp',
          ),
          246 =>
          array (
              'id' => 247,
              'name' => 'tu_van_quan_tri_doanh_nghiep',
              'label' => 'Tư vấn quản trị doanh nghiệp',
          ),
          247 =>
          array (
              'id' => 248,
              'name' => 'tu_van_thue_doanh_nghiep',
              'label' => 'Tư vấn thuế doanh nghiệp',
          ),
          248 =>
          array (
              'id' => 249,
              'name' => 'tu_van_quang_ba_thuong_hieu',
              'label' => 'Tư vấn quảng bá thương hiệu',
          ),
          249 =>
          array (
              'id' => 250,
              'name' => 'tu_van_san_pham',
              'label' => 'Tư vấn sản phẩm',
          ),
          250 =>
          array (
              'id' => 251,
              'name' => 'tu_van_mo_hinh_kinh_doanh',
              'label' => 'Tư vấn mô hình kinh doanh',
          ),
          251 =>
          array (
              'id' => 252,
              'name' => 'tro_ly_tu_xa',
              'label' => 'Trợ lý từ xa',
          ),
          252 =>
          array (
              'id' => 253,
              'name' => 'tao_thuong hieu',
              'label' => 'Tạo thương hiệu',
          ),
          253 =>
          array (
              'id' => 254,
              'name' => 'cham_soc_khach_hang',
              'label' => 'Chăm sóc khách hàng',
          ),
          254 =>
          array (
              'id' => 255,
              'name' => 'captcha',
              'label' => 'Captcha',
          ),
          255 =>
          array (
              'id' => 256,
              'name' => 'excel',
              'label' => 'Excel',
          ),
          256 =>
          array (
              'id' => 257,
              'name' => 'ho_tro_khach_hang',
              'label' => 'Hỗ trợ khách hàng',
          ),
          257 =>
          array (
              'id' => 258,
              'name' => 'ho_tro_dien_thoai',
              'label' => 'Hỗ trợ điện thoại',
          ),
          258 =>
          array (
              'id' => 259,
              'name' => 'ho_tro_ki_thuat',
              'label' => 'Hỗ trợ kĩ thuật',
          ),
          259 =>
          array (
              'id' => 260,
              'name' => 'microsoft_office',
              'label' => 'Microsoft Office',
          ),
          260 =>
          array (
              'id' => 261,
              'name' => 'microsoft_outlook',
              'label' => 'Microsoft Outlook',
          ),
          261 =>
          array (
              'id' => 262,
              'name' => 'nhap_du_lieu_website',
              'label' => 'Nhập dữ liệu website',
          ),
          262 =>
          array (
              'id' => 263,
              'name' => 'nhap_du_lieu_bang_tinh',
              'label' => 'Nhập dữ liệu bảng tính',
          ),
          263 =>
          array (
              'id' => 264,
              'name' => 'quan_ly_thoi_gian',
              'label' => 'Quản lý thời gian',
          ),
          264 =>
          array (
              'id' => 265,
              'name' => 'xu_ly_du_lieu',
              'label' => 'Xử lý dữ liệu',
          ),
          265 =>
          array (
              'id' => 266,
              'name' => 'xu_ly_email',
              'label' => 'Xử lý email',
          ),
          266 =>
          array (
              'id' => 267,
              'name' => 'xu_ly_don_hang',
              'label' => 'Xử lý đơn hàng',
          ),
          267 =>
          array (
              'id' => 268,
              'name' => 'xu_ly_dien_thoai',
              'label' => 'Xử lý điện thoại ',
          ),
          268 =>
          array (
              'id' => 269,
              'name' => 'tro_ly_truc_tuyen',
              'label' => 'Trợ lý trực tuyến',
          ),
          269 =>
          array (
              'id' => 270,
              'name' => 'bao_cao_tai_chinh',
              'label' => 'Báo cáo tài chính',
          ),
          270 =>
          array (
              'id' => 271,
              'name' => 'bao_cao_thue_doanh_nghiep',
              'label' => 'Báo cáo thuế doanh nghiệp',
          ),
          271 =>
          array (
              'id' => 272,
              'name' => 'bat_dong_san',
              'label' => 'Bất động sản',
          ),
          272 =>
          array (
              'id' => 273,
              'name' => 'lap_ho_so_goi_von',
              'label' => 'Lập hồ sơ gọi vốn',
          ),
          273 =>
          array (
              'id' => 274,
              'name' => 'hop_dong',
              'label' => 'Hợp đồng',
          ),
          274 =>
          array (
              'id' => 275,
              'name' => 'ke_toan',
              'label' => 'Kế toán',
          ),
          275 =>
          array (
              'id' => 276,
              'name' => 'kiem_toan',
              'label' => 'Kiểm toán',
          ),
          276 =>
          array (
              'id' => 277,
              'name' => 'kinh_doanh',
              'label' => 'Kinh doanh',
          ),
          277 =>
          array (
              'id' => 278,
              'name' => 'luat_lao_dong',
              'label' => 'Luật lao động',
          ),
          278 =>
          array (
              'id' => 279,
              'name' => 'luat_nha_dat',
              'label' => 'Luật nhà đất',
          ),
          279 =>
          array (
              'id' => 280,
              'name' => 'luat_thue',
              'label' => 'Luật thuế',
          ),
          280 =>
          array (
              'id' => 281,
              'name' => 'gay_quy',
              'label' => 'Gây quỹ',
          ),
          281 =>
          array (
              'id' => 282,
              'name' => 'quan_ly_nhan_su',
              'label' => 'Quản lý nhân sự',
          ),
          282 =>
          array (
              'id' => 283,
              'name' => 'quan_ly_kho_hang',
              'label' => 'Quản lý kho hàng',
          ),
          283 =>
          array (
              'id' => 284,
              'name' => 'quan_ly_rui_ro',
              'label' => 'Quản lý rủi ro',
          ),
          284 =>
          array (
              'id' => 285,
              'name' => 'phap_ly',
              'label' => 'Pháp lý',
          ),
          285 =>
          array (
              'id' => 286,
              'name' => 'phan_tich_kinh_doanh',
              'label' => 'Phân tích kinh doanh',
          ),
          286 =>
          array (
              'id' => 287,
              'name' => 'phan_tich_tai_chinh',
              'label' => 'Phân tích tài chính',
          ),
          287 =>
          array (
              'id' => 288,
              'name' => 'nhan_su',
              'label' => 'Nhân sự',
          ),
          288 =>
          array (
              'id' => 289,
              'name' => 'nghien_cuu_phap_ly',
              'label' => 'Nghiên cứu pháp lý',
          ),
          289 =>
          array (
              'id' => 290,
              'name' => 'tai_chinh',
              'label' => 'Tài chính',
          ),
          290 =>
          array (
              'id' => 291,
              'name' => 'thue',
              'label' => 'Thuế',
          ),
          291 =>
          array (
              'id' => 292,
              'name' => 'thu_tuc_visa',
              'label' => 'Thủ tục visa',
          ),
          292 =>
          array (
              'id' => 293,
              'name' => 'tuyen_dung',
              'label' => 'Tuyển dụng',
          ),
          293 =>
          array (
              'id' => 294,
              'name' => 'viet_van_ban_quy_pham_phap_luat',
              'label' => 'Viêt văn bản quy phạm pháp luật',
          ),
        ));
    }
}
