<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->foreign('district_id', 'fk_users_districts1')->references('id')->on('districts')->onUpdate('CASCADE');
			$table->foreign('role_id', 'fk_users_role1')->references('id')->on('roles')->onUpdate('CASCADE');
			$table->foreign('user_status_id', 'fk_users_user_statuses1')->references('id')->on('user_statuses')->onUpdate('CASCADE');
		});

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropForeign('fk_users_districts1');
			$table->dropForeign('fk_users_role1');
			$table->dropForeign('fk_users_user_statuses1');
		});
	}

}
