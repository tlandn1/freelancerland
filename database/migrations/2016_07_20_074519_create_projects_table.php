<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->text('description', 65535)->nullable();
			$table->dateTime('end_receive_bid')->nullable();
			$table->integer('budget')->nullable();
			$table->integer('second_category_id')->index('fk_projects_second_categories1_idx')->unsigned();
			$table->integer('city_id')->index('fk_projects_cities1_idx')->unsigned();
			$table->integer('budget_estimate_id')->index('fk_projects_budget_estimate1_idx')->unsigned();
			$table->integer('project_status_id')->index('fk_projects_project_statuses1_idx')->unsigned();
			$table->integer('user_id')->unsigned()->index('fk_projects_users1_idx');
			$table->string('code')->nullable();
			$table->string('slug')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects');
	}

}
