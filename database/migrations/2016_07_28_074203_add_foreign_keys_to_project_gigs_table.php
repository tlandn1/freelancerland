<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToProjectGigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_gigs', function(Blueprint $table)
        {
            $table->foreign('user_id', 'fk_project_gigs_user1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('gig_id', 'fk_project_gigs_gig1')->references('id')->on('gigs')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('project_gig_status_id', 'fk_project_gigs_project_gig_status1')->references('id')->on('project_gig_statuses')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_gigs', function(Blueprint $table)
        {
            $table->dropForeign('fk_project_gigs_user1');
            $table->dropForeign('fk_project_gigs_gig1');
            $table->dropForeign('fk_project_gigs_project_gig_status1');
        });
    }
}
