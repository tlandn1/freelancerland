<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersSecondCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users_second_categories', function(Blueprint $table)
		{
			$table->foreign('second_category_id', 'fk_users_has_second_categories_second_categories1')->references('id')->on('second_categories')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'fk_users_has_second_categories_users1')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users_second_categories', function(Blueprint $table)
		{
			$table->dropForeign('fk_users_has_second_categories_second_categories1');
			$table->dropForeign('fk_users_has_second_categories_users1');
		});
	}

}
