<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEducationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('education', function(Blueprint $table)
		{
			$table->foreign('user_id', 'fk_education_users1')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('education', function(Blueprint $table)
		{
			$table->dropForeign('fk_education_users1');
		});
	}

}
