<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkingCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('working_companies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('company_name');
			$table->string('position');
			$table->text('description', 65535);
			$table->date('from')->nullable();
			$table->date('to')->nullable();
			$table->integer('user_id')->index('fk_working_companies_users1_idx')->unsigned();
		});

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('working_companies');
	}

}
