<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gigs', function(Blueprint $table)
        {
            $table->increments('id', true);
            $table->string('title');
            $table->text('description', 65535)->nullable();
            $table->integer('price')->nullable();
            $table->integer('deliver_day')->nullable();
            $table->string('slug')->nullable();
            $table->integer('user_id')->index('fk_gigs_user1_idx')->unsigned()->nullable();
            $table->integer('gig_status_id')->index('fk_gigs_status1_idx')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gigs');
    }
}
