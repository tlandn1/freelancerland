<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProjectsSkillsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('projects_skills', function(Blueprint $table)
		{
			$table->foreign('project_id', 'fk_projects_has_skills_projects1')->references('id')->on('projects')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('skill_id', 'fk_projects_has_skills_skills1')->references('id')->on('skills')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('projects_skills', function(Blueprint $table)
		{
			$table->dropForeign('fk_projects_has_skills_projects1');
			$table->dropForeign('fk_projects_has_skills_skills1');
		});
	}

}
