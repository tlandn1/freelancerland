<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSecondCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('second_categories', function(Blueprint $table)
		{
			$table->foreign('first_category_id', 'fk_second_categories_first_categories1')->references('id')->on('first_categories')->onUpdate('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('second_categories', function(Blueprint $table)
		{
			$table->dropForeign('fk_second_categories_first_categories1');
		});
	}

}
