<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersSkillsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_skills', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->index('fk_users_has_skills_users1_idx')->unsigned();
			$table->integer('skill_id')->index('fk_users_has_skills_skills1_idx')->unsigned();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_skills');
	}

}
