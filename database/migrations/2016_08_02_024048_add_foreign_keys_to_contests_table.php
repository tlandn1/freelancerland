<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToContestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contests', function(Blueprint $table)
		{
			$table->foreign('contests_regulation_id', 'fk_contests_contests_regulation1')->references('id')->on('contests_regulation')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('contests_service_id', 'fk_contests_contests_service1')->references('id')->on('contests_service')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'fk_contests_users1')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contests', function(Blueprint $table)
		{
			$table->dropForeign('fk_contests_contests_regulation1');
			$table->dropForeign('fk_contests_contests_service1');
			$table->dropForeign('fk_contests_users1');
		});
	}

}
