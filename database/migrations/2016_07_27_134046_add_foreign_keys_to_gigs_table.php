<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToGigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gigs', function(Blueprint $table)
        {
            $table->foreign('user_id', 'fk_gigs_user1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('gig_status_id', 'fk_gigs_status1')->references('id')->on('gig_statuses')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gigs', function(Blueprint $table)
        {
            $table->dropForeign('fk_gigs_user1');
            $table->dropForeign('fk_gigs_status1');
        });
    }
}
