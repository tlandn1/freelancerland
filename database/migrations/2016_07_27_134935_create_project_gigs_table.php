<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectGigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_gigs', function(Blueprint $table)
        {
            $table->increments('id', true);
            $table->integer('user_id')->index('fk_project_gigs_user1_idx')->unsigned();
            $table->integer('gig_id')->index('fk_project_gigs_gig1_idx')->unsigned();
            $table->integer('project_gig_status_id')->index('fk_project_gigs_project_gig_status1_idx')->unsigned();
            $table->text('user_review_text', 65535)->nullable();
            $table->float('user_review_rating')->nullable();
            $table->timestamp('user_review_time')->nullable();
            $table->text('seller_review_text', 65535)->nullable();
            $table->float('seller_review_rating')->nullable();
            $table->timestamp('seller_review_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('project_gigs');
    }
}
