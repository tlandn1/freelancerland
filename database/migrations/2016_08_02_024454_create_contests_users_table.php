<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContestsUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contests_users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('contests_id')->index('fk_contests_has_users_contests1_idx')->unsigned();
			$table->integer('user_id')->index('fk_contests_has_users_users1_idx')->unsigned();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contests_users');
	}

}
