<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToGigsExtraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gigs_extra', function(Blueprint $table)
        {
            $table->foreign('gig_id', 'fk_gigs_extra_gig1')->references('id')->on('gigs')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gigs_extra', function(Blueprint $table)
        {
            $table->dropForeign('fk_gigs_extra_gig1');
        });
    }
}
