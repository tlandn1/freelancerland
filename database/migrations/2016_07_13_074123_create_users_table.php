<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('email')->unique('email_UNIQUE');
			$table->string('password');
			$table->string('provider_id')->nullable();
			$table->string('provider')->nullable();
			$table->string('avatar');
			$table->boolean('receive_email')->nullable();
			$table->string('phone')->nullable();
			$table->string('remember_token')->nullable();
			$table->timestamps();
			$table->text('note', 65535)->nullable();
			$table->integer('sodu_tk')->nullable();
			$table->string('skype')->nullable();
			$table->string('facebook')->nullable();
			$table->string('website')->nullable();
			$table->integer('role_id')->index('fk_users_role1_idx')->unsigned();
			$table->integer('district_id')->index('fk_users_districts1_idx')->unsigned()->nullable();
			$table->integer('user_status_id')->index('fk_users_user_statuses1_idx')->unsigned();
			$table->string('cmnd_number')->nullable();
			$table->date('cmnd_date_of_birth')->nullable();
			$table->string('cmnd_address')->nullable();
			$table->string('about_job_title')->nullable();
			$table->text('about_description', 65535)->nullable();
		});

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
