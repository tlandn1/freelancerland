<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNaptienTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('naptien', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('naptien_code')->nullable();
			$table->integer('sotien')->nullable();
			$table->text('note', 65535)->nullable();
			$table->string('status')->nullable();
			$table->timestamps();
			$table->integer('user_id')->index('fk_naptien_users1_idx')->unsigned();
		});

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('naptien');
	}

}
