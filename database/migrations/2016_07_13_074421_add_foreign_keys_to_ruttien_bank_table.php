<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRuttienBankTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ruttien_bank', function(Blueprint $table)
		{
			$table->foreign('user_id', 'fk_ruttien_bank_users1')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ruttien_bank', function(Blueprint $table)
		{
			$table->dropForeign('fk_ruttien_bank_users1');
		});
	}

}
