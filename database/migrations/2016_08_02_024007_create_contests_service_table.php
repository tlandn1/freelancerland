<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContestsServiceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contests_service', function(Blueprint $table)
		{
			$table->increments('id', true);
			$table->string('name', 45);
			$table->integer('contests_field_id')->index('fk_contests_service_contests_field1_idx')->unsigned();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contests_service');
	}

}
