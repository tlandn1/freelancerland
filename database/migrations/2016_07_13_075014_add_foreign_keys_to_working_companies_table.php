<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToWorkingCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('working_companies', function(Blueprint $table)
		{
			$table->foreign('user_id', 'fk_working_companies_users1')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('working_companies', function(Blueprint $table)
		{
			$table->dropForeign('fk_working_companies_users1');
		});
	}

}
