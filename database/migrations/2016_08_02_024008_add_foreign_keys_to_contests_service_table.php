<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToContestsServiceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contests_service', function(Blueprint $table)
		{
			$table->foreign('contests_field_id', 'fk_contests_service_contests_field1')->references('id')->on('contests_field')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contests_service', function(Blueprint $table)
		{
			$table->dropForeign('fk_contests_service_contests_field1');
		});
	}

}
