<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersSecondCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_second_categories', function(Blueprint $table)
		{
			$table->increments('id', true);
			$table->integer('user_id')->index('fk_users_has_second_categories_users1_idx')->unsigned();
			$table->integer('second_category_id')->index('fk_users_has_second_categories_second_categories1_idx')->unsigned();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_second_categories');
	}

}
