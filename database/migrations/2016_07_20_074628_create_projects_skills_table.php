<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsSkillsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects_skills', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('project_id')->index('fk_projects_has_skills_projects1_idx')->unsigned();
			$table->integer('skill_id')->index('fk_projects_has_skills_skills1_idx')->unsigned();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects_skills');
	}

}
