<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersSkillsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users_skills', function(Blueprint $table)
		{
			$table->foreign('skill_id', 'fk_users_has_skills_skills1')->references('id')->on('skills')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'fk_users_has_skills_users1')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users_skills', function(Blueprint $table)
		{
			$table->dropForeign('fk_users_has_skills_skills1');
			$table->dropForeign('fk_users_has_skills_users1');
		});
	}

}
