<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_projects', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->index('fk_users_has_projects_users1_idx')->unsigned();
			$table->integer('project_id')->index('fk_users_has_projects_projects1_idx')->unsigned();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_projects');
	}

}
