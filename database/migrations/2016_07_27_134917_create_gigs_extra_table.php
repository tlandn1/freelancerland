<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGigsExtraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gigs_extra', function(Blueprint $table)
        {
            $table->increments('id', true);
            $table->string('title');
            $table->integer('price')->nullable();
            $table->integer('deliver_day')->nullable();
            $table->integer('gig_id')->index('fk_gigs_extra_gig1_idx')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gigs_extra');
    }
}
