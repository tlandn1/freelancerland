<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToContestsUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contests_users', function(Blueprint $table)
		{
			$table->foreign('contests_id', 'fk_contests_has_users_contests1')->references('id')->on('contests')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'fk_contests_has_users_users1')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contests_users', function(Blueprint $table)
		{
			$table->dropForeign('fk_contests_has_users_contests1');
			$table->dropForeign('fk_contests_has_users_users1');
		});
	}

}
