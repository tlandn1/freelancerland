<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contests', function(Blueprint $table)
		{
			$table->increments('id', true);
			$table->string('title');
			$table->text('description', 65535);
			$table->integer('budget')->nullable();
			$table->date('time_end_contest')->nullable();
			$table->integer('user_id')->index('fk_contests_users1_idx')->unsigned();
			$table->integer('contests_regulation_id')->index('fk_contests_contests_regulation1_idx')->unsigned();
			$table->integer('contests_service_id')->index('fk_contests_contests_service1_idx')->unsigned();
			$table->string('slug')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contests');
	}

}
