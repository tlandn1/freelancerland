<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSecondCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('second_categories', function(Blueprint $table)
		{
			$table->increments('id', true);
			$table->string('name');
			$table->string('slug')->nullable();
			$table->integer('first_category_id')->index('fk_second_categories_first_categories1_idx')->unsigned();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('second_categories');
	}

}
