<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('projects', function(Blueprint $table)
		{
			$table->foreign('budget_estimate_id', 'fk_projects_budget_estimate1')->references('id')->on('budget_estimate')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('city_id', 'fk_projects_cities1')->references('id')->on('cities')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('project_status_id', 'fk_projects_project_statuses1')->references('id')->on('project_statuses')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('second_category_id', 'fk_projects_second_categories1')->references('id')->on('second_categories')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'fk_projects_users1')->references('id')->on('users')->onUpdate('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('projects', function(Blueprint $table)
		{
			$table->dropForeign('fk_projects_budget_estimate1');
			$table->dropForeign('fk_projects_cities1');
			$table->dropForeign('fk_projects_project_statuses1');
			$table->dropForeign('fk_projects_second_categories1');
			$table->dropForeign('fk_projects_users1');
		});
	}

}
