// This is included with the Parsley library itself,
// thus there is no use in adding it to your project.


Parsley.addMessages('vi', {
    defaultMessage: "Giá trị nhập vào không hợp lệ",
    type: {
        email: "Vui lòng nhập email của bạn",
        url: "url không hợp lệ",
        number: "Giá trị nhập vào phải là số",
        integer: "Giá trị nhập vào phải là số nguyên",
        digits: "Giá trị nhập vào phải là các chữ số",
        alphanum: "Giá trị nhập vào là chữ và số"
    },
    notblank: "Ô này không thể trống",
    required: "Vui lòng nhập vào (hoặc chọn) giá trị",
    pattern: "Email nhập vào không hợp lệ",
    min: "Giá trị nhập vào nhỏ hơn hoặc bằng %s.",
    max: "Giá trị nhập vào lớn hơn hoặc bằng %s.",
    range: "Giá trị nhập vào từ %s đến %s.",
    minlength: "Nhập tối thiểu %s ký tự",
    maxlength: "Nhập tối đa %s ký tự",
    length: "Độ dài chuổi nhập vào không hợp lệ. Số ký tự nhập vào từ %s đến %s ký tự",
    mincheck: "Bạn phải chọn ít nhất %s lựa chọn",
    maxcheck: "Bạn phải chọn %s lựa chọn hoặc ít hơn",
    check: "Bạn phải lựa chọn từ %s đến %s lựa chọn.",
    equalto: "This value should be the same."
});

Parsley.setLocale('vi');