function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

var jsEncode = {
    encode: function(s, k) {
        var enc = "";
        var str = "";
        // make sure that input is string
        str = s.toString();
        for (var i = 0; i < s.length; i++) {
            // create block
            var a = s.charCodeAt(i);
            // bitwise XOR
            var b = a ^ k;
            enc = enc + String.fromCharCode(b);
        }
        return enc;
    }
};
var firstSecondCategoryObj, citiesDistrictsObj;

$(document).ready(function() {
    // Create Base64 Object
    var Base64 = {
        _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
        encode: function(e) {
            var t = "";
            var n, r, i, s, o, u, a;
            var f = 0;
            e = Base64._utf8_encode(e);
            while (f < e.length) {
                n = e.charCodeAt(f++);
                r = e.charCodeAt(f++);
                i = e.charCodeAt(f++);
                s = n >> 2;
                o = (n & 3) << 4 | r >> 4;
                u = (r & 15) << 2 | i >> 6;
                a = i & 63;
                if (isNaN(r)) {
                    u = a = 64
                } else if (isNaN(i)) {
                    a = 64
                }
                t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
            }
            return t
        },
        decode: function(e) {
            var t = "";
            var n, r, i;
            var s, o, u, a;
            var f = 0;
            e = e.replace(/[^A-Za-z0-9+/=]/g, "");
            while (f < e.length) {
                s = this._keyStr.indexOf(e.charAt(f++));
                o = this._keyStr.indexOf(e.charAt(f++));
                u = this._keyStr.indexOf(e.charAt(f++));
                a = this._keyStr.indexOf(e.charAt(f++));
                n = s << 2 | o >> 4;
                r = (o & 15) << 4 | u >> 2;
                i = (u & 3) << 6 | a;
                t = t + String.fromCharCode(n);
                if (u != 64) {
                    t = t + String.fromCharCode(r)
                }
                if (a != 64) {
                    t = t + String.fromCharCode(i)
                }
            }
            t = Base64._utf8_decode(t);
            return t
        },
        _utf8_encode: function(e) {
            e = e.replace(/rn/g, "n");
            var t = "";
            for (var n = 0; n < e.length; n++) {
                var r = e.charCodeAt(n);
                if (r < 128) {
                    t += String.fromCharCode(r)
                } else if (r > 127 && r < 2048) {
                    t += String.fromCharCode(r >> 6 | 192);
                    t += String.fromCharCode(r & 63 | 128)
                } else {
                    t += String.fromCharCode(r >> 12 | 224);
                    t += String.fromCharCode(r >> 6 & 63 | 128);
                    t += String.fromCharCode(r & 63 | 128)
                }
            }
            return t
        },
        _utf8_decode: function(e) {
            var t = "";
            var n = 0;
            var r = c1 = c2 = 0;
            while (n < e.length) {
                r = e.charCodeAt(n);
                if (r < 128) {
                    t += String.fromCharCode(r);
                    n++
                } else if (r > 191 && r < 224) {
                    c2 = e.charCodeAt(n + 1);
                    t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                    n += 2
                } else {
                    c2 = e.charCodeAt(n + 1);
                    c3 = e.charCodeAt(n + 2);
                    t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                    n += 3
                }
            }
            return t
        }
    };
    var key = "123";
    if (typeof firstSecondCategoryJSON != 'undefined') {
        firstSecondCategoryObj = JSON.parse(jsEncode.encode(Base64.decode(firstSecondCategoryJSON), key));
    }
    if (typeof citiesDistrictsJSON != 'undefined') {
        citiesDistrictsObj = JSON.parse(jsEncode.encode(Base64.decode(citiesDistrictsJSON), key));
    }
});
// End  Encrypt Decrypt Stuffs

// First time parse correct secondary fields:
// selectbox, selectbox_parent, selectbox_child, textbox
// function setFirstOptionSelected(className, triggerChange) {
//     // Default value of triggerChange is false
//     triggerChange = typeof triggerChange !== 'undefined' ? triggerChange : false;
//
//     var element = $(className);
//     element.find('option:eq(0)').prop('selected', true);
//
//     if (triggerChange) {
//         if (element.attr('type') == 'text') {
//             element.trigger('keyup');
//         } else {
//             element.trigger('change');
//         }
//     }
//
// }

/**********************************************
          select City, District
***********************************************/

$(document).ready(function() {
    // Dynamic change Cities, District
    $('.city_id').change(function() {
        // Remove all divSecondCategoryField
        var targetSelectBox = $('[name=district_id]');
        targetSelectBox.empty();

        // console.log(citiesDistrictsObj[$(this).val()]);
        $.each(citiesDistrictsObj[$(this).val()], function(index, element) {
            // console.log(index + element);
            targetSelectBox.append("<option value='" + index + "'>" + element + "</option>");
        });

        // Set default value Select2 District selectbox
        targetSelectBox.trigger('change.select2');

    });

});

/**********************************************
       select firstCategory, secondCategory
***********************************************/
$(document).ready(function() {
    // Dynamic change Cities, District
    $('.fll-firstCategory').change(function() {
        // Remove all divSecondCategoryField
        var targetSelectBox = $('[name=second_category_id]');
        targetSelectBox.empty();

        // console.log(citiesDistrictsObj[$(this).val()]);
        $.each(firstSecondCategoryObj[$(this).val()], function(index, element) {
            // console.log(index + element);
            targetSelectBox.append("<option value='" + index + "'>" + element + "</option>");
        });

        // Set default value Select2 District selectbox
        targetSelectBox.trigger('change.select2');

    });

});