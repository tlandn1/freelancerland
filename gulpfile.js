var elixir = require('laravel-elixir');
var gulp = require('gulp');
var csslint = require('gulp-csslint');
var prettify = require('gulp-jsbeautifier');
var exec = require('child_process').exec;
var phplint = require('gulp-phplint');
var argv = require('yargs').argv;
var gulpif = require('gulp-if');
var cleanCss = require('gulp-clean-css');
var uglifyJs = require('gulp-uglify');
var concat = require('gulp-concat');

//var noimagemin = (argv.noimagemin) ? true : false;
//console.log(noimagemin);
//return 0;

require('laravel-elixir-remove');
require('elixir-jshint');
require('laravel-elixir-imagemin');

elixir.config.css.autoprefix.options = {
    browsers: ['last 3 versions', 'IE >= 9'],
    cascade: false
};

var paths = {
    'jquery': 'resources/assets/vendor/jquery',
    'bootstrap': 'resources/assets/vendor//bootstrap-sass/assets',
    'iCheck': 'resources/assets/vendor/iCheck',
    'datatable': 'resources/assets/vendor/datatables',
    'select2': 'resources/assets/vendor/select2',
    'dropzone': 'resources/assets/vendor/dropzone',
    'js': 'resources/assets/js',
    'croppic': 'resources/assets/vendor/croppic',
    'sweetalert2': 'resources/assets/vendor/sweetalert2',
    'parsleyjs': 'resources/assets/vendor/parsleyjs',
    'bxslider': 'resources/assets/vendor/bxslider',
    'blockui': 'resources/assets/vendor/blockui',
    'jqueryMaskPlugin': 'resources/assets/vendor/jquery-mask-plugin',
    'twitterBootstrapWizard': 'resources/assets/vendor/twitter-bootstrap-wizard',
    'remUnitPolyfill': 'resources/assets/vendor/rem-unit-polyfill',
    'jssocials': 'resources/assets/vendor/jssocials',
    'trumbowyg': 'resources/assets/vendor/trumbowyg',
    'jQueryWaccp': 'resources/assets/vendor/jQuery-Word-and-Character-Counter-Plugin',
    'fontAwesome': 'resources/assets/vendor/font-awesome',
    'slick': 'resources/assets/vendor/slick-carousel',
    'bootstrapDatetimepicker': 'resources/assets/vendor/eonasdan-bootstrap-datetimepicker-npm',
    'typeahead' : 'resources/assets/vendor/typeahead.js',
    'bootstrapTagsinput' : 'resources/assets/vendor/bootstrap-tagsinput',
    'bootstrapDatetimepicker': 'resources/assets/vendor/eonasdan-bootstrap-datetimepicker-npm',
    'moment': 'resources/assets/vendor/moment',
    'bootbox': 'resources/assets/vendor/bootbox',
    'jqueryBarRating': 'resources/assets/vendor/jquery-bar-rating',
	'listJs': 'resources/assets/vendor/list.js'
};

elixir(function (mix) {
    // Clean house
    if (!argv.noremove) {
        mix.remove([
            'public/css/backend/*.*',
            'public/js/backend/*.*',
            'public/images/**/*.*',
            'public/css/frontend/*.*',
            'public/js/frontend/*.*',
        ]);
    }

    // Optimize images, auto copy to public/images after optimized
    if (!argv.noimagemin) {
        mix.imagemin();
    }

    // Prettify SCSS files
    gulp.task('prettify-scss', function () {
        gulp.src(['resources/assets/sass/**/*.scss'])
                .pipe(prettify())
                .pipe(gulp.dest('resources/assets/sass'));
    });
    mix.task('prettify-scss');

    // Prettify JS files
    gulp.task('prettify-js', function () {
        gulp.src(['resources/assets/js/**/*.js'])
                .pipe(prettify())
                .pipe(gulp.dest('resources/assets/js'));
    });
    mix.task('prettify-js');

    // PHP CS Fixer, install with composer global require fabpot/php-cs-fixer
    // Run pretty slow, so don't need to run frequently
    // php-cs-fixer fix --config-file=.php_cs
    gulp.task('php-cs-fixer', function (cb) {
        exec('php-cs-fixer fix --config-file=.php_cs', function (err, stdout, stderr) {
            console.log(stdout);
            console.log(stderr);
            cb(err);
        });
    });
    // mix.task('php-cs-fixer');

    // Copy resources images to public images, don't need because of imagemin
    // mix.copy('resources/assets/images', 'public/images');

    // AutoPrefixer already included in Elixir, so no need to add, see
    // elixir.config.css.autoprefix.options above

    // Copy frontend CSS files
    mix.copy([
        paths.select2 + '/select2.min.css',
        paths.dropzone + '/min/dropzone.min.css',
        paths.croppic + '/assets/css/main-croppic.css',
        paths.croppic + '/assets/css/croppic.css',
        paths.sweetalert2 + '/sweetalert2.min.css',
        paths.datatable + '/dataTables.bootstrap.css',
        paths.datatable + '/integration/dataTables.fontAwesome.css',
        paths.bxslider + '/jquery.bxslider.css',
        paths.jssocials + '/jssocials.css',
        paths.jssocials + '/jssocials-theme-flat.css',
        paths.trumbowyg + '/ui/trumbowyg.min.css',
        paths.trumbowyg + '/plugins/colors/ui/trumbowyg.colors.min.css',
        paths.fontAwesome + '/css/font-awesome.min.css',
        paths.slick + '/slick.css',
        paths.bootstrapTagsinput + '/bootstrap-tagsinput.css',
        paths.bootstrapDatetimepicker + '/css/bootstrap-datetimepicker.min.css',
        paths.jqueryBarRating + '/themes/fontawesome-stars.css',
        paths.jqueryBarRating + '/themes/fontawesome-stars-o.css',
    ], 'public/css/frontend');

    // Copy frontend JS files
    mix.copy([
        paths.jquery + '/jquery.min.js',
//        paths.bootstrap + '/javascripts/bootstrap.min.js',
        paths.select2 + '/select2.full.min.js',
        paths.dropzone + '/min/dropzone.min.js',
        paths.js + '/dropzone-config.js',
        paths.datatable + '/jquery.dataTables.min.js',
        paths.datatable + '/dataTables.bootstrap.min.js',
        paths.croppic + '/croppic.js',
        paths.sweetalert2 + '/sweetalert2.min.js',
        paths.parsleyjs + '/parsley.min.js',
        paths.js + '/parsley-vi.js',
        paths.js + '/cookie.js',
        paths.js + '/ajax-dynamic-selectbox.js',
        paths.blockui + '/jquery.blockUI.min.js',
        paths.bxslider + '/jquery.bxslider.min.js',
        paths.jqueryMaskPlugin + '/jquery.mask.min.js',
        paths.remUnitPolyfill + '/rem.min.js',
        paths.jssocials + '/jssocials.min.js',
        paths.twitterBootstrapWizard + '/jquery.bootstrap.wizard.js',
        paths.trumbowyg + '/trumbowyg.min.js',
        paths.trumbowyg + '/plugins/colors/trumbowyg.colors.min.js',
        paths.trumbowyg + '/plugins/upload/trumbowyg.upload.min.js',
        paths.jQueryWaccp + '/jquery.word-and-character-counter.min.js',
        paths.js + '/frontend/buy-services.js',
        paths.js + '/frontend/recharge.js',
        paths.slick + '/slick.min.js',
        paths.typeahead + '/typeahead.bundle.min.js',
        paths.bootstrapTagsinput + '/bootstrap-tagsinput.min.js',
        paths.bootstrapDatetimepicker + '/js/bootstrap-datetimepicker.min.js',
        paths.moment + '/moment.js',
        paths.bootbox + '/bootbox.min.js',
        paths.jqueryBarRating + '/jquery.barrating.min.js',
		paths.listJs + '/list.min.js',
    ], 'public/js/frontend');

    // Copy lang vi.min.js to trumbowyg.vi.min.js
    mix.copy(paths.trumbowyg + '/langs/vi.min.js', 'public/js/frontend/trumbowyg.vi.min.js');

    // Copy trumbo svg icons
    mix.copy(paths.trumbowyg + '/ui/icons.svg', 'public/js/frontend/ui/icons.svg');
    mix.copy(paths.trumbowyg + '/ui/icons.svg', 'public/js/builds/ui/icons.svg');

    // Copy FontAwesome fonts to fonts folder
    mix.copy(paths.fontAwesome + '/fonts', 'public/css/fonts');

    // Copy data tables Vietnamese language
    mix.copy(paths.datatable + '/i18n/Vietnamese.json', 'public/js/datatables.vietnamese.json');

    // Sass Bootstrap CSS
    mix.sass([
        'frontend/bootstrap.scss'
    ], 'public/css/frontend/bootstrap.css');

    // Sass frontend main CSS
    mix.sass([
        'frontend/main.scss'
    ], 'public/css/frontend/all.css');

    // // Sass frontend 503
    // mix.sass([
    //     'frontend/503.scss'
    // ], 'public/css/frontend/503.css');
    //
    // // Sass frontend print
    // mix.sass([
    //     'frontend/print.scss'
    // ], 'public/css/frontend/print.css');
    // Merge Bootstrap JS
    gulp.task('bootstrap-js-customize', function() {
        return gulp.src([
//            paths.bootstrap + '/javascripts/bootstrap/affix.js',
//            paths.bootstrap + '/javascripts/bootstrap/alert.js',
//            paths.bootstrap + '/javascripts/bootstrap/button.js',
//            paths.bootstrap + '/javascripts/bootstrap/carousel.js',
            paths.bootstrap + '/javascripts/bootstrap/collapse.js',
            paths.bootstrap + '/javascripts/bootstrap/dropdown.js',
            paths.bootstrap + '/javascripts/bootstrap/modal.js',
//            paths.bootstrap + '/javascripts/bootstrap/scrollspy.js',
            paths.bootstrap + '/javascripts/bootstrap/tab.js',
            paths.bootstrap + '/javascripts/bootstrap/transition.js',
            paths.bootstrap + '/javascripts/bootstrap/tooltip.js',
//            paths.bootstrap + '/javascripts/bootstrap/popover.js',
            ])
          .pipe(concat('bootstrap.min.js'))
          .pipe(uglifyJs())
          .pipe(gulp.dest('public/js/frontend/'));
    });
    mix.task('bootstrap-js-customize');

    // Merge frontend JS files
    mix.scripts([
        'pubsub.js',
        'ajax-helpers.js',
        'frontend/main.js'
    ], 'public/js/frontend');

   // Do backend stuffs
    if (!argv.nobackend) {
        require('./gulpfile-backend')
    }

    // Lint CSS files, ignore croppic.css, trumbowyg.min.css
    gulp.task('csslint', function () {
        gulp.src(['public/css/**/*.css', '!**/croppic.css', '!**/trumbowyg.min.css'
            , '!public/css/builds/*.css', '!**/bootstrap.min.css'])
                .pipe(csslint())
                .pipe(csslint.reporter());
    });
    mix.task('csslint');

    // Lint JS files
    mix.jshint(['public/js/**/all.js']);

    // Lint PHP files
    var opts = {skipPassedFiles : true};
    gulp.task('phplint', function() {
        gulp.src(['tests/**/*.php', 'app/**/*.php', 'config/*.php', 'database/**/*.php'])
          .pipe(phplint('',opts))
            .pipe(phplint.reporter('fail'));
    });
    mix.task('phplint');

    gulp.task('watch', function() {
        gulp.watch('resources/assets/sass/**/*.scss', ['sass']);
    });

    // Production mode
    if (argv.production) {
        // Uglify all JS
        gulp.task('uglifyJs', function() {
            return gulp.src(['public/js/frontend/*.js', '!**/*.min.js'])
              .pipe(uglifyJs())
              .pipe(gulp.dest('public/js/frontend'));
        });
        mix.task('uglifyJs');

        // Clean CSS
        gulp.task('minifycss', function() {
            return gulp.src(['public/css/frontend/*.css', '!**/*.min.css'])
              .pipe(cleanCss())
              .pipe(gulp.dest('public/css/frontend'));
        });
        mix.task('minifycss');
    }

});
