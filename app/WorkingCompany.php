<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkingCompany extends Model
{
    protected $tables = 'working_companies';

    protected $fillable = ['company_name','position', 'description', 'from', 'to', 'user_id'];

    public $timestamps = false;

    public function user()
    {
      return $this->belongsTo("App\User", 'user_id');
    }
}
