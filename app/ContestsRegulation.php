<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContestsRegulation extends Model
{

  protected $table = 'contests_regulation';

  protected $fillable = ['id', 'name','label','description'];

  public function contests()
  {
      return $this->hasMany('App\Contests');
  }

}
