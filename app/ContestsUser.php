<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContestsUser extends Model
{
  protected $table = 'contests_users';

  protected $fillable = ['id', 'user_id','contests_id'];

}
