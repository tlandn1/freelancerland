<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NapTien extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'naptien';

  /**
  * The database primary key value.
  *
  * @var string
  */
  protected $primaryKey = 'id';

  /**
   * Attributes that should be mass-assignable.
   *
   * @var array
   */
  protected $fillable = ['id','naptien_code','sotien', 'note', 'user_id'];

  public function user()
  {
      return $this->belongsTo('App\User');
  }
}
