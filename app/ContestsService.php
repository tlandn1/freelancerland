<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContestsService extends Model
{
    protected $table = 'contests_service';

    protected $fillable = ['id', 'name','contests_field_id'];
    
    public $timestamps = false;

}
