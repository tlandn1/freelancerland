<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectGig extends Model
{
    protected $table = 'project_gigs';

    protected $fillable = ['id','user_id','gig_id','project_gig_status_id','user_review_text','user_review_rating','seller_review_text','seller_review_rating'];

    public function gig()
    {
        return $this->belongsTo('App\Gig','gig_id');
    }

    public function projectGigStatus()
    {
        return $this->belongsTo('App\ProjectGigStatus','project_gig_status_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
