<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    protected $table = 'ruttien_bank';

    protected $fillable = ['id', 'title', 'description', 'user_id'];

    public $timestamps = false;

    public function user()
    {
      return $this->belongsTo('App\User', 'user_id');
    }
}
