<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostImage extends Model
{
    protected $table = 'post_images';
    protected $fillable = ['id',
        'path',
        'pathorigin',
        'paththumb',
    ];
    public static $rules = [
        'img' => 'required|mimes:png,gif,jpeg,jpg',
    ];
    public static $messages = [
        'img.mimes' => 'Chỉ hỗ trợ các định dạng: png, gif, jpeg, jpg.',
        'img.required' => 'Vui lòng chọn hình.',
    ];

    public function post()
    {
        return $this->belongsTo('App\Post');
    }
}
