<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectStatus extends Model
{
  protected $table = 'project_statuses';

  protected $fillable = ['id', 'name','label'];

  public function projects()
  {
      return $this->hasMany("App\Project");
  }
}
