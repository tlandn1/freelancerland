<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Skill extends Model
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'name',
        'save_to' => 'slug',
        'unique' => true,
        'on_update' => true,
    ];

    protected $table = 'skills';

    protected $fillable = ['name', 'label', 'slug'];

    public $timestamps = false;

    public function users()
    {
      return $this->belongsToMany("App\User");
    }

    public function projects()
    {
        return $this->belongsToMany('App\Project');
    }
}
