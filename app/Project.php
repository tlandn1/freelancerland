<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Project extends Model implements SluggableInterface
{
  use SluggableTrait;

  protected $sluggable = [
      'build_from' => 'title',
      'save_to' => 'slug',
      'unique' => true,
      'on_update' => true,
  ];

  protected $table = 'projects';

  protected $fillable = ['id', 'title','description','end_receive_bid','budget','second_category_id','budget_estimate_id','city_id','slug'];

  public function budgetEstimate()
  {
      return $this->belongsTo('App\BudgetEstimate');
  }

  public function city()
  {
      return $this->belongsTo('App\City');
  }

  public function secondCategory()
  {
      return $this->belongsTo('App\SecondCategory');
  }

  public function projectStatus()
  {
      return $this->belongsTo('App\ProjectStatus');
  }
  // một nhiều
  public function user()
  {
      return $this->belongsTo('App\User');
  }

  public function skills()
  {
      return $this->belongsToMany('App\Skill','projects_skills');
  }
}
