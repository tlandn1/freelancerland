<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Skill extends Model
{
  protected $table = 'users_skills';

  protected $fillable = ['user_id','skill_id'];

  public $timestamps = false;
}
