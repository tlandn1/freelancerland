<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
<<<<<<< HEAD
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

class Contests extends Model implements SluggableInterface, HasMediaConversions
{
  use SluggableTrait,
      HasMediaTrait;

class Contests extends Model implements SluggableInterface
{
  use SluggableTrait;

  protected $sluggable = [
      'build_from' => 'title',
      'save_to' => 'slug',
      'unique' => true,
      'on_update' => true,
  ];

  protected $table = 'contests';

  protected $fillable = ['id', 'title','description','budget','user_id','time_end_contest','contests_regulation_id','contests_service_id','slug'];

  public function contestService()
  {
      return $this->belongsTo('App\ContestsService');
  }

  public function user()
  {
      return $this->belongsTo('App\User');
  }

  public function users()
  {
      return $this->belongsTo('App\User','contests_users');
  }

  public function contestsRegulation()
  {
      return $this->belongsTo('App\ContestsRegulation');
  }

}
