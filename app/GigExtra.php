<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GigExtra extends Model
{
    protected $table = 'gigs_extra';

    protected $fillable = ['id','title','price','deliver_day','gig_id'];

    public function gig()
    {
        return $this->belongsTo('App\Gig','gig_id');
    }
}
