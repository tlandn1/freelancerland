<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class SecondCategory extends Model
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'name',
        'save_to' => 'slug',
        'unique' => false,
        'on_update' => true,
    ];

    protected $table = 'second_categories';
    protected $fillable = ['name', 'first_category_id', 'slug'];
    public $timestamps = false;

    public function firstCategory()
    {
        return $this->belongsTo('App\FirstCategory','first_category_id');
    }

    public function projects()
    {
        return $this->hasMany("App\Project", 'second_category_id');
    }

    public function users()
    {
        return $this->belongsToMany("App\User");
    }
}
