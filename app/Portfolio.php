<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class Portfolio extends Model implements HasMedia
{
    use HasMediaTrait;

    public function registerMediaConversions()
    {
        $thumbWidth = config('freelancerland.PORTFOLIO_IMAGE_THUMB_WIDTH');
        $thumbHeight = config('freelancerland.PORTFOLIO_IMAGE_THUMB_HEIGHT');
        $this->addMediaConversion('thumb')
                ->setManipulations(['w' => $thumbWidth, 'h' => $thumbHeight])
                ->performOnCollections('portfolio_files');
    }

    protected $table = 'portfolio';

    protected $fillable = ['title', 'images', 'description', 'link', 'user_id'];

    public $timestamps = false;

    public function getFirstImageAttribute()
    {
        $image = $this->getFirstMediaUrl('portfolio_files');

        if ($image) {
            return $image;
        }

        return '/images/portfolios/default-image.png';
    }
    //
    public function getFirstImageThumbAttribute()
    {

        $image = $this->getFirstMediaUrl('portfolio_files', 'thumb');

        if ($image) {
            return $image;
        }

        return '/images/portfolios/img-default-thumb.jpg';
    }

    public function user()
    {
        return $this->belongsTo("App\User", 'user_id');
    }
}
