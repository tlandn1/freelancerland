<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContestsField extends Model
{
  protected $table = 'contests_field';

  protected $fillable = ['id', 'name'];

}
