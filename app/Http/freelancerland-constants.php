<?php

/* User Statuses */
define('USER_STATUS_ACTIVE_ID', 1);
define('USER_STATUS_BANNED_ID', 2);

/* User Roles */
define('USER_ROLE_FREELANCER_ID', 1);
define('USER_ROLE_ADMIN_ID', 2);
define('USER_ROLE_CLIENT_ID', 3);


// $usedBy có 2 giá trị
// 'avatar' : dùng khi user update avatar
// 'post' : dùng khi user post bài có hình
define('IMAGE_UPLOAD_USED_BY_AVATAR', 'avatar');
define('IMAGE_UPLOAD_USED_BY_POST', 'post');

// NapTien Statuses
define('NAPTIEN_STATUS_PENDING', 'pending');
define('NAPTIEN_STATUS_PAID', 'paid');

// Note Statuses
define('NOTE_INFO', 1);
define('NOTE_SUCCESS', 2);
define('NOTE_ERROR', 3);

define('WATERMARK_NAME', 'watermark');

/* Project Statuses */
define('PROJECT_STATUS_PENDING_ID', 1);
define('PROJECT_STATUS_ACTIVE_ID', 2);
define('PROJECT_STATUS_SUSPEND_ID', 3);
define('PROJECT_STATUS_ASSIGNED_ID', 4);
define('PROJECT_STATUS_FINISH_ID', 5);

/* Gig Statuses */
define('GIG_STATUS_WAITING_ID', 1);
define('GIG_STATUS_ACTIVE_ID', 2);
define('GIG_STATUS_REFUSED_ID', 3);
define('GIG_STATUS_ARCHIVED_ID', 4);

/* Project Statuses */
define('PROJECT_GIG_STATUS_PENDING_ID', 1);
define('PROJECT_GIG_STATUS_WORKINNG_ID', 2);
define('PROJECT_GIG_STATUS_SUSPEND_ID', 3);
define('PROJECT_GIG_STATUS_ASSIGNED_ID', 4);
define('PROJECT_GIG_STATUS_FINISH_ID', 5);