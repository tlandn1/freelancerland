<?php
use App\City;
use App\FirstCategory;
use App\SecondCategory;

    // Functions only
function random_str($length, $keyspace = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ')
{
    $str = '';
    $max = mb_strlen($keyspace, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
        $str .= $keyspace[random_int(0, $max)];
    }

    return $str;
}

    // CDN Stuffs
function cdn( $asset ){
        // If in local environment, ignore CDN
    if( env('APP_ENV') == 'local' )
        return asset( $asset );

        // Verify if KeyCDN URLs are present in the config file
    if( !Config::get('freelancerland.cdn') )
        return asset( $asset );

    if(empty(Config::get('freelancerland.CDN_MINIFY')))
        return asset( $asset );

        // Ignore if start with http or https or //,
    if (starts_with($asset, 'http:') || starts_with($asset, 'https:') || starts_with($asset, '//')) {
        return asset( $asset );
    }

        // Get file name incl extension and CDN URLs
    $cdns = Config::get('freelancerland.cdn');
    $assetName = basename( $asset );

        // Remove query string
    $assetName = explode("?", $assetName);
    $assetName = $assetName[0];

        // Select the CDN URL based on the extension
    foreach( $cdns as $cdn => $types ) {
        if( preg_match('/^.*\.(' . $types . ')$/i', $assetName) )
            return cdnPath($cdn, $asset);
    }

        // In case of no match use the last in the array
    end($cdns);
    return cdnPath( key( $cdns ) , $asset);

}

function cdnPath($cdn, $asset) {
    return  "//" . rtrim($cdn, "/") . "/" . ltrim( $asset, "/");
}
    // End CDN Stuffs, <img src="{{ cdn( "/img/yourImg.png" ) }}" alt="Your Image loaded from KeyCDN" />

function formatPrice($price)
{
    return number_format($price, 0, '.', '.').' đ';
}

function checkNote($id)
{
    if ($id == NOTE_INFO) {
        return 'note-info';
    } elseif ($id == NOTE_SUCCESS) {
        return 'note-success';
    } elseif ($id == NOTE_ERROR) {
        return 'note-error';
    } else {
        return '';
    }
}
function buildCityToanQuoc()
{
    $city = new City;
    $city->slug = TOAN_QUOC_SLUG;
    $city->name = TOAN_QUOC_NAME;

    return $city;
}

function buildCitiesDistrictsJSON()
{
    $cities = City::all();
    foreach ($cities as $city) {
        $result[$city->id] = $city->districts->lists('name', 'id');
    }

    return json_encode($result, JSON_HEX_APOS);
}

function buildFirstSecondCategoryJSON()
{
    $firstCategories = FirstCategory::all();
    foreach ($firstCategories as $firstCategory) {
        $result[$firstCategory->id] = $firstCategory->secondCategories->lists('name', 'id');
    }

    return json_encode($result, JSON_HEX_APOS);
}

    // Route static pages
function registerStaticPagesRoutes()
{
    $pages = App\Static_Page::all();

    foreach ($pages as $page) {
        Route::get($page->slug, function () use ($page) {
          $controller = new App\Http\Controllers\StaticPagesController;

          return $controller->index($page);
      });
    }
}