<?php
//--------------Trang chủ-----------------
Route::get('/', function () {
    return view('frontend.nhap.welcome');
});

//----------- Đăng nhập, Đăng ký, Reset Password-----------------------
// GET|HEAD | login                   |      | App\Http\Controllers\Auth\AuthController@showLoginForm          | web,guest  |
// POST     | login                   |      | App\Http\Controllers\Auth\AuthController@login                  | web,guest  |
// GET|HEAD | logout                  |      | App\Http\Controllers\Auth\AuthController@logout                 | web        |
// POST     | password/email          |      | App\Http\Controllers\Auth\PasswordController@sendResetLinkEmail | web,guest  |
// POST     | password/reset          |      | App\Http\Controllers\Auth\PasswordController@reset              | web,guest  |
// GET|HEAD | password/reset/{token?} |      | App\Http\Controllers\Auth\PasswordController@showResetForm      | web,guest  |
// GET|HEAD | register                |      | App\Http\Controllers\Auth\AuthController@showRegistrationForm   | web,guest  |
// POST     | register                |      | App\Http\Controllers\Auth\AuthController@register               | web,guest
// Route::auth();

// Lien he
Route::get('lien-he', ['as'=>'lien-he', 'uses'=>'ContactController@getContact']);
Route::post('lien-he',['as'=>'len-he', 'uses'=>'ContactController@postContact']);

// Dang nhap
Route::get('dang-nhap', ['as' => 'dang-nhap', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('auth/login', ['as' => 'post-dang-nhap', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'dang-xuat', 'uses' => 'Auth\AuthController@getLogout']);

// Dang ky
Route::get('dang-ky', ['as' => 'dang-ky', 'uses' => 'Auth\AuthController@showRegistrationForm']);
Route::post('register', ['as' => 'post-dang-ky', 'uses' => 'Auth\AuthController@register']);

// Quen mat khau
Route::get('quen-mat-khau', ['as' => 'quen-mat-khau', 'uses' => 'Auth\PasswordController@getEmail']);
Route::post('quen-mat-khau', ['as' => 'post-quen-mat-khau', 'uses' => 'Auth\PasswordController@postEmail']);
Route::get('doi-mat-khau/{token}', ['as' => 'doi-mat-khau', 'uses' =>  'Auth\PasswordController@getReset']);
Route::post('doi-mat-khau', ['as' => 'post-doi-mat-khau', 'uses' =>  'Auth\PasswordController@postReset']);

//Gigs
Route::get('goi-cong-viec', ['as' => 'goi-cong-viec', 'uses' =>  'GigController@index']);
Route::get('goi-viec/{slug}', ['as' => 'goi-viec', 'uses' =>  'GigController@show']);
Route::post('user-review-gig/{id}', ['as' => 'user-review-gig', 'uses' =>  'GigController@userReviewGig']);

// Socialite
Route::get('social/login/redirect/{provider}', ['uses' => 'Auth\AuthController@redirectToProvider', 'as' => 'social.login']);
Route::get('social/login/{provider}', 'Auth\AuthController@handleProviderCallback');

Route::group(['middleware' => 'auth'], function () {
  // Trang cá nhân
  Route::group(['prefix' => 'trang-ca-nhan'], function () {
    Route::get('thong-tin-ca-nhan', ['as' => 'informationUser', 'uses' => 'UserController@getProfile']);
    Route::post('thong-tin-ca-nhan',['as'=>'post-thong-tin-ca-nhan','uses' => 'UserController@postProfile']);

    Route::post('ho-so-nang-luc',['as'=>'post-ho-so-nang-luc','uses' => 'UserController@postPortfolio']);
    Route::post('xoa-ho-so-nang-luc/{id}',['as'=>'xoa-ho-so-nang-luc','uses' => 'UserController@removePortfolio']);

    Route::post('ho-so-kinh-nghiem',['as'=>'post-ho-so-kinh-nghiem','uses' => 'UserController@postWorkingCompany']);
    Route::post('xoa-ho-so-kinh-nghiem/{id}',['as'=>'xoa-ho-so-kinh-nghiem','uses' => 'UserController@removeWorkingCompany']);

    Route::post('ho-so-hoc-tap',['as'=>'post-ho-so-hoc-tap','uses' => 'UserController@postEducation']);
    Route::post('xoa-ho-so-hoc-tap{id}',['as'=>'xoa-ho-so-hoc-tap','uses' => 'UserController@removeEducation']);

    Route::post('ho-so-ky-nang',['as'=>'post-ho-so-ky-nang','uses' => 'UserController@postSkill']);
    Route::get('ajax-getdata-skill-json',['as'=>'get-data-skill','uses'=>'UserController@ajaxGetUserSkill']);

    Route::get('ajax-getdata-user-experience/{id}',['as'=>'get-data-experience','uses'=>'UserController@ajaxGetUserExperience']);
    Route::put('ajax-update-user-experience/{id}',['as'=>'put-data-experience','uses'=>'UserController@ajaxUpdateUserExperience']);

    Route::get('ajax-getdata-user-education/{id}',['as'=>'get-data-education','uses'=>'UserController@ajaxGetUserEducation']);
    Route::put('ajax-update-user-education/{id}',['as'=>'put-data-education','uses'=>'UserController@ajaxUpdateUserEducation']);

    Route::get('ajax-getdata-user-portfolio/{id}',['as'=>'get-data-portfolio','uses'=>'UserController@ajaxGetUserPortfolio']);
    Route::put('ajax-update-user-portfolio/{id}',['as'=>'put-data-portfolio','uses'=>'UserController@ajaxUpdateUserPortfolio']);

    Route::get('doi-mat-khau', ['as' => 'changepassword', 'uses' => 'UserController@getPassword']);
    Route::post('doi-mat-khau',['as' => 'post-changepassword', 'uses' => 'UserController@postPassword']);


  });
  // Rut tien
  Route::get('rut-tien',['as'=>'rut-tien','uses'=>'WithdrawController@withdrawMoneyList']);
  Route::post('them-tai-khoan-rut-tien',['as'=>'post-them-tai-khoan-rut-tien','uses'=>'WithdrawController@postAccountBank']);
  Route::get('ruttien-datatables-ajax-data/{id}',['as'=>'get-date-withdraw','uses'=>'WithdrawController@getWithdrawMoneyAjaxData']);
  Route::put('ajax-update-withdraw/{id}',['as'=>'put-data-withdraw','uses'=>'WithdrawController@ajaxUpdateWithdraw']);
  Route::post('delete-select',['as'=>'xoa-rut-tien-chon','uses'=>'WithdrawController@deleteAccountBank']);
  Route::post('delete-account-bank/{id}',['as'=>'xoa-tai-khoan-ngan-hang','uses'=>'WithdrawController@destroy']);

  // Nạp Tiền
  Route::get('nap-tien',['as'=>'naptien','uses'=>'NapTienController@getRecharge']);
  Route::post('update-nap-tien/{naptienCode}',['as'=>'update-naptien','uses'=>'NapTienController@updateRecharge']);
  Route::patch('ajax-nap-tien/{naptienCode}',['as'=>'ajax-naptien','uses'=>'NapTienController@ajaxRecharge']);
  // Thanh toán Nạp Tiền
  Route::get('thanh-toan/banking-recharge/{id}',['as'=>'banking-recharge','uses'=>'Payment\NganLuongController@checkOutNganluongRecharge']);
  Route::get('thanh-toan/ngan-luong-recharge/{id}',['as'=>'ngan-luong-recharge','uses'=>'Payment\NganLuongController@checkOutNganluongRecharge']);
  Route::get('thanh-toan/ngan-luong-recharge-hoan-thanh',['as'=>'ngan-luong-recharge-hoan-thanh','uses'=>'Payment\NganLuongController@successNganluongRecharge']);
  Route::get('thanh-toan/bao-kim-recharge/{id}',['as'=>'bao-kim-recharge','uses'=>'Payment\BaoKimController@checkOutBaokimRecharge']);
  Route::get('thanh-toan/bao-kim-recharge-hoan-thanh',['as'=>'bao-kim-recharge-hoan-thanh','uses'=>'Payment\BaoKimController@successBaokimRecharge']);
});

//Thông tin tài khoản
Route::get('thong-tin-tai-khoan', ['as' => 'get-thong-tin-tai-khoan', 'uses' => 'UserController@getInformationAccount']);
/*****************************************
              Tìm Freelancer
*****************************************/
Route::group(['prefix' => 'list-freelancer'],function () {
  Route::get('/', ['as' => 'tim-freelancer', 'uses' => 'UserController@getFreelancer']);
  Route::get('/city-{slugCity}', ['as' => 'tim-freelancer-theo-tinh-thanh', 'uses' => 'UserController@getFreelancerByCity']);
  Route::get('/{slugFirstCategory}/{slugSecondCategory}', ['as' => 'tim-freelancer-theo-nganh', 'uses' => 'UserController@getFreelancerBySecondCategory']);
  // Route::get('/skill-{slugKyNang}', ['as' => 'tim-freelancer-theo-ky-nang', 'uses' => 'UserController@getFreelancerBySkill']);

  // Tìm kiếm freelancer theo tên
  Route::get('key-word-{name}',['as'=>'tim-kiem-theo-ten','uses'=>'UserController@searchFreelancerByName']);
});
// Route::get('cities/districts/{id}', ['as'=>'city-districts','uses'=>'UserController@getCode']);

/*****************************************
              Đăng dự án
*****************************************/
Route::get('dang-du-an',['as'=>'create-project','uses'=>'ProjectController@createProject']);
Route::post('post-dang-du-an',['as'=>'post-create-project','uses'=>'ProjectController@postProject']);
Route::get('ajax-skill-json',['as'=>'ajax-data-skill','uses'=>'ProjectController@ajaxGetUserSkill']);

/*****************************************
              Đăng cuộc thi
*****************************************/
Route::get('dang-cuoc-thi',['as'=>'create-contests','uses'=>'ContestsController@createContests']);
Route::post('post-dang-cuoc-thi',['as'=>'post-create-contests','uses'=>'ContestsController@postContests']);

/*****************************************
                  Admin
*****************************************/
Route::group(['prefix' => 'admin', 'middleware' => 'auth'],function () {
      Route::get('/', function () {
        return view('backend.homepage');
      });

      Route::resource('user', 'Backend\UserController');
      Route::post('remove-user-selected', ['as'=>'destroy-multiUser','uses'=>'Backend\UserController@removeUserSelected']);
      Route::get('user-datatables-ajax-data', 'Backend\UserController@getDataTablesAjaxData');

      Route::resource('city', 'Backend\CityController');
      Route::get('city-datatables-ajax-data', 'Backend\CityController@getDataTablesAjaxData');

      Route::resource('district', 'Backend\DistrictController');
      Route::get('district-datatables-ajax-data', 'Backend\DistrictController@getDataTablesAjaxData');

      Route::resource('first-category', 'Backend\FirstCategoryController');
      Route::get('first-category-datatables-ajax-data', 'Backend\FirstCategoryController@getDataTablesAjaxData');

      Route::resource('second-category', 'Backend\SecondCategoryController');
      Route::get('second-category-datatables-ajax-data', 'Backend\SecondCategoryController@getDataTablesAjaxData');

      Route::post('user/{id}/changepassword', ['as' => 'user.changepassword', 'uses' => 'Backend\UserController@changeUserPassword']);

      Route::resource('static-page', 'Backend\StaticPagesController');
      Route::get('static-page-datatables-ajax-data', 'Backend\StaticPagesController@getDataTablesAjaxData');
      Route::post('remove-static-page-selected', ['as' => 'destroy-multiStaticPage', 'uses' => 'Backend\StaticPagesController@removeStaticPageSelected']);

});

// Dropzone
Route::post('upload', ['as' => 'upload-post', 'uses' => 'ImageController@postUpload']);
Route::post('upload/delete', ['as' => 'upload-remove', 'uses' => 'ImageController@deleteUpload']);

Route::post('uploadImage/{usedBy}', 'CroppicController@postUpload');
Route::post('cropImage/{usedBy}', 'CroppicController@postCrop');
// static page
if (!App::runningInConsole()) {
    registerStaticPagesRoutes();
}
