<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ChangePasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        // 'old_password' => 'required',
        'password' => 'required|min:8|max:30',
        'password_confirmation' => 'required|same:password',
      ];
    }

    public function messages()
    {
        return [
            'old_password.required' => 'Vui lòng nhập mật khẩu cũ',
            'password.required' =>  'Vui lòng nhập mật khẩu mới',
            'password.min'  =>  'Mật khẩu của bạn tối thiểu 8 ký tự',
            'password.max'  =>  'Mật khẩu của bạn tối đa 30 ký tự',
            'password_confirmation.required' =>  'Vui lòng xác nhận mật khẩu',
            'password_confirmation.same' => 'Xác nhận mật khẩu chưa đúng. Vui lòng kiểm tra!',
        ];
    }
}
