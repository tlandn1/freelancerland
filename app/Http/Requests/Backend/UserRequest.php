<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'name' => 'required|max:40',
          'email' => 'required|unique:users,email',
          'password' => 'required|min:8|max:30',
        ];
    }

    public function messages() {
        return [
          'name.required' => 'Bạn chưa nhập tên',
          'email.required'  =>   'Bạn chưa nhập email',
          'email.unique' =>  'Email đã có trong database, xin mời nhập lại',
          'password.required' =>  'Chưa nhập password',
          'password.min'  =>  'Password dài hơn 7 ký tự',
          'password.max'  =>  'Password ngắn hơn 30 ký tự',
          'name.max'  =>  'Tên nhập vào phải ngắn hơn 40 ký tự',
        ];
    }
}
