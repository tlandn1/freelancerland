<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class FirstCategoryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|unique:first_categories,name,'.$this->id,
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Vui lòng nhập tên Chuyên mục cấp 1',
            'name.max'  =>   'Tên Chuyên mục cấp 1 không quá 255 ký tự',
            'name.unique' =>  'Tên Chuyên mục cấp 1 này đã tồn tại. Hãy nhập tên khác',
        ];
    }
}
