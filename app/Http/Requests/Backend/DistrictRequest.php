<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class DistrictRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100|unique:districts,name,'.$this->id,
        ];
    }

    public function messages()
    {
        return [
          'name.required' => 'Vui lòng nhập tên quận/huyện/tỉnh/thành phố',
          'name.max'  =>   'Tên quận/huyện/tỉnh/thành phố không quá :max ký tự',
          'name.unique' =>  'Tên quận/huyện/tỉnh/thành phố này đã tồn tại. Hãy nhập tên khác',
        ];
    }
}
