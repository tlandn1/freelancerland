<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class UserProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
          'phone' => 'min:6|max:11',
          'name' => 'required|max:40',
      ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Vui lòng nhập tên',
            'name.max' => 'Tên không quá :max ký tự',
            'phone.min' => 'Số điện thoại ít nhất :min ký tự',
            'phone.max' => 'Số điện thoại nhiều nhất :max ký tự',
        ];
    }
}
