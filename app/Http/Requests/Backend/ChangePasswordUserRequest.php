<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class ChangePasswordUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|min:8|max:30',
        ];
    }

    public function messages()
    {
        return [
             'password.required' =>  'Vui lòng nhập mật khẩu',
             'password.min'  =>  'Nhập mật khẩu tối thiểu :min ký tự',
             'password.max'  =>  'Nhập mật khẩu không quá :max ký tự',
         ];
    }
}
