<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class StaticPagesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'slug'=>'required',
            'content'=>'required',
        ];
    }

    public function messages()
    {
      return [
           'title.required' => 'Vui lòng nhập tiêu đề',
           'slug.required' => 'Vui lòng nhập url cho trang',
           'content.required' => 'Vui lòng nhập nội dung',
      ];
   }
}
