<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\CityRequest;
use DebugBar;
use App\City;
use Yajra\Datatables\Datatables;

class CityController extends Controller
{
    public function index()
    {
        return view('backend.city.list');
    }

    public function create()
    {
        $type_list = (['Tỉnh' => 'Tỉnh', 'Thành Phố' => 'Thành Phố']);

        return view('backend.city.add', compact('type_list'));
    }

    public function store(CityRequest $request)
    {
        City::create($request->all());

        return redirect()->route('admin.city.index')->with(['flash_type' => 'alert-success', 'flash_message' => 'Thêm tỉnh/thành phố thành công']);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $city = City::findOrFail($id);
        $type_list = (['Tỉnh' => 'Tỉnh', 'Thành Phố' => 'Thành Phố']);

        return view('backend.city.edit', compact('city', 'type_list'));
    }

    public function update(CityRequest $request, $id)
    {
        $city = City::findOrFail($id);
        $city->update($request->all());

        return redirect()->back()->with(['flash_type' => 'alert-success', 'flash_message' => 'Sửa tỉnh/thành phố thành công']);
    }

    public function destroy($id)
    {
        $city = City::findOrFail($id);
        $city->delete();

        return redirect()->route('admin.city.index')->with(['flash_type' => 'alert-success', 'flash_message' => 'Xóa tỉnh/thành phố thành công']);
    }

    public function getDataTablesAjaxData()
    {
        $cities = City::select(['id', 'name', 'type']);

        return Datatables::of($cities)
                ->addColumn('delete', function ($city) {
                    return view('backend.city.deleteButton', compact('city'))->render();
                })
                ->addColumn('edit', function ($city) {
                    return view('backend.city.editButton', compact('city'))->render();
                })
                ->make(true);
    }
}
