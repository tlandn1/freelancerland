<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\Backend\StaticPagesRequest;
use App\Http\Controllers\Controller;
use App\Static_Page;
use Yajra\Datatables\Datatables;

class StaticPagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

         return view('backend.static-page.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.static-page.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StaticPagesRequest $request)
    {
      $page_static = new Static_Page();
      $page_static->fill($request->all());
      $page_static->save();

      return redirect()->route('admin.static-page.index')->with(['flash_type' => 'alert-success', 'flash_message' =>'Thêm mới trang tĩnh thành công']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $page_static = Static_Page::findOrFail($id);
      return view('backend.static-page.edit',compact('page_static'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $page_static = Static_Page::findOrFail($id);
      $page_static->fill($request->all());
      $page_static->update();

      return redirect()->back()->with(['flash_type' => 'alert-success', 'flash_message' => 'Sữa trang tĩnh thành công']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $page_static = Static_Page::findOrFail($id);
      $page_static->destroy($id);
      return redirect()->route('admin.static-page.index')->with(['flash_type' => 'alert-success', 'flash_message' => 'Xóa trang tĩnh thành công']);
    }

    public function removeStaticPageSelected(Request $request)
    {
        $selectArray = $request->row_checkbox;
        if ($selectArray != null) {
            foreach ($selectArray as $selected) {
                $static_page = Static_Page::findOrFail($selected);
                $static_page->delete();
            }
        }

        return redirect()->route('admin.static-page.index')->with(['flash_type' => 'alert-success', 'flash_message' => 'Xóa mục đã chọn thành công']);
    }

   public function getDataTablesAjaxData()
   {
      $static_pages = Static_Page::select(['id', 'title', 'content', 'slug']);

      return Datatables::of($static_pages)
               ->addColumn('checkbox', function ($user) {
                   return '<td><input type="checkbox" name="row_checkbox[]" value="'.$user->id.'"/></td>';
               })
               ->addColumn('view_page', function ($static_page) {
                   return '<a href="'.$static_page->slug.'" target="_blank">View page</a>';
               })
               ->addColumn('delete', function ($static_page) {
                   return view('backend.static-page.delete-button', compact('static_page'))->render();
               })
               ->addColumn('edit', function ($static_page) {
                   return view('backend.static-page.edit-button', compact('static_page'))->render();
               })
               ->make(true);
   }
}
