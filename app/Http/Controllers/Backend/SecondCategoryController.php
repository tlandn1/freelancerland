<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SecondCategory;
use App\FirstCategory;
use Yajra\Datatables\Datatables;
use App\Http\Requests\Backend\SecondCategoryRequest;

class SecondCategoryController extends Controller
{
    public function index()
    {
        return view('backend.secondcategory.list');
    }

    public function create()
    {
        $first_category = FirstCategory::lists('name', 'id');

        return view('backend.secondcategory.add', compact('first_category'));
    }

    public function store(SecondCategoryRequest $request)
    {
        $second_category = SecondCategory::create($request->all());

        return redirect()->route('admin.second-category.index')->with(['flash_type' => 'alert-success', 'flash_message' => trans('freelancer-message-backend.secondCategory.add')]);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $second_category = SecondCategory::findOrFail($id);
        $first_category = FirstCategory::lists('name', 'id');

        return view('backend.secondcategory.edit', compact('second_category', 'first_category'));
    }

    public function update(SecondCategoryRequest $request, $id)
    {
        $second_category = SecondCategory::findOrFail($id);
        $second_category->update($request->all());

        return redirect()->back()->with(['flash_type' => 'alert-success', 'flash_message' => trans('freelancer-message-backend.secondCategory.edit')]);
    }

    public function destroy($id)
    {
        $second_category = SecondCategory::findOrFail($id);
        $second_category->delete();

        return redirect()->route('admin.second-category.index')->with(['flash_type' => 'alert-success', 'flash_message' => trans('freelancer-message-backend.secondCategory.delete')]);
    }

    public function getDataTablesAjaxData()
    {
        //        $second_categories = SecondCategory::select(['id','name','first_category_id']);

        $second_categories = SecondCategory::join('first_categories', 'second_categories.first_category_id', '=', 'first_categories.id')
                ->select(['second_categories.id', 'second_categories.name', 'first_categories.name as firstCategoryName']);

        return Datatables::of($second_categories)
                ->addColumn('delete', function ($second_category) {
                    return view('backend.secondcategory.deleteButton', compact('second_category'))->render();
                })
                ->addColumn('edit', function ($second_category) {
                    return view('backend.secondcategory.editButton', compact('second_category'))->render();
                })
                ->make(true);
    }
}
