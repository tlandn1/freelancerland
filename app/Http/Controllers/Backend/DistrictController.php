<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\District;
use App\City;
use App\Http\Requests\Backend\DistrictRequest;
use Yajra\Datatables\Datatables;

class DistrictController extends Controller
{
    public function index()
    {
        return view('backend.district.list');
    }

    public function create()
    {
        $cities = City::lists('name', 'id');
        $type_list = (['Quận' => 'Quận', 'Huyện' => 'Huyện', 'Xã' => 'Xã', 'Thành Phố' => 'Thành Phố']);

        return view('backend.district.add', compact('cities', 'type_list'));
    }

    public function store(DistrictRequest $request)
    {
        District::create($request->all());

        return redirect()->route('admin.district.index')->with(['flash_type' => 'alert-success', 'flash_message' => 'Thêm tỉnh/thành phố/quận/huyện thành công']);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $districts = District::findOrFail($id);
        $cities = City::lists('name', 'id');
        $type_list = (['Quận' => 'Quận', 'Huyện' => 'Huyện', 'Xã' => 'Xã', 'Thành Phố' => 'Thành Phố']);

        return view('backend.district.edit', compact('districts', 'cities', 'type_list'));
    }

    public function update(DistrictRequest $request, $id)
    {
        $district = District::findOrFail($id);
        $district->update($request->all());

        return redirect()->back()->with(['flash_type' => 'alert-success', 'flash_message' => 'Sửa tỉnh/thành phố/quận/huyện thành công']);
    }

    public function destroy($id)
    {
        $district = District::findOrFail($id);
        $district->delete();

        return redirect()->route('admin.district.index')->with(['flash_type' => 'alert-success', 'flash_message' => 'Xóa tỉnh/thành phố/quận/huyện thành công']);
    }

    public function getDataTablesAjaxData()
    {
        //$districts = District::select(['id', 'name','type','order','city_id']);

        $districts = District::join('cities', 'districts.city_id', '=', 'cities.id')
                ->select(['districts.id', 'districts.name', 'districts.type', 'cities.name as cityName']);

        return Datatables::of($districts)
                        ->addColumn('delete', function ($district) {
                            return view('backend.district.deleteButton', compact('district'))->render();
                        })
                        ->addColumn('edit', function ($district) {
                            return view('backend.district.editButton', compact('district'))->render();
                        })
                        ->make(true);
    }
}
