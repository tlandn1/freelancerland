<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\FirstCategory;
use Input;
use Yajra\Datatables\Datatables;
use App\Http\Requests\Backend\FirstCategoryRequest;

class FirstCategoryController extends Controller
{
    public function index()
    {
        return view('backend.firstcategory.list');
    }

    public function create()
    {
        return view('backend.firstcategory.add');
    }

    public function store(FirstCategoryRequest $request)
    {
        FirstCategory::create($request->all());

        return redirect()->route('admin.first-category.index')->with(['flash_type' => 'alert-success', 'flash_message' => trans('freelancer-message-backend.firstCategory.add')]);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $first_category = FirstCategory::findOrFail($id);

        return view('backend.firstcategory.edit', compact('first_category'));
    }

    public function update(FirstCategoryRequest $request, $id)
    {
        $first_category = FirstCategory::findOrFail($id);
        $first_category->update($request->all());

        return redirect()->back()->with(['flash_type' => 'alert-success', 'flash_message' => trans('freelancer-message-backend.firstCategory.edit')]);
    }

    public function destroy($id)
    {
        $first_category = FirstCategory::findOrFail($id);
        $first_category->delete();

        return redirect()->route('admin.first-category.index')->with(['flash_type' => 'alert-success', 'flash_message' => trans('freelancer-message-backend.firstCategory.delete')]);
    }

    public function getDataTablesAjaxData()
    {
        $first_categories = FirstCategory::select(['id', 'name']);

        return Datatables::of($first_categories)
                ->addColumn('delete', function ($first_category) {
                    return view('backend.firstcategory.deleteButton', compact('first_category'))->render();
                })
                ->addColumn('edit', function ($first_category) {
                    return view('backend.firstcategory.editButton', compact('first_category'))->render();
                })
                ->make(true);
    }
}
