<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\UserStatus;
use App\Role;
use Yajra\Datatables\Datatables;
use App\Http\Requests\Backend\UserRequest;
use App\Http\Requests\Backend\ChangePasswordUserRequest;
use Hash;
use App\Http\Requests\Backend\UserProfileRequest;

class UserController extends Controller
{
    public function index() {
      return view('backend.user.list');
    }

    public function create()
    {
        $status = UserStatus::lists('name', 'id');
        $role = Role::lists('name', 'id');
        $birthday = \Carbon\Carbon::now();

        return view('backend.user.add', compact('status', 'role','birthday'));
    }

    public function store(UserRequest $request)
    {
        $user = new User();
        $user->fill($request->all());
        $user->password = Hash::make($request->password);
        $avatar = $request->avatar;
          if($avatar == null){
            $user->avatar = '/images/users/avatar-default.jpg';
          }else {
            $user->avatar = $avatar;
          }
        $user->user_status_id = $request->status_id;

        $user->save();
        return redirect()->route('admin.user.index')->with(['flash_type' => 'alert-success', 'flash_message' => trans('freelancer-message-backend.user.add')]);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $status = UserStatus::lists('name', 'id');
        $role = Role::lists('name', 'id');

        return view('backend.user.edit', compact('user', 'role', 'status'));
    }

    public function update($id, UserProfileRequest $request)
    {
        $user = User::findOrFail($id);
        if (!isset($request->receive_email)) {
            $user->receive_email = 0;
        }
        $user->update($request->all());

        return redirect()->route('admin.user.edit', [$id])->with(['flash_type' => 'alert-success', 'flash_message' => trans('freelancer-message-backend.user.edit')]);
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('admin.user.index')->with(['flash_type' => 'alert-success', 'flash_message' => trans('freelancer-message-backend.user.delete')]);
    }

    public function removeUserSelected(Request $request)
    {
      $selectArray = $request->row_checkbox;
      if ($selectArray != null) {
          foreach ($selectArray as $selected) {
            $user = User::findOrFail($selected);
            $user->delete();
          }
      }

      return redirect()->route('admin.user.index')->with(['flash_type' => 'alert-success', 'flash_message' => 'Xóa thành viên thành công']);
    }

    public function changeUserPassword(ChangePasswordUserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $user->password = Hash::make($request->password);
        $user->save();

        return redirect()->back()->with(['flash_type' => 'alert-success', 'flash_message' =>  'Thay đổi mật khẩu thành công']);
    }

    public function getDataTablesAjaxData()
    {
        $users = User::join('user_statuses', 'users.user_status_id', '=', 'user_statuses.id')
                ->join('roles', 'users.role_id', '=', 'roles.id')
                ->select(['users.id', 'users.avatar', 'users.name', 'users.email', 'users.receive_email', 'user_statuses.label as statusLabel', 'roles.label as roleLabel']);

        return Datatables::of($users)
                ->addColumn('checkbox', function ($user) {
                    return '<td><input type="checkbox" name="row_checkbox[]" value="'.$user->id.'"/></td>';
                })
                ->editColumn('avatar', function ($user) {
                    if (!empty($user->avatar)) {
                        return '<img class="profile-user-img img-responsive img-circle" src="'.$user->avatar.'" >';
                    }
                })
                ->editColumn('receive_email', function ($user) {
                    if ($user->receive_email) {
                        return 'Yes';
                    } else {
                        return 'No';
                    }
                })
                ->addColumn('delete', function ($user) {
                    return view('backend.user.deleteButton', compact('user'))->render();
                })
                ->addColumn('edit', function ($user) {
                    return view('backend.user.editButton', compact('user'))->render();
                })
                ->make(true);
    }
}
