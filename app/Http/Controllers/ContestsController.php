<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ContestsField;
use App\ContestsService;


class ContestsController extends Controller
{
    /********************************
        Show form create Contests
    *********************************/
    public function createContests()
    {
        $contests_field = ContestsField::lists('name','id');
        $contests_service = ContestsService::select(['id','name','contests_field_id'])->get();

        $contestsServiceJson = json_encode($contests_service);


        return view('frontend.create-contests',compact('contestsServiceJson','contests_field'));
    }

    /*********************************
          create new contests
    **********************************/
    public function postContests(Request $request)
    {
      return redirect()->back();
    }
}
