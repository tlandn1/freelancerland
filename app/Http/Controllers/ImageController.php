<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Logic\Image\ImageRepository;
use Illuminate\Support\Facades\Input;

class ImageController extends Controller
{
    protected $image;

    public function __construct(ImageRepository $imageRepository)
    {
        $this->image = $imageRepository;
    }

    public function postUpload()
    {
        $photo = Input::all();
        $response = $this->image->upload($photo);

        return $response;
    }

    public function deleteUpload()
    {
        $id = Input::get('id');

        if (!$id) {
            return 0;
        }

        $response = $this->image->delete($id);

        return $response;
    }
}
