<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\NapTien;
use App\Logic\Payment\Baokim\BaoKimPaymentPro;
class NapTienController extends Controller
{
    /****************************
          Show trang nạp tiền
    *****************************/
    public function getRecharge()
    {
      //list Bank BK
      $baokim = new BaoKimPaymentPro();
      $banks = $baokim->get_seller_info();

      $rechargeCode = strtoupper(random_str(6));


      return view('frontend.naptien.recharge',compact(
        'banks','rechargeCode'));
    }

    /**************************************************
      Cập nhật hóa đơn nạp tiền khi thay đổi số tiền nạp
    ***************************************************/
    public function updateRecharge(Request $request, $code){
      $checkCode = NapTien::where('naptien_code',$code)->first();
      if($checkCode){
        $code = strtoupper(random_str(6));
      }
      if($request->price >= 20000 && $request->price%1000 == 0){
        $naptien = new NapTien();
        $naptien->naptien_code = $code;
        $naptien->sotien = $request->price;
        $naptien->status = NAPTIEN_STATUS_PENDING;
        $naptien->user_id = $this->loggedInUser->id;
        if($naptien->save()){
          return redirect()->away($request->link_payment);
        }
      }
    }
}
