<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User, App\Skill, App\City, App\FirstCategory, App\SecondCategory, App\User_Skill;
use DB;
use App\WorkingCompany, App\Education, App\District, App\Users_Projects, App\Users_SecondCategories;

class SearchController extends Controller
{
    /**************************
          Show freelancer
    ***************************/
    public function getFreelancer()
    {
      // content right
      $users = User::whereNotIn('role_id', [USER_ROLE_ADMIN_ID])->orderBy('id', 'DESC')->paginate(10);
      // content menu left
      $firstCategories = FirstCategory::get();
      $skills = Skill::get();
      $cities = City::get();

      return view('frontend.visitor.list-freelancer', compact('users','firstCategories', 'skills', 'cities'));
    }
    /******************************************
            Tìm freelancer theo tên, kỹ năng
    *******************************************/
    public function searchFreelancerByNameBySkill(Request $request)
    {
      $query = $request->search;
      $users = User::where('name', 'LIKE', '%'. $query .'%')
                      ->orWhere("email","LIKE","%{$query}%")
                      ->whereNotIn('role_id', [USER_ROLE_ADMIN_ID])
                      ->orderBy('id', 'DESC')->paginate(10);

      // dd($skillsId);
      // if($usersId){
      //   $users = User::where('name', 'LIKE', '%'. $query .'%')
      //                 ->whereNotIn('role_id', [USER_ROLE_ADMIN_ID])
      //                 ->orderBy('id', 'DESC')->paginate(10);
      // } else {
      //   $skillsId = Skill::where('name', 'LIKE', '%'. $query .'%')->lists('id');
      //   dd($skillsId);
      //   $usersId = User_Skill::whereIn('skill_id', $skillsId)->lists('id');
        // $users = User::whereIn('id', $usersId)
                      // ->whereNotIn('role_id', [USER_ROLE_ADMIN_ID])
                      // ->orderBy('id', 'DESC')->paginate(10);
      // }

      // content menu left
      $firstCategories = FirstCategory::get();
      $skills = Skill::get();
      $cities = City::get();
      return view('frontend.visitor.list-freelancer', compact('users','firstCategories', 'skills', 'cities'));
    }

    /******************************************
            Lấy tên freelancer
    *******************************************/
    public function autoCompleteNameFreelancer(Request $request)
    {
        $dataUser = User::select("name","email")->where("name","LIKE","%{$request->input('query')}%")
                                        ->orWhere("email","LIKE","%{$request->input('query')}%")
                                        ->get();
        // $dataSkill = Skill::select("label")->where("label","LIKE","%{$request->input('query')}%")->get();

//             \Debugbar::info($dataUser);
// \Debugbar::info($dataSkill);
//         if(empty($dataSkill)){
//           $data = $dataUser;
//         } elseif (empty($dataUser)) {
//           $data = $dataSkill;
//         } elseif (!empty($dataUser) && !empty($dataSkill)) {
//           $data = array_merge($dataUser, $dataSkill);
//         }

        return response()->json($dataUser);
    }

    /******************************************
            Tìm freelancer theo tên
    *******************************************/
    public function searchFreelancerByName(Request $request)
    {
      $query = $request->search;
      $url = Request::url();
      $path = $url.$query;
      dd($path);
      $users = User::where('name', 'LIKE', '%'. $query .'%')
                    ->whereNotIn('role_id', [USER_ROLE_ADMIN_ID])
                    ->orderBy('id', 'DESC')->paginate(10);

      // content menu left
      $firstCategories = FirstCategory::get();
      $skills = Skill::get();
      $cities = City::get();
      return view('frontend.visitor.list-freelancer', compact('users','firstCategories', 'skills', 'cities'));
    }
    /***********************************
        Show freelancer theo ngành
    ************************************/
    public function getFreelancerBySecondCategory($slugFirstCategory,$slugSecondCategory)
    {
      // content right
      $secondCategoryID = SecondCategory::where('slug', $slugSecondCategory)->lists('id');
      $listUserSecondCategoryId = Users_SecondCategories::where('second_category_id', $secondCategoryID)->lists('user_id');
      $users = User::whereNotIn('role_id', [USER_ROLE_ADMIN_ID])
                    ->whereIn('id',$listUserSecondCategoryId)->orderBy('id', 'DESC')->paginate(10);

      // content menu left
      $firstCategories = FirstCategory::get();
      $skills = Skill::get();
      $cities = City::get();
      return view('frontend.visitor.list-freelancer', compact('users','firstCategories', 'skills', 'cities'));
    }
    /******************************************
      Show freelancer theo tỉnh, Thành phố
    *******************************************/
    public function getFreelancerByCity($slugCity)
    {
      // content right
      $city = City::where('slug', $slugCity)->first();
      $listDistrictId = District::where('city_id', $city->id)->lists('id');
      $users = User::whereNotIn('role_id', [USER_ROLE_ADMIN_ID])
                    ->whereIn('district_id', $listDistrictId)->orderBy('id', 'DESC')->paginate(10);
      // content menu left
      $firstCategories = FirstCategory::get();
      $skills = Skill::get();
      $cities = City::get();
      return view('frontend.visitor.list-freelancer', compact('users','firstCategories', 'skills', 'cities'));
    }

    /******************************************
          Show freelancer theo kỹ năng
    *******************************************/
    public function getFreelancerBySkill($slugKyNang)
    {
      // content right
      $skillId = Skill::where('slug', $slugKyNang)->lists('id');
      $listUsersSkillsId = User_Skill::whereIn('skill_id', $skillId)->lists('user_id');
      $users = User::whereNotIn('role_id', [USER_ROLE_ADMIN_ID])
                    ->whereIn('id', $listUsersSkillsId)->orderBy('id', 'DESC')->paginate(10);
      // content menu left
      $firstCategories = FirstCategory::get();
      $skills = Skill::get();
      $cities = City::get();
      return view('frontend.visitor.list-freelancer', compact('users','firstCategories', 'skills', 'cities'));
    }
}
