<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Controllers\Controller;
use Hash;
use App\User, App\Portfolio, App\Skill, App\City, App\WorkingCompany, App\Education, App\User_Skill;
use App\FirstCategory, App\SecondCategory, App\District, App\Project, App\Users_Projects, App\Users_SecondCategories;


class UserController extends Controller
{

    /**************************
      Show thông tin tài khoản
    ***************************/
    public function getInformationAccount()
    {
      $users = User::where('id',$this->loggedInUser->id)->first();
<<<<<<< HEAD

      return view('frontend.user.information-account', compact('users'));
=======
      $firstCategories = FirstCategory::get();

      return view('frontend.user.information-account', compact('users', 'firstCategories'));
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
    }

    /********************************************
      Show trang thông tin cá nhân của user đang login
    *********************************************/
    public function getProfile()
    {
      $userSkills = $this->loggedInUser->skills()->lists('label')->all();
      $list_userSkill = implode(',', $userSkills);
      $cities = City::lists('name','id');
<<<<<<< HEAD
      // $district = District::first();
=======
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
      $citiesDistrictsJSON = buildCitiesDistrictsJSON();
      $firstCategories = FirstCategory::lists('name','id');
      $firstSecondCategoryJSON = buildFirstSecondCategoryJSON();

<<<<<<< HEAD
=======
      $secondCategoryIds = Users_SecondCategories::where('user_id',$this->loggedInUser->id)->lists('second_category_id');
      $secondCategoryNames = SecondCategory::whereIn('id',$secondCategoryIds)->lists('name');

>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
      // Encrypt Stuffs
      $jsEncode = new \jsEncode;
      $citiesDistrictsJSONCR = base64_encode($jsEncode->encodeString($citiesDistrictsJSON, config('freelancerland.CRYPT_KEY')));
      $firstSecondCategoryJSONCR = base64_encode($jsEncode->encodeString($firstSecondCategoryJSON, config('freelancerland.CRYPT_KEY')));

      return view('frontend.profile.profile-user',compact('list_userSkill',
                                                          'cities',
                                                          'citiesDistrictsJSONCR',
                                                          'firstCategories',
<<<<<<< HEAD
=======
                                                          'secondCategoryNames',
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
                                                          'firstSecondCategoryJSONCR'));
    }

    /********************************************
      Cập nhật hồ sơ cá nhân của user đang login
    *********************************************/
    public function postProfile(Request $request)
    {
        $user = $this->loggedInUser;
        if (!isset($request->receive_email)) {
            $user->receive_email = 0;
        }
        $user->update($request->all());

        return redirect()->back()->with('message', 'Cập nhật thông tin cá nhân thành công');
    }

    /********************************************
      Cập nhật hồ sơ năng lực của user đang login
    *********************************************/
    public function postPortfolio(Request $request)
    {
<<<<<<< HEAD
=======
      // $this->validate($request, [
      //       'image' => 'required|image|mimes:jpeg,png,jpg,gif,pdf,doc,docx,rar|max:6144',
      //   ]);
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
      $sencondCategoryId = $request->second_category_id;
      $listSencondCategoryId = SecondCategory::where('id',$sencondCategoryId)->lists('id')->all();
      $user = $this->loggedInUser;

      $portfolio = new Portfolio();
      $portfolio->fill($request->all());
      $portfolio->user_id = $this->loggedInUser->id;
      $portfolio->save();

      $file = $request->image;
      if (!empty($file)) {
          $destinationPath = public_path().'/uploads/portfolios/';
          $filename = $file->getClientOriginalName();
          $file->move($destinationPath, $filename);

          $portfolio = Portfolio::where('user_id',$this->loggedInUser->id)
                                    ->where('title',$request->title)->first();
          $portfolioId = Portfolio::findOrFail($portfolio->id);
          $portfolioId->addMedia(public_path().'/uploads/portfolios/'.$filename)->preservingOriginal()->toMediaLibrary('portfolio_files');
      }

      $user->secondcategories()->attach($listSencondCategoryId);
      return redirect()->back()->with('message', 'Cập nhật hồ sơ năng lực thành công');
    }

    /********************************************
      Xóa 1 hồ sơ năng lực của user đang login
    *********************************************/
    public function removePortfolio($id)
    {
      $portfolio = Portfolio::destroy($id);

      return redirect()->back()->with('message', 'Xóa hồ sơ năng lực thành công');
    }

    /*******************************************************
      Cập nhật hồ sơ kinh nghiệm làm việc của user đang login
    ********************************************************/
    public function postWorkingCompany(Request $request)
    {
      $experience = new WorkingCompany();

      $experience->fill($request->all());
      $experience->user_id = $this->loggedInUser->id;
      $experience->save();

      return redirect()->back()->with('message', 'Cập nhật hồ sơ kinh nghiệm làm việc thành công');
    }

    /********************************************
      Xóa 1 hồ sơ kinh nghiệm của user đang login
    *********************************************/
    public function removeWorkingCompany($id)
    {
      $experience = WorkingCompany::destroy($id);

      return redirect()->back()->with('message', 'Xóa hồ sơ kinh nghiệm thành công');
    }

    /***************************************
      Thêm hồ sơ học tập cho user đang login
    ****************************************/
    public function postEducation(Request $request)
    {
      $education = new Education();

      $education->fill($request->all());
      $education->user_id = $this->loggedInUser->id;
      $education->save();

      return redirect()->back()->with('message', 'Cập nhật hồ sơ học tập thành công');
    }

    /********************************************
      Xóa 1 hồ sơ học tập của user đang login
    *********************************************/
    public function removeEducation($id)
    {
      $education = Education::destroy($id);

      return redirect()->back()->with('message', 'Xóa hồ sơ học tập thành công');
    }

    /****************************************
      Thêm hồ sơ kỹ năng của user đang login
    *****************************************/
    public function postSkill(Request $request)
    {
      $user = $this->loggedInUser;
      if (\Request::has('list_skills')) {
          $skills = \Request::get('list_skills');
          $skills = explode(',', $skills);
          $user->skills()->sync(Skill::whereIn('label', $skills)->lists('id')->all());
      }else {
          User_Skill::where('user_id', $user->id)->delete();
      }
      return redirect()->back()->with('message', 'Cập nhật hồ sơ kỹ năng thành công');
    }
    /***************************************
      Show trang thay đổi mật khẩu cho user
    ****************************************/
    public function getPassword()
    {
        return view('frontend.user.change-password');
    }

    /***************************************
      Thay đổi password cho user đang login
    ****************************************/
    public function postPassword(ChangePasswordRequest $request)
    {
        $user = $this->loggedInUser;
<<<<<<< HEAD
        
=======
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
        if ($user->password) {
          if (Hash::check($request->old_password, $user->password)) {
              $user->password = Hash::make($request->password);
              $user->save();
          } else {
              return redirect()->back()->with('error', 'Mật khẩu cũ chưa đúng! Vui lòng kiểm tra lại.');
          }
        }else {
          $user->password = Hash::make($request->password);
          $user->save();
        }

        return redirect()->back()->with('message', 'Thay đổi mật khẩu thành công');
    }

    /******************************************
      ajax lấy data từ bảng Experience của user login
      theo value id truyền vào trả về JSON
    ******************************************/
    public function ajaxGetUserExperience($id)
    {
      $experience = WorkingCompany::where('user_id',$this->loggedInUser->id)
                              ->where('id',$id)->first();

      return \Response::json($experience);
    }

    /****************************************************
      Ajax cập nhật hồ sơ kinh nghiệm của user đang login
      sử dụng form modal
    ******************************************************/
    public function ajaxUpdateUserExperience($id,Request $request)
    {
      $experience = WorkingCompany::findOrFail($id);

      $experience->fill($request->all());
      $experience->update();

      return \Response::json($experience);
    }

    /******************************************
      ajax lấy data từ bảng Education của user login
      theo value id truyền vào trả về JSON
    ******************************************/
    public function ajaxGetUserEducation($id)
    {
      $education = Education::where('user_id',$this->loggedInUser->id)
                              ->where('id',$id)->first();

      return \Response::json($education);
    }

    /****************************************************
      Ajax cập nhật hồ sơ học tập của user đang login
      sử dụng form modal
    ******************************************************/
    public function ajaxUpdateUserEducation($id,Request $request)
    {
      $education = Education::findOrFail($id);

      $education->fill($request->all());
      $education->update();

      return \Response::json($education);
    }

    /******************************************
      ajax lấy data từ bảng Portfolio của user login
      theo value id truyền vào trả về JSON
    ******************************************/
    public function ajaxGetUserPortfolio($id)
    {
      $portfolio = Portfolio::where('user_id',$this->loggedInUser->id)
                              ->where('id',$id)->first();

      return \Response::json($portfolio);
    }

    /****************************************************
      Ajax cập nhật hồ sơ năng lực của user đang login
      sử dụng form modal
    ******************************************************/
    public function ajaxUpdateUserPortfolio($id,Request $request)
    {
      $portfolio = Portfolio::findOrFail($id);

      $portfolio->fill($request->all());
      $portfolio->update();

<<<<<<< HEAD
=======
      //thêm second category
      $sencondCategoryId = $request->second_category_id;
      $listSencondCategoryId = SecondCategory::where('id',$sencondCategoryId)->lists('id')->all();
      $user = $this->loggedInUser;
      $user->secondcategories()->attach($listSencondCategoryId);

      //thêm image vào Portfolio
      // $file = $request->image_edit;
      // if ($file) {
      //     $destinationPath = public_path().'/uploads/portfolios/';
      //     $filename = $file->getClientOriginalName();
      //     $file->move($destinationPath, $filename);
      //
      //     $portfolio = Portfolio::where('user_id',$this->loggedInUser->id)
      //                               ->where('title',$request->title)->first();
      //     $portfolioId = Portfolio::findOrFail($portfolio->id);
      //     $portfolioId->addMedia(public_path().'/uploads/portfolios/'.$filename)->preservingOriginal()->toMediaLibrary('portfolio_files');
      // }

>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
      return \Response::json($portfolio);
    }

    /****************************************************
      Ajax Lấy dữ liệu từ bản skill trả về JSON
    ******************************************************/
    public function ajaxGetUserSkill()
    {
      $skills = Skill::select('label')->get();

      return \Response::json($skills);
    }

<<<<<<< HEAD
    /**************************
          Show freelancer
    ***************************/
    public function getFreelancer()
    {
      // content right
      $users = User::whereNotIn('role_id', [USER_ROLE_ADMIN_ID])->orderBy('id', 'DESC')->paginate(10);
      // content menu left
      $firstCategories = FirstCategory::get();
      $skills = Skill::get();
      $cities = City::get();

      return view('frontend.visitor.list-freelancer', compact('users','firstCategories', 'skills', 'cities'));
    }
    /***********************************
        Show freelancer theo ngành
    ************************************/
    public function getFreelancerBySecondCategory($slugFirstCategory,$slugSecondCategory)
    {
      // content right
      $secondCategoryID = SecondCategory::where('slug', $slugSecondCategory)->lists('id');
      $listUserSecondCategoryId = Users_SecondCategories::where('second_categories_id', $secondCategoryID)->lists('users_id');
      $users = User::whereNotIn('role_id', [USER_ROLE_ADMIN_ID])
                    ->whereIn('id',$listUserSecondCategoryId)->orderBy('id', 'DESC')->paginate(10);

      // content menu left
      $firstCategories = FirstCategory::get();
      $skills = Skill::get();
      $cities = City::get();
      return view('frontend.visitor.list-freelancer', compact('users','firstCategories', 'skills', 'cities'));
    }
    /******************************************
      Show freelancer theo tỉnh, Thành phố
    *******************************************/
    public function getFreelancerByCity($slugCity)
    {
      // content right
      $city = City::where('slug', $slugCity)->first();
      $listDistrictId = District::where('city_id', $city->id)->lists('id');
      $users = User::whereNotIn('role_id', [USER_ROLE_ADMIN_ID])
                    ->whereIn('district_id', $listDistrictId)->orderBy('id', 'DESC')->paginate(10);
      // content menu left
      $firstCategories = FirstCategory::get();
      $skills = Skill::get();
      $cities = City::get();
      return view('frontend.visitor.list-freelancer', compact('users','firstCategories', 'skills', 'cities'));
    }
    /******************************************
          Show freelancer theo kỹ năng
    *******************************************/
    public function getFreelancerBySkill($slugKyNang)
    {

    }

    /******************************************
            Tìm freelancer theo tên
    *******************************************/
    public function searchFreelancerByName($name, Request $request)
    {
      $query = $request->search;
      $users = User::where('name', 'LIKE', '%'. $name .'%')->orderBy('id', 'DESC')->paginate(10);
      // dd($users);
      // content menu left
      $firstCategories = FirstCategory::get();
      $skills = Skill::get();
      $cities = City::get();
      return view('frontend.visitor.list-freelancer', compact('users','firstCategories', 'skills', 'cities'));
    }
=======

>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
}
