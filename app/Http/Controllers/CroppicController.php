<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManager;
use App\PostImage;

class CroppicController extends Controller
{
    protected $imageManager;
//    protected $watermarkPath;

    public function __construct()
    {
        $this->imageManager = new ImageManager;
//        $this->watermarkPath = config('chodangtin.watermarkPath');
    }

    public function postUpload($usedBy)
    {
        // $usedBy có 2 giá trị
        // 'avatar' : dùng khi user update avatar
        // 'post' : dùng khi user post bài có hình
        $form_data = Input::all();

        $validator = Validator::make($form_data, PostImage::$rules, PostImage::$messages);

        if ($validator->fails()) {
            return Response::json([
                        'status' => 'error',
                        'message' => $validator->messages()->first(),
                            ], 200);
        }

        $photo = $form_data['img'];

        $original_name = $photo->getClientOriginalName();
        $original_extension = $photo->getClientOriginalExtension();

        $temp = pathinfo($original_name);
        $original_name_without_ext = basename($original_name, '.'.$temp['extension']);

        $filename = $this->sanitize($original_name_without_ext);
        $allowed_filename = $this->createUniqueFilename($filename, $original_extension, $usedBy);

        $filename_ext = $allowed_filename.'.'.$original_extension;

        if ($usedBy == IMAGE_UPLOAD_USED_BY_AVATAR) {
            $upload_path = env('IMAGE_AVATAR_UPLOAD_PATH');
            $upload_pathOrigin = $upload_path.'origins/';
            $returnJsonUploads = '/uploads/users/origins/';
        } elseif ($usedBy == IMAGE_UPLOAD_USED_BY_POST) {
            $upload_path = env('IMAGE_UPLOAD_PATH');
            $upload_pathOrigin = $upload_path.'origins/';
            $returnJsonUploads = '/uploads/posts/origins/';
        }

        //$image = $manager->make( $photo )->encode($original_extension)->save($upload_path . $filename_ext );
        $image = $this->imageManager->make($photo)->save($upload_pathOrigin.$filename_ext);

        // Return JSON width and height
        $returnJsonWidth = $image->width();
        $returnJsonHeight = $image->height();

        // Generate default crop image (in case user don't crop image, we
        // will use this image
        $avatarWidth = config('freelancerland.IMAGE_AVATAR_WIDTH');
        $avatarHeight = config('freelancerland.IMAGE_AVATAR_HEIGHT');
        $croppedFileName = config('freelancerland.CROPPED_IMAGE_PREFIX').$filename_ext;
        $image->fit($avatarWidth, $avatarHeight)
//        ->insert(public_path() . $this->watermarkPath, getRandomWatermarkPosition())
        ->save($upload_path.$croppedFileName);

        if (!$image) {
            return Response::json([
                        'status' => 'error',
                        'message' => 'Server bị lỗi trong quá trình xử lý',
                            ], 200);
        }

        //$this->saveImageToDatabase($allowed_filename, $original_name, $usedBy);

        return Response::json([
                    'status' => 'success',
                    'url' => $returnJsonUploads.$filename_ext,
                    'width' => $returnJsonWidth,
                    'height' => $returnJsonHeight,
                        ], 200);
    }

    public function postCrop($usedBy)
    {
        // $usedBy có 2 giá trị
        // 'avatar' : dùng khi user update avatar
        // 'post' : dùng khi user post bài có hình

        $form_data = Input::all();
        $image_url = $form_data['imgUrl'];
        //$fullImageURL = url() . $image_url;
        $fullImageURL = public_path().$image_url;

        \Debugbar::info($fullImageURL);

        // resized sizes
        $imgW = $form_data['imgW'];
        $imgH = $form_data['imgH'];
        // offsets
        $imgY1 = $form_data['imgY1'];
        $imgX1 = $form_data['imgX1'];
        // crop box
        $cropW = $form_data['width'];
        $cropH = $form_data['height'];
        // rotation angle
        $angle = $form_data['rotation'];

        $filename_array = explode('/', $image_url);
        $filename = $filename_array[sizeof($filename_array) - 1];
        $croppedFileName = 'cropped-'.$filename;

        if ($usedBy == IMAGE_UPLOAD_USED_BY_AVATAR) {
            $upload_path = env('IMAGE_AVATAR_UPLOAD_PATH');
            $returnJsonUploads = '/uploads/users/';
        } elseif ($usedBy == IMAGE_UPLOAD_USED_BY_POST) {
            $year_folder = date('Y');
            $month_folder = $year_folder.'/'.date('m');
            $date_folder = $month_folder.'/'.date('d');
            $path = $date_folder.'/crops/';

            $upload_path = config('freelancerland.IMAGE_POST_UPLOAD_PATH').$path;
            $returnJsonUploads = '/uploads/posts/'.$path;
        }

        $image = $this->imageManager->make($fullImageURL);
        $image->resize($imgW, $imgH)->rotate(-$angle)->crop($cropW, $cropH, $imgX1, $imgY1)
//                ->insert(public_path() . $this->watermarkPath, getRandomWatermarkPosition())
                ->save($upload_path.$croppedFileName);

        if (!$image) {
            return Response::json([
                        'status' => 'error',
                        'message' => 'Server bị lỗi trong quá trình xử lý',
                            ], 200);
        }

        return Response::json([
                    'status' => 'success',
                    'url' => $returnJsonUploads.$croppedFileName,
                        ], 200);
    }

    private function sanitize($string, $force_lowercase = true, $anal = false)
    {
        $strip = ['~', '`', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '=', '+', '[', '{', ']',
            '}', '\\', '|', ';', ':', '"', "'", '&#8216;', '&#8217;', '&#8220;', '&#8221;', '&#8211;', '&#8212;',
            'â€”', 'â€“', ',', '<', '.', '>', '/', '?',];
        $clean = trim(str_replace($strip, '', strip_tags($string)));
        $clean = preg_replace('/\s+/', '-', $clean);
        $clean = ($anal) ? preg_replace('/[^a-zA-Z0-9]/', '', $clean) : $clean;

        return ($force_lowercase) ?
                (function_exists('mb_strtolower')) ?
                        mb_strtolower($clean, 'UTF-8') :
                        strtolower($clean) :
                $clean;
    }

    private function createUniqueFilename($filename, $original_extension, $usedBy)
    {
        if ($usedBy == IMAGE_UPLOAD_USED_BY_AVATAR) {
            $upload_path = env('IMAGE_AVATAR_UPLOAD_PATH');
            $upload_pathOrigin = $upload_path.'origins/';
        } elseif ($usedBy == IMAGE_UPLOAD_USED_BY_POST) {
            $upload_path = env('IMAGE_UPLOAD_PATH');
            $upload_pathOrigin = $upload_path.'origins/';
        }

        $full_image_path = $upload_pathOrigin.$filename.'.'.$original_extension;

        if (File::exists($full_image_path)) {
            // Generate token for image
            $image_token = substr(sha1(mt_rand()), 0, 6);

            return $filename.'-'.$image_token;
        }

        return $filename;
    }
}
