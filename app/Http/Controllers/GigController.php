<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Gig;
use App\ProjectGig;

class GigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gigs = Gig::where('gig_status_id', GIG_STATUS_ACTIVE_ID)->get();
        return view('frontend.gig.index',compact('gigs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slugGig)
    {
        $gig = Gig::findBySlug($slugGig);

        $activeReview = false;
        $projectGigID = null;
        $countComments = false;
        foreach ( $gig->projectGigs as $projectGig ) {
            if( ! $activeReview && $projectGig->user_id == 2 &&  $projectGig->project_gig_status_id == PROJECT_GIG_STATUS_FINISH_ID && ! $projectGig->user_review_rating ){
                $activeReview = true;
                $projectGigID = $projectGig->id;
            }
            if( ! $countComments && $projectGig->user_review_rating ){
                $countComments = true;
            }
            if( $activeReview && $countComments ){
                break;
            }
        }

        return view('frontend.gig.gig-detail',compact('gig','activeReview','projectGigID','countComments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function userReviewGig(Request $request, $id){
        $rating = array_sum($request->review_rating) / count($request->review_rating);

        $projectGig = ProjectGig::find($id);
        if( $request->type == 1 ){
            $projectGig->user_review_rating = $rating;
            $projectGig->user_review_text = $request->review_text;
        }
        else{
            $projectGig->seller_review_rating = $rating;
            $projectGig->seller_review_text = $request->review_text;
        }

        if($projectGig->update()){
            return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => trans('freelancerland-message-frontend.gigreview.success')]);
        }        
        else{
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => trans('freelancerland-message-frontend.gigreview.notification')]);
        }

    }
}
