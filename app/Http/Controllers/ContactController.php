<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Mail;

class ContactController extends Controller
{
   public function getContact()
   {
      return view('frontend.contact');
   }

   public function postContact(Request $request)
   {
      $lienheEmail = config('freelancerland.EMAIL_LIENHE');
      $data = $request->only('name', 'email', 'phone', 'messages');

      $email_content = view('emails.contents.contact', compact(
                       'data'
               ))->render();

      Mail::queue('emails.mail-template1', ['email_content' => $email_content], function ($message) use ($lienheEmail, $data) {
         $message->from($lienheEmail);
         $message->replyTo($data['email']);
         $message->to($lienheEmail)->subject('[LIÊN HỆ] Liên hệ từ Freelancerland.vn');
      });

      return back()->with(['flash_level' => 'success', 'flash_message' => trans('freelancerland-message-frontend.contact.notification')]);
   }
}
