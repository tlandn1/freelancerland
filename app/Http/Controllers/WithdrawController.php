<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Withdraw;
use App\User;
use Yajra\Datatables\Datatables;

class WithdrawController extends Controller
{
   public function withdrawMoneyList()
   {
      return view('frontend.ruttien-bank.rut-tien');
   }
   /*************************************
   add account_bank
   *************************************/
   public function postAccountBank(Request $request)
   {
      $account_bank = new Withdraw();
      $account_bank->fill($request->all());
      $account_bank->user_id = $this->loggedInUser->id;
      $account_bank->save();

      // return \Response::json($account_bank);
      return redirect('/rut-tien')->with('message', 'Thêm tài khoản ngân hàng thành công');
   }

   /******************************************
      ajax lấy data từ bảng ruttien_bank của user login
      theo value id truyền vào trả về JSON
   ******************************************/
   public function getWithdrawMoneyAjaxData($id)
   {
      $withdraw = Withdraw::where('user_id',$this->loggedInUser->id)
                            ->where('id',$id)->first();
      return \Response::json($withdraw);
   }

   /****************************************************
      Ajax cập nhật tài khoản rút tiền của user đang login
      sử dụng form modal
   ******************************************************/
   public function ajaxUpdateWithdraw($id,Request $request)
   {
      $withdraw = Withdraw::findOrFail($id);
      $withdraw->fill($request->all());
      $withdraw->update();

      return \Response::json($withdraw);
   }

   /*************************************
   delete select account_bank
   *************************************/
   public function deleteAccountBank(Request $request)
   {
      $selectArray = $request->row_checkbox;
      if ($selectArray != null) {
            foreach ($selectArray as $selected) {
               $rut_tien = Withdraw::findOrFail($selected);
               $rut_tien->delete();
            }
      }
      return redirect()->back()->with('message', 'Xóa chọn tài khoản ngân hàng thành công');
   }

   public function destroy($id)
   {
      $withdraw = Withdraw::findOrFail($id);
      $withdraw->destroy($id);

      return redirect()->back()->with('message', 'Xóa tài khoản ngân hàng thành công');

      // if($request->ajax()){
      //    $withdraw->destroy($id);
      // }

   }
}
