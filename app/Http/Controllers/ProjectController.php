<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\City, App\FirstCategory, App\SecondCategory;
use App\BudgetEstimate;
use App\Skill;
use App\Project;
class ProjectController extends Controller
{
    /******************************
       show view đăng dự án mới
    *******************************/
    public function createProject()
    {
      // $firstCategories = FirstCategory::all();
      $firstCategories = FirstCategory::lists('name','id');
      $cities = City::lists('name', 'id');
      $budgetEstimate = BudgetEstimate::lists('label','id');
      $firstSecondCategoryJSON = buildFirstSecondCategoryJSON();

      $jsEncode = new \jsEncode;
      $firstSecondCategoryJSONCR = base64_encode($jsEncode->encodeString($firstSecondCategoryJSON, config('freelancerland.CRYPT_KEY')));

      return view('frontend.create-project',compact('firstCategories',
                                                    'cities',
                                                    'firstSecondCategoryJSONCR',
                                                    'budgetEstimate'));
    }

    /*******************
      Submit lưu dự án
    *********************/
    public function postProject(Request $request)
    {
      $project = new Project();
      \Debugbar::info($request->all());
      $project->fill($request->all());
      $project->project_status_id = PROJECT_STATUS_PENDING_ID;
      $project->code = strtoupper(random_str(6));
      $project->user_id = $this->loggedInUser->id;
      $project->save();
      if (\Request::has('list_skills')) {
          $skills = \Request::get('list_skills');
          $skills = explode(',', $skills);
          foreach ($skills as  $skill) {
                \Debugbar::info($skill);
                $skill_id = Skill::select('id')->where('label',$skill)->first();
                $project->skills()->attach($skill_id);
          }
      }

      $response = [
        'status' => 'success',
        'project_title' => $project->title,
        'project_budget_estimate' => $project->budgetEstimate->label,
        'project_city' => $project->city->name,
        'project_description' => $project->description,
        'project_firstCategory' => $project->secondCategory->firstCategory->name,
        'project_secondCategory' => $project->secondCategory->name,
        'project_receive_bid' => $project->end_receive_bid,
    ];


      return \Response::json($response);
    }

    /****************************************************
      Ajax Lấy dữ liệu từ bản skill trả về JSON
    ******************************************************/
    public function ajaxGetUserSkill()
    {
      $skills = Skill::select('label')->get();

      return \Response::json($skills);
    }
}
