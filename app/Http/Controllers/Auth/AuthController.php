<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Auth;
use Event;
use App\Events\AdminLoginEvent;
use App\Events\UserRegisterEvent;
use Socialite;

class AuthController extends Controller
{

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:40',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8|max:30',
            ], [
            'name.required' => '* Vui lòng nhập tên',
            'name.max' => '* Nhập tên không quá 40 ký tự',
            'email.required' => '* Vui lòng nhập email đăng nhập',
            'email.email' => '* Email không chính xác, xin kiểm tra lại',
            'email.unique' => '* Email này đã sử dụng để đăng ký, vui lòng nhập email khác!',
            'password.required' => '* Vui lòng nhập mật khẩu đăng nhập',
            'password.min' => '* Mật khẩu ít nhất 8 ký tự',
            'password.max' => '* Mật khẩu nhiều nhất 30 ký tự',
        ]);
    }

    protected function create(array $data)
    {
        if (empty($data['receive_email'])) {
            $data['receive_email'] = 0;
        }

        return User::create([
          'name' => $data['name'],
          'email' => $data['email'],
          'password' => bcrypt($data['password']),
          'receive_email' => $data['receive_email'],
          'user_status_id' => USER_STATUS_ACTIVE_ID,
          'role_id' => USER_ROLE_FREELANCER_ID,
          'avatar' => $data['avatar'],
        ]);
    }

    /***************************************
                    Login
    ****************************************/
    public function postLogin(Request $request)
    {
        $auth = [
        'email' => $request->email,
        'password' => $request->password,
        ];
        $checkUserActive = User::where('email', $request->email)->first();
        if (!empty($checkUserActive)) {
            if ($checkUserActive->user_status_id == USER_STATUS_ACTIVE_ID) {
                if (Auth::attempt($auth)) {
                    $user = User::findOrFail(Auth::user()->id);
                    if ($user->isAdmin()) {
                        $adminName = $user->name;
                        //debug($request);
                        $message = 'User '.$user->name.' đã đăng nhập vào backend Chodangtin.vn lúc '.date('H:i:s').' ngày '.date('d/m/Y')
                        .' ; IP '.$request->ip();
                        event(new AdminLoginEvent($message));

                        return redirect('admin');
                    } else {
                        return redirect('/');
                    }
                } else {
                    return redirect()->back()->with(['message' => 'Nhập sai mật khẩu. Vui lòng kiểm tra lại']);
                }
            } else {
                return redirect()->back()->with(['message' => 'Tài khoản này đang bị khóa']);
            }
        } else {
            return redirect()->back()->with(['message' => 'Nhập sai mật khẩu hoặc email. Vui lòng kiểm tra lại']);
        }
    }

    /***************************************
                    Logout
    ****************************************/
    public function getLogout()
    {
        Auth::logout();

        return redirect('/');
    }

    /***************************************
                    Register
    ****************************************/
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }
        $user = $this->create($request->all());
        $user->save();
        Auth::login($user, true);
        event(new UserRegisterEvent($user));

        return redirect('/');
    }

    /***************************************
                Login with social
    ****************************************/
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        // Lấy thông tin user đăng bằng google, yahoo, facebook
        $user = Socialite::driver($provider)->user();
        $name = $user->getName();
        $email = $user->getEmail();
        $avatar = $user->getAvatar();

        $providerName = $provider;

        //Kiểm tra mail này có chưa
        $socialUserCheck = User::where('email', '=', $user->email)->first();

        if(!empty($socialUserCheck))
        {
            //Kiểm tra provider_id có null không
            $test_provider_id = User::whereNull('provider_id')->first();

            if(!empty($test_provider_id)){
                  $fields = Array(
                    'name' => $name,
                    'email' => $email,
                    'avatar' => $avatar,
                    'role_id' => USER_ROLE_FREELANCER_ID,
                    'user_status_id' => USER_STATUS_ACTIVE_ID,
                    'receive_email' => 1,
                    'provider' => $providerName,
                    'provider_id' => $user->id,
                  );
                  //update lại user
                  User::find($socialUserCheck->id)->update($fields);

                  return $this->returnHome($socialUserCheck);
            }else{
                  return $this->returnHome($socialUserCheck);
            }
        }
        else
        {
            if ($providerName == 'yahoo') {
                $name = $user->user['givenName'].' '.$user->user['familyName'];
                $email = $user->user['emails'][0]['handle'];
            }

            $socialUserCheck = User::create([
              'name' => $name,
              'email' => $email,
              'avatar' => $avatar,
              'role_id' => USER_ROLE_FREELANCER_ID,
              'user_status_id' => USER_STATUS_ACTIVE_ID,
              'receive_email' => 1,
              'provider' => $providerName,
              'provider_id' => $user->id,
              // 'tin_free' => TIN_FREE,
              // 'uptin_free' => UPTIN_FREE,
              ]);

            return $this->returnHome($socialUserCheck);
        }
    }

    private function returnHome($socialUserCheck)
    {
        Auth::login($socialUserCheck, true);
        return redirect('/');
    }

}
