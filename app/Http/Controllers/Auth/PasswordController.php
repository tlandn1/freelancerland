<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Password;
use Validator;

class PasswordController extends Controller
{
    // use ResetsPasswords;
    use ResetsPasswords{
      reset as postResetTrait;
    }

    protected $redirectTo = '/';
    protected $subject = 'Link lấy lại mật khẩu tại freelancerlannd.vn';

    public function __construct()
    {
        $this->middleware('guest');
    }

    /********************************************
                  Quên mật khẩu
    *********************************************/
    public function getEmail()
    {
        return view('auth.passwords.email');
    }

    protected function validateSendResetLinkEmail(Request $request)
    {
        $this->validate($request, [
          'email' => 'required|email'],
          [
            'email.required' => ' Vui lòng nhập email đăng nhập',
            'email.email' => ' Email không chính xác, xin kiểm tra lại',
          ]);
    }

    protected function getSendResetLinkEmailSuccessResponse($response)
    {
        return redirect()->back()->with(['status' => 'Link lấy lại mật khẩu tại freelancerland đã gửi đến mail của bạn']);
    }

    protected function getSendResetLinkEmailFailureResponse($response)
    {
        return redirect()->back()->withErrors(['message' => 'Nhập sai email. Vui lòng kiểm tra lại']);
    }

    /********************************************
                  Đặt lại mật khẩu
    *********************************************/
    public function getReset($token = null)
    {
        if (is_null($token)) {
            throw new NotFoundHttpException;
        }

        return view('auth.passwords.reset')->with('token', $token);
    }

    public function reset(Request $request)
    {
        $this->validate($request, [
              'token' => 'required',
              'email' => 'required|email',
              'password' => 'required|confirmed|min:8|max:30',
          ],
          [
            'email.required' => ' Vui lòng nhập email đăng nhập',
            'email.email' => ' Email không chính xác, xin kiểm tra lại',
            'password.required' => ' Vui lòng nhập mật khẩu đăng nhập',
            'password.confirmed' => ' Xác nhận lại mật khẩu không trùng với mật khẩu',
            'password.min' => ' Mật khẩu ít nhất 8 ký tự',
            'password.max' => ' Mật khẩu nhiều nhất 30 ký tự',
          ]);
        return $this->postResetTrait($request);
    }

    protected function getResetFailureResponse(Request $request, $response)
    {
        return redirect()->back()
            ->withInput($request->only('email'))
            ->withErrors(['email' => 'Nhập sai email. Vui lòng kiểm tra lại']);
    }


}
