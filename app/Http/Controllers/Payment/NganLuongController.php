<?php
namespace App\Http\Controllers\Payment;

use Illuminate\Http\Request;

use App\Logic\Payment\Nganluong\NL_Checkout;
use App\Http\Requests;
use App\NapTien;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Order;
use App\User;

class NganLuongController extends Controller
{

    public function checkOutNganluong($id){
        $order = Order::where('order_code', $id)->first();
        if($order){
            //dd('abc');
            $receiver = config('freelancerland.RECEIVER');
            //Mã đơn hàng
            $order_code = $order->order_code;
            //Khai báo url trả về
            $return_url = url()."/thanh-toan/ngan-luong-hoan-thanh";
            // Link nut hủy đơn hàng
            $cancel_url = url();
            //Giá của cả giỏ hàng
            $txh_name = 'MrRaovat';
            $txt_email = 'mrtesttest123@gmail.com';
            $txt_phone = '0989685847';
            $price = $order->total_price;
            //$price = 2000;
            //Thông tin giao dịch
            $transaction_info = "Thong tin giao dich";
            $currency = "vnd";
            $quantity = 1;
            $tax = 0;
            $discount = 0;
            $fee_cal = 0;
            $fee_shipping = 0;
            $order_description = "Thong tin don hang: ".$order_code;
            $buyer_info = $txh_name."*|*".$txt_email."*|*".$txt_phone;
            $affiliate_code = "";
            //Khai báo đối tượng của lớp NL_Checkout
            $nl = new NL_Checkout();
            $nl->nganluong_url = config('freelancerland.NGANLUONG_URL');
            $nl->merchant_site_code = config('freelancerland.MERCHANTNL_ID');
            $nl->secure_pass = config('freelancerland.MERCHANT_PASS');
            //Tạo link thanh toán đến nganluong.vn
            $url= $nl->buildCheckoutUrlExpand($return_url, $receiver, $transaction_info, $order_code, $price, $currency, $quantity, $tax, $discount , $fee_cal,    $fee_shipping, $order_description, $buyer_info , $affiliate_code);
            //$url= $nl->buildCheckoutUrl($return_url, $receiver, $transaction_info, $order_code, $price);
            if ($order_code != "") {
                //một số tham số lưu ý
                //&cancel_url=http://yourdomain.com --> Link bấm nút hủy giao dịch
                //&option_payment=bank_online --> Mặc định forcus vào phương thức Ngân Hàng
                $url .='&cancel_url='. $cancel_url;
                //$url .='&option_payment=bank_online';

                echo '<meta http-equiv="refresh" content="0; url='.$url.'" >';
                //&lang=en --> Ngôn ngữ hiển thị google translate
            }
        }
        else{
            $note['content'] = 'Không tìm thấy thông tin hóa đơn này.';
            $note['status'] = NOTE_ERROR;
            return view('frontend.payment.notification',compact('note'));
        }
    }

    public function successNganluong(Request $request){
        if(isset($request)){
            $transaction_info = $request['transaction_info'];
            $order_code = $request['order_code'];
            $price = $request['price'];
            $payment_id = $request['payment_id'];
            $payment_type = $request['payment_type'];
            $error_text = $request['error_text'];
            $secure_code = $request['secure_code'];
            //Khai báo đối tượng của lớp NL_Checkout
            $nl = new NL_Checkout();
            $nl->merchant_site_code = config('freelancerland.MERCHANTNL_ID');
            $nl->secure_pass = config('freelancerland.MERCHANT_PASS');
            //Tạo link thanh toán đến nganluong.vn
            $checkpay = $nl->verifyPaymentUrl($transaction_info, $order_code, $price, $payment_id, $payment_type, $error_text, $secure_code);
            if ($checkpay) {
                $order = Order::where('order_code', $request['order_code'])->first();
                $order->order_status_id = ORDER_STATUS_PAID_ID;
                if($order->update()){
                    $note['content'] = 'Thanh toán thành công.';
                    $note['status'] = NOTE_SUCCESS;
                    return view('frontend.payment.notification',compact('note'));
                }
            }else{
                $this->errorOrder();
            }
        }
    }

    public function checkOutNganluongRecharge($id){
        $naptien = NapTien::where('naptien_code', $id)->first();
        if($naptien){
            //dd('abc');
            $receiver = config('freelancerland.RECEIVER');
            //Mã đơn hàng
            $order_code = $naptien->naptien_code;
            //Khai báo url trả về
            $return_url = url()."/thanh-toan/ngan-luong-recharge-hoan-thanh";
            // Link nut hủy đơn hàng
            $cancel_url = url();
            //Giá của cả giỏ hàng
            $txh_name = 'MrRaovat';
            $txt_email = 'mrtesttest123@gmail.com';
            $txt_phone = '0989685847';
            //$price = $order->total_price;
            $price = $naptien->sotien;
            //Thông tin giao dịch
            $transaction_info = "Thong tin giao dich";
            $currency = "vnd";
            $quantity = 1;
            $tax = 0;
            $discount = 0;
            $fee_cal = 0;
            $fee_shipping = 0;
            $order_description = "Thong tin don hang: ".$order_code;
            $buyer_info = $txh_name."*|*".$txt_email."*|*".$txt_phone;
            $affiliate_code = "";
            //Khai báo đối tượng của lớp NL_Checkout
            $nl = new NL_Checkout();
            $nl->nganluong_url = config('freelancerland.NGANLUONG_URL');
            $nl->merchant_site_code = config('freelancerland.MERCHANTNL_ID');
            $nl->secure_pass = config('freelancerland.MERCHANT_PASS');
            //Tạo link thanh toán đến nganluong.vn
            $url= $nl->buildCheckoutUrlExpand($return_url, $receiver, $transaction_info, $order_code, $price, $currency, $quantity, $tax, $discount , $fee_cal,    $fee_shipping, $order_description, $buyer_info , $affiliate_code);
            //$url= $nl->buildCheckoutUrl($return_url, $receiver, $transaction_info, $order_code, $price);
            if ($order_code != "") {
                //một số tham số lưu ý
                //&cancel_url=http://yourdomain.com --> Link bấm nút hủy giao dịch
                //&option_payment=bank_online --> Mặc định forcus vào phương thức Ngân Hàng
                $url .='&cancel_url='. $cancel_url;
                //$url .='&option_payment=bank_online';

                echo '<meta http-equiv="refresh" content="0; url='.$url.'" >';
                //&lang=en --> Ngôn ngữ hiển thị google translate
            }
        }
        else{
            $note['content'] = 'Không tìm thấy thông tin hóa đơn này.';
            $note['status'] = NOTE_ERROR;
            return view('frontend.payment.notification',compact('note'));
        }
    }

    public function successNganluongRecharge(Request $request){
        if(isset($request)){
            $transaction_info = $request['transaction_info'];
            $order_code = $request['order_code'];
            $price = $request['price'];
            $payment_id = $request['payment_id'];
            $payment_type = $request['payment_type'];
            $error_text = $request['error_text'];
            $secure_code = $request['secure_code'];
            //Khai báo đối tượng của lớp NL_Checkout
            $nl = new NL_Checkout();
            $nl->merchant_site_code = config('freelancerland.MERCHANTNL_ID');
            $nl->secure_pass = config('freelancerland.MERCHANT_PASS');
            //Tạo link thanh toán đến nganluong.vn
            $checkpay = $nl->verifyPaymentUrl($transaction_info, $order_code, $price, $payment_id, $payment_type, $error_text, $secure_code);
            if ($checkpay) {
                $naptien = NapTien::where('naptien_code', $request['order_code'])->first();
                if($naptien->status == NAPTIEN_STATUS_PENDING){
                    $naptien->status = NAPTIEN_STATUS_PAID;
                    $user = User::find($this->user->id);
                    $user->sodu_tk = $user->sodu_tk + $naptien->sotien;
                    $user->update();
                    if($naptien->update()){
                        removeCookie('naptien-code');
                        $note['content'] = 'Thanh toán thành công.';
                        $note['status'] = NOTE_SUCCESS;
                        return view('frontend.payment.notification',compact('note'));
                    }
                }
                else{
                    $note['content'] = 'Hóa đơn này đã thanh toán.';
                    $note['status'] = NOTE_INFO;
                    return view('frontend.payment.notification',compact('note'));
                }
            }else{
                $this->errorOrder();
            }
        }
    }
}
