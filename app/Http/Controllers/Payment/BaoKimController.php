<?php
namespace App\Http\Controllers\Payment;

use Illuminate\Http\Request;

use App\Logic\Payment\Baokim\BaoKimPayment;
use App\Logic\Payment\Baokim\BaoKimPaymentPro;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Order;
use App\NapTien;
use App\PaymentLog;

class BaoKimController extends Controller
{
	public function checkOutBaokim(Request $request,$id){
		//dd($id);
		$methodId = 0;
		if(isset($request['bankid']) && !empty($request['bankid'])){
			$methodId = $request['bankid'];
		}
		$order = Order::where('order_code', $id)->first();
		if($order){
			$baokim_args = array(
				'url_success' 	=> route('bao-kim-hoan-thanh'),
				'url_cancel' 	=> url(),
				'total_amount' 	=> strval($order->total_price),
				//'total_amount' 	=> 5000,
				'order_id' 		=> strval($order->order_code),
				'active_submit' => strval('submit'),
				'bank_payment_method_id' => strval($methodId),
				'payer_message' => strval('Ok'),
				'extra_fields_value' => strval(''),
				'extra_payment' => strval('')
				);

			if(isset($baokim_args)&&$baokim_args){
				foreach($baokim_args as $key=>$value){
					$baokim_args[$key]=trim($value);
				}
			}

			if(isset($request['bankid']) && !empty($request['bankid'])){
				$data = $baokim_args;
				$baokim = new BaoKimPaymentPro();
				$result = $baokim->pay_by_card($data);
				if(!empty($result['error'])){
					echo '<p><strong style="color:red">' . $result['error'] . '</strong></p></div>';
					die;
				}
				if(isset($result['redirect_url']) && !empty(isset($result['redirect_url']))){
					$baokim_url = $result['redirect_url'];
				}
				else{
					$baokim_url = $result['guide_url'];
				}
				echo "<script>window.location='".$baokim_url."'</script>";

			}else{
				$data = $baokim_args;
				$baokim = new BaoKimPayment();
				$baokim_url = $baokim->createRequestUrl($data);
				echo "<script>window.location='".$baokim_url."'</script>";
			}
		}
		else{
			$note['content'] = 'Không tìm thấy thông tin hóa đơn này.';
			$note['status'] = NOTE_ERROR;
			return view('frontend.payment.notification',compact('note'));
		}
	}
	public function successBaokim(Request $request){
		if(isset($request)){
			//dd($request->order_id);
			$baokim = new BaoKimPayment();
			if($baokim->verifyResponseUrl($request->all())){
				$order = Order::where('order_code', $request['order_id'])->first();
				$order->order_status_id = ORDER_STATUS_PAID_ID;
				if($order->update()){
					$note['content'] = 'Thanh toán thành công cho mã hóa đơn ' . $request['order_id'] . '.';
					$note['status'] = NOTE_SUCCESS;

					// Payment log
					paymentLog($request->getRequestUri());

					return view('frontend.payment.notification',compact('note'));
				}
			}
			else{
				$this->errorOrder();
			}
		}
	}
	public function successBaokimBPN(Request $request){
		// $myFile = "bpn.log";
		// $fh = fopen($myFile, 'a') or die("can't open file");

		$req = '';
		if(isset($request)){
			foreach ( $request->all() as $key => $value ) {
				$value = urlencode ( stripslashes ( $value ) );
				$req .= "&$key=$value";
			}

			// fwrite($fh, $req);

			$ch = curl_init();
			//Dia chi chay BPN test
			//curl_setopt($ch, CURLOPT_URL,'http://sandbox.baokim.vn/bpn/verify');
			//Dia chi chay BPN that
			curl_setopt($ch, CURLOPT_URL,'https://www.baokim.vn/bpn/verify');
			curl_setopt($ch, CURLOPT_VERBOSE, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
			$result = curl_exec($ch);
			$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$error = curl_error($ch);

			if($result != '' && strstr($result,'VERIFIED') && $status==200){
				//thuc hien update hoa don
				// fwrite($fh, ' => VERIFIED');
				$transaction_status = $request->transaction_status;
				//kiem tra trang thai giao dich
				if ($transaction_status == 4||$transaction_status == 13){
					//Trang thai giao dich =4 la thanh toan truc tiep = 13 la thanh toan an toan
					// fwrite($fh, ' => Thanh Cong');
					$order = Order::where('order_code', $request->order_id)->first();
					if($order){
						$order->order_status_id = ORDER_STATUS_PAID_ID;
						$order->update();
					}

					$naptien = Naptien::where('naptien_code', $request->order_id)->first();
					if($naptien){
						if($naptien->status == NAPTIEN_STATUS_PENDING){
							removeCookie('naptien-code');
							$naptien->status = NAPTIEN_STATUS_PAID;
							$naptien->update();
							$user = User::find($this->user->id);
							$user->sodu_tk = $user->sodu_tk + $naptien->sotien;
							$user->update();
						}
					}

					// Payment log
					paymentLog($req);
				}
			}else{
				// fwrite($fh, ' => INVALID');
				$this->errorOrder();
			}
			if ($error){
				// fwrite($fh, " | ERROR: $error");
				$this->errorOrder();
			}
			// fwrite($fh, "\r\n");
			// fclose($fh);
		}
	}

	public function checkOutBaokimRecharge(Request $request,$id){
		//dd($id);
		$methodId = 0;
		if(isset($request['bankid']) && !empty($request['bankid'])){
			$methodId = $request['bankid'];
		}
		$naptien = Naptien::where('naptien_code', $id)->first();
		if($naptien){
			$baokim_args = array(
				'url_success' 	=> route('bao-kim-recharge-hoan-thanh'),
				'url_cancel' 	=> url(),
				// 'total_amount' 	=> strval($naptien->total_price),
				'total_amount' 	=> $naptien->sotien,
				'order_id' 		=> strval($naptien->naptien_code),
				'active_submit' => strval('submit'),
				'bank_payment_method_id' => strval($methodId),
				'payer_message' => strval('Ok'),
				'extra_fields_value' => strval(''),
				'extra_payment' => strval('')
				);

			if(isset($baokim_args)&&$baokim_args){
				foreach($baokim_args as $key=>$value){
					$baokim_args[$key]=trim($value);
				}
			}

			if(isset($request['bankid']) && !empty($request['bankid'])){
				$data = $baokim_args;
				$baokim = new BaoKimPaymentPro();
				$result = $baokim->pay_by_card($data);
				if(!empty($result['error'])){
					echo '<p><strong style="color:red">' . $result['error'] . '</strong></p></div>';
					die;
				}
				if(isset($result['redirect_url']) && !empty(isset($result['redirect_url']))){
					$baokim_url = $result['redirect_url'];
				}
				else{
					$baokim_url = $result['guide_url'];
				}
				echo "<script>window.location='".$baokim_url."'</script>";

			}else{
				$data = $baokim_args;
				$baokim = new BaoKimPayment();
				$baokim_url = $baokim->createRequestUrl($data);
				echo "<script>window.location='".$baokim_url."'</script>";
			}
		}
		else{
			$note['content'] = 'Không tìm thấy thông tin hóa đơn này.';
			$note['status'] = NOTE_ERROR;
			return view('frontend.payment.notification',compact('note'));
		}
	}
	public function successBaokimRecharge(Request $request){
		if(isset($request)){
			//dd($request->order_id);
			$baokim = new BaoKimPayment();
			if($baokim->verifyResponseUrl($request->all())){
				$naptien = Naptien::where('naptien_code', $request['order_id'])->first();

				if($naptien->status == NAPTIEN_STATUS_PENDING){
                    $naptien->status = NAPTIEN_STATUS_PAID;
                    $user = User::find($this->user->id);
                    $user->sodu_tk = $user->sodu_tk + $naptien->sotien;
                    $user->update();
                    if($naptien->update()){
                        removeCookie('naptien-code');
                        $note['content'] = 'Thanh toán thành công.';
                        $note['status'] = NOTE_SUCCESS;
                        paymentLog($request->getRequestUri());
                        return view('frontend.payment.notification',compact('note'));
                    }
                }
                else{
                    $note['content'] = 'Hóa đơn này đã thanh toán.';
                    $note['status'] = NOTE_INFO;
                    return view('frontend.payment.notification',compact('note'));
                }
			}
			else{
				$this->errorOrder();
			}
		}
	}
}
