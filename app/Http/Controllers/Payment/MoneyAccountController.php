<?php

namespace App\Http\Controllers\Payment;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Order;
use App\User;

class MoneyAccountController extends Controller
{
	public function checkOut($id){
    	//dd($this->user->sodu_tk);
		$user = User::find($this->user->id);
		$order = Order::where('order_code', $id)->first();
		if($order && $order->order_status_id == ORDER_STATUS_PENDING_ID){
			if($order->total_price <= $user->sodu_tk){
				$order->order_status_id = ORDER_STATUS_PAID_ID;
				$user->sodu_tk = $user->sodu_tk - $order->total_price;
				if($order->update() && $user->update()){
					$note['content'] = 'Thanh toán thành công.';
					$note['status'] = NOTE_SUCCESS;
					//paymentLog($request->getRequestUri());
					return view('frontend.payment.notification',compact('note'));
				}
				else{
					$note['content'] = 'Cập nhật hóa đơn lỗi, vui lòng liên hệ Admin';
					$note['status'] = NOTE_ERROR;
					return view('frontend.payment.notification',compact('note'));
				}
			}
			else{
				$note['content'] = 'Số tiền trong tài khoản không đủ, nhấn vào <a href="'. route('naptien') .'">đây</a> để nạp tiền';
				$note['status'] = NOTE_INFO;
				return view('frontend.payment.notification',compact('note'));
			}
		}
		else{
			$note['content'] = 'Hóa đơn này đã thanh toán hoặc mã hóa đơn sai.';
			$note['status'] = NOTE_INFO;
			return view('frontend.payment.notification',compact('note'));
		}
	}
}
