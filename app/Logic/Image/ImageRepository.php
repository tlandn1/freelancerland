<?php

namespace App\Logic\Image;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManager;
use App\Post;
use App\PostImage;

class ImageRepository
{
    protected $config_IMAGE_POST_UPLOAD_PATH;
    protected $config_POST_IMAGE_THUMB_WIDTH;
    protected $config_CROPPED_IMAGE_PREFIX;
    protected $config_THUMB_IMAGE_PREFIX;
//    protected $watermarkPath;

    public function __construct()
    {
        $this->config_IMAGE_POST_UPLOAD_PATH = config('freelancerland.IMAGE_POST_UPLOAD_PATH');
        $this->config_POST_IMAGE_THUMB_WIDTH = config('freelancerland.POST_IMAGE_THUMB_WIDTH');
        $this->config_CROPPED_IMAGE_PREFIX = config('freelancerland.CROPPED_IMAGE_PREFIX');
        $this->config_THUMB_IMAGE_PREFIX = config('freelancerland.THUMB_IMAGE_PREFIX');
//        $this->watermarkPath = config('chodangtin.watermarkPath');
    }

    public function upload($form_data)
    {

        //custom path
        $year_folder = date('Y');
        $month_folder = $year_folder.'/'.date('m');
        $date_folder = $month_folder.'/'.date('d');
        $origins_folder = $date_folder.'/origins/';
        $crops_folder = $date_folder.'/crops/';
        $thumbs_folder = $date_folder.'/thumbs/';

        $validator = Validator::make($form_data, PostImage::$rules, PostImage::$messages);

        if ($validator->fails()) {
            return Response::json([
                        'error' => true,
                        'message' => $validator->messages()->first(),
                        'code' => 400,
                            ], 400);
        }

        $photo = $form_data['img'];

        $originalName = $photo->getClientOriginalName();
        $originalExtension = $photo->getClientOriginalExtension();

        $originalNameWithoutExt = substr($originalName, 0, strlen($originalName) - 4);

        $filename = $this->sanitize($originalNameWithoutExt);
        $allowed_filename = $this->createUniqueFilename($originalExtension, $origins_folder, $filename);

        $filenameExt = $allowed_filename.'.'.$originalExtension;

        //custom src
        $path = $origins_folder.$filenameExt;
        $path_crop = $crops_folder.$this->config_CROPPED_IMAGE_PREFIX.$filenameExt;
        $path_thumb = $thumbs_folder.$this->config_THUMB_IMAGE_PREFIX.$filenameExt;
        //end custom src

        $imageManager = new ImageManager();
        $managerEncodePhoto = $imageManager->make($photo)->encode($originalExtension);

        $uploadSuccess1 = $this->original($managerEncodePhoto, $path);
        $uploadSuccess2 = $this->cropDefaultImage($managerEncodePhoto, $path_crop);
        $uploadSuccess3 = $this->thumb($managerEncodePhoto, $path_thumb);

        if (!$uploadSuccess1 || !$uploadSuccess2 || !$uploadSuccess3) {
            return Response::json([
                        'error' => true,
                        'message' => 'Server error while uploading',
                        'code' => 500,
                            ], 500);
        }

        $sessionImage = new PostImage;
        $sessionImage->path = $path_crop;
        $sessionImage->pathorigin = $path;
        $sessionImage->paththumb = $path_thumb;
        $sessionImage->save();

        $idImage = $sessionImage->id;

        return Response::json([
                    'error' => false,
                    'code' => 200,
                    'path' => '/uploads/posts/'.$path,
                    'id' => $idImage,
                        ], 200);
    }

    private function createUniqueFilename($originalExtension, $path, $filename)
    {
        $full_size_dir = $this->config_IMAGE_POST_UPLOAD_PATH.$path;
        $full_image_path = $full_size_dir.$filename.'.'.$originalExtension;

        if (File::exists($full_image_path)) {
            // Generate token for image
            $imageToken = substr(sha1(mt_rand()), 0, 6);

            return $filename.'-'.$imageToken;
        }

        return $filename;
    }

    /**
     * Optimize Original Image.
     */
    public function original($managerEncodePhoto, $filename)
    {
        $image = $managerEncodePhoto
//                ->insert(public_path() . $this->watermarkPath, getRandomWatermarkPosition())
                ->save($this->config_IMAGE_POST_UPLOAD_PATH.$filename);

        return $image;
    }

    /**
     * Create Icon From Original.
     */
    public function cropDefaultImage($managerEncodePhoto, $filenamecrop)
    {
        /*$manager = new ImageManager();

        list($width, $height) = getimagesize(url('/') . '/uploads/posts/' . $filename);
        if ($width > 1000 || $height > 500) {
            $image = $manager->make($photo)->encode($originalExtension)->resize(1000, 500, function($constraint) {
                        $constraint->aspectRatio();
                    })->save(config('chodangtin.IMAGE_POST_UPLOAD_PATH') . $filenamecrop);
        } else {
            $image = $manager->make($photo)->encode($originalExtension)->save(config('chodangtin.IMAGE_POST_UPLOAD_PATH') . $filenamecrop);
        }*/

        $postImageWidth = config('freelancerland.POST_IMAGE_WIDTH');
        $postImageHeight = config('freelancerland.POST_IMAGE_HEIGHT');
        $image = $managerEncodePhoto->fit($postImageWidth, $postImageHeight)
//                ->insert(public_path() . $this->watermarkPath, getRandomWatermarkPosition())
                ->save(config('freelancerland.IMAGE_POST_UPLOAD_PATH').$filenamecrop);

        return $image;
    }

    public function thumb($managerEncodePhoto, $filenamethumb)
    {
        $image = $managerEncodePhoto->resize(
                        $this->config_POST_IMAGE_THUMB_WIDTH, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
//                ->insert(public_path() . $this->watermarkPath, getRandomWatermarkPosition())
                ->save($this->config_IMAGE_POST_UPLOAD_PATH.$filenamethumb);

        return $image;
    }

    /**
     * Delete Image From Session folder, based on original filename.
     */
    public function delete($id)
    {
        $full_size_dir = $this->config_IMAGE_POST_UPLOAD_PATH;

        $sessionImage = PostImage::find($id);

        if (empty($sessionImage)) {
            return Response::json([
                        'error' => true,
                        'code' => 400,
                            ], 400);
        }

        if (File::exists($full_size_dir.$sessionImage->path)) {
            File::delete($full_size_dir.$sessionImage->path);
        }

        if (File::exists($full_size_dir.$sessionImage->pathorigin)) {
            File::delete($full_size_dir.$sessionImage->pathorigin);
        }

        if (File::exists($full_size_dir.$sessionImage->paththumb)) {
            File::delete($full_size_dir.$sessionImage->paththumb);
        }

        if (!empty($sessionImage)) {
            $sessionImage->delete();
        }

        return Response::json([
                    'error' => false,
                    'code' => 200,
                    'id' => $id,
                        ], 200);
    }

    public function sanitize($string, $force_lowercase = true, $anal = false)
    {
        $strip = ['~', '`', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '=', '+', '[', '{', ']',
            '}', '\\', '|', ';', ':', '"', "'", '&#8216;', '&#8217;', '&#8220;', '&#8221;', '&#8211;', '&#8212;',
            'â€”', 'â€“', ',', '<', '.', '>', '/', '?', ];
        $clean = trim(str_replace($strip, '', strip_tags($string)));
        $clean = preg_replace('/\s+/', '-', $clean);
        $clean = ($anal) ? preg_replace('/[^a-zA-Z0-9]/', '', $clean) : $clean;

        return ($force_lowercase) ?
                (function_exists('mb_strtolower')) ?
                        mb_strtolower($clean, 'UTF-8') :
                        strtolower($clean) :
                $clean;
    }
}
