<?php

namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Gig extends Model implements SluggableInterface
{
	use SluggableTrait;

	protected $sluggable = [
        'build_from' => 'title',
        'save_to' => 'slug',
        'unique' => false,
        'on_update' => true,
    ];

	protected $table = 'gigs';

	protected $fillable = ['id','title','description','price','deliver_day','user_id','slug','gig_status_id'];

	public function gigsExtra()
	{
		return $this->hasMany("App\GigExtra", 'gig_id','id');
	}

	public function projectGigs()
	{
		return $this->hasMany("App\ProjectGig", 'gig_id','id');
	}

	public function gigStatus()
    {
        return $this->belongsTo('App\GigStatus','gig_status_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
