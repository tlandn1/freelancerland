<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class City extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'name',
        'save_to' => 'slug',
        'unique' => true,
        'on_update' => true,
    ];

    protected $table = 'cities';

    protected $fillable = ['name', 'type', 'slug'];

    public $timestamps = false;

    public function districts()
    {
        return $this->hasMany('App\District');
    }

    public function projects()
    {
        return $this->hasMany('App\Project');
    }
}
