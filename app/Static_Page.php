<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Static_Page extends Model
{
   protected $table = 'static_pages';
   protected $fillable = ['title', 'content','slug'];
}
