<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectGigStatus extends Model
{
    protected $table = 'project_gig_statuses';

    protected $fillable = ['id','name','label'];

    public function projectGigs()
	{
		return $this->hasMany("App\ProjectGig", 'project_gig_status_id','id');
	}
}
