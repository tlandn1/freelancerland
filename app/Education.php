<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    protected $table = 'education';

    protected $fillable = ['name_school', 'year', 'degree', 'description', 'user_id'];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo("App\User", 'user_id');
    }
}
