<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_Projects extends Model
{
  protected $table = 'users_projects';

  protected $fillable = ['user_id','project_id'];
}
