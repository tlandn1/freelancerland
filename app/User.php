<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Authenticatable
{

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','name', 'email', 'password', 'provider_id', 'provider', 'avatar',
        'receive_email', 'phone', 'note', 'sodu_tk', 'skype', 'facebook', 'website', 'role_id',
        'user_status_id', 'district_id', 'cmnd_number', 'cmnd_address', 'about_job_title',
        'about_description' ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        if ($this->role->id == USER_ROLE_ADMIN_ID) {
            return true;
        }

        return false;
    }

    // public function isReviewer()
    // {
    //     if ($this->role->id == USER_ROLE_REVIEWER_ID) {
    //         return true;
    //     }
    //
    //     return false;
    // }

    public function status()
    {
        return $this->belongsTo('App\UserStatus','user_status_id');
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function district()
    {
        return $this->belongsTo('App\District');
    }

    public function educations()
    {
        return $this->hasMany("App\Education", 'user_id','id');
    }

    public function portfolios()
    {
        return $this->hasMany("App\Portfolio", 'user_id','id');
    }

    public function workingCompanies()
    {
        return $this->hasMany("App\WorkingCompany", 'user_id','id');
    }

    public function skills()
    {
        return $this->belongsToMany("App\Skill",'users_skills');
    }

    public function withdraw ()
    {
        return $this->hasMany('App\Withdraw', 'user_id','id');
    }

    public function naptien()
    {
        return $this->hasMany("App\NapTien");
    }

    public function projects()
    {
        return $this->hasMany("App\Project");
    }

    public function secondcategories()
    {
        return $this->belongsToMany("App\SecondCategory", 'users_second_categories');
    }

    public function gigs()
    {
        return $this->hasMany("App\Gig",'user_id','id');
    }

    public function projectGigs()
    {
        return $this->hasMany("App\ProjectGig",'user_id','id');
    }

    public function contests()
    {
        return $this->hasMany("App\Contests");
    }

}
