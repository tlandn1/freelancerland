<?php

namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class District extends Model implements SluggableInterface
{
    use SluggableTrait;
    protected $table = 'districts';

    protected $sluggable = [
        'build_from' => 'name',
        'save_to' => 'slug',
        'unique' => true,
        'on_update' => true,
    ];

    protected $fillable = ['name', 'type', 'slug', 'city_id'];

    public $timestamps = false;

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function users()
    {
        return $this->hasMany("App\User");
    }

    // public function posts()
    // {
    //     return $this->hasMany("App\Post");
    // }
}
