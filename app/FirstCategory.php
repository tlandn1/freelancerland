<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class FirstCategory extends Model implements SluggableInterface
{
    use SluggableTrait;
    protected $table = 'first_categories';

    protected $sluggable = [
        'build_from' => 'name',
        'save_to' => 'slug',
        'unique' => true,
        'on_update' => true,
    ];

    protected $fillable = ['name', 'slug'];

    public $timestamps = false;

    public function secondCategories()
    {
        return $this->hasMany("App\SecondCategory", 'first_category_id','id');
    }
}
