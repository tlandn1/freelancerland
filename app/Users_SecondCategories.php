<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_SecondCategories extends Model
{
  protected $table = 'users_second_categories';

  protected $fillable = ['user_id','second_category_id'];
}
