<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetEstimate extends Model
{
  protected $table = 'budget_estimate';

  protected $fillable = ['id', 'label'];

  public function projects()
  {
      return $this->hasMany('App\Project','budget_estimate_id');
  }
}
