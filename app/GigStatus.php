<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GigStatus extends Model
{
    protected $table = 'gig_statuses';

    protected $fillable = ['id','name','label'];

    public function gigs()
	{
		return $this->hasMany("App\Gig", 'gig_status_id','id');
	}
}
