var elixir = require('laravel-elixir');
var gulp = require('gulp');

var paths = {
    'jquery': 'resources/assets/vendor/jquery',
    'bootstrap': 'resources/assets/vendor/bootstrap-sass/assets',
    'adminlte': 'resources/assets/vendor/adminlte',
    'iCheck': 'resources/assets/vendor/adminlte/plugins/iCheck',
    'datatable': 'resources/assets/vendor/datatables',
    'parsleyjs': 'resources/assets/vendor/parsleyjs',
    'dropzone': 'resources/assets/vendor/dropzone',
    'js': 'resources/assets/js',
	'sass': 'resources/assets/sass',
    'croppic': 'resources/assets/vendor/croppic',
    'select2': 'resources/assets/vendor/select2',
    // 'ekko_lightbox': 'resources/assets/vendor/ekko-lightbox',
    'blockui': 'resources/assets/vendor/blockui',
    // 'jqueryMaskPlugin': 'resources/assets/vendor/jquery-mask-plugin',
    'sweetalert2': 'resources/assets/vendor/sweetalert2',
    // 'typeahead' : 'resources/assets/vendor/typeahead.js',
    'trumbowyg': 'resources/assets/vendor/trumbowyg',
    'jQueryWaccp': 'resources/assets/vendor/jQuery-Word-and-Character-Counter-Plugin',
    'fontAwesome': 'resources/assets/vendor/font-awesome',
    'bootstrapDatetimepicker': 'resources/assets/vendor/eonasdan-bootstrap-datetimepicker-npm',
    'moment': 'resources/assets/vendor/moment',
};

elixir.config.sourcemaps = false;

elixir(function (mix) {
    // Copy backend CSS files
    mix.copy([
        paths.adminlte + '/css/AdminLTE.min.css',
        paths.adminlte + '/css/skins/skin-blue.min.css',
        paths.iCheck + '/square/blue.css',
        paths.dropzone + '/min/dropzone.min.css',
        paths.croppic + '/assets/css/main-croppic.css',
        paths.croppic + '/assets/css/croppic.css',
        paths.sweetalert2 + '/sweetalert2.min.css',
        paths.select2 + '/select2.min.css',
        // paths.ekko_lightbox + '/ekko-lightbox.min.css',
        paths.datatable + '/dataTables.bootstrap.css',
        paths.trumbowyg + '/ui/trumbowyg.min.css',
        paths.trumbowyg + '/plugins/colors/ui/trumbowyg.colors.min.css',
        paths.fontAwesome + '/css/font-awesome.min.css',
        paths.sass + '/backend/bootstrap.min.css', // backend use full Bootstrap CSS
        paths.bootstrapDatetimepicker + '/css/bootstrap-datetimepicker.min.css',
    ], 'public/css/backend');

    // Copy backend JS files
    mix.copy([
        paths.jquery + '/jquery.min.js',
        paths.bootstrap + '/javascripts/bootstrap.min.js',
        paths.adminlte + '/js/adminlte.min.js',
        paths.datatable + '/jquery.dataTables.min.js',
        paths.datatable + '/dataTables.bootstrap.min.js',
        paths.iCheck + '/icheck.min.js',
        paths.croppic + '/croppic.js',
        paths.select2 + '/select2.min.js',
        paths.dropzone + '/min/dropzone.min.js',
        paths.js + '/dropzone-config.js',
        paths.sweetalert2 + '/sweetalert2.min.js',
        // paths.ekko_lightbox + '/ekko-lightbox.min.js',
        paths.blockui + '/jquery.blockUI.min.js',
        paths.js + '/ajax-dynamic-selectbox.js',
        // paths.jqueryMaskPlugin + '/jquery.mask.min.js',
        paths.parsleyjs + '/parsley.min.js',
        paths.js + '/parsley-vi.js',
        paths.bootstrapDatetimepicker + '/js/bootstrap-datetimepicker.min.js',
        paths.moment + '/moment.js',
        // paths.typeahead + '/typeahead.bundle.min.js',
        paths.trumbowyg + '/trumbowyg.min.js',
        paths.trumbowyg + '/plugins/colors/trumbowyg.colors.min.js',
        paths.jQueryWaccp + '/jquery.word-and-character-counter.min.js',
    ], 'public/js/backend');

    // Copy lang vi.min.js to trumbowyg.vi.min.js
    mix.copy(paths.trumbowyg + '/langs/vi.min.js', 'public/js/backend/trumbowyg.vi.min.js');

    // Copy trumbo svg icons
    mix.copy(paths.trumbowyg + '/ui/icons.svg', 'public/js/backend/ui/icons.svg');

    // Sass main website CSS
    mix.sass([
        'backend/main.scss'
    ], 'public/css/backend/all.css');

    // Merge backend JS files
    mix.scripts([
        'pubsub.js',
        'ajax-helpers.js',
        'backend/main.js'
    ], 'public/js/backend');

});
