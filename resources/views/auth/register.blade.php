@extends('frontend.master')
@section('title')
Đăng ký | Freelancerland
@stop
@section('class','loginpage')
@section('content')
<div id="wrap-main-content">
	<div class="wrap-login">
		<div class="container">
			<div class="row">
				@include('errors.testExit')
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="login-social clearfix">
						<a href="{{ route('social.login', ['google']) }}" class="login-gg"><i class="fa fa-google-plus"></i> Đăng nhập bằng Google</a>
						<a href="{{ route('social.login', ['facebook']) }}" class="login-face"><i class="fa fa-facebook"></i> Đăng nhập bằng Facebook</a>
						<a href="{{ route('social.login', ['yahoo']) }}" class="login-yahoo"><i class="fa fa-yahoo"></i> Đăng nhập bằng Yahoo</a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-login">
					<div class="wrap-login-form">
						<div class="login-form clearfix">
							{!! Form::open(['route'=>'post-dang-ky','method' => 'POST','id' => 'register-form','data-parsley-validate'=>'','class'=>'parsley']) !!}
								{!! Form::hidden('avatar','/images/users/avatar-default.jpg') !!}
								<div class="panel panel-default">
								  <div class="panel-heading"><strong>Đăng ký</strong></div>

								  <div class="panel-body">
										<div class="form-group name">
												{!! Form::text('name',null,['class' => 'form-control','required'=>'','data-parsley-maxlength'=>'40']) !!}
												@if ($errors->has('name')){!!$errors->first('name')!!}@endif
										</div>

										<div class="form-group email">
												{!! Form::email('email',null,['class' => 'form-control','required'=>'','data-parsley-type'=>'email']) !!}
													@if ($errors->has('email')){!!$errors->first('email')!!}@endif
										</div>

										<div class="form-group password">
												{!! Form::password('password',['class' => 'form-control','required'=>'','data-parsley-length'=>'[8,30]']) !!}
													@if ($errors->has('password')){!!$errors->first('password')!!}@endif
										</div>

	                  <p>Thông qua việc đăng ký, tôi đồng ý với<a class="text-orange" href="#"> Quy định chung</a> và
	                    <a href="#">Điều khoản sử dụng</a>, bao gồm <a href="#">Chính sách bảo mật</a> của freelancerland.vn</p>

										<div class="form-group">
											<div class="checkbox">
												<label>
													{!! Form::checkbox('receive_email',1,true) !!} Nhận thông tin (khuyến mãi, thông báo, ...) từ freelancerland.vn
												</label>
											</div>
										</div>

										<button type="submit" class="btn btn-blue">Đăng ký</button>
									</div>
									{!! Form::close() !!}
							  </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('script')

{!! Minify::javascript(array(
      '/js/frontend/parsley.min.js',
      '/js/frontend/parsley-vi.js'
  ))->withFullUrl() !!}
<script>
$(document).ready(function () {
    var parsleyForm = $('.parsley').parsley();
    if (parsleyForm) {
        parsleyForm.on('field:validated', function () {
            var ok = $('.parsley-error').length === 0;
            $('.bs-callout-info').toggleClass('hidden', !ok);
            $('.bs-callout-warning').toggleClass('hidden', ok);
        })
                .on('form:submit', function () {
                    return true; // Don't submit form for this demo
                });
    }

    $('[name=name]').attr('placeholder', htmlDecode('Họ tên *'));
		$('[name=email]').attr('placeholder', htmlDecode('Email *'));
		$('[name=password]').attr('placeholder', htmlDecode('Mật khẩu *'));
});
</script>
@stop
