@extends('frontend.master')
@section('title')
Đăng nhập | Freelancerland
@stop
@section('class','loginpage')

@section('content')
  <div id="wrap-main-content">
  	<div class="wrap-login">
  		<div class="container">
  			<div class="row">
  				<div class="col-xs-12 col-sm-6 col-md-6">
            <div class="login-social clearfix">
  						<a href="{{ route('social.login', ['google']) }}" class="login-gg"><i class="fa fa-google-plus"></i> Đăng nhập bằng Google</a>
  						<a href="{{ route('social.login', ['facebook']) }}" class="login-face"><i class="fa fa-facebook"></i> Đăng nhập bằng Facebook</a>
  						<a href="{{ route('social.login', ['yahoo']) }}" class="login-yahoo"><i class="fa fa-yahoo"></i> Đăng nhập bằng Yahoo</a>
  					</div>
  				</div>

  				<div class="col-xs-12 col-sm-6 col-md-6 col-login">
  					<div class="wrap-login-form">
  						<div class="login-form clearfix">
  							{!! Form::open(['route'=>'post-dang-nhap','method' => 'POST','id' => 'login-form','data-parsley-validate'=>'','class'=>'parsley']) !!}
                <div class="panel panel-default">
  								<div class="panel-heading"><strong>Đăng nhập</strong></div>
  								<div class="panel-body">
                    @include('errors.errorMessage')
  									<div class="form-group">
  											{!! Form::text('email',null,['class' => 'form-control','placeholder' => 'Email *','required'=>'','data-parsley-type'=>'email']) !!}
  											@if ($errors->has('email')){!!$errors->first('email')!!}@endif
  									</div>
  									<div class="form-group password">
  											{!! Form::password('password',['class' => 'form-control','placeholder' => 'Mật khẩu *','required'=>'']) !!}
  											@if ($errors->has('password')){!!$errors->first('password')!!}@endif
  									</div>
  									<div class="form-group">
  								    <div class="checkbox">
  								      <label>
  								        {!! Form::checkbox('remember') !!} Lưu đăng nhập trên máy
  								      </label>
  								    </div>
  								  </div>
  									<button id="btn-login" type="submit" class="btn btn-blue">Đăng nhập</button>
  									<a href="{{ route('quen-mat-khau') }}">Quên mật khẩu</a>
  									{!! Form::close() !!}
  								</div>
  					</div>
  				</div>
  			</div>
  		</div>
  	</div>
  </div>

@stop
@section('script')

	{!! Minify::javascript(array(
      '/js/frontend/parsley.min.js',
      '/js/frontend/parsley-vi.js'
  ))->withFullUrl() !!}
<script>
	$(document).ready(function() {
		var parsleyForm = $('.parsley').parsley();
		if (parsleyForm) {
				parsleyForm.on('field:validated', function() {
								var ok = $('.parsley-error').length === 0;
								$('.bs-callout-info').toggleClass('hidden', !ok);
								$('.bs-callout-warning').toggleClass('hidden', ok);
						})
						.on('form:submit', function() {
								return true; // Don't submit form for this demo
						});
		}
	});
</script>
@stop
