@extends('frontend.master')
@section('title')
Đặt lại mật khẩu | Freelancerland
@stop
@section('class','loginpage')

@section('content')
<div id="wrap-main-content">
	<div class="wrap-login">
		<div class="container">
			<div class="row">
				<div class="col-xs-8 col-xs-offset-2 col-login">
					<div class="wrap-login-form">
						<div class="login-form clearfix">
							{!! Form::open(['route' => ['post-doi-mat-khau'], 'method' => 'POST','id' => 'register-form','data-parsley-validate'=>'','class'=>'parsley']) !!}
  						<input type="hidden" name="token" value="{{ $token }}">
							<div class="panel panel-default">
								<div class="panel-heading"><strong>Vui lòng điền thông tin để đặt lại mật khẩu.</strong></div>
								<div class="panel-body">

									@include('errors.error')

									<div class="form-group email">
											{!! Form::text('email',null,['class' => 'form-control','required'=>'']) !!}
									</div>
									<div class="form-group">
											{!! Form::password('password',['class' => 'form-control','required'=>'','data-parsley-length'=>'[8,30]', 'id' => 'password']) !!}
									</div>
									<div class="form-group">
											{!! Form::password('password_confirmation',['class' => 'form-control','required'=>'','data-parsley-length'=>'[8,30]','data-parsley-equalto' => '#password','data-parsley-equalto-message'  => ' Xác nhận lại mật khẩu không trùng với mật khẩu']) !!}
									</div>
									<button type="submit" class="btn btn-blue">Gửi</button>
									{!! Form::close() !!}
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')

	{!! Minify::javascript(array(
      '/js/frontend/parsley.min.js',
      '/js/frontend/parsley-vi.js'
  ))->withFullUrl() !!}

<script>
	$('[name=email]').attr('placeholder', htmlDecode('Nhập email *'));
	$('[name=password]').attr('placeholder', htmlDecode('Nhập mật khẩu *'));
	$('[name=password_confirmation]').attr('placeholder', htmlDecode('Xác nhận lại mật khẩu *'));
</script>
@stop
