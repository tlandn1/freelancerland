@extends('frontend.master')
@section('title')
Quên mật khẩu | freelancesrland
@stop
@section('class','loginpage')
@section('content')
<div id="wrap-main-content">
	<div class="wrap-login">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 col-login">
					<div class="wrap-login-form">
						<div class="login-form clearfix">
							{!! Form::open(['route'=>'post-quen-mat-khau','method' => 'POST','id' => 'register-form','data-parsley-validate'=>'','class'=>'parsley']) !!}
							<div class="panel panel-default">
								<div class="panel-heading"><strong>Vui lòng nhập email của bạn để lấy lại mật khẩu.</strong></div>
								<div class="panel-body">

									@include('errors.error')
									@include('errors.successLinkStatus')

									<div class="form-group email">
											{!! Form::text('email',null,['class' => 'form-control','required'=>'']) !!}
									</div>
									<button type="submit" class="btn btn-blue"><i class="fa fa-btn fa-envelope"></i> Lấy lại mật khẩu</button>
									{!! Form::close() !!}
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('script')

	{!! Minify::javascript(array(
      '/js/frontend/parsley.min.js',
      '/js/frontend/parsley-vi.js'
  ))->withFullUrl() !!}

<script>
	$('[name=email]').attr('placeholder', htmlDecode('Nhập email *'));
</script>
@stop
