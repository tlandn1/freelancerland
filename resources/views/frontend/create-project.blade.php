@extends('frontend.master')
@section('styles-vendor')

  {!! Minify::stylesheet(array(
    '/css/frontend/sweetalert2.min.css',
    '/css/frontend/jquery.bxslider.css',
    '/css/frontend/trumbowyg.min.css',
    '/css/frontend/trumbowyg.colors.min.css',
    '/css/frontend/bootstrap-tagsinput.css',
    '/css/frontend/select2.min.css',
    '/css/frontend/bootstrap-datetimepicker.min.css'
  ))->withFullUrl() !!}

@stop
@section('title')
Đăng dự án mới
@stop
@section('content')
    <div id="wrap-main-content" class="col-xs-12">
  		<div class="fll-title-create-project col-md-6 col-md-offset-3 text-center">
  			<h2>Đăng dự án mới</h2>
  		</div>
  		<div class="fill-content-create-project">
  			<div class="container">
  				<div class="row">
  					<div class="col-xs-12">

  						<div id="step-create-project">
  							<div class="navbar col-md-6 col-md-offset-3">
  								<div class="navbar-inner">
  									<div class="">
  										<ul class="clearfix">
  											<li class="step-1 first">
  												<span class="title-tab">Tạo dự án</span>
  												<a href="#tab1" data-toggle="tab">
  													<img src="{{ cdn('/images/tao-tin.png')}}" alt="step-1">
  												</a>
  											</li>
  											<li class="step-2">
  												<span class="title-tab">Kiểm tra lại</span>
  												<a href="#tab2" data-toggle="tab">
  													<img class="none-active" src="{{ cdn('/images/active-kiem-tra.png')}}" alt="step-2">
  													<img class="active" src="{{ cdn('/images/kiem-tra.png')}}" alt="step-2">
  												</a>
  											</li>
  											<li class="step-3 last">
  												<span class="title-tab">Hoàn tất</span>
  												<a href="#tab3" data-toggle="tab">
  													<img class="none-active" src="{{ cdn('/images/active-hoan-tat.png')}}" alt="step-3">
  													<img class="active" src="{{ cdn('/images/hoan-tat.png')}}" alt="step-3">
  												</a>
  											</li>
  										</ul>
  									</div>
  								</div>
  							</div><!--end # step-create-project-->

  							<div class="tab-content">
  								<div class="tab-pane" id="tab1">
  									@include('frontend.partials.create-project.create-step1')
  								</div>
  								<div class="tab-pane fll-review-project" id="tab2">
  									@include('frontend.partials.create-project.create-step2')
  								</div>
  								<div class="tab-pane" id="tab3">
  									@include('frontend.partials.create-project.create-step3')
  								</div>
  								<ul class="pager wizard">
  									<li class="previous first" style="display:none;"><a href="javascript:;">Đầu tiên</a></li>
  									<li class="previous"><a class="btn btn-blue" href="javascript:;">Trở lại</a></li>
  									<li class="next last" style="display:none;"><a href="javascript:;">Cuối cùng</a></li>
  									<li class="next"><a class="btn btn-blue" href="javascript:;">Tiếp tục</a></li>
  								</ul>
  							</div><!--end .tab-content-->

  						</div>
  					</div>
  				</div>
  			</div>
  		</div><!--end .fill-content-create-project-->

  	</div><!--end #wrap-main-content-->
@stop
@section('scripts-vendor')
  {!! Minify::javascript(array(
    '/js/frontend/jquery.bootstrap.wizard.js',
    '/js/frontend/jquery.blockUI.min.js',
    '/js/frontend/jquery.mask.min.js',
    '/js/frontend/parsley.min.js',
    '/js/frontend/parsley-vi.js',
    '/js/frontend/ajax-dynamic-selectbox.js',
    '/js/frontend/trumbowyg.min.js',
    '/js/frontend/trumbowyg.vi.min.js',
    '/js/frontend/jquery.word-and-character-counter.min.js',
    '/js/frontend/trumbowyg.colors.min.js',
    '/js/frontend/trumbowyg.upload.min.js',
    '/js/frontend/select2.full.min.js',
    '/js/frontend/typeahead.bundle.min.js',
    '/js/frontend/bootstrap-tagsinput.min.js',
    '/js/frontend/moment.js',
    '/js/frontend/bootstrap-datetimepicker.min.js'
  ))->withFullUrl() !!}
@stop

@section('script')
  <script>
      $(function () {
          $('.datetimepicker').datetimepicker({
          //  format: 'DD/MM/YYYY'
          format: 'DD-MM-YYYY'
          });
      });
      $(document).ready(function () {
  			$( ".fll-firstCategory" ).trigger( "change" );

        var navigationFn = {
          goToSection: function(id) {
            $('html, body').animate({
              scrollTop: $(id).offset().top
            }, 0);
            return false;
            e.preventDefault();
          }
        }

        $("#step-create-project").bootstrapWizard({
          tabClass: 'nav nav-pills',
          onTabClick: function(tab, navigation, index) {
            return false;
          },
          onNext: function(tab, navigation, index) {
            if (index == 1) {
              if ($('#createProject').parsley().validate()) {
                navigationFn.goToSection('#step-create-project');
                var form = $('#createProject');
                var method = form.find('input[name="_method"]').val() || 'POST';
                var route = form.prop('action');
                console.log(route);
                $.ajax({
                  type: method,
                  url: route,
                  data: form.serialize(),
                            // dataType: 'html',
                            success: function(data) {
                              if (!$('#step-create-project .step-1').hasClass('done')) {
                                $('#step-create-project .step-1').addClass('done');
                              }
                                console.log(data);
                                $(".fll-firstCate-name").text(data.project_firstCategory);
                                $(".fll-secondcate-name").text(data.project_secondCategory);
                                $(".fll-location").text(data.project_city);
                                $(".fll-project-title").text(data.project_title);
                                $(".fll-budget-estimate").text(data.project_budget_estimate);
                                $(".fll-receive-bid").text(data.project_receive_bid);
                            }
                        });
              } else {
                return false;
              }
            }
            if (index == 2) {
              $('#step-create-project .step-2').addClass('done');
              $('#step-create-project .pager.wizard').remove();
            }
          }
        });

        //count characters
        $('[name=title]').counter({ // this is a class from the div with contenteditable="true"
          target: '.input-text-counter',
          count: 'up',
          goal: 75,
          msg: ' '
        });
        // Init editor
        // var editor = $('#form_review').trumbowyg();
        // Init counter
        var $trumbo = $('.trumbowyg-editor');
        if ($trumbo.length) {
            $trumbo.counter({ // this is a class from the div with contenteditable="true"
              target: '.trumbo-counter',
              count: 'up',
              goal: 3000,
              msg: ' '
            });
        }
  		});

      /** JS typeahead form thêm vào hồ sơ kỹ năng của user login  **/
      var engine = new Bloodhound({
          prefetch: '/ajax-skill-json',
          // '...' = displayKey: '...'
          datumTokenizer: Bloodhound.tokenizers.obj.whitespace('label'),
          queryTokenizer: Bloodhound.tokenizers.whitespace,
      });

      engine.initialize();

      $('#fll-tag-skill').tagsinput({
          typeaheadjs: {
            name: 'engine',
            displayKey: 'label',
            valueKey: 'label',
            source: engine.ttAdapter()
          },
          tagClass: 'big label label-primary',
          maxTags: 5,
          freeInput: false
      });

    var firstSecondCategoryJSON = '{!! $firstSecondCategoryJSONCR !!}';
  </script>
@stop
