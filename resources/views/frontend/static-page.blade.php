@extends('frontend.master')
@section('title')
{{ $page->title }} | Chợ Đăng Tin
@stop
@section('class','page-static')
@section('content')
<div class="wrap-breadcrumb">
   <div class="container">
       <div class="row">
           <div class="col-xs-12">
             <ul class="breadcrumb">
                 <li><a href="/" >Trang chủ</a></li>
                 <li class="active">{!! $page->title !!}</li>
            </ul>
           </div>
       </div>
   </div>
</div>

<div class="container">
  <div class="row">
      <div class="col-md-3">
           @include('frontend.partials.left-menu-static-page')
      </div>
      <div class="col-md-9">
          <h1>{!! $page->title !!}</h1>
          <p>{!! $page->content !!}</p>
      </div>
  </div>
</div>
@stop
