@extends('frontend.master')
@section('styles-vendor')

@stop
@section('title')
Nạp tiền | Freelancerland
@stop
@section('content')
    <div class="container">
        <div class="row">
					<div class="col-md-3">
            @include('frontend.partials.left-menu')
					</div>
					<div class="col-md-9">
						<div class="row">
							<div class="col-xs-12 ">
								<h1 class="title">Nạp tiền vào tài khoản</h1>
							</div>
							<div class="col-md-7">
								<div class="form-group">
									{!! Form::open(['data-add','route'=>['update-naptien',$rechargeCode],'method' => 'POST','id'=>'updateOrder','name'=>'updateOrder']) !!}
									{!! Form::hidden('price',20000) !!}
									{!! Form::hidden('method_id',1) !!}
									{!! Form::hidden('link_payment',"") !!}
									{!! Form::hidden('id',$rechargeCode) !!}
									<div class="form-group method-payment">
										<h4>1. Chọn mệnh giá</h4>
										<div class="group-payment">
											<div class="group-prices clearfix">
												{!! Form::button('20.000',array('type' => 'button', 'class' => 'choose-price btn active')) !!}
												{!! Form::button('50.000',array('type' => 'button', 'class' => 'choose-price btn')) !!}
												{!! Form::button('100.000',array('type' => 'button', 'class' => 'choose-price btn')) !!}
												{!! Form::button('150.000',array('type' => 'button', 'class' => 'choose-price btn')) !!}
												{!! Form::button('200.000',array('type' => 'button', 'class' => 'choose-price btn')) !!}
												{!! Form::button('250.000',array('type' => 'button', 'class' => 'choose-price btn')) !!}
												{!! Form::button('300.000',array('type' => 'button', 'class' => 'choose-price btn')) !!}
												{!! Form::button('500.000',array('type' => 'button', 'class' => 'choose-price btn')) !!}
												{!! Form::button('1.000.000',array('type' => 'button', 'class' => 'choose-price btn')) !!}
											</div>
											<div class="group-price-other clearfix">
												{!! Form::label('set_price','Hoặc nhập mệnh giá khác') !!}
												{!! Form::text('set_price', '',array('class'=>'text-right form-control')) !!}
											</div>
											<div class="parsley-required error"></div>
										</div>
									</div>
									<div class="form-group method-payment">
										<h4>2. Chọn hình thức thanh toán</h4>
										<div class="group-payment">
											@include('frontend.partials.payment-recharge.payment-method')
										</div>
									</div>
									{!! Form::close() !!}
								</div>
							</div>
							<div class="col-md-5">
								@include('frontend.partials.payment-recharge.review-order-recharge')
							</div>
						</div>
					</div>
				</div>
    </div><!--end container-->
@stop
@section('scripts-vendor')
  {!! Minify::javascript(array(
		'/js/frontend/jquery.mask.min.js'
		))->withFullUrl() !!}
@stop

@section('script')
<script>
jQuery(function($) {
    $(document).ready(function() {
        $('input[name=set_price]').mask('000.000.000.000.000', {
            reverse: true
        });
        $('input[name=set_price]').on('input', function() {
            var priceGet = $(this).val();
            var priceFormat = priceGet.replace(/\./gi, "");
            $('input[name=price]').val(priceFormat);
            $('.choose-price').removeClass('active');
            $(".info-ordertotal strong").text(priceGet + ' đ');
            $('input[name=method_id]').val(2);

            if (priceFormat < 20000 || priceFormat % 1000 != 0) {
                $('.error').text('Số tiền nhập vào phải lớn hơn 20.000 đ và là bội số của 1.000');
            } else {
                $('.error').text('');
            }
        });

        $('.group-prices .choose-price').click(function(event) {
            event.preventDefault();
            if (!$(this).hasClass('active')) {
                $('input[name=set_price]').val('');
                $('.error').text('');
                $('input[name=method_id]').val(1);
                $('.group-prices .choose-price').removeClass('active');
                $(this).addClass('active');
                $('input[name="price"]').val($(this).text().replace(/\./gi, ""));
                $(".info-ordertotal strong").text($(this).text());
            }
        });

        $('.payment-link').click(function(event) {
            event.preventDefault();
            $('input[name=link_payment]').val($(this).attr('href'));
            $('#updateOrder').submit();
        });
    });
});
</script>
@stop
