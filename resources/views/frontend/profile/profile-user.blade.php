@extends('frontend.master')
@section('styles-vendor')

  {!! Minify::stylesheet(array(
        '/css/frontend/main-croppic.css',
        '/css/frontend/croppic.css',
        '/css/frontend/bootstrap-datetimepicker.min.css',
        '/css/frontend/bootstrap-tagsinput.css',
  ))->withFullUrl() !!}

@stop
@section('title')
Hồ sơ của {{ $loggedInUser->name }}
@stop
@section('content')
    <div class="container">
      <div class="row">
        <div class="col-md-2">
          @include('frontend.partials.left-menu')
        </div><!--end col-md-2-->

        <div class="col-md-10">
            <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                <li class="active"><a href="#fll-info-person" data-toggle="tab">Thông tin cá nhân</a></li>
                <li><a href="#fll-info-experience" data-toggle="tab">Hồ sơ kinh nghiệm</a></li>
                <li><a href="#fll-info-portfolio" data-toggle="tab">Hồ sơ năng lực</a></li>
                <li><a href="#fll-info-skill" data-toggle="tab">Kỹ năng</a></li>
                <li><a href="#fll-info-education" data-toggle="tab">Học tập</a></li>
            </ul><!--end nav-tabs-->

            <div class="tab-content">
              @include('frontend.partials.profile-tab-content.tab-info-person')
              @include('frontend.partials.profile-tab-content.tab-info-experience')
              @include('frontend.partials.profile-tab-content.tab-info-portfolio')
              @include('frontend.partials.profile-tab-content.tab-info-skill')
              @include('frontend.partials.profile-tab-content.tab-info-education')
            </div><!-- end .tab-content -->
        </div><!--end col-md-10-->
      </div><!-- end row-->
    </div><!--end container-->

    <!-- Modal form sửa hồ sơ kinh nghiệm user hiện tại đang login -->
    @include('frontend.partials.modal-edit-profile.edit-experience')
    <!-- Modal form sửa hồ sơ kinh nghiệm user hiện tại đang login -->
    @include('frontend.partials.modal-edit-profile.edit-education')
    <!-- Modal form sửa hồ sơ năng lực user hiện tại đang login -->
    @include('frontend.partials.modal-edit-profile.edit-portfolio')
@stop
@section('scripts-vendor')
{!! Minify::javascript(array(
   '/js/frontend/bootbox.min.js',
    '/js/frontend/typeahead.bundle.min.js',
    '/js/frontend/croppic.js',
     '/js/frontend/parsley.min.js',
     '/js/frontend/parsley-vi.js',
     '/js/frontend/moment.js',
     '/js/frontend/bootstrap-tagsinput.min.js',
     '/js/frontend/bootstrap-datetimepicker.min.js',
     '/js/frontend/ajax-dynamic-selectbox.js',
     '/js/frontend/select2.full.min.js',
<<<<<<< HEAD
     '/js/frontend/imagepreview.js',
=======
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
  ))->withFullUrl() !!}

@stop

@section('script')

<script>
    jQuery(document).ready(function ($) {
    $('#tabs').tab();
    });
    //////Scrip preview avatar user
    $('.thumb').attr('src', '<?php echo $loggedInUser->avatar; ?>');
    var editUserSettings = [
        '/uploadImage/avatar', // uploadUrl
        '/cropImage/avatar', // cropUrl
        '/uploads/users/', // croppedDir
    ];

    var eyeCandy = $('#croppic');
    var croppicHeaderOptions = {
        uploadUrl: editUserSettings[0],
        cropUrl: editUserSettings[1],
        outputUrlId:'avatar',
        cropData: {
            'width': eyeCandy.width(),
            'height': eyeCandy.height()
        },
        doubleZoomControls:false,
        //imgEyecandy:false,
        loaderHtml: '<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
        onAfterImgUpload: 	function(){
            var originImageUrl = $('#croppic > div.cropImgWrapper > img').attr('src');
            var originFileName = originImageUrl.split("/").pop(); // => profile-54b58.jpg
            var fullCroppedImagePath = editUserSettings[2]+ "{{ env('CROPPED_IMAGE_PREFIX', 'cropped-')}}" + originFileName;
            //console.log(fullCroppedImagePath);
            $('#avatar').attr('value', fullCroppedImagePath);
        },
    };
    var cropperBox = new Croppic('croppic', croppicHeaderOptions);

    $('input[name=receive_email]').click(function(){
        this.value = this.checked ? 1:0;
    })
// <!--******************************************
//           datetimepicker
// ******************************************-->
  $(function () {
      $('.datetimepicker').datetimepicker({
<<<<<<< HEAD
      //  format: 'DD/MM/YYYY'
      format: 'DD-MM-YYYY'
      });
  });

=======
        format: 'DD-MM-YYYY',
      });
  });
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
// <!--******************************************
//           data-toggle="tooltip"
// ******************************************-->
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
// <!--******************************************
//
// ******************************************-->
$(document).ready(function() {
    /** JS form chỉnh sử hồ sơ kinh nghiệm của user login **/
    $('#updateExperienceForm')
        .on('submit', function(e) {
            // Save the form data via an Ajax request
            e.preventDefault();

            var $form = $(e.target),
                id    = $form.find('[name="id"]').val();
            // The url and method might be different in your application
            $.ajax({
                url: '/trang-ca-nhan/ajax-update-user-experience/' + id,
                method: 'PUT',
                data: $form.serialize()
            }).success(function(response) {
                // Get the cells
                var $button = $('button[data-id="' + response.id + '"]'),
                    $tr     = $button.closest('tr'),
                    $cells  = $tr.find('td');

                // Update the cell data
                $cells
                    .eq(0).html(response.company_name).end()
                    .eq(1).html(response.position).end()
                    .eq(2).html("Từ " + response.from + " đến " + response.to).end()
                    .eq(3).html(response.description).end();

                // Hide the dialog
                $form.parents('.bootbox').modal('hide');

                // You can inform the user that the data is updated successfully
                // by highlighting the row or showing a message box
                bootbox.alert('Chỉnh sửa hồ sơ kinh nghiệm thành công!');
            });
        });

    $('.fll-btnEdit-experience').on('click', function() {
        // Get the record's ID via attribute
        var id = $(this).attr('data-id');

        $.ajax({
            url: '/trang-ca-nhan/ajax-getdata-user-experience/' + id,
            method: 'GET'
        }).success(function(response) {
            // Populate the form fields with the data returned from server
            $('#updateExperienceForm')
                .find('[name="id"]').val(response.id).end()
                .find('[name="company_name"]').val(response.company_name).end()
                .find('[name="position"]').val(response.position).end()
                .find('[name="from"]').val(response.from).end()
                .find('[name="to"]').val(response.to).end()
                .find('[name="description"]').val(response.description).end();

            // Show the dialog
            bootbox
                .dialog({
                    title: 'Chỉnh sửa hồ sơ kinh nghiệm',
                    message: $('#updateExperienceForm'),
                    show: false // We will show it manually later
                })
                .on('shown.bs.modal', function() {
                    $('#updateExperienceForm')
                        .show()                             // Show the login form
                        // .formValidation('resetForm'); // Reset form
                })
                .on('hide.bs.modal', function(e) {
                    // Bootbox will remove the modal (including the body which contains the login form)
                    // after hiding the modal
                    // Therefor, we need to backup the form
                    $('#updateExperienceForm').hide().appendTo('body');
                })
                .modal('show');
        });
    });

    /** JS form chỉnh sửa hồ sơ học tập của user Login **/
    $('#updateEducationForm')
        .on('submit', function(e) {
            // Save the form data via an Ajax request
            e.preventDefault();

            var $form = $(e.target),
                id    = $form.find('[name="id"]').val();
            // The url and method might be different in your application
            $.ajax({
                url: '/trang-ca-nhan/ajax-update-user-education/' + id,
                method: 'PUT',
                data: $form.serialize()
            }).success(function(response) {
                // Get the cells
                var $button = $('button[data-id="' + response.id + '"]'),
                    $tr     = $button.closest('tr'),
                    $cells  = $tr.find('td');

                // Update the cell data
                $cells
                    .eq(0).html(response.name_school).end()
                    .eq(1).html(response.year).end()
                    .eq(2).html(response.degree).end()
                    .eq(3).html(response.description).end();

                // Hide the dialog
                $form.parents('.bootbox').modal('hide');

                // You can inform the user that the data is updated successfully
                // by highlighting the row or showing a message box
                bootbox.alert('Chỉnh sửa hồ sơ học tập thành công!');
            });
        });

    $('.fll-btnEdit-education').on('click', function() {
        // Get the record's ID via attribute
        var id = $(this).attr('data-id');

        $.ajax({
            url: '/trang-ca-nhan/ajax-getdata-user-education/' + id,
            method: 'GET'
        }).success(function(response) {
            // Populate the form fields with the data returned from server
            $('#updateEducationForm')
                .find('[name="id"]').val(response.id).end()
                .find('[name="name_school"]').val(response.name_school).end()
                .find('[name="year"]').val(response.year).end()
                .find('[name="degree"]').val(response.degree).end()
                .find('[name="description"]').val(response.description).end();

            // Show the dialog
            bootbox
                .dialog({
                    title: 'Chỉnh sửa hồ sơ học tập',
                    message: $('#updateEducationForm'),
                    show: false // We will show it manually later
                })
                .on('shown.bs.modal', function() {
                    $('#updateEducationForm')
                        .show()                             // Show the login form
                        // .formValidation('resetForm'); // Reset form
                })
                .on('hide.bs.modal', function(e) {
                    // Bootbox will remove the modal (including the body which contains the login form)
                    // after hiding the modal
                    // Therefor, we need to backup the form
                    $('#updateEducationForm').hide().appendTo('body');
                })
                .modal('show');
        });
    });

    /** JS form chỉnh sửa hồ sơ năng lực của user Login **/
    $('#updatePortfolioForm')
        .on('submit', function(e) {
            // Save the form data via an Ajax request
            e.preventDefault();

            var $form = $(e.target),
                id    = $form.find('[name="id"]').val();
            // The url and method might be different in your application
            $.ajax({
                url: '/trang-ca-nhan/ajax-update-user-portfolio/' + id,
                method: 'PUT',
                data: $form.serialize()
            }).success(function(response) {
                // Get the cells
                var $button = $('button[data-id="' + response.id + '"]'),
                    $tr     = $button.closest('tr'),
                    $cells  = $tr.find('td');

                // Update the cell data
                $cells
<<<<<<< HEAD
                    .eq(0).html(response.title).end()
                    .eq(1).html(response.link).end()
                    .eq(2).html(response.description).end();
=======
                    .eq(0).html(response.second_category_id).end()
                    .eq(1).html(response.title).end()
                    .eq(3).html(response.link).end()
                    .eq(4).html(response.description).end();
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a

                // Hide the dialog
                $form.parents('.bootbox').modal('hide');

                // You can inform the user that the data is updated successfully
                // by highlighting the row or showing a message box
                bootbox.alert('Chỉnh sửa hồ sơ học tập thành công!');
            });
        });

    $('.fll-btnEdit-portfolio').on('click', function() {
        // Get the record's ID via attribute
        var id = $(this).attr('data-id');

        $.ajax({
            url: '/trang-ca-nhan/ajax-getdata-user-portfolio/' + id,
            method: 'GET'
        }).success(function(response) {
            // Populate the form fields with the data returned from server
            $('#updatePortfolioForm')
                .find('[name="id"]').val(response.id).end()
<<<<<<< HEAD
                .find('[name="title"]').val(response.title).end()
=======
                .find('[name="second_category_id"]').val(response.second_category_id).end()
                .find('[name="title"]').val(response.title).end()
                // .find('[name="image"]').val(response.image).end()
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
                .find('[name="link"]').val(response.link).end()
                .find('[name="description"]').val(response.description).end();

            // Show the dialog
            bootbox
                .dialog({
                    title: 'Chỉnh sửa hồ sơ học tập',
                    message: $('#updatePortfolioForm'),
                    show: false // We will show it manually later
                })
                .on('shown.bs.modal', function() {
                    $('#updatePortfolioForm')
                        .show()                             // Show the login form
                        // .formValidation('resetForm'); // Reset form
                })
                .on('hide.bs.modal', function(e) {
                    // Bootbox will remove the modal (including the body which contains the login form)
                    // after hiding the modal
                    // Therefor, we need to backup the form
                    $('#updatePortfolioForm').hide().appendTo('body');
                })
                .modal('show');
        });
    });
});

    /** JS typeahead form thêm vào hồ sơ kỹ năng của user login  **/
    var engine = new Bloodhound({
        prefetch: '/trang-ca-nhan/ajax-getdata-skill-json',
        // '...' = displayKey: '...'
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('label'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
    });

    engine.initialize();

    $('#fll-user-skill').tagsinput({
        typeaheadjs: {
          name: 'engine',
          displayKey: 'label',
          valueKey: 'label',
          source: engine.ttAdapter()
        },
        tagClass: 'big label label-primary',
        maxTags: 10,
        freeInput: false
    });

// <!--********************************************
//         Select box City, District
//    Select box firstCategory, secondCategory
// **********************************************-->
$(document).ready(function () {
  $( ".city_id" ).trigger( "change" );
  $( ".fll-firstCategory" ).trigger( "change" );
});
<<<<<<< HEAD
=======
$(".fll-firstCategory").click( function() {
   $('#second-category').removeAttr('disabled');
   $('#edit-second-category').removeAttr('disabled');
});
$(".city_id").click( function() {
   $('#district_id').removeAttr('disabled');
});

>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
var citiesDistrictsJSON = '{!! $citiesDistrictsJSONCR !!}';
var firstSecondCategoryJSON = '{!! $firstSecondCategoryJSONCR !!}';

// <!--********************************************
//         upload image in portfolio
// **********************************************-->
<<<<<<< HEAD
=======
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $('#thumb-portfolio').attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}
$("#imageUploadPortfolio").change(function () {
  readURL(this);
});

// <!--********************************************
//         kiểm tra kích thước file upload
// **********************************************-->
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
// $('input[name=image]').change(function()
// {
//     // AJAX Request
//     $.image( '/media-upload', {file: $(this).val()} )
//         .done(function( data )
//         {
//             if(data.error)
//             {
//                 // Log the error
//                 console.log(error);
//             }
//             else
//             {
//                 // Change the image attribute
//                 $( 'img.preview' ).attr( 'src', data.path );
//             }
//         });
// });

// $(function() {
//     $('#preview').imagepreview({
//         input: '[name="image"]',
//         preview: '#preview',
//     });
// });

</script>
@stop
