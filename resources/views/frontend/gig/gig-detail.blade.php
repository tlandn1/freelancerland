@extends('frontend.master')
@section('styles-vendor')
{!! Minify::stylesheet(array(
	'/css/frontend/fontawesome-stars-o.css',
	'/css/frontend/jquery.bxslider.css',
	))->withFullUrl() !!}
	@stop
	@section('title')
	{{ $gig->title }} | Freelancerland
	@stop
	@section('content')



	<div class="container">
		<div class="row">
			<div class="col-sm-8">

				@include('frontend.partials.flash-messager')

				<div class="panel panel-default">
					<h3>{{ $gig->title }}</h3>
					<div class="row">
						<div class="col-sm-6">
							Rating
						</div>
						<div class="col-sm-6">
							<i class="fa fa-clock-o" aria-hidden="true"></i> {{ $gig->deliver_day }} Ngày
						</div>
					</div>
					<ul class="slider-gallery">
						@for( $i = 0 ; $i < 5 ; $i++ )
						<li><img class="img-responsive" src="http://placehold.it/800x400"></li>
						@endfor
					</ul>
					<div id="slider-gallery-pager" class="hidden-print">
						@for( $i = 0 ; $i < 5 ; $i++ )
						<a data-slide-index="{{ $i }}" href=""><img class="img-responsive" src="http://placehold.it/50x20"></a>
						@endfor
					</div>
					
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">Mô tả</div>
					<div class="panel-body">
						{{ $gig->description }}
					</div>
				</div>

				@if( $activeReview )
				{!! Form::open(['url'=>array('user-review-gig', $projectGigID),'method' => 'POST']) !!}
				<div class="panel panel-default">
					<div class="panel-heading">
						Đánh giá công việc
					</div>
					<div class="panel-body">
						<h4>Đánh giá</h4>
						<label>Thái độ làm việc</label>
						{{ Form::select('review_rating[]', array(
							'1' => '1', 
							'2' => '2',
							'3' => '3',
							'4' => '4',
							'5' => '5'), 1, array('class' => 'useRatingAwesome')) 
						}}
						<label>Kết quả công việc</label>
						{{ Form::select('review_rating[]', array(
							'1' => '1', 
							'2' => '2',
							'3' => '3',
							'4' => '4',
							'5' => '5'), 1, array('class' => 'useRatingAwesome')) 
						}}
						<hr/>
						<div class="form-group">
							{!! Form::textarea('review_text',null,array('class'=>'form-control')) !!}
						</div>
						<div class="form-group">
							{!! Form::hidden('type',1) !!}
							{!! Form::button('Đánh giá',array( 'type'=>'submit', 'class'=>'btn btn-primary')) !!}
						</div>
					</div>
				</div>
				{!! Form::close() !!}
				@endif

				@if( $countComments )
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<div class="col-sm-6">
								Danh sách đánh giá
							</div>
							<div class="col-sm-6">
								{!! Form::select('review', array('goodreview' => 'Đánh giá tốt', 'badreview' => 'Phàn nàn'), null,array('class'=>'pull-right form-control')); !!}
							</div>
						</div>
					</div>
					<div class="panel-body">
						@foreach( $gig->projectGigs as $projectGig)
						@if( $projectGig->user_review_rating )
						<div class="media-list">
							<div class="media">
								<div class="media-left">
									<a href="#">
										<img class="media-object img-circle" src="http://placehold.it/64x64">
									</a>
								</div>
								<div class="media-body">
									<h4 class="media-heading">
										{{ $projectGig->user->name }} - 
										{{ Form::select('rating', array(
											'1' => '1', 
											'2' => '2',
											'3' => '3',
											'4' => '4',
											'5' => '5'), 1, array(
											'class' => 'useCurrentRatingAwesome',
											'data-current-rating' => $projectGig->user_review_rating )) 
										}}
									</h4>
									<p>{{ $projectGig->user_review_text }}</p>
									[Thời gian]

									@if( $projectGig->seller_review_rating )
									<div class="media">
										<div class="media-left">
											<a href="#">
												<img class="media-object img-circle" src="http://placehold.it/40x40">
											</a>
										</div>
										<div class="media-body">
											<h4 class="media-heading">
												Seller's Feedback
												{{ Form::select('rating', array(
													'1' => '1', 
													'2' => '2',
													'3' => '3',
													'4' => '4',
													'5' => '5'), 1, array(
													'class' => 'useCurrentRatingAwesome',
													'data-current-rating' => $projectGig->seller_review_rating )) 
												}}
											</h4>
											<p>{{ $projectGig->seller_review_text }}</p>
											[Thời gian]
										</div>
									</div>
									@else
									{!! Form::open(['url'=>array('user-review-gig', $projectGig->id),'method' => 'POST']) !!}
									<div class="panel panel-default">
										<div class="panel-heading">
											Đánh giá người mua
										</div>
										<div class="panel-body">
											<h4>Đánh giá</h4>
											<label>Thái độ làm việc</label>
											{{ Form::select('review_rating[]', array(
												'1' => '1', 
												'2' => '2',
												'3' => '3',
												'4' => '4',
												'5' => '5'), 1, array('class' => 'useRatingAwesome')) 
											}}
											<label>Kết quả công việc</label>
											{{ Form::select('review_rating[]', array(
												'1' => '1', 
												'2' => '2',
												'3' => '3',
												'4' => '4',
												'5' => '5'), 1, array('class' => 'useRatingAwesome')) 
											}}
											<hr/>
											<div class="form-group">
												{!! Form::textarea('review_text',null,array('class'=>'form-control')) !!}
											</div>
											<div class="form-group">
												{!! Form::hidden('type',2) !!}
												{!! Form::button('Đánh giá',array( 'type'=>'submit', 'class'=>'btn btn-primary')) !!}
											</div>
										</div>
									</div>
									{!! Form::close() !!}
									@endif

								</div>
							</div>
						</div>
						@endif
						@endforeach
					</div>
				</div>
				@endif
			</div><!--main left-->

			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Giá: {{ formatPrice( $gig->price ) }}</h4>
					</div>
					<div class="panel-body">
						<a href="#" class="btn btn-success">Đặt hàng</a>
					</div>
				</div>

				@if( !$gig->gigsExtra->isEmpty() )
				<div class="panel panel-default">
					<div class="panel-heading">Dịch vụ cộng thêm</div>
					<div class="panel-body">
						{!! Form::open(['url'=>'','method' => 'GET']) !!}
						<table>
							<thead>
								<tr>
									<th>Chọn</th>
									<th>Tên gói</th>
									<th>Giá</th>
								</tr>
							</thead>
							<tbody>
								@foreach( $gig->gigsExtra as $gigExtra )
								<tr>
									<td>
										{!! Form::checkbox('gigsExtra', $gigExtra->id, false); !!}
									</td>
									<td>
										<h4>{{ $gigExtra->title }}</h4>
										<i class="fa fa-clock-o" aria-hidden="true"></i> {{ $gigExtra->deliver_day }} Ngày
									</td>
									<td>
										{{ formatPrice( $gigExtra->price ) }}
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						{!! Form::close() !!}
					</div>
				</div>
				@endif

				<div class="text-center">
					<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					<a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
					<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						Thông tin Freelancer
					</div>
					<div class="panel-body">
						<div class="text-center">
							<img class="img-circle" src="http://placehold.it/130x130"/>
							<h4>{{ $gig->user->name }}</h4>
							<p>Top Rated Seller</p>
						</div>
						<hr/>
						<div class="row">
							<div class="col-sm-6">
								From
								United States
								Positive Rating
								97%
							</div>
							<div class="col-sm-6">
								Speaks
								English
								Avg. Response Time
								2 Hrs.
							</div>
						</div>
						<hr/>
						<div>
							{{ $gig->user->about_description }}
						</div>
						<hr/>
						<button type="button" class="btn btn-default"><i class="fa fa-comment-o" aria-hidden="true"></i> Default</button>
					</div>
				</div>
			</div><!--sidebar-->
		</div>
	</div><!--end container-->
	@stop
	@section('scripts-vendor')
	{!! Minify::javascript(array(
		'/js/frontend/jquery.barrating.min.js',
		'/js/frontend/jquery.bxslider.min.js',
		))->withFullUrl() !!}

		@stop

		@section('script')

		@stop
