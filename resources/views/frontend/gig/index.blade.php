@extends('frontend.master')
@section('styles-vendor')

@stop
@section('title')
Gói công việc | Freelancerland
@stop
@section('content')
<div class="container">
	<div class="row">
		@foreach( $gigs as $gig )
		<div class="col-xs-3">
			<div class="panel panel-default">
				<img class="img-responsive" src="http://placehold.it/350x200">
				<div class="panel-body">
					<h4><a href="{{ route('goi-viec',$gig->slug) }}" >{{ $gig->title }}</a></h4>
					Giá chỉ từ
					<h3>{{ $gig->price }}</h3>
					<i class="fa fa-clock-o" aria-hidden="true"></i> {{ $gig->deliver_day }} Ngày
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div><!--end container-->
@stop
@section('scripts-vendor')

@stop

@section('script')

@stop
