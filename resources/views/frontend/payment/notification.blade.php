@extends('frontend.master')
@section('title')
Thanh toán | Chợ Đăng Tin
@stop
@section('class','payment-notification')
@section('content')

<div class="wrap-payment-nganluong">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class=" main-content-post">
          @if ($note)
          <h1 class="note {{ checkNote($note['status']) }}">{!! $note['content'] !!}</h1>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
@stop