@extends('frontend.master')
@section('title')
Liên Hệ | Freelancerland.vn
@stop
@section('class','contact-page')
@section('content')
{{--@include('frontend.partials.breadcrumb')--}}
<div class="container">
    <div class="row">
        <!-- <div class="col-xs-12"> -->
            <div class="col-md-3">
               @include('frontend.partials.left-menu-static-page')
            </div>
            <div class="col-md-9">
              <div class="contact">
                <h1>Liên Hệ</h1>
                <p>
                  Freelancerland.vn luôn luôn mong muốn nhận được những ý kiến phản ánh của khách hàng, giúp chúng tôi điều chỉnh, nâng cao chất lượng dịch vụ và phục vụ khách hàng tốt, hiệu quả hơn.
                </p>
                <h2>Mọi góp ý, xin liên hệ:</h2>
                <div>
                  <p> - Email: <a href="#">lienhe@freelancerland.vn</a></p>
                  <p> - Điện thoại: 099 327 9150</p>
                  <p> - Văn phòng giao dịch: K285/35 Trần Cao Vân, Quận Thanh Khê, Đà Nẵng</p>
                </div>
              </div>
              <div class="form-contact">
              <h2>Quý khách hàng có thể chia sẻ ý kiến tại đây: </h2>
              {!! Form::open(['route'=>['lien-he'],'method' => 'POST','class'=>'parsley','data-parsley-validate'=>'']) !!}
              @if (Session::has('flash_message'))
                <div class="alert alert-{!! Session::get('flash_level') !!}">
                    {!! Session::get('flash_message') !!}
                </div>
              @endif
              <div class="form-group">
                  {!! Form::label('name', 'Họ Tên *') !!}
                  {!! Form::text('name',null,['class' => 'form-control','required'=>'','placeholder'=>'']) !!}
              </div>

              <div class="form-group">
                  {!! Form::label('email', 'Email *') !!}
                  {!! Form::email('email',null,['class' => 'form-control','required'=>'','placeholder'=>'']) !!}
              </div>

              <div class="form-group">
                  {!! Form::label('phone', 'Số điện thoại') !!}
                  {!! Form::text('phone',null,['class' => 'form-control','placeholder'=>'']) !!}
              </div>

              <div class="form-group">
                  {!! Form::label('messages', 'Nội dung *') !!}
                  {!! Form::textarea('messages',null,['class' => 'form-control useCounter','required'=>'','placeholder'=>'Vui lòng nhập Tiếng Việt có dấu']) !!}
                  <div class="count-characters">Đã nhập <span class="input-text-counter"> </span>/2000 ký tự</div>
              </div>

              <div class="form-group">
                  {!! Form::submit('Gửi', ['class'=>'btn btn-primary']) !!}
              </div>
              {!! Form::close() !!}
            </div>
          </div>
        <!-- </div> -->
    </div>
</div>
@stop
@section('scripts-vendor')

{!! Minify::javascript(array(
      '/js/frontend/jquery.word-and-character-counter.min.js',
      '/js/frontend/parsley.min.js',
      '/js/frontend/parsley-vi.js'
  ))->withFullUrl() !!}

@stop
