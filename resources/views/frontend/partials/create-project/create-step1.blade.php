    <div class="row">

    	<div class="col-md-6 col-md-offset-3">
    		<h3 class="title-insert-info">NHẬP THÔNG TIN</h3>
    		<div class="row">
    			{!! Form::open(['data-add','route'=>'post-create-project','method' => 'POST','id' => 'createProject','data-parsley-validate'=>'','class'=>'parsley']) !!}
          <div class="col-xs-12">
            <div class="form-group">
              {!! Form::label('Chọn lĩnh vực') !!}
              {!! Form::select('first_category_id',$firstCategories,null,['class'=>'form-control fll-firstCategory']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('Chọn loại công việc') !!}
              {!! Form::select('second_category_id',[],null,['class'=>'form-control fll-secondCategory']) !!}
            </div>
          </div>
          <div class="col-xs-12">
            <div class="form-group">
              {!! Form::label('Tên công việc *') !!}
              {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Vui lòng nhập Tiếng Việt có dấu','required'=>'','data-parsley-maxlength'=>'75']) !!}
              <div class="count-characters">Đã nhập <span class="input-text-counter"> </span>/75 ký tự</div>
            </div>
            <div class="form-group">
              {!! Form::label('Mô tả dự án *') !!}
              {!! Form::textarea('description',null,['class'=>'form-control useTrumbowyg','placeholder'=>'Vui lòng nhập Tiếng Việt có dấu','required'=>'','data-parsley-maxlength'=>'3000','data-parsley-errors-container'=>"#errors"]) !!}
              <div id="errors"></div>
              <div class="count-characters">Đã nhập <span class="trumbo-counter"> </span>/3000 ký tự</div>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="form-group">
                {!! Form::label('end_receive_bid','Hạn nhận chào giá') !!}
                <div class="input-group date datetimepicker">
                  {!! Form::text('end_receive_bid',null,['class' => 'form-control']) !!}
                  <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                </div>
            </div>
          </div>
        	<div class="col-xs-12">
    				<div class="form-group">
    					{!! Form::label('Tỉnh/Thành phố') !!}
    					{!! Form::select('city_id',$cities,null,['class'=>'form-control useSelect2 city_id']) !!}
    				</div>
    			</div>
          <div class="col-xs-12">
            <div class="form-group">
              {!! Form::label('Ngân sách dự kiến') !!}
              {!! Form::select('budget_estimate_id',$budgetEstimate,null,['class'=>'form-control']) !!}
            </div>
          </div>
          <div class="col-xs-12">
    				<div class="form-group">
    					{!! Form::label('Yêu cầu kỹ năng') !!}
    					{!! Form::text('list_skills',null,['class'=>'form-control','data-role'=>'tagsinput','id'=>'fll-tag-skill']) !!}
    				</div>
    			</div>
    			{!! Form::close() !!}
    		</div>
    	</div>

    </div><!-- end .row-->
