		<div class="row">
			<div class="col-xs-12">
				<div class="border-box">
					<ul>
						<li>
							<span>Chuyên mục:</span>
							<strong class="fll-firstCate-name">&nbsp;</strong>
							<div> > <strong class="fll-secondcate-name">&nbsp;</strong></div>
						</li>
						<li>
							<span>Khu vực</span>
							<strong><span class="fll-location">&nbsp;</span></strong>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<h4><i class="fa fa-circle"></i> <span class="fll-project-title">&nbsp;</h4></span>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="border-box">
					<div class="price"></div>
					<ul>
						<li class="fll-budget-estimate"><span>Ngân sách dự kiến:</span><h2><span>&nbsp;</span></h2></li>
						<li><span>Hạn nhận chào giá</span> <strong class="fll-receive-bid">&nbsp;</strong></li>
						<li><span>Yêu cầu kỹ năng:</span> <strong class="fll-skill">&nbsp;</strong></li>
					</ul>
					<div class="border-box content">
					</div>
					<ul class="more-info">
					</ul>
				</div>
			</div>
		</div>
