<footer class="wrap-footer hidden-print">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 info-site">
                <div class="title-footer">
                  <i class="fa fa-coffee"></i>  Việc freelancer
                </div>
                <hr/>
                <ul class="menu-nav">
                    <li><a href="#">Lâp trình web</a></li>
                    <li><a href="#">Lập trình di động</a></li>
                    <li><a href="#">Đồ họa và thiết kế</a></li>
                    <li><a href="#">SEO</a></li>
                    <li><a href="#">Thiết kế website</a></li>
                    <li><a href="#">Quản lý fanpage</a></li>
                </ul>
            </div>
            <div class="col-xs-12 menu-footer-stay-connect">
                <div class="title-footer">
                  <i class="fa fa-coffee"></i>  Làm freelancer
                </div>
                <hr/>
                <ul class="menu-nav">
                    <li><a href="#">IT và lập trình</a></li>
                    <li><a href="#">Thiết kế</a></li>
                    <li><a href="#">Marketing & Bán hàng</a></li>
                    <li><a href="#">Viết lách & dịch thuật</a></li>
                </ul>
            </div>
            <div class="col-xs-12 menu-footer-support">
                <div class="title-footer">
                  <i class="fa fa-coffee"></i>  Freelancer
                </div>
                <hr/>
                <ul class="menu-nav">
                    <li><a href="#">Giới thiệu về freelancerland</a></li>
                    <li><a href="#">Điều khoản sử dụng</a></li>
                    <li><a href="#">Điều khoản khách hàng</a></li>
                    <li><a href="#">Điều khoản freelancer</a></li>
                    <li><a href="#">Đối tác</a></li>
                    <li><a href="#">Liên hệ</a></li>
                    <li><a href="#">Blog freelancerland</a></li>
                </ul>
            </div>
            <div class="col-xs-12 menu-footer-about-us">
                <div class="title-footer">
                  <i class="fa fa-coffee"></i>  Bạn chưa biết
                </div>
                <hr/>
                <ul class="menu-nav">
                    <li><a href="#">Trợ giúp</a></li>
                    <li><a href="#">Câu hỏi thường gặp</a></li>
                    <li><a href="#">Quy định bảo mật</a></li>
                    <li><a href="#">Hướng dẫn thanh toán</a></li>
                    <li><a href="#">Hướng dẫn sử dụng</a></li>
                    <li><a href="#">Gợi ý hoàn thiện hồ sơ</a></li>
                    <li><a href="#">Xác thực hồ sơ</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<div class="wrap-footer-bottom hidden-print">
    <div class="container">
      <div class="wrap-footer hidden-print">
          <div class="container">
        <div class="row">
            <div class="col-xs-12 info-site">
                <div class="title-footer">
                  Kết nối ngay
                </div>
                <!-- <div class="pull-left">
                  <ul class="social-icons">
                    <li><i class="fa fa-facebook"></i></li>
                    <li><i class="fa fa-twitter"></i></li>
                    <li><i class="fa fa-google-plus"></i></li>
                    <li><i class="fa fa-linkedin"></i></a></li>
                    <li><i class="fa fa-youtube"></i></li>
                  </ul>
                </div> -->
                <div>
                    <i class="fa fa-facebook"></i>
                    <i class="fa fa-twitter"></i>
                    <i class="fa fa-google-plus"></i>
                    <i class="fa fa-linkedin"></i>
                    <i class="fa fa-youtube"></i>
                </div>
            </div>
            <div class="col-xs-12 menu-footer-stay-connect">
                <div class="title-footer">
                  Hỗ trợ thanh toán
                </div>
                VISA
            </div>
            <div class="col-xs-12 menu-footer-support">
                <div class="title-footer">
                  Ngôn ngữ
                </div>
                Tiếng việt
            </div>
        </div>
        </div>
    </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="content-footer">
                  <div>Freelancerland ĐN - Lựa chọn số 1 của doanh nghiệp</div>
                  <div><i class="fa fa-map-marker"></i> <span>K285/35 Trần Cao Vân, Quận Thanh Khê, Đà Nẵng</span></div>
                  <div><i class="fa fa-phone"></i> 099 327 9150</div>
                  <div><i class="fa fa-envelope-o"></i> <a href="mailto:lienhe@chodangtin.vn">lienhe@freelancerland.vn</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
