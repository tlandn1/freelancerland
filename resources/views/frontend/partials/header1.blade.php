
<header>
    <div class="top-header hidden-print">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 left-header social">
                    <ul class="clearfix">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6 right-header auth">
                    <ul class="clearfix">
                        @if(!$loggedInUser)
                        <li id="bt-member"><a href="/dang-nhap" class="bt-member"><i class="fa fa-unlock-alt"></i> Đăng nhập</a></li>
                        <li><a href="/dang-ky"><i class="fa fa-user-plus"></i> Đăng ký</a></li>
                        <li><a href="/quen-mat-khau"><i class="fa fa-question"></i> Quên mật khẩu</a></li>
                        @else
                        <li class="account-coin">
                            <i class="fa fa-money" aria-hidden="true"></i>
                            {{ formatPrice($loggedInUser->sodu_tk) }}
                        </li>

                        @include('frontend.partials.header-notifications')

                        @include('frontend.partials.header-messages')

                        <li class="dropdown user-control">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                @if($user->avatar)
                                <img class="img-circle" src="{{ cdn($user->avatar) }}">
                                @else
                                <img class="img-circle" src="{{ cdn('/images/users/avatar-default.jpg') }}">
                                @endif
                                {{ $user->name }} <i class="fa fa-bars"></i></span>
                            </a>
                            <ul class="dropdown-menu left">
                                <li id="favorite-posting" class="item-darker"><a href="{{ route('yeu-thich') }}"><i class="fa fa-heart"></i>Quản lý tin yêu thích</a></li>
                                <li id="manage-posting" class="item-darker"><a href="{{ route('tin-dang-duoc-duyet') }}"><i class="fa fa-desktop"></i>Quản lý tin đăng</a></li>
                                <li id="manage-order"><a href="{{ route('hoa-don') }}"><i class="fa fa-list-alt"></i>Quản lý hóa đơn</a></li>
                                <li id="withdrawMoney"><a href="{{ route('rut-tien') }}"><i class="fa fa-university" aria-hidden="true"></i>{{ trans('chodangtin-form-frontend.tai-khoan-ngan-hang') }}</a></li>
                                <li id="rechargeHistory"><a href="{{ route('lichsu-naptien') }}"><i class="fa fa-history" aria-hidden="true"></i>{{ trans('chodangtin-form-frontend.lich-su-nap-tien') }}</a></li>
                                <li id="messages" class="item-darker"><a href="{{ route('tin-nhan.index') }}"><i class="fa fa-commenting-o" aria-hidden="true"></i>Tin nhắn</a></li>
                                <li id="notification" class="item-darker"><a href="{{ route('thong-bao') }}"><i class="fa fa-bell-o"></i>Thông báo</a></li>
                                <li id="information-user"><a href="{{ route('informationUser') }}"><i class="fa fa-user"></i>Thông tin cá nhân</a></li>
                                <li><a href="{{ route('changepassword') }}"><i class="fa fa-key"></i>Đổi mật khẩu</a></li>
                                <li id="fa-sign-out"><a href="{{ route('auth.logout') }}"><i class="fa fa-sign-out"></i>Đăng xuất</a></li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-4 logo">
                    <a href="{{ url() }}">
                        <img src="{{ cdn('/images/logo.png') }}" class="img-responsive">
                    </a>
                </div>
                <div class="col-xs-12 col-sm-9 col-md-8 wrap-right-header clearfix">
                    <div class="call-us">
                        <div class="img-icon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="info">
                            <h5>Điện thoại</h5>
                            <h5><a href="">099 327 9150</a></h5>
                        </div>
                    </div>
                    <div class="email">
                        <div class="img-icon">
                            <i class="fa fa-envelope"></i>
                        </div>
                        <div class="info">
                            <h5>Email</h5>
                            <h5><a href="mailto:lienhe@chodangtin.vn">lienhe@chodangtin.vn</a></h5>
                        </div>
                    </div>
                    <div class="add-post hidden-print">
                        <a href="/dang-tin"><i class="fa fa-plus-circle"></i> Đăng tin miễn phí</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
