<footer class="wrap-footer hidden-print">
  <div id="footer" class="container">
  <div class="row text-center">
      <div class="col-sm-6 col-sm-offset-3">
          <strong>Copyright &copy; 2016 <a href="/">freelancerland.vn</a></strong>
      </div>
  </div>
</div>
</footer>
