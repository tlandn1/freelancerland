    <div class="tab-pane" id="fll-info-skill">
      <h4>THÊM VÀO HỒ SƠ KỸ NĂNG</h4>
      {!! Form::open(['route'=>'post-ho-so-ky-nang','class'=>'form-horizontal','method'=>'POST']) !!}
        <div class="form-group">
            {!! Form::label('skill','Kỹ năng chính',['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-4">
              {!! Form::text('list_skills',$list_userSkill,['class'=>'form-control','data-role'=>'tagsinput','id'=>'fll-user-skill']) !!}
            </div>
        </div>
        {!! Form::submit('Lưu hồ sơ',['class'=>'btn btn-success']) !!}
      {!! Form::close() !!}
    </div><!-- end tab-pane-->
