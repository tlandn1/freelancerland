    <div class="tab-pane" id="fll-info-education">
      <h4>QUÁ TRÌNH HỌC TẬP</h4>
      <div class="table-responsive">
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
                <th>Nơi học tập</th>
                <th>Năm tốt nghiệp</th>
                <th>Loại bằng</th>
                <th>Mô tả</th>
                <th>Hành động</th>
            </tr>
          </thead>
          <tbody>
            @foreach($loggedInUser->educations as $education)
            <tr>
                <td>{!! $education->name_school !!}</td>
                <td>{!! $education->year !!}</td>
                <td>{!! $education->degree !!}</td>
                <td>{!! $education->description !!}</td>
                <td>
                  <button type="button" data-id="{!! $education->id !!}" class="btn btn-default fll-btnEdit-education">Sửa</button>
                  {!! Form::open(['route'=>['xoa-ho-so-hoc-tap',$education['id']],'method'=>'POST']) !!}
                    {!! Form::button('Xóa',['type'=>'submit','class'=>'btn btn-default']) !!}
                  {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>

      <h4>THÊM HỒ SƠ</h4>
      {!! Form::open(['route'=>'post-ho-so-hoc-tap','class'=>'form-horizontal','method'=>'POST']) !!}
        <div class="form-group">
            {!! Form::label('name_school','Trên trường',['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-4">
              {!! Form::text('name_school',null,['class' => 'form-control','placeholder'=>'Tên trường, khóa học']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('year','Năm tốt nghiệp',['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-4 input-group date datetimepicker">
<<<<<<< HEAD
              {!! Form::text('year',null,['class' => 'form-control']) !!}
=======
              {!! Form::text('year',null,['class' => 'form-control datetimepicker']) !!}
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
              <span class="input-group-addon">
                <i class="fa fa-calendar" aria-hidden="true"></i>
              </span>
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('degree','Loại bằng',['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-4">
              {!! Form::text('degree',null,['class' => 'form-control','placeholder'=>'Loại bằng']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('description','Mô tả',['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-6">
              {!! Form::textarea('description',null,['class' => 'form-control','placeholder'=>'Mô tả']) !!}
            </div>
        </div>
        {!! Form::submit('Lưu hồ sơ',['class'=>'btn btn-success']) !!}
      {!! Form::close() !!}
    </div><!-- end tab-pane-->
