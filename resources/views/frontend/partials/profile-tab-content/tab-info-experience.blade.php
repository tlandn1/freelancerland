    <div class="tab-pane" id="fll-info-experience">
      <h4>KINH NGHIỆM LÀM VIỆC</h4>
      <div class="table-responsive">
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
                <th>Tên Công Ty</th>
                <th>Vị trí</th>
                <th>Thời gian</th>
                <th>Mô tả</th>
                <th>Hành động</th>
            </tr>
          </thead>
          <tbody>
            @foreach($loggedInUser->workingCompanies as $workingCompany)
            <tr>
                <td>{!! $workingCompany->company_name !!}</td>
                <td>{!! $workingCompany->position !!}</td>
                <td>Từ {!! $workingCompany->from !!} đến {!! $workingCompany->to !!}</td>
                <td>{!! $workingCompany->description !!}</td>
                <td>
                  <button type="button" data-id="{!! $workingCompany->id !!}" class="btn btn-default fll-btnEdit-experience">Sửa</button>
                  {!! Form::open(['route'=>['xoa-ho-so-kinh-nghiem',$workingCompany['id']],'method'=>'POST']) !!}
                    {!! Form::button('Xóa',['type'=>'submit','class'=>'btn btn-default']) !!}
                  {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <h4>THÊM KINH NGHIỆM LÀM VIỆC</h4>
<<<<<<< HEAD
      {!! Form::open(['route'=>'post-ho-so-kinh-nghiem','class'=>'form-horizontal','method'=>'POST']) !!}
      <table class="">
          <tbody>
            <tr>
              <td class="col-md-3">Tên Công Ty</td>
              <td class="col-md-6 pull-left" data-toggle='tooltip' data-placement='right' title="VD: Tên công ty TNHH ABC">
                {!! Form::text('company_name',null,['class' => 'form-control','placeholder'=>'Tên Công ty']) !!}
              </td>
            </tr>
            <tr>
              <td class="col-md-3"></td>
              <td class="col-md-9">
                <div class="subtitle">
                  Tên công ty bạn đã làm.
                </div>
              </td>
            </tr>
            <tr>
              <td class="col-md-3">Vị trí</td>
              <td class="col-md-6 pull-left" data-toggle='tooltip' data-placement='right' title="VD: Nhân viên, Leader vv...">
                {!! Form::text('position',null,['class' => 'form-control','placeholder'=>'Vị trí làm việc']) !!}
              </td>
            </tr>
            <tr>
              <td class="col-md-3"></td>
              <td class="col-md-9">
                <div class="subtitle">
                  Vị trí làm việc trong công ty
                </div>
              </td>
            </tr>
            <tr>
              <td class="col-md-3">Ngày bắt đầu</td>
              <td class="col-md-6 pull-left " data-toggle='tooltip' data-placement='right' title="VD: 01-01-2016">
                <div class="input-group date datetimepicker">
                  {!! Form::text('from',null,['class' => 'form-control']) !!}
                  <span class="input-group-addon">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                  </span>
                </div>
              </td>
            </tr>
            <tr>
              <td class="col-md-3"></td>
              <td class="col-md-9">
                <div class="subtitle">
                  Ngày bắt đầu làm việc.
                </div>
              </td>
            </tr>
            <tr>
              <td class="col-md-3">Ngày kết thúc</td>
              <td class="col-md-6 pull-left" data-toggle='tooltip' data-placement='right' title="VD: 01-01-2016">
                <div class="input-group date datetimepicker">
                  {!! Form::text('to',null,['class' => 'form-control']) !!}
                  <span class="input-group-addon">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                  </span>
                </div>
              </td>
            </tr>
            <tr>
              <td class="col-md-3"></td>
              <td class="col-md-9">
                <div class="subtitle">
                  Ngày kết thúc làm việc.
                </div>
              </td>
            </tr>
            <tr>
              <td class="col-md-3">Mô tả</td>
              <td class="col-md-9" data-toggle='tooltip' data-placement='top' title="Mô tả">
                {!! Form::textarea('description',null,['class' => 'form-control']) !!}
              </td>
            </tr>
            <tr>
              <td class="col-md-3"></td>
              <td class="col-md-9">
                <div class="subtitle">
                  Hãy viết thật chi tiết những việc đã làm tại công ty để người xem có thể hiểu được những công việc thực sự bạn đã làm.
                </div>
              </td>
            </tr>
          </tbody>
      </table>
=======
      {!! Form::open(['route'=>'post-ho-so-kinh-nghiem','class'=>'form-horizontal parsley', 'files'=>true, 'method'=>'POST', 'data-parsley-validate']) !!}
      <dl class="dl-horizontal">
          <dt class="">Tên Công Ty (*)</dt>
          <dd class="" >
            {!! Form::text('company_name',null,['class' => 'form-control','placeholder'=>'Tên Công ty', 'required'=>'', 'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'VD: Tên công ty TNHH ABC', 'style' => 'width: 50%;']) !!}
            <div class="subtitle">
              Tên công ty bạn đã làm.
            </div>
          </dd>
      </dl>

      <dl class="dl-horizontal">
          <dt class="">Vị trí</dt>
          <dd class="" >
            {!! Form::text('position',null,['class' => 'form-control','placeholder'=>'Vị trí làm việc', 'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'VD: Giám đốc, Nhân viên, Leader vv...', 'style' => 'width: 50%;']) !!}
            <div class="subtitle">
              Vị trí làm việc trong công ty.
            </div>
          </dd>
      </dl>
      <dl class="dl-horizontal">
          <dt class="">Ngày bắt đầu</dt>
          <dd class="" >
            <div class="input-group date datetimepicker" data-toggle="tooltip" data-placement="right" title="VD: 01-01-2016" style = "width: 50%;">
              {!! Form::text('from',null,['class' => 'form-control datetimepicker']) !!}
              <span class="input-group-addon">
                <i class="fa fa-calendar" aria-hidden="true"></i>
              </span>
            </div>
            <div class="subtitle">
              Ngày bắt đầu làm việc.
            </div>
          </dd>
      </dl>
      <dl class="dl-horizontal">
          <dt class="">Ngày kết thúc</dt>
          <dd class="" >
            <div class="input-group date datetimepicker" data-toggle="tooltip" data-placement="right" title="VD: 01-01-2016" style = "width: 50%;">
              {!! Form::text('to',null,['class' => 'form-control datetimepicker']) !!}
              <span class="input-group-addon">
                <i class="fa fa-calendar" aria-hidden="true"></i>
              </span>
            </div>
            <div class="subtitle">
              Ngày kết thúc làm việc.
            </div>
          </dd>
      </dl>
      <dl class="dl-horizontal">
          <dt class="">
            {!! Form::label('Mô tả') !!}
          </dt>
          <dd class="" style = 'width: 75%;'>
            {!! Form::textarea('description',null,['class' => 'form-control', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Mô tả kinh nghiệm làm việc tại công ty']) !!}
            <div class="subtitle">
              Hãy viết thật chi tiết những việc đã làm tại công ty để người xem có thể hiểu được những công việc thực sự bạn đã làm.
            </div>
          </dd>
      </dl>
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
    {!! Form::submit('Lưu hồ sơ',['class'=>'btn btn-success']) !!}
  {!! Form::close() !!}
</div><!-- end tab-pane-->
