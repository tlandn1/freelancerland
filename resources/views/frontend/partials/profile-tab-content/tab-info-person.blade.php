    <div class="tab-pane active" id="fll-info-person">
      {!! Form::model($loggedInUser,['route'=>['post-thong-tin-ca-nhan'],'method' => 'POST','class'=>'form-horizontal']) !!}
        @if (Session::has('message'))<div class="alert alert-success">{{ Session::get('message') }}</div>@endif
        <h4>THÔNG TIN CƠ BẢN</h4>
        <div class="form-group">
            {!! Form::label('avatar','Ảnh đại diện',['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-4" id="avatar-croppic">
              {!! Form::hidden('avatar', $loggedInUser->avatar,['class'=>'form-control', 'id' => 'avatar']) !!}
              <div id="croppic" style="width:{{  config('freelancerland.IMAGE_AVATAR_WIDTH') }}px;height:{{ config('freelancerland.IMAGE_AVATAR_HEIGHT') }}px"></div>
            </div>
            <div class="col-sm-4">
              <img alt="avatar-user" class="img-thumbnail thumb">
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('email','Email',['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-4">
              {!! Form::text('email',null,['class' => 'form-control','disabled','id'=>'email']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('name','Họ và tên',['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-4" data-toggle='tooltip' data-placement='right' title="Họ và tên của bạn">
              {!! Form::text('name',null,['class' => 'form-control']) !!}
              @if ($errors->has('name'))<p style="color:red;">{!!$errors->first('name')!!}</p>@endif
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('phone','Số điện thoại',['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-4" data-toggle='tooltip' data-placement='right' title="Số điện thoại">
              {!! Form::text('phone',null,['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('skype','Skype',['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-4" data-toggle='tooltip' data-placement='right' title="Nick skype của bạn">
              {!! Form::text('skype',null,['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('facebook','Facebook',['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-4" data-toggle='tooltip' data-placement='right' title="Nick facebook của bạn">
              {!! Form::text('facebook',null,['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('website','Website',['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-4" data-toggle='tooltip' data-placement='right' title="Tên website của bạn nếu có">
              {!! Form::text('website',null,['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
          {!! Form::label('city','Tỉnh/Thành phố',['class'=>'col-sm-2 control-label']) !!}
          <div class="col-sm-4" data-toggle='tooltip' data-placement='right' title="Tên thành phố bạn đang sống">
            {!! Form::select('city_id', $cities, ( isset($loggedInUser->district->city->id) ? $a=$loggedInUser->district->city->id : 'null' ),
                ['class'=>'form-control city_id', 'id' => 'city_id']) !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('district','Quận/Huyện/Thị Xã/Thành phố',['class'=>'col-sm-2 control-label']) !!}
          <div class="col-sm-4" data-toggle='tooltip' data-placement='right' data-original-title="Tên quận, huyện, thị xã, thành phố bạn đang sống">
<<<<<<< HEAD
            {!! Form::select('district_id', [] ,( isset($loggedInUser->district->id) ? $loggedInUser->district->id : 'null' ), ['class'=>'form-control district_id', 'id' => 'district_id']) !!}
=======
            {!! Form::select('district_id', [] ,( isset($loggedInUser->district->id) ? $loggedInUser->district->id : 'null' ),
                ['class'=>'form-control district_id', 'id' => 'district_id', 'disabled'=>'disabled']) !!}
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
          </div>
        </div>

        <h4>THÔNG TIN KHÁC</h4>
        <div class="form-group">
            {!! Form::label('socmnd','Số CMND',['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-4" data-toggle='tooltip' data-placement='right' title="Số chứng minh nhân dân của bạn">
              {!! Form::text('cmnd_number',null,['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('cmnd_date_of_birth','Ngày sinh',['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-4 input-group date datetimepicker" data-toggle='tooltip' data-placement='right' title="Ngày sinh của bạn">
<<<<<<< HEAD
              {!! Form::text('cmnd_date_of_birth',null,['class'=>'form-control','placeholder'=>'Nhập ngày sinh']) !!}
=======
              {!! Form::text('cmnd_date_of_birth',null,['class'=>'form-control datetimepicker','placeholder'=>'Nhập ngày sinh']) !!}
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
              <span class="input-group-addon">
                <i class="fa fa-calendar" aria-hidden="true"></i>
              </span>
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('address','Địa chỉ',['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-4" data-toggle='tooltip' data-placement='right' title="Nguyên quán của bạn">
              {!! Form::text('cmnd_address',null,['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('about_job_title','Kinh nghiệm làm việc',['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-6">
              {!! Form::textarea('about_job_title',null,['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('about_description','Giới thiệu bản thân',['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-6">
              {!! Form::textarea('about_description',null,['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
          <div class="checkbox col-sm-4 col-sm-offset-2">
            <label>
              {!! Form::checkbox('receive_email',$loggedInUser['receive_email'],($loggedInUser['receive_email'] == 1) ? true : false) !!}
              Nhận thông tin từ website
            </label>
          </div>
        </div>
        {!! Form::submit('Lưu các thay đổi',['class'=>'btn btn-success']) !!}
      {!! Form::close() !!}
    </div><!-- end tab-pane-->
