<div class='sidebar-left'>
  <div class="left-menu">
    <div class="panel-group" id="accordion-menu" role="tablist" aria-multiselectable="true">

      <div class="panel">
        <div class="panel-heading" role="tab" id="heading-1">
          <h4 class="panel-title">
            <a role="button" class="collapsed had-child" data-toggle="collapse" data-parent="#accordion-menu" href="#collapse-menu-1" aria-expanded="false" aria-controls="collapse-menu-1">
              <i class="fa fa-caret-right" aria-hidden="true"></i> Giới thiệu
            </a>
          </h4>
        </div>
        <div id="collapse-menu-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-1">
          <div class="panel-body">
            <ul class="box-menu-child">
               <li><a class="menu-child" href="/gioi-thieu/gioi-thieu-ve-chung-toi"><i class="fa fa-caret-right" aria-hidden="true"></i> Về chúng tôi </a></li>
               <li><a class="menu-child" href="/gioi-thieu/dieu-khoan-su-dung"><i class="fa fa-caret-right" aria-hidden="true"></i> Điều khoản sử dụng</a></li>
               <li><a class="menu-child" href="/gioi-thieu/dieu-khoan-freelancer"><i class="fa fa-caret-right" aria-hidden="true"></i> Điều khoản freelancer</a></li>
               <li><a class="menu-child" href="/gioi-thieu/dieu-khoan-khach-hang"><i class="fa fa-caret-right" aria-hidden="true"></i> Điều khoản khách hàng</a></li>
               <li><a class="menu-child" href="/gioi-thieu/chinh-sach-bao-mat"><i class="fa fa-caret-right" aria-hidden="true"></i> Chính sách bảo mật</a></li>
               <li><a class="menu-child" href="/gioi-thieu/xac-thuc-ho-so"><i class="fa fa-caret-right" aria-hidden="true"></i> Xác thực hồ sơ</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="panel">
        <div class="panel-heading" role="tab" id="heading-2">
          <h4 class="panel-title">
            <a role="button" class="collapsed had-child" data-toggle="collapse" data-parent="#accordion-menu" href="#collapse-menu-2" aria-expanded="false" aria-controls="collapse-menu-2">
              <i class="fa fa-caret-right" aria-hidden="true"></i> Quy chế hoạt động
            </a>
          </h4>
        </div>
        <div id="collapse-menu-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-2">
          <div class="panel-body">
            <ul class="box-menu-child">
              <li><a class="menu-child" href="/quy-che-hoat-dong/nguyen-tac-chung"><i class="fa fa-caret-right" aria-hidden="true"></i> Nguyên tắc chung </a></li>
              <li><a class="menu-child" href="/quy-che-hoat-dong/quy-dinh-chung"><i class="fa fa-caret-right" aria-hidden="true"></i> Quy định chung</a></li>
              <li><a class="menu-child" href="/quy-che-hoat-dong/quy-trinh-giao-dich"><i class="fa fa-caret-right" aria-hidden="true"></i> Quy trình giao dịch</a></li>
              <li><a class="menu-child" href="/quy-che-hoat-dong/quy-trinh-thanh-toan"><i class="fa fa-caret-right" aria-hidden="true"></i> Quy trình thanh toán </a></li>
              <li><a class="menu-child" href="/quy-che-hoat-dong/dam-bao-an-toan-giao-dich"><i class="fa fa-caret-right" aria-hidden="true"></i> Đảm bảo an toàn giao dịch</a></li>
              <li><a class="menu-child" href="/quy-che-hoat-dong/bao-ve-thong-tin-ca-nhan-khach-hang"><i class="fa fa-caret-right" aria-hidden="true"></i> Bảo vệ thông tin cá nhân khách hàng</a></li>
              <li><a class="menu-child" href="/quy-che-hoat-dong/quan-ly-thong-tin-xau"><i class="fa fa-caret-right" aria-hidden="true"></i> Quản lý thông tin xấu </a></li>
              <li><a class="menu-child" href="/quy-che-hoat-dong/trach-nhiem-trong-truong-hop-phat-sinh-loi-ky-thuat"><i class="fa fa-caret-right" aria-hidden="true"></i> Trách nhiệm trong trường hợp phát sinh lỗi kỷ thuật</a></li>
              <li><a class="menu-child" href="/quy-che-hoat-dong/quyen-va-nghia-vu-cua-ban-quan-ly"><i class="fa fa-caret-right" aria-hidden="true"></i> Quyền và nghĩa vụ của Ban Quản lý ChoDangTin.vn</a></li>
              <li><a class="menu-child" href="/quy-che-hoat-dong/quyen-va-trach-nhiem-thanh-vien-tham-gia"><i class="fa fa-caret-right" aria-hidden="true"></i> Quyền và trách nhiệm thành viên tham gia ChoDangTin.vn </a></li>
              <li><a class="menu-child" href="/quy-che-hoat-dong/dieu-khoan-ap-dung"><i class="fa fa-caret-right" aria-hidden="true"></i> Điều khoản áp dụng</a></li>
              <li><a class="menu-child" href="/quy-che-hoat-dong/dieu-khoan-cam-ket"><i class="fa fa-caret-right" aria-hidden="true"></i> Điều khoản cam kết</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="panel">
        <div class="panel-heading" role="tab" id="heading-3">
          <h4 class="panel-title">
            <a role="button" class="collapsed had-child" data-toggle="collapse" data-parent="#accordion-menu" href="#collapse-menu-3" aria-expanded="false" aria-controls="collapse-menu-3">
              <i class="fa fa-caret-right" aria-hidden="true"></i> Hướng dẫn sử dụng
            </a>
          </h4>
        </div>
        <div id="collapse-menu-3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-3">
          <div class="panel-body">
            <ul class="box-menu-child">
              <li><a class="menu-child" href="/huong-dan-su-dung/danh-cho-freelancer"><i class="fa fa-caret-right" aria-hidden="true"></i> Dành cho freelancer </a></li>
              <li><a class="menu-child" href="/huong-dan-su-dung/danh-cho-khach-hang"><i class="fa fa-caret-right" aria-hidden="true"></i> Dành cho khách hàng </a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="panel">
        <div class="panel-heading" role="tab" id="heading-4">
          <h4 class="panel-title">
            <a role="button" class="collapsed had-child" data-toggle="collapse" data-parent="#accordion-menu" href="#collapse-menu-4" aria-expanded="false" aria-controls="collapse-menu-4">
              <i class="fa fa-caret-right" aria-hidden="true"></i> Các hình thức thanh toán
            </a>
          </h4>
        </div>
        <div id="collapse-menu-4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-4">
          <div class="panel-body">
            <ul class="box-menu-child">
              <li><a class="menu-child" href="/hinh-thuc-thanh-toan/muc-luc"><i class="fa fa-caret-right" aria-hidden="true"></i> Mục lục các hình thức thanh toán </a></li>
              <li><a class="menu-child" href="/hinh-thuc-thanh-toan/so-du-tai-khoan"><i class="fa fa-caret-right" aria-hidden="true"></i> Sử dụng số dư tài khoản </a></li>
              <li><a class="menu-child" href="/hinh-thuc-thanh-toan/chuyen-khoan-ngan-hang"><i class="fa fa-caret-right" aria-hidden="true"></i> Chuyển khoản ngân hàng </a></li>
              <li><a class="menu-child" href="/hinh-thuc-thanh-toan/internet-banking"><i class="fa fa-caret-right" aria-hidden="true"></i> Internet banking</a></li>
              <li><a class="menu-child" href="/hinh-thuc-thanh-toan/the-noi-dia"><i class="fa fa-caret-right" aria-hidden="true"></i> Thẻ nội địa</a></li>
              <li><a class="menu-child" href="/hinh-thuc-thanh-toan/ngan-luong"><i class="fa fa-caret-right" aria-hidden="true"></i> Ngân Lượng </a></li>
              <li><a class="menu-child" href="/hinh-thuc-thanh-toan/bao-kim"><i class="fa fa-caret-right" aria-hidden="true"></i> Bảo Kim </a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="panel">
        <div class="panel-heading" role="tab" id="heading-5">
          <h4 class="panel-title">
            <a role="button" class="collapsed had-child" data-toggle="collapse" data-parent="#accordion-menu" href="#collapse-menu-5" aria-expanded="false" aria-controls="collapse-menu-5">
              <i class="fa fa-caret-right" aria-hidden="true"></i> Bảng giá
            </a>
          </h4>
        </div>
        <div id="collapse-menu-5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-5">
          <div class="panel-body">
            <ul class="box-menu-child">
              <li><a class="menu-child" href="/bang-gia/muc-luc"><i class="fa fa-caret-right" aria-hidden="true"></i> Mục lục bảng giá </a></li>
              <!-- <li><a class="menu-child" href="/bang-gia/bang-gia-quang-cao"><i class="fa fa-caret-right" aria-hidden="true"></i> Bảng giá quảng cáo </a></li> -->
              <li><a class="menu-child" href="/bang-gia/bang-gia-tin-vip"><i class="fa fa-caret-right" aria-hidden="true"></i> Bảng giá tin VIP </a></li>
              <li><a class="menu-child" href="/bang-gia/bang-gia-tin-noi-bat-quang-cao"><i class="fa fa-caret-right" aria-hidden="true"></i> Bảng giá tin nổi bật, tin quảng cáo </a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="panel">
         <a class="menu-child" href="/cau-hoi-thuong-gap/cau-hoi-thuong-gap"><i class="fa fa-caret-right" aria-hidden="true"></i> Câu hỏi thường gặp </a>
      </div>
      <div class="panel">
         <a href='/lien-he'><i class="fa fa-caret-right" aria-hidden="true"></i> Liên hệ</a>
      </div>
    </div>
  </div>
</div>
