
<header>
    <nav class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Freelancer</a>
    </div>

    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
          @if(!$loggedInUser)
            <li><a href="#">Tìm việc</a></li>
            <li><a href="{{ route('tim-freelancer') }}">Tìm freelancer</a></li>
            <li><a href="{{ route('goi-cong-viec') }}">Gói công việc</a></li>
            <li><a href="#">Trợ giúp</a></li>
            <li><a href="{{ route('dang-nhap') }}">Đăng nhập</a></li>
            <li><a href="{{ route('dang-ky') }}">Đăng ký</a></li>
          @else
            <li><a href="#">Góc làm việc</a></li>
            <li><a href="{{ route('create-project') }}">Đăng việc</a></li>
            <li><a href="{{ route('create-contests') }}">Tạo cuộc thi</a></li>
            <li><a href="#">Tìm việc</a></li>
            <li><a href="{{ route('tim-freelancer') }}">Tìm freelancer</a></li>
            <li><a href="#">Mời bạn bè</a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  @if($loggedInUser->avatar)
                    <img class="img-circle" src="{{ $loggedInUser->avatar }}">
                  @else
                    <img class="img-circle" src="{{ asset('/images/users/avatar-default.jpg') }}">
                  @endif
                  {{ $loggedInUser->name }} <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ route('get-thong-tin-tai-khoan') }}"> Thông tin tài khoản</a></li>
                    <li><a href="{{ route('informationUser') }}"> Profile</a></li>
                    <li><a href="#"> Mời bạn bè</a></li>
                    <li><a href="{{ route('post-changepassword') }}"> Đổi mật khẩu</a></li>
                    <li><a href="{{ route('dang-xuat') }}"><i class="fa fa-btn fa-sign-out"></i> Đăng xuất</a></li>
                </ul>
            </li>
            <li><a href="#"><i class="fa fa-envelope-o"></i></a></li>
          @endif
        </ul>
    </div>
  </nav>
</header>
