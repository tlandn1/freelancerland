<div class="panel panel-default wrap-info-order">
	<div class="panel-heading thanhtoan-naptien"><strong>Thông tin thanh toán</strong></div>
	<div class="panel-body">
		<table style="width:100%" class="info-orders">
			<tbody>
				<tr>
					<td>Mã hóa đơn</td>
					<td class="pull-right info-ordercode">
						@if(isset($order))
						{{ $order->naptien_code }}
						@else
						{{ $rechargeCode }}
						@endif
					</td>
				</tr>
				<tr>
					<td>Trạng thái</td>
					<td class="pull-right info-orderstatus">
						@if(isset($order))
						@if($order->status == NAPTIEN_STATUS_PENDING)
						{{ 'Chưa thanh toán' }}
						@else
						{{ 'Đã thanh toán' }}
						@endif
						@else
						Chưa thanh toán
						@endif
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="total-price">
		<span><strong>Thành tiền</strong></span>
		<span class="pull-right info-ordertotal">
			<strong>
				@if(isset($order))
				{{ formatPrice($order->sotien) }}
				@else
				{{ formatPrice(20000) }}
				@endif
			</strong>
		</span>
	</div>
	<p class="tax-VAT">(Giá đã bao gồm 10% VAT)</p>
	<div class="hidden-print">
		<a href="#" class="btn btn-print" onclick="return PrintDetail();"><i class="fa fa-print"></i> In hóa đơn</a>
	</div>
</div>
