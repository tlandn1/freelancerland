<?php
	if(isset($order->naptien_code) && $order->naptien_code){
		$rechargeCode = $order->naptien_code;
	}
?>
<div class="panel-group list-payment" id="accordion-payment" role="tablist" aria-multiselectable="true">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				{{-- <a class="payment-link" href="{{ route('banking') }}"> --}}
				<a class="payment-link" href="/thanh-toan/banking/naptien/{{ $rechargeCode }}">
					<img src="{{ cdn('/images/icon_bank.png') }}" alt="" />
					<span>Chuyển khoản qua ngân hàng</span>
				</a>
			</h4>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a class="payment-link" href="{{ route('ngan-luong-recharge',$rechargeCode) }}">
					<img src="{{ cdn('/images/icon_nganluong.png') }}" alt="" />
					<span>Cổng thanh toán trực tuyến Ngân Lượng</span>
				</a>
			</h4>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a class="payment-link" href="{{ route('bao-kim-recharge',$rechargeCode) }}">
					<img src="{{ cdn('/images/icon_baokim.png') }}" alt="" />
					<span>Cổng thanh toán trực tuyến Bảo kim</span>
				</a>
			</h4>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="heading-1">
			<h4 class="panel-title">
				<a role="button" class="collapsed had-child" data-toggle="collapse" data-parent="#accordion-payment" href="#collapse-payment-1" aria-expanded="false" aria-controls="collapse-payment-1">
					<img src="{{ cdn('/images/inter-bank.png') }}" alt="" />
					<span>Internet banking</span>
				</a>
			</h4>
		</div>
		<div id="collapse-payment-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-1">
			<div class="panel-body">
				<h3>Chọn ngân hàng thanh toán</h3>
				<div class="list-bank clearfix">
					@foreach($banks as $bank)
					@if($bank['payment_method_type'] == config('freelancerland.PAYMENT_METHOD_TYPE_INTERNET_BANKING'))
					<a class="payment-link" data-bankid="{{ $bank['id'] }}" href="{{ route('bao-kim-recharge',$rechargeCode.'?bankid='.$bank['id']) }}"><img class="img-bank" id="{{ $bank['id'] }}" src="{{ cdn($bank['logo_url']) }}" title="{{ $bank['name'] }}"/></a>
					@endif
					@endforeach
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="heading-2">
			<h4 class="panel-title">
				<a role="button" class="collapsed had-child" data-toggle="collapse" data-parent="#accordion-payment" href="#collapse-payment-2" aria-expanded="false" aria-controls="collapse-payment-2">
					<img src="{{ cdn('/images/icon_smartlink.png') }}" alt="" />
					<span>Thẻ nội địa ATM</span>
				</a>
			</h4>
		</div>
		<div id="collapse-payment-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-2">
			<div class="panel-body">
				<h3>Chọn ngân hàng thanh toán</h3>
				<div class="list-bank clearfix">
					@foreach($banks as $bank)
					@if($bank['payment_method_type'] == config('freelancerland.PAYMENT_METHOD_TYPE_LOCAL_CARD'))
					@if($bank['id'] != 152 && $bank['id'] != 151)
					<a class="payment-link" data-bankid="{{ $bank['id'] }}" href="{{ route('bao-kim-recharge',$rechargeCode.'?bankid='.$bank['id']) }}"><img class="img-bank" id="{{ $bank['id'] }}" src="{{ cdn($bank['logo_url']) }}" title="{{ $bank['name'] }}"/></a>
					@endif
					@endif
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
