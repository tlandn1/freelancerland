<!-- REQUIRED JS SCRIPTS -->

{!! Minify::javascript(array(
     '/js/frontend/jquery.min.js',
    // '/js/frontend/bootstrap.min.js',
     '/js/frontend/select2.full.min.js'
  ))->withFullUrl() !!}

  {!! Html::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js') !!}
<!-- Optional JS, depends each page
<script src="/js/frontend/jquery.dataTables.min.js"></script>
<script src="/js/frontend/dataTables.bootstrap.min.js"></script>
<script src="/js/frontend/parsley.min.js"></script>
<script src="/js/frontend/parsley-vi.js"></script>
<script src="/js/frontend/croppic.js"></script>
<script src="/js/frontend/sweetalert2.min.js"></script>
<script src="/js/frontend/select2.min.js"></script>
<script src="/js/frontend/dropzone.min.js"></script>
<script src="/js/frontend/dropzone-config.js"></script>
<script src="/js/frontend/cookie.js"></script>
<script src="/js/frontend/jquery.flexisel.js"></script>
-->
@yield('scripts-vendor')

<!-- REQUIRED JS, main js -->
{!! Minify::javascript('/js/frontend/all.js')->withFullUrl() !!}
@yield('script')

<!--[if lte IE 9]>
<script src="/js/frontend/rem.min.js"></script>
<![endif] -->
