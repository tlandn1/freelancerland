<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('title')</title>

    {!! Minify::stylesheet(array(
          '/css/frontend/font-awesome.min.css',
          '/css/frontend/select2.min.css',
    ))->withFullUrl() !!}

    {!! Html::style('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css') !!}
    <!-- Optional CSS, depends each page
    <link href="/css/backend/main-croppic.css" rel="stylesheet"/>
    <link href="/css/backend/croppic.css" rel="stylesheet"/>
    <link href="/css/backend/sweetalert2.min.css" rel="stylesheet"/>
    <link href="/css/backend/select2.min.css" rel="stylesheet"/>
    <link href="/css/backend/dropzone.min.css" rel="stylesheet"/>
    <link href="/css/backend/ekko-lightbox.min.css" rel="stylesheet"/>
    <link href="/css/backend/dataTables.bootstrap.css" rel="stylesheet"/>
    -->
    @yield('styles-vendor')

    <!-- Main backend CSS file -->
    {!! Minify::stylesheet('/css/frontend/all.css')->withFullUrl() !!}
    @yield('style')


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries
    https://cdnjs.com/libraries/html5shiv
    https://cdnjs.com/libraries/respond.js
    -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]
    {!! Minify::javascript(array(
        '//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js'
      ))->withFullUrl() !!}
    [endif] -->
</head>
