<!-- Modal form sửa hồ sơ học tập của user hiện tại đang login -->
{!! Form::open(['class'=>'form-horizontal','id'=>'updateEducationForm','style'=>'display:none']) !!}
  {!! Form::hidden('id') !!}
  <div class="form-group">
      {!! Form::label('name_school','Trên trường',['class'=>'col-sm-3 control-label']) !!}
      <div class="col-sm-4">
        {!! Form::text('name_school',null,['class' => 'form-control','placeholder'=>'Tên trường, khóa học']) !!}
      </div>
  </div>
  <div class="form-group">
      {!! Form::label('year','Năm tốt nghiệp',['class'=>'col-sm-3 control-label']) !!}
      <div class="col-sm-4 input-group date datetimepicker">
        {!! Form::text('year',null,['class' => 'form-control']) !!}
        <span class="input-group-addon">
          <i class="fa fa-calendar" aria-hidden="true"></i>
        </span>
      </div>
  </div>
  <div class="form-group">
      {!! Form::label('degree','Loại bằng',['class'=>'col-sm-3 control-label']) !!}
      <div class="col-sm-4">
        {!! Form::text('degree',null,['class' => 'form-control','placeholder'=>'Loại bằng']) !!}
      </div>
  </div>
  <div class="form-group">
      {!! Form::label('description','Mô tả',['class'=>'col-sm-3 control-label']) !!}
      <div class="col-sm-6">
        {!! Form::textarea('description',null,['class' => 'form-control','placeholder'=>'Mô tả']) !!}
      </div>
  </div>
  <div class="form-group">
      <div class="col-xs-5 col-xs-offset-3">
        {!! Form::submit('Lưu hồ sơ',['class'=>'btn btn-success']) !!}
      </div>
  </div>
{!! Form::close() !!}
