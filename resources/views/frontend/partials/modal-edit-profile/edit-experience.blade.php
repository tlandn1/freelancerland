<!-- Modal form sửa hồ sơ kinh nghiệm user hiện tại đang login -->
{!! Form::open(['class'=>'form-horizontal','id'=>'updateExperienceForm','style'=>'display:none']) !!}
  {!! Form::hidden('id') !!}
  <div class="form-group">
      {!! Form::label('company_name','Tên Công Ty',['class'=>'col-sm-3 control-label']) !!}
      <div class="col-sm-4">
        {!! Form::text('company_name',null,['class' => 'form-control','placeholder'=>'Tên Công ty']) !!}
      </div>
  </div>
  <div class="form-group">
      {!! Form::label('position','Vị trí',['class'=>'col-sm-3 control-label']) !!}
      <div class="col-sm-4">
        {!! Form::text('position',null,['class' => 'form-control','placeholder'=>'Vị trí làm việc']) !!}
      </div>
  </div>
  <div class="form-group">
      {!! Form::label('from','Ngày bắt đầu',['class'=>'col-sm-3 control-label']) !!}
      <div class="col-sm-4 input-group date datetimepicker">
        {!! Form::text('from',null,['class' => 'form-control']) !!}
        <span class="input-group-addon">
          <i class="fa fa-calendar" aria-hidden="true"></i>
        </span>
      </div>
  </div>
  <div class="form-group">
      {!! Form::label('to','Ngày kết thúc',['class'=>'col-sm-3 control-label']) !!}
      <div class="col-sm-4 input-group date datetimepicker">
        {!! Form::text('to',null,['class' => 'form-control']) !!}
        <span class="input-group-addon">
          <i class="fa fa-calendar" aria-hidden="true"></i>
        </span>
      </div>
  </div>
  <div class="form-group">
      {!! Form::label('description','Mô tả',['class'=>'col-sm-3 control-label']) !!}
      <div class="col-sm-6">
        {!! Form::textarea('description',null,['class' => 'form-control','placeholder'=>'Mô tả']) !!}
      </div>
  </div>
  <div class="form-group">
      <div class="col-xs-5 col-xs-offset-3">
        {!! Form::submit('Lưu hồ sơ',['class'=>'btn btn-success']) !!}
      </div>
  </div>
{!! Form::close() !!}
