@extends('frontend.master')
@section('title')
Tìm freelancer
@stop

@section('styles-vendor')

  {!! Minify::stylesheet(array(
      //  '/css/frontend/tagmanager.min.css',
        '/css/backend/main-croppic.css',
        '/css/backend/croppic.css',
        '/css/backend/bootstrap-datetimepicker.min.css'
  ))->withFullUrl() !!}

@stop

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      @include('frontend.visitor.left-content')
    </div> <!--end content left-->
    <div class="col-md-9">
      @include('frontend.visitor.right-content')
    </div> <!--end content right-->
  </div>
</div>
@stop

@section('scripts-vendor')
<script src="//oss.maxcdn.com/bootbox/4.2.0/bootbox.min.js"></script>
<<<<<<< HEAD
{!! Minify::javascript(array(
    '/js/frontend/typeahead.bundle.min.js',
  //  '/js/frontend/tagmanager.min.js',
=======
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
{!! Minify::javascript(array(
  //  '/js/frontend/typeahead.bundle.min.js',
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
    '/js/backend/croppic.js',
     '/js/backend/parsley.min.js',
     '/js/backend/parsley-vi.js',
     '/js/backend/moment.js',
     '/js/backend/bootstrap-datetimepicker.min.js'
  ))->withFullUrl() !!}
@stop

@section('script')
<<<<<<< HEAD
=======
<script type="text/javascript">
/*****************************************
        fielter name in form search
******************************************/
    var path = "{{ route('auto-complete-name') }}";
    $('input.typeahead').typeahead({
        source:  function (query, process) {
        return $.get(path, { query: query }, function (data) {
              // console.log(data);
                return process(data);
            });
        }
    });
    /*****************************************
            Tìm kiếm city trong list
    ******************************************/
    // $(function(){
    //     var inputVal = $('.input-search').val();
    //     $('.li-tim-theo-ten').click(function(){
    //       var path = "{{ route('tim-kiem-theo-ten') }}".append(inputVal);
    //       alert(path);
    //     });
    // });
    $(function(){
    	$('#search_city').keyup(function(){
    		var current_query = $('#search_city').val();
    		if (current_query !== "") {
    			$(".list-group-city li").hide();
    			$(".list-group-city li").each(function(){
    				var current_keyword = $(this).text();
    				if (current_keyword.toLowerCase().indexOf(current_query) >=0) {
    					$(this).show();
    				};
    			});
    		} else {
    			$(".list-group-city li").show();
    		};
    	});
    });
    /*****************************************
            Tìm kiếm city trong list
    ******************************************/
    $(function(){
    	$('#search_skill').keyup(function(){
    		var current_query = $('#search_skill').val();
    		if (current_query !== "") {
    			$(".list-group-skills li").hide();
    			$(".list-group-skills li").each(function(){
    				var current_keyword = $(this).text();
    				if (current_keyword.toLowerCase().indexOf(current_query) >=0) {
    					$(this).show();
    				};
    			});
    		} else {
    			$(".list-group-skills li").show();
    		};
    	});
    });
</script>
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
<script>

  /*******************************************
    Kích vào menu bên trái, tự động sổ xuống
  *******************************************/
  $(document).ready(function() {
      $('[id^=detail-]').hide();
      $('.toggle').click(function() {
          $input = $( this );
          $target = $('#'+$input.attr('data-toggle'));
          $target.slideToggle();
      });
  });
  /*****************************
        Chuyển đổi Icon
  *****************************/
  $(document)
    .on('click', '.panel-heading span.click-able', function(e){
        $(this).parents('.panel').find('.panel-collapse').collapse('toggle');
    })
    .on('show.bs.collapse', '.panel-collapse', function () {
        var $span = $(this).parents('.panel').find('.panel-heading span.click-able');
        $span.find('i').removeClass('fa-plus').addClass('fa-minus');
    })
    .on('hide.bs.collapse', '.panel-collapse', function () {
        var $span = $(this).parents('.panel').find('.panel-heading span.click-able');
        $span.find('i').removeClass('fa-minus').addClass('fa-plus');
    })

  function toggleChevron(e) {
		$(e.target)
				.prev('.panel-heading')
				.find("i.indicator")
				.toggleClass('fa-angle-down fa-angle-left');
	}
	$('#accordion').on('hidden.bs.collapse', toggleChevron);
	$('#accordion').on('shown.bs.collapse', toggleChevron);
  /*****************************
            checkbox
  *****************************/
  $(function () {
    $('.list-group.checked-list-box .list-group-item').each(function () {

        // Settings
        var $widget = $(this),
            $checkbox = $('<input type="checkbox" class="hidden" />'),
            color = ($widget.data('color') ? $widget.data('color') : "primary"),
            style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
            settings = {
                on: {
                    icon: 'fa fa-check-square-o'
                },
                off: {
                    icon: 'fa fa-square-o'
                }
            };

        $widget.css('cursor', 'pointer')
        $widget.append($checkbox);

        // Event Handlers
        $widget.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });


        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $widget.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $widget.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$widget.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $widget.addClass(style + color + ' active');
            } else {
                $widget.removeClass(style + color + ' active');
            }
        }

        // Initialization
        function init() {

            if ($widget.data('checked') == true) {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
            }

            updateDisplay();

            // Inject the icon if applicable
            if ($widget.find('.state-icon').length == 0) {
                $widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span> ');
            }
        }
        init();
    });
});

  /*****************************

  *****************************/
  // $(document).ready(function() {
  //     $(".bottomcity" ).click(function() {
  //         $('.ln-category-city').toggleClass( "hideskill" );
  //         $('.layered-navigation').toggleClass("hide-adv");
  //     });
  //
  // });

  /*******************************************
          button of form search
  ********************************************/
  $(function(){

    $(".input-group-btn .dropdown-menu li a").click(function(){
        var selText = $(this).html();
       $(this).parents('.input-group-btn').find('.btn-search').html(selText);
   });
});
<<<<<<< HEAD

/*****************************************
        fielter name in form search
******************************************/
// var usersName = new Bloodhound({
//   datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
//   queryTokenizer: Bloodhound.tokenizers.whitespace,
//   prefetch: '../data/films/post_1960.json',
//   remote: {
//     url: '../data/films/queries/%QUERY.json',
//     wildcard: '%QUERY'
//   }
// });
//
// $('.typeahead').typeahead(null, {
//   name: 'users-name',
//   display: 'value',
//   source: usersName
// });
=======
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
/*****************************

*****************************/

</script>
@stop
