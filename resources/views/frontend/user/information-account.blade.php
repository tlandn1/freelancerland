@extends('frontend.master')
@section('title')
Thông tin tài khoản
@stop
@section('content')
<aside class="container">
  <div class="row">
    <!-- content left -->
    <section class="col-md-9">
        <section class="row">
            <section class="col-md-5" align="center">
              <img class="form-group img-circle" src="{!! $users['avatar'] !!}">

              <div class="form-group">
                ID: {!! $users['id'] !!}
              </div> <!---->

              <div class="form-group">
                Lần cuối cùng online: {{ $users['updated_at'] }}
              </div> <!--end-->
            </section> <!--end-->

            <section class="col-md-7">
              <div class="form-group">
                {{ $users['name'] }}
                <i class="fa fa-phone"></i>
                <i class="fa fa-star-o"></i>
                Cập nhật thông tin
              </div> <!--end-->

              <div class="form-group">
<<<<<<< HEAD
                @foreach($users->portfolioes as $portfolio)
=======
                @foreach($users->portfolios as $portfolio)
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
                  {!! $portfolio->title !!}
                @endforeach
              </div> <!--end-->

              <div class="form-group">
                <i class="fa fa-map-marker"></i> {{ $users->district->city->name }}
              </div> <!--end-->

              <div class="form-group">
                <div class="bot-links" style="float: left; width: 100%; padding-top: 10px;">
<<<<<<< HEAD
                	<a href="#" style="display: inline-block; padding: 5px; background: #ccc; font-size: 12px; margin-bottom: 5px; color: #9c9c9c; text-decoration:none;">Sit amet</a>
                  <a href="#" style="display: inline-block; padding: 5px; background: #ccc; font-size: 12px; margin-bottom: 5px; color: #9c9c9c; text-decoration:none;">Sit amet</a>
                  <a href="#" style="display: inline-block; padding: 5px; background: #ccc; font-size: 12px; margin-bottom: 5px; color: #9c9c9c; text-decoration:none;">Sit amet</a>
                  <a href="#" style="display: inline-block; padding: 5px; background: #ccc; font-size: 12px; margin-bottom: 5px; color: #9c9c9c; text-decoration:none;">Sit amet</a>
=======
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
                  @foreach($users->skills as $skill)
                    <a href="#" style="display: inline-block; padding: 5px; background: #ccc; font-size: 12px; margin-bottom: 5px; color: #9c9c9c; text-decoration:none;">{!! $skill->label !!}</a>
                  @endforeach
                </div>
              </div> <!--end-->
            </section>
        </section> <!--end-->
        <hr>
<<<<<<< HEAD
        <!-- Hồ sơ năng lực -->
        <section class="row">
          <h4 class="col-md-12">Hồ sơ năng lực</h4>
            @foreach($users->portfolioes as $portfolio)
              <p class="col-md-12">
                {!! $portfolio->description !!}
              </p>
            @endforeach
=======

        <!-- Kỹ năng -->
        <section class="row">
          <div class="col-md-12">
            <h3>Lĩnh vực chuyên môn
              <button data-toggle="modal" data-target="#squarespaceModal" class="btn pull-right center-block">Thêm hoặc Sửa</button>
            </h3>
            @include('frontend.user.modal-edit-information-account.edit-second-category')
            <div class="well">
              <div class="media">
                <table class="">
                  <thead>
                    <tr>
                      <th>Lĩnh vực</th>
                      <th>Chi tiết</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
aaaaaaaaaaaaaaa
                      </td>
                      <td>
                        @foreach($users->secondcategories as $secondcategory)
                          {!! $secondcategory->name !!},
                        @endforeach
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div><!--end media-->
            </div><!--end well-->
          </div><!--end col-md-12-->
        </section> <!---->
        <hr>
        <!-- Hồ sơ năng lực -->
        <section class="row">
          <div class="col-md-12">
            <h4 class="">Hồ sơ năng lực</h4>
            @foreach($users->portfolios as $portfolio)
              <div class="well">
                <div class="media">
                  <div class="pull-left">
                    <img class="img-thumbnail" src="{{ $portfolio->firstImage }}" alt="" style="width:200px;height:200px">
                  </div> <!--end pull-left-->

                  <div class="media-body">
                    <table class="table table-user-information">
                        <tbody>
                          <tr>
                            <td>Tiêu đề:</td>
                            <td>{!! $portfolio->title !!}</td>
                          </tr>
                          <tr>
                            <td>Link:</td>
                            <td>{!! $portfolio->link !!}</td>
                          </tr>
                          <tr>
                            <td>Mô tả kinh nghiệm:</td>
                            <td>{!! $portfolio->description !!}</td>
                          </tr>
                        </tbody>
                    </table>
                  </div> <!--end media-body-->
                </div> <!--end media-body-->
              </div> <!--end well-->
            @endforeach
          </div>
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
        </section> <!--end-->
        <hr>
        <!-- Kỹ năng -->
        <section class="row">
          <h4 class="col-md-12">Kỹ năng</h4>
        </section> <!---->
        <hr>
        <!-- Công việc (Đã được giao) -->
        <section class="row">
          <h4 class="col-md-12">Công việc (Đã được giao)</h4>
        </section> <!---->
        <hr>
        <!-- Kinh nghiệm làm việc -->
        <section class="row">
<<<<<<< HEAD
          <h4 class="col-md-12">Kinh nghiệm làm việc</h4>
          @foreach($users->workingCompanies as $workingCompany)
            <div class="col-md-3">
              <img src="/images/default-image.png" alt="" class="img-thumbnail">
            </div>
            <div class="col-md-9">
              <table class="table table-user-information">
                  <tbody>
                    <tr>
                      <td>Công ty:</td>
                      <td>{{ $workingCompany['company_name'] }}</td>
                    </tr>
                    <tr>
                      <td>Vị trí:</td>
                      <td>{{ $workingCompany['position'] }}</td>
                    </tr>
                    <tr>
                      <td>Mô tả kinh nghiệm:</td>
                      <td>{{ $workingCompany['description'] }}</td>
                    </tr>
                    <tr>
                      <td>Từ năm:</td>
                      <td>{{ $workingCompany['from'] }}</td>
                    </tr>
                    <tr>
                      <td>Đến năm:</td>
                      <td>{{ $workingCompany['to'] }}</td>
                    </tr>
                  </tbody>
              </table>
            </div>
          @endforeach
=======
          <div class="col-md-12">
            <h4 class="">Kinh nghiệm làm việc</h4>
            @foreach($users->workingCompanies as $workingCompany)
              <div class="well">
                <div class="media">
                  <div class="pull-left">
                    <img class="img-thumbnail" src="/images/portfolios/default-image.png" alt="" style="width:200px;height:200px">
                  </div> <!--end pull-left-->

                  <div class="media-body">
                    <table class="table table-user-information">
                        <tbody>
                          <tr>
                            <td>Công ty:</td>
                            <td>{{ $workingCompany['company_name'] }}</td>
                          </tr>
                          <tr>
                            <td>Vị trí:</td>
                            <td>{{ $workingCompany['position'] }}</td>
                          </tr>
                          <tr>
                            <td>Mô tả kinh nghiệm:</td>
                            <td>{{ $workingCompany['description'] }}</td>
                          </tr>
                          <tr>
                            <td>Từ năm:</td>
                            <td>{{ $workingCompany['from'] }}</td>
                          </tr>
                          <tr>
                            <td>Đến năm:</td>
                            <td>{{ $workingCompany['to'] }}</td>
                          </tr>
                        </tbody>
                    </table>
                  </div> <!--end media-body-->
                </div> <!--end media-body-->
              </div> <!--end well-->
            @endforeach
          </div>
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
        </section> <!--end-->
        <hr>
        <!-- Học vấn -->
        <section class="row">
<<<<<<< HEAD
          <h4 class="col-md-12">Học vấn</h4>
          @foreach($users->educations as $education)
            <div class="col-md-3">
              <img src="/images/default-image.png" alt="" class="img-thumbnail">
            </div>
            <div class="col-md-9">
              <table class="table table-user-information">
                  <tbody>
                    <tr>
                      <td style="width: 170px;">Học trường:</td>
                      <td>{{ $education['name_school'] }}</td>
                    </tr>
                    <tr>
                      <td>Năm học:</td>
                      <td>{{ $education['year'] }}</td>
                    </tr>
                    <tr>
                      <td>Bằng cấp:</td>
                      <td>{{ $education['degree'] }}</td>
                    </tr>
                    <tr>
                      <td>Chuyên ngành:</td>
                      <td>{{ $education['description'] }}</td>
                    </tr>
                  </tbody>
              </table>
            </div>
          @endforeach
=======
          <div class="col-md-12">
            <h4 class="">Học vấn</h4>
            @foreach($users->educations as $education)
              <div class="well">
                <div class="media">
                  <div class="pull-left">
                    <img class="img-thumbnail" src="/images/portfolios/default-image.png" alt="" style="width:200px;height:200px">
                  </div> <!--end pull-left-->

                  <div class="media-body">
                    <table class="table table-user-information">
                        <tbody>
                          <tr>
                            <td style="width: 170px;">Học trường:</td>
                            <td>{{ $education['name_school'] }}</td>
                          </tr>
                          <tr>
                            <td>Năm học:</td>
                            <td>{{ $education['year'] }}</td>
                          </tr>
                          <tr>
                            <td>Bằng cấp:</td>
                            <td>{{ $education['degree'] }}</td>
                          </tr>
                          <tr>
                            <td>Chuyên ngành:</td>
                            <td>{{ $education['description'] }}</td>
                          </tr>
                        </tbody>
                    </table>
                  </div> <!--end media-body-->
                </div> <!--end media-body-->
              </div> <!--end well-->
            @endforeach
          </div>
>>>>>>> 67129f8ce299d13807f382abaae02ce46130d70a
        </section>
    </section> <!---->
    <!-- content right -->
    <section class="col-md-3">
      <section>
        <h4>Thông tin liên lạc</h4>
        <div class="form-group">
          <i class="fa fa-phone"></i> {!! $users['phone'] !!}
        </div>
        <div class="form-group">
          <i class="fa fa-envelope-o"></i> {!! $users['email'] !!}
        </div>
      </section>
      <hr>
      <section>
        <h4>Tóm lượt</h4>
        <div class="form-group">
          <label class="" style="text-align: left;">
            <a title="0">0</a>
          </label>
          <i class="fa fa-star-o"></i>
          <i class="fa fa-star-o"></i>
          <i class="fa fa-star-o"></i>
          <i class="fa fa-star-o"></i>
          <i class="fa fa-star-o"></i>
          <label class="" style="text-align: right;">
            <a title="0">0</a> Đánh giá
          </label>
        </div>
        <div class="form-group">
          Đã kiếm được <a title="0">0</a> VNĐ
        </div>
        <div class="form-group">
          Hoàn thành việc
          <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
              0%
            </div>
          </div>
        </div>
        <div class="form-group">
          Được thuê lại
          <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
              0%
            </div>
          </div>
        </div>
      </section>
      <hr>
      <section>
        <h4>Kỹ Năng Tiếng Anh</h4>
        <div class="form-group">

        </div>
        <div class="form-group">

        </div>
      </section>
    </section> <!---->
  </div>
</aside>
@stop
