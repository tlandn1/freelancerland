@extends('frontend.master')
@section('styles-vendor')

  {!! Minify::stylesheet(array(
        '/css/backend/main-croppic.css',
        '/css/backend/croppic.css',
        '/css/backend/bootstrap-datetimepicker.min.css'
  ))->withFullUrl() !!}

@stop
@section('title')
Hồ sơ của {{ $loggedInUser->name }}
@stop
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="information-user">
        <h1 class="title-1">Thông tin cá nhân</h1>
        {!! Form::open(['url'=>array('trang-ca-nhan/thong-tin-ca-nhan'),'method' => 'POST']) !!}
        <div class="col-md-6">
        @if (Session::has('message'))<div class="alert alert-success">{{ Session::get('message') }}</div>@endif
        <img alt="avatar-user" class="img-thumbnail thumb">

        <div class="form-group" id="email-user">
            {!! Form::label('email','Email') !!}
            {!! Form::text('email',$loggedInUser->email,['class' => 'form-control','disabled','id'=>'email']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('name','Họ và tên') !!}
            {!! Form::text('name',$loggedInUser->name,['class' => 'form-control']) !!}
            @if ($errors->has('name'))<p style="color:red;">{!!$errors->first('name')!!}</p>@endif
        </div>
        <div class="form-group">
            {!! Form::label('phone','Số điện thoại') !!}
            {!! Form::text('phone',$loggedInUser->phone,['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('skype','Skype') !!}
            {!! Form::text('skype',$loggedInUser->skype,['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('facebook','Facebook') !!}
            {!! Form::text('facebook',$loggedInUser->facebook,['class' => 'form-control']) !!}
        </div>


        <h1>Thông tin khác</h1>
        <div class="form-group">
            {!! Form::label('socmnd','Số CMND') !!}
            {!! Form::text('cmnd_number',$loggedInUser->cmnd_number,['class' => 'form-control']) !!}
        </div>
        {{-- <div class="form-group">
            {!! Form::label('birthday','Số CMND') !!}
            {!! Form::text('cmnd_date_of_birth',$loggedInUser->cmnd_date_of_birth,['class' => 'form-control']) !!}
        </div> --}}
        <div class="form-group">
          {!! Form::label('cmnd_date_of_birth','Ngày sinh') !!}
          <div class="input-group date" id="datetimepicker">
            {!! Form::text('cmnd_date_of_birth',$loggedInUser->cmnd_date_of_birth,['class'=>'form-control','placeholder'=>'Nhập ngày sinh']) !!}
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
          </div>
        </div>

        <div class="form-group">
            {!! Form::label('address','Địa chỉ') !!}
            {!! Form::text('cmnd_address',$loggedInUser->cmnd_address,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('about_job_title','Kinh nghiệm làm việc') !!}
            {!! Form::textarea('about_job_title',$loggedInUser->about_job_title,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('about_description','Giới thiệu bản thân') !!}
            {!! Form::textarea('about_description',$loggedInUser->about_description,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
          <div class="checkbox">
            <label>
              {!! Form::checkbox('receive_email',$loggedInUser['receive_email'],($loggedInUser['receive_email'] == 1) ? true : false) !!}
              Nhận thông tin từ website
            </label>
          </div>
        </div>
        <button type="submit" class="btn btn-blue">Cập nhật</button>
      </div>
      <div class="col-md-6">
        <strong>avatar</strong>
        <div class="form-group" id="avatar-croppic">
            {!! Form::hidden('avatar', $loggedInUser->avatar,['class'=>'form-control', 'id' => 'avatar']) !!}
            <div id="croppic" style="width:{{  config('freelancerland.IMAGE_AVATAR_WIDTH') }}px;height:{{ config('freelancerland.IMAGE_AVATAR_HEIGHT') }}px"></div>
        </div>
        {!! Form::close() !!}
      </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('scripts-vendor')

{!! Minify::javascript(array(
    '/js/backend/croppic.js',
     '/js/backend/parsley.min.js',
     '/js/backend/parsley-vi.js',
     '/js/backend/moment.js',
     '/js/backend/bootstrap-datetimepicker.min.js'
  ))->withFullUrl() !!}

@stop

@section('script')
<script>
    //////Scrip preview avatar user
    $('.thumb').attr('src', '<?php echo $loggedInUser->avatar; ?>');
    var editUserSettings = [
        '/uploadImage/avatar', // uploadUrl
        '/cropImage/avatar', // cropUrl
        '/uploads/users/', // croppedDir
    ];

    var eyeCandy = $('#croppic');
    var croppicHeaderOptions = {
        uploadUrl: editUserSettings[0],
        cropUrl: editUserSettings[1],
        outputUrlId:'avatar',
        cropData: {
            'width': eyeCandy.width(),
            'height': eyeCandy.height()
        },
        doubleZoomControls:false,
        //imgEyecandy:false,
        loaderHtml: '<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
        onAfterImgUpload: 	function(){
            var originImageUrl = $('#croppic > div.cropImgWrapper > img').attr('src');
            var originFileName = originImageUrl.split("/").pop(); // => profile-54b58.jpg
            var fullCroppedImagePath = editUserSettings[2]+ "{{ env('CROPPED_IMAGE_PREFIX', 'cropped-')}}" + originFileName;
            //console.log(fullCroppedImagePath);
            $('#avatar').attr('value', fullCroppedImagePath);
        },
    };
    var cropperBox = new Croppic('croppic', croppicHeaderOptions);

    $('input[name=receive_email]').click(function(){
        this.value = this.checked ? 1:0;
    })

    $(function () {
        $('#datetimepicker').datetimepicker({
         format: 'DD/MM/YYYY'
        });
    });
</script>
@stop
