@extends('frontend.master')
@section('title')
Đổi mật khẩu {{ $loggedInUser->name }}
@stop
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="ingormation-user">
        <h1 class="title-1">Thay đổi mật khẩu</h1>
        {!! Form::open(['route'=>['post-changepassword'],'method' => 'POST']) !!}
        @if (Session::has('message'))<div class="alert alert-success">{{ Session::get('message') }}</div>@endif
        @if (Session::has('error'))<div class="alert alert-danger">{{ Session::get('error') }}</div>@endif
        @if($loggedInUser->password)
          <div class="form-group">
              {!! Form::label('old-password','Mật khẩu cũ') !!}
              {!! Form::password('old_password',['class' => 'form-control']) !!}
              @if ($errors->has('old_password'))<p style="color:red;">{!!$errors->first('old_password')!!}</p>@endif
          </div>
        @endif

        <div class="form-group">
            {!! Form::label('new-pasword','Mật khẩu mới') !!}
            {!! Form::password('password',['class' => 'form-control']) !!}
            @if ($errors->has('password'))<p style="color:red;">{!!$errors->first('password')!!}</p>@endif
        </div>
        <div class="form-group">
            {!! Form::label('password-confirm','Xác nhận lại mật khẩu') !!}
            {!! Form::password('password_confirmation',['class' => 'form-control']) !!}
            @if ($errors->has('password_confirmation'))<p style="color:red;">{!!$errors->first('password_confirmation')!!}</p>@endif
        </div>
        <button type="submit" class="btn btn-blue">Thay đổi</button>
        {!! Form::close() !!}
      </div>
      </div>
    </div>
  </div>
</div>
@stop
