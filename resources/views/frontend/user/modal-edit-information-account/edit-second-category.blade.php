<div class="modal fade" id="squarespaceModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <h3 class="modal-title" id="lineModalLabel">Sửa lĩnh vực chuyên môn</h3>
        <label>Bạn chỉ được chọn 10 thể loại</label>
        <label>Bạn còn 9 thể loại</label>
      </div>

      <div class="modal-body">
        <form>
          <div id="products" class="row list-group">
            @foreach($firstCategories as $firstCategory)
              <div class="item  col-xs-4 col-lg-4 grid-group-item">
                <h4 class="group inner list-group-item-heading">
                {!! $firstCategory->name !!}</h4>
                @foreach($firstCategory->secondCategories as $secondCategory)
                  <div class="">
                    @foreach($users->secondcategories as $second_category)
                    <?php
                      $a = null;
                      if($second_category->id == $secondCategory->id){
                      $a = $secondCategory->id;
                    } ?>
                    @endforeach
                  {!! Form::checkbox('second_category',( isset($a) ? $a : ''), ( isset($a) ? true : ''),['id'=>'']) !!}
                  {!! $secondCategory->name !!}
                  </div>
                @endforeach
              </div>
            @endforeach
          </div>
          <button type="submit" class="btn btn-default">Cập nhật</button>
        </form>
      </div>
    </div>
  </div>
</div>
