<!DOCTYPE html>
<html lang="vi">
@include('frontend.partials.htmlheader')
<body class="@yield('class')">
	@include('frontend.partials.header')
	{{--@include('frontend.partials.formsearch')--}}
	@yield('content')
	@include('frontend.partials.footer1')
	@include('frontend.partials.scripts')
</body>
</html>
