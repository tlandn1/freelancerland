@extends('frontend.master')
@section('styles-vendor')

  {!! Minify::stylesheet(array(
    '/css/frontend/dropzone.min.css',
    '/css/frontend/trumbowyg.min.css',
    '/css/frontend/trumbowyg.colors.min.css',
  ))->withFullUrl() !!}

@stop
@section('title')
Đăng cuộc thi mới
@stop
@section('content')
  <div class="container">
    <div class="row">

      <div class="col-sm-8 col-sm-offset-2 text-center">
        <h2>Tạo cuộc thi mới</h2>
      </div>

      <div class="col-sm-6 col-sm-offset-3">
        {!! Form::open(['route'=>'post-create-contests','method'=>'POST','files'=>true]) !!}

          <div class="form-group">
            {!! Form::label('Chọn lĩnh vực cho cuộc thi') !!}
            {!! Form::select('contests_field',$contests_field,null,['class'=>'form-control','id'=>'fll-contests-field']) !!}
        {!! Form::open(['method'=>'POST']) !!}

          <div class="form-group">
            {!! Form::label('Chọn lĩnh vực cho cuộc thi') !!}
            {!! Form::select('contests_field',[],null,['class'=>'form-control']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('Chọn loại dịch vụ') !!}
            {!! Form::select('contests_service',[],null,['class'=>'form-control']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('Ngân sách cuộc thi') !!}
            {!! Form::number('budget',null,['class'=>'form-control']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('Tiêu đề cuộc thi') !!}
            {!! Form::text('title',null,['class'=>'form-control']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('Nội dung cuộc thi') !!}
            {!! Form::textarea('description',null,['class'=>'form-control useTrumbowyg']) !!}
          </div>

        {!! Form::close() !!}
      </div>

      <div class="col-sm-6 col-sm-offset-3">
        <div class="wrap-upload">
          <div class="how-to-create" >
            {!! Form::open(['route'=>['upload-post'],'class' => 'dropzone', 'files'=>true, 'id'=>'real-dropzone']) !!}
            <h4 style="text-align: center;color:#428bca;">THÊM TÀI LIỆU ĐÍNH KÈM!</h4>
            <p>Định dạng được hỗ trợ: png, jpg, gif, pdf, psd, xls, xlsx, doc, docx, ppt, pptx, zip, rar
              Kích thước tối đa: 10MB</p>
            <div class="dz-message">
            </div>
            <div class="fallback">
              <input name="file" type="file" multiple />
            </div>
            <div class="dropzone-previews" id="dropzonePreview"></div>
            <div class="note-dropzone"></div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>

      <div class="col-sm-6 col-sm-offset-3">
        <button type="button" class="btn btn-primary btn-lg btn-block">Đăng cuộc thi</button>
            {!! Form::textarea('description',null,['class'=>'form-control']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('Nội dung cuộc thi') !!}
            {!! Form::file('file',['class'=>'form-control']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('Ngân sách cuộc thi') !!}
            {!! Form::number('budget',null,['class'=>'form-control']) !!}
          </div>

        {!! Form::close() !!}
      </div>

    </div>
  </div>
  @include('frontend.partials.dropzone-template.dropzone-view')
@stop
@section('scripts-vendor')
  {!! Minify::javascript(array(
    '/js/frontend/dropzone.min.js',
    '/js/frontend/dropzone-config.js',
    '/js/frontend/trumbowyg.min.js',
    '/js/frontend/trumbowyg.vi.min.js',
    '/js/frontend/trumbowyg.colors.min.js',
    '/js/frontend/trumbowyg.upload.min.js',
   ))->withFullUrl() !!}

@stop

@section('script')
  <script>
  var contestsService = '{!! $contestsServiceJson !!}';
  $(document).ready(function() {

        function sevicerSelectbox(contestsField) {
          var targetSelectBox = $('[name=contests_service]');
          targetSelectBox.empty();
          var services = JSON.parse(contestsService);
          $.each(services, function(key, data) {
              if ((data.contests_field_id == contestsField)) {
                  targetSelectBox.append("<option value='" + data.id + "'>" + data.name + "</option>");
              }
          });
        }

        $( window ).load(function() {
          var contestsField = $('#fll-contests-field').val();
          sevicerSelectbox(contestsField);
        });

        $('#fll-contests-field').change(function() {
            var contestsField = $(this).val();
            sevicerSelectbox(contestsField);
        });

  });
  </script>
@stop
