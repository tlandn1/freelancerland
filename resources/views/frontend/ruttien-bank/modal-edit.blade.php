{!! Form::open(['class'=>'form-horizontal','id'=>'fll-withdrawForm','style'=>'display:none']) !!}
  {!! Form::hidden('id') !!}
  <div class="form-group">
      {!! Form::label('title','Tên ngân hàng',['class'=>'col-sm-3 control-label']) !!}
      <div class="col-sm-8">
        {!! Form::text('title',null,['class' => 'form-control','placeholder'=>'Tên ngân hàng']) !!}
      </div>
  </div>
  <div class="form-group">
      {!! Form::label('description','Thông tin tài khoản',['class'=>'col-sm-3 control-label']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('description',null,['class' => 'form-control','placeholder'=>'Thông tin tài khoản']) !!}
      </div>
  </div>
  <div class="form-group">
      <div class="col-xs-5 col-xs-offset-3">
        {!! Form::submit('Lưu hồ sơ',['class'=>'btn btn-success']) !!}
      </div>
  </div>
{!! Form::close() !!}
