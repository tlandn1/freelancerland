@extends('frontend.master')
@section('title')
Tài khoản ngân hàng của {{ $loggedInUser->name }}
@stop

@section('styles-vendor')
<!-- https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/2.5.1/jquery-confirm.min.css -->
@stop
@section('class','fll-withdrow-money')
@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-2">
          @include('frontend.partials.left-menu')
      </div><!--end .col-md-2-->
      <div class="col-md-10">
         <div class="table-responsive">
              <h4>TÀI KHOẢN NGÂN HÀNG</h4>
               @if (Session::has('message'))<div class="alert alert-success">{{ Session::get('message') }}</div>@endif
              <!-- information withdrow account bank -->
              @include('frontend.ruttien-bank.info-bank')
         </div> <!-- end .table-responsive -->
      </div><!--end .col-md-10-->
    </div> <!-- end .row -->
</div> <!-- end .container -->

<!-- Modal form add withdrow account bank -->
@include('frontend.ruttien-bank.modal-add')
<!-- Modal form edit withdrow account bank -->
@include('frontend.ruttien-bank.modal-edit')

@stop
@section('scripts-vendor')
   <script src="//oss.maxcdn.com/bootbox/4.2.0/bootbox.min.js"></script>

   {!! Minify::javascript(array(
      '/js/frontend/parsley.min.js',
      '/js/frontend/parsley-vi.js'
      //'/js/frontend/jquery.confirm.min.js'
   ))->withFullUrl() !!}

@stop
@section('script')
<script>
   $(document).ready(function() {
     // Select All CheckBox
      var resetSelectAllCheckBox = function() {
         $("[name=select_all]").prop('checked', false);
      };
      $("[name=select_all]").change(function () {
         $("input:checkbox").prop('checked', $(this).prop("checked"));
      });

    // form modal
   $('.fll-editButton').on('click', function() {
      // Get the record's ID via attribute
      var id = $(this).attr('data-id');
      $.ajax({
         url: '/ruttien-datatables-ajax-data/' + id,
         method: 'GET'
      }).success(function(response) {
         // Populate the form fields with the data returned from server
         $('#fll-withdrawForm')
            .find('[name="id"]').val(response.id).end()
            .find('[name="title"]').val(response.title).end()
            .find('[name="description"]').val(response.description).end();
         // Show the dialog
         bootbox
            .dialog({
               title: 'Chỉnh sửa thông tin tài khoản ngân hàng',
               message: $('#fll-withdrawForm'),
               show: false // We will show it manually later
            })
            .on('shown.bs.modal', function() {
               $('#fll-withdrawForm')
               .show() // Show the edit form
            })
            .on('hide.bs.modal', function(e) {
               $('#fll-withdrawForm').hide().appendTo('body');
            })
            .modal('show');
     });
   });
   $('#fll-withdrawForm')
      .on('submit', function(e) {
         // Save the form data via an Ajax request
         e.preventDefault();
            var $form = $(e.target),
            id    = $form.find('[name="id"]').val();
         $.ajax({
            url: '/ajax-update-withdraw/' + id,
            method: 'PUT',
            data: $form.serialize()
         }).success(function(response) {
            // Get the cells
            var $button = $('button[data-id="' + response.id + '"]'),
            $tr     = $button.closest('tr'),
            $cells  = $tr.find('td');

            // Update the cell data
            $cells
               .eq(1).html(response.title).end()
               .eq(2).html(response.description).end();
              // Hide the dialog
            $form.parents('.bootbox').modal('hide');
            bootbox.alert('Chỉnh sửa thông tin tài khoản ngân hàng thành công!');
         });
   });

   // delete  withdraw account bank
   $('.fll-confirm').on('click', function(e) {
      e.preventDefault();
      var btnDelete = $(this);
      bootbox.confirm('Bạn có chắc chắn xóa không?', function(result) {
         if (result) {
              btnDelete.closest('form').submit();
         } else {
            //nothing
         }
      });
   });

   // delete select withdraw account bank
   $('.fll-btnDelete').on('click', function (e) {
      e.preventDefault();
      var btnDelete = $(this);
      bootbox.confirm('Bạn có chắc chắn xóa những mục đã chọn không?', function(result) {
         if (result) {
              btnDelete.closest('form').submit();
         } else {
            //nothing
         }
      });
   })
});
// add withdraw account bank
$('#fll-accountForm')
   .on('submit', function(e) {
      $('#fll-add-withdraw-account').modal('hide');
      windows.location.reload();

});


</script>
@stop
