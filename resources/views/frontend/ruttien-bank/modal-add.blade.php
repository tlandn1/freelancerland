<div class="modal fade modal-contact" id="fll-add-withdraw-account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
           <div class="modal-header">
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Tài khoản ngân hàng</h4>
           </div>
             {!! Form::open(['url'=>array('them-tai-khoan-rut-tien'),'method' => 'POST','data-remote','data-remote'=>'','parent-modal' => 'fll-withdraw-account','name'=>'fll-withdrow-account','id'=>'fll-accountForm']) !!}
           <div class="modal-body">
                <div class="form-group">
                  {!! Form::label('Tên ngân hàng','Tên ngân hàng (Ưu tiên Vietcombank)') !!}
                  {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Ví dụ: Vietcombank, ACB, Techcombank ...','required'=>'']) !!}
                </div>

                <div class="form-group">
                  {!! Form::label('Mô tả','Thông tin tài khoản') !!}
                  <textarea required="" name="description" style="height:auto" class="form-control" rows="8" placeholder='Vui lòng nhập Tiếng Việt có dấu'></textarea>
                  <p> <strong>Ví dụ:</strong> <br />
                    1. Tên chủ tài khoản: Nguyễn Văn A<br />
                    2. Số tài khoản: 12345689<br />
                    3. Tên chi nhánh và tỉnh thành: Phòng giao dịch Thanh Khê, Đà Nẵng
                  </p>
                </div>
           </div>
           <div class="modal-footer">
                {!! Form::button('Thêm',array('type'=>'submit','class'=>'btn btn-blue fll-addButton')) !!}
                <div class="form-loading"></div>
           </div>
           {!! Form::close() !!}

        </div>
    </div>
</div>
