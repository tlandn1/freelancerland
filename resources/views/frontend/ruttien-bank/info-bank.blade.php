{!! Form::open(['route'=>'xoa-rut-tien-chon','method'=>'Post']) !!}
<table class="table table-bordered table-striped fll-table">
   <thead>
      <tr>
         <th><input type="checkbox" name="select_all"></th>
         <th>Tên ngân hàng</th>
         <th>Thông tin tài khoản</th>
         <th>Hành động</th>
      </tr>
   </thead>

   <tbody>
      @foreach($loggedInUser->withdraw as $withdraw)
      <tr>
         <td><input type="checkbox" name="row_checkbox[]" value="{!! $withdraw->id !!}"/></td>
         <td>{!! $withdraw->title !!}</td>
         <td>{!! $withdraw->description !!}</td>
         <td>
         <button type="button" data-id="{!! $withdraw->id !!}" class="btn btn-default fll-editButton">Sửa</button>
         {!! Form::open(['route'=>['xoa-tai-khoan-ngan-hang',$withdraw->id],'method'=>'POST','id'=>'formDelete']) !!}
            <!-- {{ Form::hidden('id', $withdraw->id) }} -->
            {!! Form::button('Xóa',['type'=>'submit','class'=>'btn btn-default fll-confirm','id'=>$withdraw->id]) !!}
         {!! Form::close() !!}
         </td>
      </tr>
      @endforeach
   </tbody>
</table>
{!! Form::submit(trans('Xóa mục chọn'),['class'=>'btn btn-red-2 fll-btnDelete']) !!}
<a data-toggle="modal" data-target="#fll-add-withdraw-account" class="fll-btn-add-account-bank" href="#"> Thêm tài khoản ngân hàng</a>
{!! Form::close() !!}
