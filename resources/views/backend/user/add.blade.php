@extends('backend.master')
@section('styles-vendor')

  {!! Minify::stylesheet(array(
        '/css/backend/main-croppic.css',
        '/css/backend/croppic.css',
        '/css/backend/bootstrap-datetimepicker.min.css'
  ))->withFullUrl() !!}

@stop
@section('contentheader_title')
  Thêm mới thành viên
@stop
@section('main-content')
<div class="row">
	<!-- left column -->
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
      {!! Form::open(['route'=>['admin.user.store'],'class'=>'parsley','method' => 'POST','name' => 'addNewUserForm','data-parsley-validate'=>'']) !!}
      <!-- Thông tin tài khoản -->
			<div class="row">
        <div class="col-md-12">
          <div class="box-header with-border">
            <h3 class="box-title">Thông tin tài khoản</h3>
          </div>
  				<div class="box-body">
    					<div class="col-md-6">
    						<div class="form-group">
    							{!! Form::label('Name','Họ tên *') !!}
    							{!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Nhập name','required'=>'','data-parsley-maxlength'=>'40']) !!}
    						</div>

    						<div class="form-group">
    							{!! Form::label('Email','Địa chỉ email *') !!}
    							{!! Form::email('email', null ,['class'=>'form-control','placeholder'=>'Nhập email','required'=>'']) !!}
    						</div>

    						<div class="form-group">
    							{!! Form::label('Password','Mật khẩu *') !!}
    							{!! Form::password('password',['class'=>'form-control','placeholder'=>'Nhập password','required'=>'','data-parsley-minlength'=>'8','data-parsley-maxlength'=>'30']) !!}
    						</div>
                {{--
    						<div class="form-group">
    							{!! Form::label('Password Confirmation','Xác nhận mật khẩu *') !!}
    							{!! Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'Nhập xác nhận password','required'=>'','data-parsley-minlength'=>'8','data-parsley-maxlength'=>'30']) !!}
    						</div>
                --}}
    						<div class="form-group">
    							{!! Form::label('Phone number','Số điện thoại') !!}
    							{!! Form::number('phone',null,['class'=>'form-control','placeholder'=>'Nhập số điện thoại','data-parsley-minlength'=>'6','data-parsley-maxlength'=>'11']) !!}
    						</div>

                <div class="form-group">
    							{!! Form::label('Note','Ghi chú') !!}
    							{!! Form::textarea('note',null,['class'=>'form-control','rows'=>'5']) !!}
    						</div>

                <div class="form-group">
    							{!! Form::label('Receive email','Nhận email') !!}
                  {!! Form::checkbox('receive_email',null,['class'=>' receive_email']) !!}
    						</div>

                <div class="form-group">
    							{!! Form::label('Status','Trạng thái') !!}
    							{!! Form::select('status_id',$status,null,['class'=>'form-control']) !!}
    						</div>
    					</div>
    					<div class="col-md-6">

    						<div class="form-group">
    							{!! Form::label('Role','Vai trò') !!}
    							{!! Form::select('role_id',$role,null,['class'=>'form-control']) !!}
    						</div>

                <div class="form-group">
    							{!! Form::label('sodu_tk','Số dư tài khoản') !!}
    							{!! Form::number('sodu_tk',null,['class'=>'form-control','placeholder'=>'Nhập số dư tài khoản','data-parsley-maxlength'=>'12']) !!}
    						</div>

                <div class="form-group">
    							{!! Form::label('skype','Skype') !!}
    							{!! Form::text('skype',null,['class'=>'form-control','placeholder'=>'Nhập skype','data-parsley-maxlength'=>'50']) !!}
    						</div>

                <div class="form-group">
    							{!! Form::label('facebook','Facebook') !!}
    							{!! Form::text('facebook',null,['class'=>'form-control','placeholder'=>'Nhập facebook','data-parsley-maxlength'=>'50']) !!}
    						</div>

                <div class="form-group">
    							{!! Form::label('website','Website') !!}
    							{!! Form::text('website',null,['class'=>'form-control','placeholder'=>'Nhập website','data-parsley-maxlength'=>'50']) !!}
    						</div>

                <div class="form-group ">
                    {!!  Form::label('avatar','Hình đại diện') !!}
                    {!! Form::hidden('avatar', null,['class'=>'form-control', 'id' => 'avatar']) !!}
                    <!-- <div class="col-sm-10"> -->
                        <div id="croppic" style="width:{{ config('freelancerland.IMAGE_AVATAR_WIDTH') }}px;height:{{ config('freelancerland.IMAGE_AVATAR_HEIGHT') }}px"></div>
                    <!-- </div> -->
                </div>
              </div>
  				</div>
        </div>
			</div>
      <!-- Thông tin cá nhân -->
      <div class="row">
        <div class="col-md-12">
          <div class="box-header with-border">
            <h3 class="box-title">Thông tin liên hệ</h3>
          </div>
          <div class="box-body">
            <div class="col-md-6">

              <div class="form-group">
                {!! Form::label('cmnd_number','Số CMND') !!}
                {!! Form::number('cmnd_number',null,['class'=>'form-control','placeholder'=>'Nhập số chứng minh nhân dân','data-parsley-maxlength'=>'10']) !!}
              </div>

              <div class="form-group">
                {!! Form::label('cmnd_date_of_birth','Ngày sinh') !!}
                <div class="input-group date" id="datetimepicker">
                  {!! Form::text('cmnd_date_of_birth',null,['class'=>'form-control','placeholder'=>'Nhập ngày sinh']) !!}
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>

              <div class="form-group">
                {!! Form::label('cmnd_address','Địa chỉ') !!}
                {!! Form::text('cmnd_address',null,['class'=>'form-control','placeholder'=>'Nhập địa chỉ','data-parsley-maxlength'=>'255']) !!}
              </div>
            </div>

            <div class="col-md-6">

              <div class="form-group">
                {!! Form::label('about_job_title','Nghề nghiệp chuyên môn') !!}
                {!! Form::text('about_job_title',null,['class'=>'form-control','placeholder'=>'Nhập nghề nghiệp chuyên môn','data-parsley-maxlength'=>'255']) !!}
              </div>

              <div class="form-group">
                {!! Form::label('about_description','Giới thiệu bản thân') !!}
                {!! Form::textarea('about_description',null,['class'=>'form-control','rows'=>'5']) !!}
              </div>
            </div>
          </div>
          <div class="box-footer">
            {!! Form::submit('Thêm mới',['class'=>'btn btn-primary']) !!}
          </div>
        </div>
      </div>
      {!! Form::close() !!}
		</div><!--/.col (left) -->
		<!-- right column -->
	</div><!-- /.box-body -->
</div><!-- /.box -->
</div><!--/.col (right) -->
</div>   <!-- /.row -->
@stop

@section('scripts-vendor')
  {!! Minify::javascript(array(
    '/js/backend/parsley.min.js',
    '/js/backend/parsley-vi.js',
    '/js/backend/croppic.js',
    '/js/backend/moment.js',
    '/js/backend/bootstrap-datetimepicker.min.js'
  ))->withFullUrl() !!}
@stop

@section('script')
  <script>
        $('input[name=receive_email]').click(function(){
            this.value = this.checked ? 1:0;
        })

        var editUserSettings = [
            '/uploadImage/avatar', // uploadUrl
            '/cropImage/avatar', // cropUrl
            '/uploads/users/', // croppedDir
        ];

        var eyeCandy = $('#croppic');
        var croppicHeaderOptions = {
            uploadUrl: editUserSettings[0],
            cropUrl: editUserSettings[1],
            outputUrlId:'avatar',
            cropData: {
                'width': eyeCandy.width(),
                'height': eyeCandy.height()
            },
            doubleZoomControls:false,
            //imgEyecandy:false,
            loaderHtml: '<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
            onAfterImgUpload: 	function(){
                var originImageUrl = $('#croppic > div.cropImgWrapper > img').attr('src');
                var originFileName = originImageUrl.split("/").pop(); // => profile-54b58.jpg
                var fullCroppedImagePath = editUserSettings[2]+ "{{ env('CROPPED_IMAGE_PREFIX', 'cropped-')}}" + originFileName;
                //console.log(fullCroppedImagePath);
                $('#avatar').attr('value', fullCroppedImagePath);
            },
        };
        var cropperBox = new Croppic('croppic', croppicHeaderOptions);

        $(function () {
            $('#datetimepicker').datetimepicker({
             format: 'DD/MM/YYYY'
            });
        });
  </script>
@stop
