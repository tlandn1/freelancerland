@extends('backend.master')

@section('styles-vendor')

  {!! Minify::stylesheet(array(
        '/css/backend/main-croppic.css',
        '/css/backend/croppic.css',
        '/css/backend/bootstrap-datetimepicker.min.css'
  ))->withFullUrl() !!}

@stop

@section('contentheader_title')
  Cập nhật thông tin thành viên
@stop
@section('main-content')
<div class="row">
    <div class="col-md-3">

        <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-body box-profile">
                <img class="profile-user-img img-responsive img-circle thumb"  alt="User profile picture">
                <h3 class="profile-username text-center">{{ $user['name'] }}</h3>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
        <!-- About Me Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Thông tin cá nhân</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-envelope" aria-hidden="true"></i> Email</strong>
              <p class="text-muted">
                  {{ $user['email'] }}
              </p>

              <hr>

                <strong><i class="fa fa-book margin-r-5"></i>Số điện thoại</strong>
                <p class="text-muted">
                    {{ $user['phone'] }}
                </p>

                <hr>
                <strong><i class="fa fa-map-marker margin-r-5"></i>Nhận email</strong>
                <p class="text-muted">@if($user['receive_email'] == 1)
                    Có
                    @else Không
                    @endif</p>

                <hr>
                <strong><i class="fa fa-file-text-o margin-r-5"></i> Ghi chú</strong>
                <p>{{ $user['note'] }}</p>
                <hr>

                <strong><i class="fa fa-pencil margin-r-5"></i> Trạng thái</strong>
                <p>
                  <span class="label label-{{ ($user['user_status_id'] == USER_STATUS_ACTIVE_ID) ? 'success' : 'danger' }}">{{ $user->status->name }}</span>
                </p>
    					  <hr>

                <strong><i class="fa fa-map-marker margin-r-5"></i>Vai trò</strong>
                <p class="text-muted"></p>
                {{ $user->role->name }}
                <hr>

            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
    <div class="col-md-9">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li  class="active"><a href="#profile" data-toggle="tab">Sửa thông tin cá nhân</a></li>
                <li class="change-pasword"><a href="#change_password" data-toggle="tab">Thay đổi mật khẩu</a></li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane active" id="profile">
                    {!! Form::model($user,['route'=>['admin.user.update',$user['id']],'method' => 'PATCH','name' => 'editUserForm','data-parsley-validate'=>'','class'=>'form-horizontal profile-user parsley']) !!}
                      <!-- Thông tin tài khoản -->
                      <div class="box-header with-border">
                        <h3 class="box-title">Thông tin tài khoản</h3>
                      </div>
                      <div class="box-body">
                        <div class="form-group">
                            {!!  Form::label('Name *','Họ tên',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Nhập name','required'=>'','data-parsley-maxlength'=>'40']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!!  Form::label('Email *','Địa chỉ email',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::email(null,$user['email'],['class'=>'form-control','placeholder'=>'Nhập email','required'=>'','disabled']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!!  Form::label('Phone number','Số điện thoại',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::number('phone',null,['class'=>'form-control','placeholder'=>'Nhập số điện thoại','minlength'=>'6','maxlength'=>'11']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!!  Form::label('Receive email','Nhận email',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::checkbox('receive_email',$user['receive_email'],($user['receive_email'] == 1) ? true : false,['class'=>' receive_email']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!!  Form::label('Note','Ghi chú',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::textarea('note',null,['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!!  Form::label('Status','Trạng thái',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::select('status_id',$status,null,['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!!  Form::label('Role','Vai trò',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::select('role_id',$role,null,['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
            							{!! Form::label('sodu_tk','Số dư tài khoản',['class'=>'col-sm-2 control-label']) !!}
                          <div class="col-sm-10">
                              {!! Form::number('sodu_tk',null,['class'=>'form-control','placeholder'=>'Nhập số dư tài khoản','data-parsley-maxlength'=>'12']) !!}
                          </div>
            						</div>

                        <div class="form-group">
            							{!! Form::label('skype','Skype',['class'=>'col-sm-2 control-label']) !!}
                          <div class="col-sm-10">
                          {!! Form::text('skype',null,['class'=>'form-control','placeholder'=>'Nhập skype','data-parsley-maxlength'=>'50']) !!}
                          </div>
            						</div>

                        <div class="form-group">
            							{!! Form::label('facebook','Facebook',['class'=>'col-sm-2 control-label']) !!}
                          <div class="col-sm-10">
                            {!! Form::text('facebook',null,['class'=>'form-control','placeholder'=>'Nhập facebook','data-parsley-maxlength'=>'50']) !!}
                          </div>
            						</div>

                        <div class="form-group">
            							{!! Form::label('website','Website',['class'=>'col-sm-2 control-label']) !!}
                          <div class="col-sm-10">
                            {!! Form::url('website',null,['class'=>'form-control','placeholder'=>'Nhập website','data-parsley-maxlength'=>'50']) !!}
                          </div>
            						</div>

                        <div class="form-group ">
                            {!!  Form::label('Avatar','Hình ảnh',['class'=>'col-sm-2 control-label']) !!}
                            {!! Form::hidden('avatar', $user['avatar'],['class'=>'form-control', 'id' => 'avatar']) !!}
                            <div class="col-sm-10">
                                <div id="croppic" style="width:{{  config('freelancerland.IMAGE_AVATAR_WIDTH') }}px;height:{{ config('freelancerland.IMAGE_AVATAR_HEIGHT') }}px"></div>
                            </div>
                        </div>
                      </div>
                      <!-- Thông tin liên hệ -->
                      <div class="box-header with-border">
                        <h3 class="box-title">Thông tin liên hệ</h3>
                      </div>
                      <div class="box-body">
                          <div class="form-group">
              							{!! Form::label('cmnd_number','Số CMND',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                              {!! Form::number('cmnd_number',null,['class'=>'form-control','placeholder'=>'Nhập số chứng minh nhân dân','data-parsley-maxlength'=>'10']) !!}
                            </div>
              						</div>

                          <div class="form-group">
                            {!! Form::label('cmnd_date_of_birth','Ngày sinh',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                              <div class="input-group date" id="datetimepicker">
                                {!! Form::text('cmnd_date_of_birth',null,['class'=>'form-control','placeholder'=>'Nhập ngày sinh']) !!}
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                              </div>
                            </div>
                          </div>

                          <div class="form-group">
              							{!! Form::label('cmnd_address','Địa chỉ',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                              {!! Form::text('cmnd_address',null,['class'=>'form-control','placeholder'=>'Nhập địa chỉ','data-parsley-maxlength'=>'255']) !!}
                            </div>
              						</div>

                          <div class="form-group">
              							{!! Form::label('about_job_title','Nghề nghiệp',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                              {!! Form::text('about_job_title',null,['class'=>'form-control','placeholder'=>'Nhập nghề nghiệp chuyên môn','data-parsley-maxlength'=>'255']) !!}
                            </div>
              						</div>

                          <div class="form-group">
              							{!! Form::label('about_description','Giới thiệu',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                              {!! Form::textarea('about_description',null,['class'=>'form-control','rows'=>'5']) !!}
                            </div>
              						</div>
                        </div>
                      <div class="box-footer">
                        <div class="col-sm-offset-2 col-sm-10">
                            {!! Form::submit('Cập nhật',['class'=>'btn btn-primary']) !!}
                        </div>
                      </div>
                    {!! Form::close() !!}
                </div><!-- /.tab-pane -->

                <div class="tab-pane" id="change_password">
                    {!! Form::open(['route'=>['user.changepassword',$user['id']],'id'=>'parsley','method' => 'POST','name' => 'changepasswordUserForm','data-parsley-validate'=>'','class'=>'form-horizontal']) !!}
                      <div class="form-group">
                          {!!  Form::label('Password *','Mật khẩu *',['class'=>'col-sm-2 control-label']) !!}
                          <div class="col-sm-10">
                              {!! Form::password('password',['class'=>'form-control','placeholder'=>'Nhập password','required'=>'','data-parsley-minlength'=>'8','data-parsley-maxlength'=>'30']) !!}
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                              {!! Form::submit('Đổi mật khẩu',['class'=>'btn btn-primary']) !!}
                          </div>
                      </div>
                    {!! Form::close() !!}
                </div><!-- /.tab-pane -->
            </div><!-- /.tab-content -->
        </div><!-- /.nav-tabs-custom -->
    </div><!-- /.col -->
</div><!-- /.row -->
@stop

@section('scripts-vendor')

{!! Minify::javascript(array(
    '/js/backend/croppic.js',
     '/js/backend/parsley.min.js',
     '/js/backend/parsley-vi.js',
     '/js/backend/moment.js',
     '/js/backend/bootstrap-datetimepicker.min.js'
  ))->withFullUrl() !!}
@stop

@section('script')
<script>
    //////Scrip preview avatar user
    $('.thumb').attr('src', '<?php echo $user->avatar; ?>');
    var editUserSettings = [
        '/uploadImage/avatar', // uploadUrl
        '/cropImage/avatar', // cropUrl
        '/uploads/users/', // croppedDir
    ];

    var eyeCandy = $('#croppic');
    var croppicHeaderOptions = {
        uploadUrl: editUserSettings[0],
        cropUrl: editUserSettings[1],
        outputUrlId:'avatar',
        cropData: {
            'width': eyeCandy.width(),
            'height': eyeCandy.height()
        },
        doubleZoomControls:false,
        //imgEyecandy:false,
        loaderHtml: '<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
        onAfterImgUpload: 	function(){
            var originImageUrl = $('#croppic > div.cropImgWrapper > img').attr('src');
            var originFileName = originImageUrl.split("/").pop(); // => profile-54b58.jpg
            var fullCroppedImagePath = editUserSettings[2]+ "{{ env('CROPPED_IMAGE_PREFIX', 'cropped-')}}" + originFileName;
            //console.log(fullCroppedImagePath);
            $('#avatar').attr('value', fullCroppedImagePath);
        },
    };
    var cropperBox = new Croppic('croppic', croppicHeaderOptions);

    $('input[name=receive_email]').click(function(){
        this.value = this.checked ? 1:0;
    })

    $(function () {
        $('#datetimepicker').datetimepicker({
         format: 'DD/MM/YYYY'
        });
    });
</script>
@stop
