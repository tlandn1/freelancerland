@extends('backend.master')

@section('styles-vendor')

{!! Minify::stylesheet(array(
      '/css/backend/dataTables.bootstrap.css',
      '/css/backend/sweetalert2.min.css'
))->withFullUrl()!!}

@stop

@section('contentheader_title')
Danh sách thành viên
@stop

@section('main-content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
            </div><!-- /.box-header -->
            <div class="box-body">
              {!! Form::open(['route'=>['destroy-multiUser'],'method' => 'POST']) !!}
                <table id="table-user" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th><input type="checkbox" name="select_all"></th>
                            <th>ID</th>
                            <th>Hình đại diện</th>
                            <th>Họ tên</th>
                            <th>Địa chỉ email</th>
                            <th>Nhận Email</th>
                            <th>Trạng thái</th>
                            <th>Vai trò</th>
                            <th>Xóa</th>
                            <th>Sửa</th>
                        </tr>
                    </thead>

                    <tfoot>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Hình đại diện</th>
                            <th>Họ tên</th>
                            <th>Địa chỉ email</th>
                            <th>Nhận Email</th>
                            <th>Trạng thái</th>
                            <th>Vai trò</th>
                            <th>Xóa</th>
                            <th>Sửa</th>
                        </tr>
                    </tfoot>
                </table>
                {!! Form::submit('Xóa dòng đã chọn',['class'=>'btn btn-primary btnDelete']) !!}
              {!! Form::close() !!}
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
@stop

@section('scripts-vendor')

{!! Minify::javascript(array(
    '/js/backend/jquery.dataTables.min.js',
     '/js/backend/dataTables.bootstrap.min.js',
     '/js/backend/sweetalert2.min.js'
  ))->withFullUrl() !!}

@stop

@section('script')
<script>
var resetSelectAllCheckBox = function() {
    $("[name=select_all]").prop('checked', false);
};

$("[name=select_all]").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
});
//Table User
var dataTablesAjaxURL = '/admin/user-datatables-ajax-data';
var dataTablesColumns = [{
      data: 'checkbox',
      name: 'checkbox',
      orderable: false,
      searchable: false
    }, {
        data: 'id',
        name: 'users.id'
    }, {
        data: 'avatar',
        name: 'users.avatar'
    }, {
        data: 'name',
        name: 'users.name'
    }, {
        data: 'email',
        name: 'users.email'
    }, {
        data: 'receive_email',
        name: 'users.receive_email',
        orderable: false,
        searchable: false
    }, {
        data: 'statusLabel',
        name: 'user_statuses.label'
    }, {
        data: 'roleLabel',
        name: 'roles.label'
    }, {
        data: 'delete',
        name: 'delete',
        orderable: false,
        searchable: false
    }, {
        data: 'edit',
        name: 'edit',
        orderable: false,
        searchable: false
    }];

$('#table-user').on('draw.dt', function () {
    eventFiredBtnDeleteSweetAlert();
}).DataTable({
    "language": {
            "url": "/js/datatables.vietnamese.json"
        },
    processing: true,
    serverSide: true,
    ajax: dataTablesAjaxURL,
    order: [[ 1, "desc" ]],
    columns: dataTablesColumns
});

</script>
@stop
