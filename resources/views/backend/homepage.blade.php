@extends('backend.master')
@section('styles-vendor')
@stop
@section('contentheader_title')
Dashboard
@stop
@section('main-content')
<div class="row">
  <section class="col-lg-3">
    <div class="box box-info">
      <div class="box-header ui-sortable-handle" style="cursor: move;">
        <i class="fa fa-envelope"></i>
        <h3 class="box-title">Export Emails to CSV</h3>
      </div>
      <div class="box-footer clearfix">
         <a href="#" class="btn btn-default" role="button">Export Emails to CSV</a>
      </div>
    </div>
  </section>
</div>
<!-- /.row -->
@stop
@section('scripts-vendor')
@stop
@section('script')
<script></script>
@stop
