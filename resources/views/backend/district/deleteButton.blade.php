{!! Form::open(array('route'=>array('admin.district.destroy',$district['id']),'method' => 'DELETE')) !!}
    <button id ="btnDelete-{{ $district->id }}" class="btn btn-danger btnDelete"><i class="fa fa-trash"></i></button>
{!! Form::close() !!}
