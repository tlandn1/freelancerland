@extends('backend.master')

@section('styles-vendor')
    {!! Minify::stylesheet('/css/backend/select2.min.css')->withFullUrl() !!}
@stop

@section('contentheader_title')
  Thêm mới Quận/Huyện/Thị Xã/TP
@stop
@section('main-content')
<div class="row">
  <!-- left column -->
  <div class="col-md-6">
    <!-- general form elements -->
    <div class="box box-primary">
      {!! Form::open(['route'=>['admin.district.store'], 'class'=>'parsley','method' => 'POST','data-parsley-validate'=>'','name' => 'addNewDistrictForm']) !!}
        <div class="box-body">
          <div class="form-group">
              {!! Form::label('Name *') !!}
              {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Nhập name','required'=>'', 'data-parsley-maxlength'=>'100']) !!}
          </div>

          <div class="form-group">
              {!! Form::label('Type') !!}
              {!! Form::select('type',$type_list,null,['class'=>'form-control']) !!}
          </div>

          <div class="form-group">
              {!! Form::label('City') !!}
              {!! Form::select('city_id',$cities,null,['class'=>'form-control useSelect2']) !!}
          </div>
        </div>
        <div class="box-footer">
          {!! Form::submit('Thêm mới',['class'=>'btn btn-primary']) !!}
        </div>
      {!! Form::close() !!}

    </div><!-- /.box -->
  </div><!--/.col (left) -->
  <!-- right column -->
</form>
</div><!-- /.box-body -->
</div><!-- /.box -->
</div><!--/.col (right) -->
</div>   <!-- /.row -->
@stop
@section('scripts-vendor')

{!! Minify::javascript(array(
  '/js/backend/select2.min.js',
  '/js/backend/parsley.min.js',
  '/js/backend/parsley-vi.js'
))->withFullUrl() !!}

@stop
