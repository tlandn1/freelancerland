<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>freelancerland.vn | @yield('htmlheader_title', 'Your title here') </title>

    <!-- Bootstrap 3.3.x -->
    <!--Use CDN Bootstrap, include Glyphicons for Data Tables used-->

    <!--
      {!! Minify::stylesheet(array(
            '/css/backend/bootstrap.min.css',
            '//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css'
      ))!!}
    -->


    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->

    {!! Minify::stylesheet(array(
          '/css/backend/bootstrap.min.css',
          '/css/backend/font-awesome.min.css',
          '/css/backend/AdminLTE.min.css',
          '/css/backend/skin-blue.min.css'
    ))->withFullUrl() !!}

    <!-- Optional CSS, depends each page
    <link href="/css/backend/main-croppic.css" rel="stylesheet" type="text/css" />
    <link href="/css/backend/croppic.css" rel="stylesheet" type="text/css" />
    <link href="/css/backend/sweetalert2.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/backend/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/backend/dropzone.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/backend/ekko-lightbox.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/backend/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/css/backend/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    -->
    <!-- <link href="/css/backend/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" /> -->
    @yield('styles-vendor')

    <!-- Main backend CSS file -->
    {!! Minify::stylesheet('/css/backend/all.css')->withFullUrl() !!}
    @yield('style')

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries
      https://cdnjs.com/libraries/html5shiv
      https://cdnjs.com/libraries/respond.js
    -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    {!! Minify::javascript(array(
        '//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js'
     )) !!}
    <![endif]-->
</head>
