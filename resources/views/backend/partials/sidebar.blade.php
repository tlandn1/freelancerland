<aside class="main-sidebar"> <!-- start main-sidebar -->
    <section class="sidebar"> <!-- start sidebar -->
        <ul class="sidebar-menu"> <!-- start sidebar-menu -->
         {{--<li class="header">TỈNH/THÀNH PHỐ</li>
           <li class="treeview">
                  <a href="#">
                    <i class="fa fa-university" aria-hidden="true"></i>
                      <span>Tỉnh/Thành phố</span>
                    <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                      <li><a href="{!! route('admin.city.index') !!}">Danh sách Tỉnh/Thành phố</a></li>
                      <li><a href="{!! route('admin.city.create') !!}">Thêm mới Tỉnh/Thành phố</a></li>
                  </ul>
            </li>

            <li class="treeview">
                  <a href="#">
                    <i class="fa fa-home" aria-hidden="true"></i>
                      <span>Quận/Huyện/Thị Xã/TP</span>
                    <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                      <li><a href="{!! route('admin.district.index') !!}">Danh sách Quận/Huyện/Thị Xã/TP</a></li>
                      <li><a href="{!! route('admin.district.create') !!}">Thêm mới Quận/Huyện/Thị Xã/TP</a></li>
                  </ul>
           </li> --}}

            <li class="header">CHUYÊN MỤC</li>
            <li class="treeview">
                  <a href="#">
                    <i class="fa fa-th-large"></i>
                      <span>Chuyên mục cấp 1</span>
                    <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                      <li><a href="{!! route('admin.first-category.index') !!}">Danh sách Chuyên mục cấp 1</a></li>
                      <li><a href="{!! route('admin.first-category.create') !!}">Thêm mới Chuyên mục cấp 1</a></li>
                  </ul>
            </li>

            <li class="treeview">
                  <a href="#"><i class="fa fa-th-large"></i>
                    <span>Chuyên mục cấp 2</span>
                    <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                      <li><a href="{!! route('admin.second-category.index') !!}">Danh sách Chuyên mục cấp 2</a></li>
                      <li><a href="{!! route('admin.second-category.create') !!}">Thêm mới Chuyên mục cấp 2</a></li>
                  </ul>
            </li>

            <li class="header">THÀNH VIÊN/TIN NHẮN</li>
            <li class="treeview">
                <a href="#"><i class="fa fa-users"></i>
                  <span>Thành viên</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! route('admin.user.index') !!}">Danh sách thành viên</a></li>
                    <li><a href="{!! route('admin.user.create') !!}">Thêm mới thành viên</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-wechat"></i> <span>Tin nhắn</span></a>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-file-text-o"></i> <span>Trang tĩnh</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{!! route('admin.static-page.index') !!}">Danh sách trang</a></li>
                    <li><a href="{!! route('admin.static-page.create') !!}">Thêm mới trang</a></li>
                </ul>
            </li>
        </ul> <!-- end sidebar-menu -->
    </section><!-- end sidebar -->
</aside> <!-- end main-sidebar -->
