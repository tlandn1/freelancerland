<!-- REQUIRED JS, Jquery, Bootstrap and AdminLTE JS -->
  {!! Minify::javascript(array(
    '/js/backend/jquery.min.js',
    '/js/backend/bootstrap.min.js',
    '/js/backend/adminlte.min.js'
  ))->withFullUrl() !!}
<!-- Optional JS, depends each page
<script src="/js/backend/jquery.dataTables.min.js"></script>
<script src="/js/backend/dataTables.bootstrap.min.js"></script>
<script src="/js/backend/parsley.min.js"></script>
<script src="/js/backend/parsley-vi.js"></script>
<script src="/js/backend/croppic.js"></script>
<script src="/js/backend/sweetalert2.min.js"></script>
<script src="/js/backend/select2.min.js"></script>
<script src="/js/backend/dropzone.min.js"></script>
<script src="/js/backend/dropzone-config.js"></script>
-->
<!-- <script src="/js/backend/bootstrap-datetimepicker.min.js"></script> -->
@yield('scripts-vendor')

<!-- REQUIRED JS, main js -->
{!! Minify::javascript('/js/backend/all.js')->withFullUrl() !!}
@yield('script')

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
