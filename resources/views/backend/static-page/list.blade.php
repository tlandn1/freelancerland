@extends('backend.master')
@section('styles-vendor')

  {!! Minify::stylesheet(array(
        '/css/frontend/dataTables.bootstrap.css',
        '/css/frontend/sweetalert2.min.css'
  ))->withFullUrl() !!}

@stop
@section('contentheader_title')
Danh sách trang tĩnh
@stop
@section('main-content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
              {!! Form::open(['route'=>['destroy-multiStaticPage'],'method' => 'POST']) !!}
                <table id="fll-table-static" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th><input type="checkbox" name="select_all"></th>
                            <th>ID</th>
                            <th>Tiêu đề</th>
                            <th>Url trang tĩnh</th>
                            <th>Xem trang</th>
                            <th>Xóa</th>
                            <th>Sửa</th>
                        </tr>
                    </thead>

                    <tfoot>
                      <tr>
                        <th></th>
                        <th>ID</th>
                        <th>Tiêu đề</th>
                        <th>Url trang tĩnh</th>
                        <th>Xem trang</th>
                        <th>Xóa</th>
                        <th>Sửa</th>
                      </tr>
                    </tfoot>
                </table>
                {!! Form::submit( 'Xóa mục đã chọn',['class'=>'btn btn-primary fll-btnDelete']) !!}
              {!! Form::close() !!}
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
@stop

@section('scripts-vendor')

  {!! Minify::javascript(array(
    '/js/backend/jquery.dataTables.min.js',
    '/js/backend/dataTables.bootstrap.min.js',
    '/js/backend/sweetalert2.min.js'
  ))->withFullUrl() !!}

@stop

@section('script')
<script>
var resetSelectAllCheckBox = function() {
    $("[name=select_all]").prop('checked', false);
};

$("[name=select_all]").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
});
// Table City
var dataTablesAjaxURL = '/admin/static-page-datatables-ajax-data';
var dataTablesColumns = [{
      data: 'checkbox',
      name: 'checkbox',
      orderable: false,
      searchable: false
    }, {
        data: 'id',
        name: 'id'
    }, {
        data: 'title',
        name: 'title'
    }, {
        data: 'slug',
        name: 'slug'
    }, {
        data: 'view_page',
        name: 'view_page',
        orderable: false,
        searchable: false
    }, {
        data: 'delete',
        name: 'delete',
        orderable: false,
        searchable: false
    }, {
        data: 'edit',
        name: 'edit',
        orderable: false,
        searchable: false
    }];

    $('#fll-table-static').on('draw.dt', function () {
        eventFiredBtnDeleteSweetAlert();
    }).DataTable({
        "language": {
            "url": "/js/datatables.vietnamese.json"
        },
        processing: true,
        serverSide: true,
        ajax: dataTablesAjaxURL,
        columns: dataTablesColumns
    });

</script>
@stop
