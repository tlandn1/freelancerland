@extends('backend.master')

@section('styles-vendor')

  {!! Minify::stylesheet(array(
        '/css/backend/trumbowyg.min.css',
        '/css/backend/trumbowyg.colors.min.css'
  ))->withFullUrl() !!}

@stop

@section('contentheader_title')
  Cập nhật trang {{ $page_static['title'] }}
@stop

@section('main-content')
<div class="row">
  <!-- left column -->
  <div class="col-md-12">
    <!-- general form elements -->
    <div class="box box-primary">
      {!! Form::model($page_static,['route'=>['admin.static-page.update',$page_static->id],'method' => 'PATCH','data-parsley-validate'=>'']) !!}
        <div class="box-body">
          <div class="form-group">
              {!! Form::label('title','Tiêu đề') !!}
              {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Nhập tiêu đề','required'=>'']) !!}
          </div>
          <div class="form-group">
              {!! Form::label('Slug','Url trang tĩnh') !!}
              {!! Form::text('slug',null,['class'=>'form-control','placeholder'=>'Nhập slug','required'=>'']) !!}
          </div>
          <div class="form-group">
              {!! Form::label('content','Nội dung') !!}
              {!! Form::textarea('content',null,['class'=>'form-control useTrumbowyg','placeholder'=>'Nhập nội dung...','required'=>'']) !!}
          </div>
        </div>
        <div class="box-footer">
          {!! Form::submit('Cập nhật',['class'=>'btn btn-primary']) !!}
        </div>
      {!! Form::close() !!}

    </div><!-- /.box -->
  </div><!--/.col (left) -->
</form>
</div><!-- /.box-body -->
</div><!-- /.box -->
</div><!--/.col (right) -->
</div>   <!-- /.row -->
@stop
@section('scripts-vendor')

  {!! Minify::javascript(array(
      '/js/backend/trumbowyg.min.js',
      '/js/backend/trumbowyg.vi.min.js',
      '/js/backend/trumbowyg.colors.min.js',
      '/js/backend/parsley.min.js',
      '/js/backend/parsley-vi.js'
  ))->withFullUrl() !!}

@stop
@section('script')
<script>
</script>
@stop
