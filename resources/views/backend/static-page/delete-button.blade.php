{!! Form::open(array('route'=>array('admin.static-page.destroy',$static_page['id']),'method' => 'DELETE')) !!}
    <button id ="btnDelete-{{ $static_page->id }}" class="btn btn-danger btnDelete"><i class="fa fa-trash"></i></button>
{!! Form::close() !!}
