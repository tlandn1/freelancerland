@extends('backend.master')
@section('contentheader_title')
  Cập nhật Chuyên mục cấp 1
@stop
@section('main-content')
<div class="row">
  <!-- left column -->
  <div class="col-md-6">
    <!-- general form elements -->
    <div class="box box-primary">
      {!! Form::model($first_category,['route'=>['admin.first-category.update',$first_category['id']],'method' => 'PATCH','name'=>'editFirstCategoryForm','data-parsley-validate'=>'', 'class'=>'parsley','enctype'=>'multipart/form-data']) !!}
        <div class="box-body">
          {!! Form::hidden('id') !!}

          <div class="form-group">
              {!! Form::label('Name *') !!}
              {!! Form::text('name',null,['class'=>'form-control','required'=>'', 'data-parsley-maxlength'=>'255']) !!}
          </div>

        </div>
        <div class="box-footer">
          {!! Form::submit('Cập nhật',['class'=>'btn btn-primary']) !!}
        </div>
      {!! Form::close() !!}

    </div><!-- /.box -->
  </div><!--/.col (left) -->
  <!-- right column -->
</form>
</div><!-- /.box-body -->
</div><!-- /.box -->
</div><!--/.col (right) -->
</div>   <!-- /.row -->
@stop
@section('scripts-vendor')

{!! Minify::javascript(array(
  '/js/backend/parsley.min.js',
  '/js/backend/parsley-vi.js'
))->withFullUrl() !!}

@stop
