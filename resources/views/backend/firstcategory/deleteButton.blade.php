{!! Form::open(array('route'=>array('admin.first-category.destroy',$first_category['id']),'method' => 'DELETE')) !!}
    <button id ="btnDelete-{{ $first_category->id }}" class="btn btn-danger btnDelete"><i class="fa fa-trash"></i></button>
{!! Form::close() !!}
