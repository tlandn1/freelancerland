<!DOCTYPE html>
<html>

@include('backend.partials.htmlheader')

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    @include('backend.partials.mainheader')

    @include('backend.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('backend.partials.contentheader')

        <!-- Main content -->
        <section class="content">
            @include('backend.partials.display-errors')
            @include('backend.partials.flash-messages')
            <!-- Your Page Content Here -->
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('backend.partials.footer')

</div><!-- ./wrapper -->

@include('backend.partials.scripts')

<div class="ajax-flash">
   Updated!
</div>
</body>
</html>
