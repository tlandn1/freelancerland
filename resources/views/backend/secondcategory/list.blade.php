@extends('backend.master')
@section('styles-vendor')

{!! Minify::stylesheet(array(
      '/css/backend/dataTables.bootstrap.css',
      '/css/backend/sweetalert2.min.css'
))->withFullUrl() !!}

@stop
@section('contentheader_title')
Danh sách Chuyên mục cấp 2
@stop
@section('main-content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="table-secondcategory" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>First Category</th>
                            <th>Delete</th>
                            <th>Edit</th>
                        </tr>
                    </thead>

                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>First Category</th>
                            <th>Delete</th>
                            <th>Edit</th>
                        </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
@stop
@section('scripts-vendor')

  {!! Minify::javascript(array(
    '/js/backend/jquery.dataTables.min.js',
    '/js/backend/dataTables.bootstrap.min.js',
    '/js/backend/sweetalert2.min.js'
  ))->withFullUrl() !!}

@stop

@section('script')
<script>

//Table SecondCategory
var dataTablesAjaxURL = '/admin/second-category-datatables-ajax-data';
var dataTablesColumns = [{
        data: 'id',
        name: 'second_categories.id'
    }, {
        data: 'name',
        name: 'second_categories.name'
    }, {
        data: 'firstCategoryName',
        name: 'first_categories.name'
    }, {
        data: 'delete',
        name: 'delete',
        orderable: false,
        searchable: false
    }, {
        data: 'edit',
        name: 'edit',
        orderable: false,
        searchable: false
    }];

$('#table-secondcategory').on('draw.dt', function () {
    eventFiredBtnDeleteSweetAlert();
}).DataTable({
    "language": {
            "url": "/js/datatables.vietnamese.json"
        },
    processing: true,
    serverSide: true,
    ajax: dataTablesAjaxURL,
    order: [[ 0, "desc" ]],
    columns: dataTablesColumns
});

</script>
@stop
