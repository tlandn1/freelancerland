@extends('backend.master')

@section('styles-vendor')
  {!! Minify::stylesheet('/css/backend/select2.min.css')->withFullUrl() !!}
@stop

@section('contentheader_title')
Thêm mới Chuyên mục cấp 2
@stop
@section('main-content')
<div class="row">
  <!-- left column -->
  <div class="col-md-6">
    <!-- general form elements -->
    <div class="box box-primary">
      {!! Form::open(['route'=>['admin.second-category.store'],'method' => 'POST','name' => 'addNewSecondCategoryForm','data-parsley-validate'=>'','class'=>'parsley']) !!}
        <div class="box-body">
          <div class="form-group">
            {!! Form::label('First category') !!}
            {!! Form::select('first_category_id',$first_category,null,['class'=>'form-control useSelect2']) !!}
          </div>
          <div class="form-group">
              {!! Form::label('Name *') !!}
              {!! Form::text('name',null,['class'=>'form-control','required'=>'', 'data-parsley-maxlength'=>'255']) !!}
          </div>
        </div>
        <div class="box-footer">
          {!! Form::submit('Thêm mới',['class'=>'btn btn-primary']) !!}
        </div>
      {!! Form::close() !!}

    </div><!-- /.box -->
  </div><!--/.col (left) -->
  <!-- right column -->
</form>
</div><!-- /.box-body -->
</div><!-- /.box -->
</div><!--/.col (right) -->
</div>   <!-- /.row -->
@stop
@section('scripts-vendor')

  {!! Minify::javascript(array(
    '/js/backend/select2.min.js',
    '/js/backend/parsley.min.js',
    '/js/backend/parsley-vi.js'
  ))->withFullUrl() !!}

@stop
