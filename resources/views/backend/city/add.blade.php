@extends('backend.master')

@section('contentheader_title')
Thêm mới Tỉnh/Thành phố
@stop

@section('main-content')
<div class="row">
  <!-- left column -->
  <div class="col-md-6">
    <!-- general form elements -->
    <div class="box box-primary">
      {!! Form::open(['route'=>['admin.city.store'],'method' => 'POST','data-parsley-validate'=>'', 'class'=>'parsley', 'name' => 'addNewCityForm']) !!}
        <div class="box-body">
          <div class="form-group">
              {!! Form::label('Name *') !!}
              {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Nhập name','required'=>'', 'data-parsley-maxlength'=>'100']) !!}
          </div>

          <div class="form-group">
              {!! Form::label('Type') !!}
              {!! Form::select('type',$type_list,'Tỉnh',['class'=>'form-control']) !!}
          </div>
        </div>
        <div class="box-footer">
          {!! Form::submit('Thêm mới',['class'=>'btn btn-primary']) !!}
        </div>
      {!! Form::close() !!}

    </div><!-- /.box -->
  </div><!--/.col (left) -->
  <!-- right column -->
</form>
</div><!-- /.box-body -->
</div><!-- /.box -->
</div><!--/.col (right) -->
</div>   <!-- /.row -->
@stop
@section('scripts-vendor')

{!! Minify::javascript(array(
  '/js/backend/parsley.min.js',
  '/js/backend/parsley-vi.js'
))->withFullUrl() !!}
@stop
