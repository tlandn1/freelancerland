@extends('backend.master')

@section('contentheader_title')
Cập nhật tỉnh/thành phố
@stop
@section('main-content')
<div class="row">
  <!-- left column -->
  <div class="col-md-6">
    <!-- general form elements -->
    <div class="box box-primary">
      {!! Form::model($city,['route'=>['admin.city.update',$city['id']],'method' => 'PATCH','data-parsley-validate'=>'', 'class'=>'parsley']) !!}
        <div class="box-body">
          {!! Form::hidden('id') !!}

          <div class="form-group">
              {!! Form::label('Name *') !!}
              {!! Form::text('name',null,['class'=>'form-control','required'=>'', 'data-parsley-maxlength'=>'100']) !!}
          </div>

          <div class="form-group">
              {!! Form::label('Type') !!}
              {!! Form::select('type',$type_list,null,['class'=>'form-control']) !!}
          </div>
        </div>
        <div class="box-footer">
          {!! Form::submit('Cập nhật',['class'=>'btn btn-primary']) !!}
        </div>
      {!! Form::close() !!}

    </div><!-- /.box -->
  </div><!--/.col (left) -->
  <!-- right column -->
</form>
</div><!-- /.box-body -->
</div><!-- /.box -->
</div><!--/.col (right) -->
</div>   <!-- /.row -->
@stop
@section('scripts-vendor')

{!! Minify::javascript(array(
  '/js/backend/parsley.min.js',
  '/js/backend/parsley-vi.js'
))->withFullUrl() !!}

@stop
