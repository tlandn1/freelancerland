@extends('backend.master')
@section('styles-vendor')

{!! Minify::stylesheet(array(
      '/css/backend/dataTables.bootstrap.css',
      '/css/backend/sweetalert2.min.css'
))->withFullUrl() !!}

@stop
@section('contentheader_title')
'Danh sách Tỉnh/Thành phố'
@stop
@section('main-content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <table id="table-city" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Delete</th>
                            <th>Edit</th>
                        </tr>
                    </thead>

                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Delete</th>
                            <th>Edit</th>
                        </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
@stop

@section('scripts-vendor')

{!! Minify::javascript(array(
  '/js/backend/jquery.dataTables.min.js',
  '/js/backend/dataTables.bootstrap.min.js',
  '/js/backend/sweetalert2.min.js'
))->withFullUrl() !!}

@stop

@section('script')
<script>

// Table City
var dataTablesAjaxURL = '/admin/city-datatables-ajax-data';
var dataTablesColumns = [{
        data: 'id',
        name: 'id'
    }, {
        data: 'name',
        name: 'name'
    }, {
        data: 'type',
        name: 'type'
    }, {
        data: 'delete',
        name: 'delete',
        orderable: false,
        searchable: false
    }, {
        data: 'edit',
        name: 'edit',
        orderable: false,
        searchable: false
    }];

    $('#table-city').on('draw.dt', function () {
        eventFiredBtnDeleteSweetAlert();
    }).DataTable({
        "language": {
            "url": "/js/datatables.vietnamese.json"
        },
        processing: true,
        serverSide: true,
        ajax: dataTablesAjaxURL,
        columns: dataTablesColumns
    });

</script>
@stop
