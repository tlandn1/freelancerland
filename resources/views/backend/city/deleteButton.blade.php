{!! Form::open(array('route'=>array('admin.city.destroy',$city['id']),'method' => 'DELETE')) !!}
    <button id ="btnDelete-{{ $city->id }}" class="btn btn-danger btnDelete"><i class="fa fa-trash"></i></button>
{!! Form::close() !!}
