// Setup Ajax X-CSRF-TOKEN
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var eventFiredBtnDeleteSweetAlert = function() {
    // Use sweetalert AFTER DataTables
    $('.btnDelete').on('click', function(e) {
        e.preventDefault();

        var btnDelete = $(this);
        // swal({
        //     title: "Bạn có chắc chắn?",
        //     text: "Hành động này không thể phục hồi!",
        //     type: "warning",
        //     showCancelButton: true,
        //     confirmButtonColor: "#DD6B55",
        //     confirmButtonText: "Vâng, hãy xóa nó",
        //     cancelButtonText: "Hủy",
        //     closeOnConfirm: true
        // }, function() {
        //     btnDelete.closest('form').submit();
        // });
        swal({
            title: "Bạn có chắc chắn?",
            text: "Hành động này không thể phục hồi!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Vâng, hãy xóa nó",
            cancelButtonText: "Hủy",
            closeOnConfirm: true
        }).then(function(isConfirm) {
            if (isConfirm === true) {
                btnDelete.closest('form').submit();
            }
        });
    });
};
jQuery(function($) {
    $('.useTrumbowyg').trumbowyg({
        fullscreenable: false,
        closable: true,
        lang: 'vi',
        btns: [
            ['viewHTML'],
            ['undo', 'redo'],
            ['formatting'],
            ['bold', 'italic'],
            ['link'],
            ['insertImage'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['foreColor', 'backColor'],
            ['fullscreen'],
        ]
    });
});