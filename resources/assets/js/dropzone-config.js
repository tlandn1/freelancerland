var photo_counter = 0;
Dropzone.options.realDropzone = {

        uploadMultiple: false,
        parallelUploads: 20,
        maxFilesize: 6,
        maxFiles: 5,
        previewsContainer: '#dropzonePreview',
        previewTemplate: document.querySelector('#preview-template').innerHTML,
        addRemoveLinks: true,
        dictRemoveFile: 'Remove',
        dictFileTooBig: 'Hình ảnh không được quá 6MB',
        dictMaxFilesExceeded: 'Bạn không thể upload quá 20 hình',
        paramName: 'img',

        // The setting up of the dropzone
        init: function() {

            this.on("removedfile", function(file) {

                $.ajax({
                    type: 'POST',
                    url: '/upload/delete',
                    data: {
                        id: file.imgid
                    },
                    dataType: 'html',
                    success: function(data) {
                        var rep = JSON.parse(data);
                        if (rep.code == 200) {
                            photo_counter--;
                            $("#photoCounter").text("(" + photo_counter + ")");

                            //remove id in input
                            var list_id = document.getElementById('image').value;
                            var array_id = list_id.split(";");
                            var id = rep.id;
                            for (var i = 0; i < array_id.length; i++) {
                                array_id[i] = parseInt(array_id[i]);
                            }
                            var index = array_id.indexOf(parseInt(id));
                            if (index > -1) {
                                array_id.splice(index, 1);
                            }
                            document.getElementById('image').value = array_id.join(";");
                        }

                    }
                });

            });
        },
        error: function(file, response) {
            if ($.type(response) === "string")
                var message = response; //dropzone sends it's own error messages in string
            else
                var message = response.message;
            file.previewElement.classList.add("dz-error");
            _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                node = _ref[_i];
                _results.push(node.textContent = message);
            }
            return _results;
        },
        success: function(file, response) {
                photo_counter++;
                $("#photoCounter").text("(" + photo_counter + ")");

                //add id in input
                var imgid = document.getElementById('image').value;
                if (imgid != "") {
                    imgid = imgid + ';' + response.id;
                    document.getElementById('image').value = imgid;
                } else {
                    document.getElementById('image').value = response.id;
                }

                //add edit and id image
                $(file.previewElement).find('.edit-img').attr('data-link', response.path);
                file.imgid = response.id;

                //call croppic
                var eyeCandy = $('#croppic');

                $('.edit-img').click(function() {
                    var cropperOptions = {
                        cropUrl: '/cropImage/post',
                        loadPicture: $(this).attr('data-link'),
                        onAfterImgCrop: function() {
                            d = new Date();
                            var srcimg = $('#croppic img.croppedImg').attr('src');
                            $('#croppic img.croppedImg').attr("src", srcimg + '?' + d.getTime());
                            swal({
                                title: "Sửa hình thành công!",
                                html: '<div class="conten-alert"><p>Nhấn <span class="img-btn-close"></span> để tiếp tục đăng tin.</p><p>Nhấn <span class="img-btn-refresh"></span> để sửa lại hình.</p></div>',
                                confirmButtonText: 'Đóng thông báo',
                            });
                        },
                        cropData: {
                            'width': eyeCandy.width(),
                            'height': eyeCandy.height()
                        },
                        loaderHtml: '<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
                    };

                    var cropperHeader = new Croppic('croppic', cropperOptions);
                    cropperHeader.reset();
                });
            } <<
            << << < HEAD
    } ===
    === =
} >>>
>>> > 67129 f8ce299d13807f382abaae02ce46130d70a