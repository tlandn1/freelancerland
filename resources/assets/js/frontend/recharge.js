jQuery(function($) {
    function updateOrders() {
        var form = $('#updateOrders');
        var method = form.find('input[name="_method"]').val() || 'POST';
        var route = form.prop('action');
        $.ajax({
            type: method,
            url: route,
            data: form.serialize(),
            success: function(data) {
                $(".info-ordertotal strong").text(data.price);
            }
        });
    }

    $(document).ready(function() {
        $('input[name=set_price]').mask('000.000.000.000.000', {
            reverse: true
        });
        $('input[name=set_price]').on('input', function() {
            var price_get = $(this).val();
            $('input[name=price]').val(price_get.replace(/\./gi, ""));
            $('.choose-price').removeClass('active');
            $(".info-ordertotal strong").text(price_get + ' đ');
            $('input[name=method_id]').val(2);
        });

        $('.group-prices .choose-price').click(function(event) {
            event.preventDefault();
            if (!$(this).hasClass('active')) {
                $('input[name=set_price]').val('');
                $('.error').text('');
                $('input[name=method_id]').val(1);
                $('.group-prices .choose-price').removeClass('active');
                $(this).addClass('active');
                $('input[name="price"]').val($(this).text().replace(/\./gi, ""));

                updateOrders();
            }
        });

        $('.payment-link').click(function(event) {
            if ($('input[name=method_id]').val() == 2) {
                if ($('input[name=price]').val() < 20000 || $('input[name=price]').val() % 1000 != 0) {
                    event.preventDefault();
                    $('.error').text('Số tiền nhập vào phải lớn hơn 20.000 đ và là bội số của 1.000');
                } else {
                    updateOrders();
                }
            }
        });
    });
});