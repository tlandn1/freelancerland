/*******************************************
          Setup Ajax X-CSRF-TOKEN
          *******************************************/
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/*************************************************
    Đưa chuột vào Freelancer tự động sổ xuống
    **************************************************/
$(document).ready(function() {
    $(".dropdown").hover(
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).slideDown("fast");
            $(this).toggleClass('open');
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).slideUp("fast");
            $(this).toggleClass('open');
        }
    );
});
/*************************************************

**************************************************/
// For left sidebar, auto mark active
// window.location.pathname = /gioi-thieu/ve-chodangtin
var aHref = $('a[href="' + window.location.pathname + '"] , a[href="' + window.location + '"]');
if (aHref.length) {
    aHref.parent().addClass('active');
    aHref.closest('.panel-collapse').addClass('in');
    // alert(aHref);
}
// End For left sidebar, auto mark active

function doFormParsley() {
    $('.parsley').each(function(index) {
        //console.log( index + ": " + $( this ).text() );
        var parsleyForm = $(this).parsley();
        if (parsleyForm) {
            parsleyForm.on('field:validated', function() {
                    var ok = $('.parsley-error').length === 0;
                    $('.bs-callout-info').toggleClass('hidden', !ok);
                    $('.bs-callout-warning').toggleClass('hidden', ok);
                })
                .on('form:submit', function() {
                    return true; // Don't submit form for this demo
                });
        }
    });
}

function htmlDecode(value) {
    return $("<textarea/>").html(value).text();
}

function htmlEncode(value) {
    return $('<textarea/>').text(value).html();
}

// use Select2
if ($('.useSelect2').length) {
    // console.log('exist');
    $('.useSelect2').select2({
        //theme: "bootstrap"
    });
}

// use rating awesome
if ($('.useRatingAwesome').length) {
    // console.log('exist');
    $('.useRatingAwesome').barrating({
        theme: 'fontawesome-stars-o'
    });
}

if ($('.useCurrentRatingAwesome').length) {
    $('.useCurrentRatingAwesome').each(function(index, el) {
        var currentRating = $(this).data('current-rating');
        $(this).barrating({
            theme: 'fontawesome-stars-o',
            initialRating: currentRating,
            readonly: true,
        });
    });
}

function doSlickCarousel(e, bScroll) {
    e.slick({
        dots: false,
        infinite: bScroll,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [{
            breakpoint: 1199,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: true
            }
        }, {
            breakpoint: 767,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 479,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
}

if ($(".crousel-post-slick").length) {
    var e_carousel = $(".crousel-post-slick");
    var length_carousel_posts = e_carousel.children('li').length;
    // console.log(length_carousel_posts);
    if (length_carousel_posts > 4) {
        doSlickCarousel(e_carousel, true);
    } else {
        doSlickCarousel(e_carousel, false);
    }
}

function doVerticalCarousel(e, bScroll) {
    e.bxSlider({
        mode: 'vertical',
        slideWidth: 300,
        minSlides: 5,
        pager: false,
        controls: bScroll,
        nextText: '<i class="fa fa-angle-right"></i>',
        prevText: '<i class="fa fa-angle-left"></i>',
    });
}
if ($(".slider-vertical-carousel").length) {
    var e_carousel = $(".slider-vertical-carousel");
    var length_carousel_posts = e_carousel.children('.row-item').length;
    // console.log(length_carousel_posts);
    if (length_carousel_posts > 5) {
        doVerticalCarousel(e_carousel, true);
    } else {
        doVerticalCarousel(e_carousel, false);
    }
}

jQuery(function($) {
    // $.subscribe('form.submitted', function() {
    //     $('.flash').fadeIn(500).delay(1000).fadeOut(500);
    // });
    if ($(".useTrumbowyg").length) {
        $('.useTrumbowyg').trumbowyg({
            fullscreenable: false,
            closable: true,
            lang: 'vi',
            btns: [
                ['viewHTML'],
                ['undo', 'redo'],
                ['formatting'],
                ['bold', 'italic'],
                ['link'],
                ['insertImage'],
                ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                ['unorderedList', 'orderedList'],
                ['foreColor', 'backColor'],
                ['fullscreen'],
            ]
        });
    }

    if ($(".slider-gallery").length) {
        $('.slider-gallery').bxSlider({
            pagerCustom: '#slider-gallery-pager',
            nextText: '<i class="fa fa-chevron-right"></i>',
            prevText: '<i class="fa fa-chevron-left"></i>',
        });
    }



    function checkCookie() {
        //show post list img
        if (typeof readCookie !== "undefined") {
            //console.log(readCookie('chodangtin-list-image'));
            var cookieListImage = readCookie('chodangtin-list-image');
            if (cookieListImage === null || cookieListImage === 'true') {
                $('.show-list.list-img').addClass('active');
                $('.show-list.list-none-img').removeClass('active');
            } else {
                $('.show-list.list-none-img').addClass('active');
                $('.show-list.list-img').removeClass('active');
            }

            //select order post
            if (readCookie('chodangtin-order-post')) {
                $(".change-order-by").val(readCookie('chodangtin-order-post'));
            }
        }
    }

    $('a.remove-cookie').click(function(event) {
        eraseCookie('chodangtin-list-image');
        eraseCookie('chodangtin-order-post');
    });

    function locationReload() {
        var uri = window.location.toString();
        if (uri.indexOf("?") > 0) {
            uri = uri.substring(0, uri.indexOf("?"));
        }
        location.href = uri;
    }

    function createCookieListImage(event, value) {
        event.preventDefault();
        createCookie('chodangtin-list-image', value, 60);
        locationReload(); //xu ly ajax
    }
    $('a.show-list.list-img').click(function(event) {
        createCookieListImage(event, true);
    });
    $('a.show-list.list-none-img').click(function(event) {
        createCookieListImage(event, false);
    });

    $('.change-order-by').change(function() {
        createCookie('chodangtin-order-post', $(this).val(), 60);
        locationReload(); //xu ly ajax
    });

    $(document).ready(function() {
        $('.front-back .front').click(function(event) {
            event.preventDefault();
            $(this).hide();
        });

        $('[data-toggle="tooltip"]').tooltip();

        $(".js-example-basic-single").select2({});
        $(".select-2").select2({});
        $('.select-2-parent').select2({
            dropdownCssClass: "parent-child"
        });

        checkCookie();
    });

    //Js parsleyjs validated
    doFormParsley();

    // Auto focus on first form field
    $("input:text, textarea").first().focus();

    // count charecters
    if ($('.useCounter').length) {
        $('.useCounter').counter({ // this is a class from the div with contenteditable="true"
            target: '.input-text-counter',
            count: 'up',
            goal: 2000,
            msg: ' '
        });
    }

    if ($('#useCounter').length) {
        $('#useCounter').counter({ // this is a class from the div with contenteditable="true"
            target: '.input-textarea-counter',
            count: 'up',
            goal: 2000,
            msg: ' '
        });
    }
    // Then focus back to title field
    // $('.useCounter').focus();

});
var eventFiredBtnDeleteSweetAlert = function() {
    // Use sweetalert AFTER DataTables
    $('.btnDelete').on('click', function(e) {
        e.preventDefault();

        var btnDelete = $(this);
        swal({
            title: "Bạn có chắc chắn?",
            text: "Hành động này không thể phục hồi!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Vâng, hãy xóa nó",
            cancelButtonText: "Hủy",
            closeOnConfirm: true
        }).then(function(isConfirm) {
            if (isConfirm === true) {
                btnDelete.closest('form').submit();
            }
        });
    });
};