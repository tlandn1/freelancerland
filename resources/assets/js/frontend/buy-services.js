if (window.addEventListener)
    window.addEventListener("load", loadOrders, false);
else if (window.attachEvent)
    window.attachEvent("onload", loadOrders);
else window.onload = loadOrders;

var up_count = 1;

function getDiscount(post_type, count_day) {
    // console.log(discountObj);
    var reduce = 0;
    $.each(discountObj, function(key, data) {
        if ((data.post_type_id == post_type) && (data.min_days <= count_day)) {
            reduce = data.reduce;
            return false;
        }
    });

    return reduce;
}

function formatPrice(num) {
    return num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.") + ' đ';
}

function loadOrders() {
    var post_type = $('input:radio[name=post_type]:checked').val();
    var count_day = $('input[name=day_count]').val();
    // console.log(postTypesObj);
    $.each(postTypesObj, function(key, data) {
        if (data.id == post_type) {
            var reduce = getDiscount(post_type, count_day);
            console.log(reduce);
            var order_price = parseInt(data.price) * parseInt(count_day);
            var order_discount = reduce * parseInt(order_price);
            var order_totalprice = parseInt(data.price) * parseInt(count_day) - parseInt(order_discount);
            var order_beforvat = order_totalprice * 0.9;
            var order_vat = order_totalprice * 0.1;
            $(".order-price").text(formatPrice(order_price));
            $(".order-discount").text(formatPrice(order_discount));
            $(".order-beforvat").text(formatPrice(order_beforvat));
            $(".order-vat").text(formatPrice(order_vat));
            $(".order-totalprice").text(formatPrice(order_totalprice));
            loadDaysCountInput(data.up_count);
        }

    });
}

function loadDaysCountInput(count) {
    // console.log(count);
    var cloneBoxTime = $('.cloneBoxTime');
    cloneBoxTime.empty();
    for (i = 1; i < count; i++) {
        var upTimeClone = $('.up_time').clone();
        upTimeClone.removeClass('up_time');
        upTimeClone.find('select').eq(0).attr('name', 'up_time_hour_' + i);
        upTimeClone.find('select').eq(1).attr('name', 'up_time_minute_' + i);
        upTimeClone.appendTo(cloneBoxTime);
        // console.log(i);
    }
}

function setUpTimeString() {
    var result = '';
    for (i = 0; i < up_count; i++) {
        var hourName = 'up_time_hour_' + i;
        var minuteName = 'up_time_minute_' + i;
        console.log($('select[name=' + hourName + ']').val());
        console.log($('select[name=' + minuteName + ']').val());
        result += $('select[name=' + hourName + ']').val() + ':' + $('select[name=' + minuteName + ']').val() + ';';
    }
    $('[name=up_time_str]').val(result);
}

$(document).ready(function() {
    $(window).keydown(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
    $("input:radio[name=post_type]:first").attr('checked', true);
    $('input:radio[name=post_type]').click(function() {
        loadOrders();
    });
    $('input[name=day_count]').change(function() {
        $('input[name=day_count]').blur(function() {
            loadOrders();
        });
    });
    $('.method-payment .panel a.payment-link').click(function(e) {
        e.preventDefault();
        var bankid = $(this).attr('data-bankid');
        if (typeof bankid !== typeof undefined && bankid !== false) {
            var location = $(this).attr('href') + '/' + $('input[name=id]').val() + '?bankid=' + bankid;
        } else {
            var location = $(this).attr('href') + '/' + $('input[name=id]').val();
        }
        window.location.href = location;
    });
});

var navigationFn = {
    goToSection: function(id) {
        $('html, body').animate({
            scrollTop: $(id).offset().top
        }, 0);
        return false;
        e.preventDefault();
    }
}

$("#step-add-post").bootstrapWizard({
    tabClass: 'nav nav-pills',
    onTabClick: function(tab, navigation, index) {
        return false;
    },
    onNext: function(tab, navigation, index) {
        if (index == 1) {
            if ($('#createOrders').parsley().validate()) {
                navigationFn.goToSection('#step-add-post');

                // Set up_time_str from select boxes
                setUpTimeString();

                var form = $('#createOrders');
                var method = form.find('input[name="_method"]').val() || 'POST';
                var route = form.prop('action');
                $.ajax({
                    type: method,
                    url: route,
                    data: form.serialize(),
                    // dataType: 'html',
                    success: function(data) {
                        if (!$('#step-add-post .step-1').hasClass('done')) {
                            $('#step-add-post .step-1').addClass('done');
                        }
                        // console.log(data);
                        $("input[name=id]").val(data.info_ordercode);
                        $(".info-ordercode").text(data.info_ordercode);
                        $(".info-orderstatus").text(data.info_orderstatus);
                        $(".info-orderpost").text(data.info_postcode);
                        $(".info-ordertype").text(data.info_orderpost);
                        $(".info-ordermindays").text(data.info_orderdayscount);
                        $(".info-orderprice").text(data.info_orderprice);
                        $(".info-orderdiscount").text(data.info_discount);
                        $(".info-orderbeforvat").text(data.info_orderbeforvat);
                        $(".info-ordervat").text(data.info_vat);
                        $(".info-ordertotal").text(data.info_ordertotal);
                    }
                });
            } else {
                return false;
            }
        }
        if (index == 2) {
            $('#step-add-post .step-2').addClass('done');
            $('#step-add-post .pager.wizard').remove();
        }
    }
});

var jsEncode = {
    encode: function(s, k) {
        var enc = "";
        var str = "";
        // make sure that input is string
        str = s.toString();
        for (var i = 0; i < s.length; i++) {
            // create block
            var a = s.charCodeAt(i);
            // bitwise XOR
            var b = a ^ k;
            enc = enc + String.fromCharCode(b);
        }
        return enc;
    }
};
var postTypesObj;
var discountObj;
$(document).ready(function() {
    // Create Base64 Object
    var Base64 = {
        _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
        encode: function(e) {
            var t = "";
            var n, r, i, s, o, u, a;
            var f = 0;
            e = Base64._utf8_encode(e);
            while (f < e.length) {
                n = e.charCodeAt(f++);
                r = e.charCodeAt(f++);
                i = e.charCodeAt(f++);
                s = n >> 2;
                o = (n & 3) << 4 | r >> 4;
                u = (r & 15) << 2 | i >> 6;
                a = i & 63;
                if (isNaN(r)) {
                    u = a = 64
                } else if (isNaN(i)) {
                    a = 64
                }
                t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
            }
            return t
        },
        decode: function(e) {
            var t = "";
            var n, r, i;
            var s, o, u, a;
            var f = 0;
            e = e.replace(/[^A-Za-z0-9+/=]/g, "");
            while (f < e.length) {
                s = this._keyStr.indexOf(e.charAt(f++));
                o = this._keyStr.indexOf(e.charAt(f++));
                u = this._keyStr.indexOf(e.charAt(f++));
                a = this._keyStr.indexOf(e.charAt(f++));
                n = s << 2 | o >> 4;
                r = (o & 15) << 4 | u >> 2;
                i = (u & 3) << 6 | a;
                t = t + String.fromCharCode(n);
                if (u != 64) {
                    t = t + String.fromCharCode(r)
                }
                if (a != 64) {
                    t = t + String.fromCharCode(i)
                }
            }
            t = Base64._utf8_decode(t);
            return t
        },
        _utf8_encode: function(e) {
            e = e.replace(/rn/g, "n");
            var t = "";
            for (var n = 0; n < e.length; n++) {
                var r = e.charCodeAt(n);
                if (r < 128) {
                    t += String.fromCharCode(r)
                } else if (r > 127 && r < 2048) {
                    t += String.fromCharCode(r >> 6 | 192);
                    t += String.fromCharCode(r & 63 | 128)
                } else {
                    t += String.fromCharCode(r >> 12 | 224);
                    t += String.fromCharCode(r >> 6 & 63 | 128);
                    t += String.fromCharCode(r & 63 | 128)
                }
            }
            return t
        },
        _utf8_decode: function(e) {
            var t = "";
            var n = 0;
            var r = c1 = c2 = 0;
            while (n < e.length) {
                r = e.charCodeAt(n);
                if (r < 128) {
                    t += String.fromCharCode(r);
                    n++
                } else if (r > 191 && r < 224) {
                    c2 = e.charCodeAt(n + 1);
                    t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                    n += 2
                } else {
                    c2 = e.charCodeAt(n + 1);
                    c3 = e.charCodeAt(n + 2);
                    t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                    n += 3
                }
            }
            return t
        }
    };
    var key = "123";
    postTypesObj = JSON.parse(jsEncode.encode(Base64.decode(postTypes), key));
    discountObj = JSON.parse(jsEncode.encode(Base64.decode(discount), key));
});