jQuery(function($) {
    function reloadHeaderAjax() {
        $.ajax({
            type: "post",
            url: '/get-header-ajax',
            success: function(data) {
                // console.log(data);
                $("header").replaceWith(data);
            }
        });
    }

    // Async submit a form's input.
    var submitAjaxRequest = function(e) {
        e.preventDefault();
        var form = $(this);
        var method = form.find('input[name="_method"]').val() || 'POST';
        var formId = form.attr('parent-modal');
        var formName = form.attr('name');

        if (form.hasClass('parsley')) {
            if ($(this).parsley().isValid()) {
                $.ajax({
                    type: method,
                    url: form.prop('action'),
                    data: form.serialize(),
                    dataType: 'text',
                    beforeSend: function() {
                        if (formName != 'login-form') {
                            $('.form-loading').addClass('active');
                            form.find('button[type="submit"]').prop('disabled', true);
                        }
                    },
                    success: function(data) {
                        // console.log(data + ' ' + formName);
                        if (formName == 'login-form') {
                            if (data == 1) {
                                if (form.attr('id') == 'login-form') {
                                    $('.wrap-notification h2 span').text('Đăng nhập');
                                }
                                $('.wrap-notification').show();
                                $('.wrap-auth-ajax').remove();
                            } else {
                                form.find('.message').text(data);
                            }

                            // Reload Header Ajax
                            reloadHeaderAjax();
                        } else {
                            $('.form-loading').removeClass('active');
                            form.find('button[type="submit"]').prop('disabled', false);
                            form[0].reset();
                            $.publish('form.submitted', [formId]);
                            // Destroy parent modal
                            $('#' + formId).modal('hide').data('bs.modal', null);
                        }
                    }
                });
            }
        } else {
            $.ajax({
                type: method,
                url: form.prop('action'),
                data: form.serialize(),
                success: function() {
                    if (formName == 'formMarkRead') {
                        $idMessage = form.attr('data-id');
                        $classMessage = '.message-' + $idMessage;
                        $parentMessage = $($classMessage).parents(".message");
                        $countMessage = $parentMessage.find('span.label');
                        $headerMessage = $parentMessage.find('li.header span');
                        $nameMessage = $parentMessage.find('li.header span strong').text();
                        $reducedMessage = $countMessage.text() - 1;
                        if ($reducedMessage < 1) {
                            $countMessage.remove();
                            $parentMessage.find('li.header form').remove();
                            $($classMessage).parents('.wrap-message').find('.mark-all-read').remove();
                            $headerMessage.html('Bạn không có <strong>' + $nameMessage + '</strong> mới');
                        } else {
                            $countMessage.text($reducedMessage);
                            $headerMessage.html('Bạn có ' + $reducedMessage + ' <strong>' + $nameMessage + '</strong> chưa đọc');
                        }
                        $($classMessage).removeClass('alert-info');
                        $($classMessage).find('.media-body form.form-mark-read').remove();
                    }

                    if (formName == 'formAllMarkRead') {
                        if (form.attr('data-id') == 1) {
                            $parentMessage = $('.messages-menu');
                        } else {
                            $parentMessage = $('.notifications-menu');
                        }
                        //remove in page
                        $('.wrap-message').find('.mark-all-read').remove();
                        $('.wrap-message .item-message').removeClass('alert-info');
                        $('.wrap-message .item-message .media-body form').remove();
                        //remove in dropdown
                        $parentMessage.find('.item-message').removeClass('alert-info');
                        $parentMessage.find('.item-message .media-body form').remove();
                        $parentMessage.find('li.header span').text('Bạn không có tin nhắn mới');
                        $parentMessage.find('span.label').remove();
                        $parentMessage.find('.mark-all-read').remove();
                    }
                    $.publish('form.submitted', [formId]);
                }
            });
        }
    };

    $.subscribe('form.submitted', function(_, formId) {
        //            console.log(formId);

        // Send SweetAlert info
        if (formId == 'sendEmailContactUser') {
            swal("Email của bạn đã được gửi đi thành công!", "", "success");
        } else if (formId == 'sendReport') {
            swal("Báo cáo của bạn đã được gửi đi thành công!", "", "success");
        } else if (formId == 'sendMessage') {
            swal("Tin nhắn của bạn đã được gửi đi thành công!", "", "success");
        }

    });

    // Forms marked with the "data-remote" attribute will submit via AJAX
    $('form[data-remote]').on('submit', submitAjaxRequest);
});