<?php

return [
      // User
      'user.add' => 'Thêm mới thành viên thành công',
      'user.edit' => 'Sửa thành viên thành công',
      'user.delete' => 'Xóa thành viên thành công',
      'user.profile' => 'Cập nhật thông tin cá nhân thành công',

      // firstcategory
      'firstCategory.add' => 'Thêm mới chuyên mục cấp 1 thành công',
      'firstCategory.edit' => 'Sửa chuyên mục cấp 1 thành công',
      'firstCategory.delete' => 'Xóa chuyên mục cấp 1 thành công',

      // firstcategory
      'secondCategory.add' => 'Thêm mới chuyên mục cấp 2 thành công',
      'secondCategory.edit' => 'Sửa chuyên mục cấp 2 thành công',
      'secondCategory.delete' => 'Xóa chuyên mục cấp 2 thành công',
];
